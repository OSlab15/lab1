
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:
8010000c:	0f 20 e0             	mov    %cr4,%eax
8010000f:	83 c8 10             	or     $0x10,%eax
80100012:	0f 22 e0             	mov    %eax,%cr4
80100015:	b8 00 90 10 00       	mov    $0x109000,%eax
8010001a:	0f 22 d8             	mov    %eax,%cr3
8010001d:	0f 20 c0             	mov    %cr0,%eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
80100025:	0f 22 c0             	mov    %eax,%cr0
80100028:	bc c0 b5 10 80       	mov    $0x8010b5c0,%esp
8010002d:	b8 30 31 10 80       	mov    $0x80103130,%eax
80100032:	ff e0                	jmp    *%eax
80100034:	66 90                	xchg   %ax,%ax
80100036:	66 90                	xchg   %ax,%ax
80100038:	66 90                	xchg   %ax,%ax
8010003a:	66 90                	xchg   %ax,%ax
8010003c:	66 90                	xchg   %ax,%ax
8010003e:	66 90                	xchg   %ax,%ax

80100040 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100040:	55                   	push   %ebp
80100041:	89 e5                	mov    %esp,%ebp
80100043:	53                   	push   %ebx

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100044:	bb f4 b5 10 80       	mov    $0x8010b5f4,%ebx
{
80100049:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
8010004c:	68 40 71 10 80       	push   $0x80107140
80100051:	68 c0 b5 10 80       	push   $0x8010b5c0
80100056:	e8 35 44 00 00       	call   80104490 <initlock>
  bcache.head.prev = &bcache.head;
8010005b:	c7 05 0c fd 10 80 bc 	movl   $0x8010fcbc,0x8010fd0c
80100062:	fc 10 80 
  bcache.head.next = &bcache.head;
80100065:	c7 05 10 fd 10 80 bc 	movl   $0x8010fcbc,0x8010fd10
8010006c:	fc 10 80 
8010006f:	83 c4 10             	add    $0x10,%esp
80100072:	ba bc fc 10 80       	mov    $0x8010fcbc,%edx
80100077:	eb 09                	jmp    80100082 <binit+0x42>
80100079:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100080:	89 c3                	mov    %eax,%ebx
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
80100082:	8d 43 0c             	lea    0xc(%ebx),%eax
80100085:	83 ec 08             	sub    $0x8,%esp
    b->next = bcache.head.next;
80100088:	89 53 54             	mov    %edx,0x54(%ebx)
    b->prev = &bcache.head;
8010008b:	c7 43 50 bc fc 10 80 	movl   $0x8010fcbc,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100092:	68 47 71 10 80       	push   $0x80107147
80100097:	50                   	push   %eax
80100098:	e8 c3 42 00 00       	call   80104360 <initsleeplock>
    bcache.head.next->prev = b;
8010009d:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000a2:	83 c4 10             	add    $0x10,%esp
801000a5:	89 da                	mov    %ebx,%edx
    bcache.head.next->prev = b;
801000a7:	89 58 50             	mov    %ebx,0x50(%eax)
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000aa:	8d 83 5c 02 00 00    	lea    0x25c(%ebx),%eax
    bcache.head.next = b;
801000b0:	89 1d 10 fd 10 80    	mov    %ebx,0x8010fd10
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000b6:	3d bc fc 10 80       	cmp    $0x8010fcbc,%eax
801000bb:	72 c3                	jb     80100080 <binit+0x40>
  }
}
801000bd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801000c0:	c9                   	leave  
801000c1:	c3                   	ret    
801000c2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801000c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801000d0 <bread>:
}

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801000d0:	55                   	push   %ebp
801000d1:	89 e5                	mov    %esp,%ebp
801000d3:	57                   	push   %edi
801000d4:	56                   	push   %esi
801000d5:	53                   	push   %ebx
801000d6:	83 ec 18             	sub    $0x18,%esp
801000d9:	8b 75 08             	mov    0x8(%ebp),%esi
801000dc:	8b 7d 0c             	mov    0xc(%ebp),%edi
  acquire(&bcache.lock);
801000df:	68 c0 b5 10 80       	push   $0x8010b5c0
801000e4:	e8 e7 44 00 00       	call   801045d0 <acquire>
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000e9:	8b 1d 10 fd 10 80    	mov    0x8010fd10,%ebx
801000ef:	83 c4 10             	add    $0x10,%esp
801000f2:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
801000f8:	75 11                	jne    8010010b <bread+0x3b>
801000fa:	eb 24                	jmp    80100120 <bread+0x50>
801000fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100100:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100103:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
80100109:	74 15                	je     80100120 <bread+0x50>
    if(b->dev == dev && b->blockno == blockno){
8010010b:	3b 73 04             	cmp    0x4(%ebx),%esi
8010010e:	75 f0                	jne    80100100 <bread+0x30>
80100110:	3b 7b 08             	cmp    0x8(%ebx),%edi
80100113:	75 eb                	jne    80100100 <bread+0x30>
      b->refcnt++;
80100115:	83 43 4c 01          	addl   $0x1,0x4c(%ebx)
80100119:	eb 3f                	jmp    8010015a <bread+0x8a>
8010011b:	90                   	nop
8010011c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100120:	8b 1d 0c fd 10 80    	mov    0x8010fd0c,%ebx
80100126:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
8010012c:	75 0d                	jne    8010013b <bread+0x6b>
8010012e:	eb 60                	jmp    80100190 <bread+0xc0>
80100130:	8b 5b 50             	mov    0x50(%ebx),%ebx
80100133:	81 fb bc fc 10 80    	cmp    $0x8010fcbc,%ebx
80100139:	74 55                	je     80100190 <bread+0xc0>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
8010013b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010013e:	85 c0                	test   %eax,%eax
80100140:	75 ee                	jne    80100130 <bread+0x60>
80100142:	f6 03 04             	testb  $0x4,(%ebx)
80100145:	75 e9                	jne    80100130 <bread+0x60>
      b->dev = dev;
80100147:	89 73 04             	mov    %esi,0x4(%ebx)
      b->blockno = blockno;
8010014a:	89 7b 08             	mov    %edi,0x8(%ebx)
      b->flags = 0;
8010014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
80100153:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
8010015a:	83 ec 0c             	sub    $0xc,%esp
8010015d:	68 c0 b5 10 80       	push   $0x8010b5c0
80100162:	e8 29 45 00 00       	call   80104690 <release>
      acquiresleep(&b->lock);
80100167:	8d 43 0c             	lea    0xc(%ebx),%eax
8010016a:	89 04 24             	mov    %eax,(%esp)
8010016d:	e8 2e 42 00 00       	call   801043a0 <acquiresleep>
80100172:	83 c4 10             	add    $0x10,%esp
  struct buf *b;

  b = bget(dev, blockno);
  if((b->flags & B_VALID) == 0) {
80100175:	f6 03 02             	testb  $0x2,(%ebx)
80100178:	75 0c                	jne    80100186 <bread+0xb6>
    iderw(b);
8010017a:	83 ec 0c             	sub    $0xc,%esp
8010017d:	53                   	push   %ebx
8010017e:	e8 2d 22 00 00       	call   801023b0 <iderw>
80100183:	83 c4 10             	add    $0x10,%esp
  }
  return b;
}
80100186:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100189:	89 d8                	mov    %ebx,%eax
8010018b:	5b                   	pop    %ebx
8010018c:	5e                   	pop    %esi
8010018d:	5f                   	pop    %edi
8010018e:	5d                   	pop    %ebp
8010018f:	c3                   	ret    
  panic("bget: no buffers");
80100190:	83 ec 0c             	sub    $0xc,%esp
80100193:	68 4e 71 10 80       	push   $0x8010714e
80100198:	e8 f3 01 00 00       	call   80100390 <panic>
8010019d:	8d 76 00             	lea    0x0(%esi),%esi

801001a0 <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
801001a0:	55                   	push   %ebp
801001a1:	89 e5                	mov    %esp,%ebp
801001a3:	53                   	push   %ebx
801001a4:	83 ec 10             	sub    $0x10,%esp
801001a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001aa:	8d 43 0c             	lea    0xc(%ebx),%eax
801001ad:	50                   	push   %eax
801001ae:	e8 8d 42 00 00       	call   80104440 <holdingsleep>
801001b3:	83 c4 10             	add    $0x10,%esp
801001b6:	85 c0                	test   %eax,%eax
801001b8:	74 0f                	je     801001c9 <bwrite+0x29>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001ba:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001bd:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801001c0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001c3:	c9                   	leave  
  iderw(b);
801001c4:	e9 e7 21 00 00       	jmp    801023b0 <iderw>
    panic("bwrite");
801001c9:	83 ec 0c             	sub    $0xc,%esp
801001cc:	68 5f 71 10 80       	push   $0x8010715f
801001d1:	e8 ba 01 00 00       	call   80100390 <panic>
801001d6:	8d 76 00             	lea    0x0(%esi),%esi
801001d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801001e0 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001e0:	55                   	push   %ebp
801001e1:	89 e5                	mov    %esp,%ebp
801001e3:	56                   	push   %esi
801001e4:	53                   	push   %ebx
801001e5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001e8:	83 ec 0c             	sub    $0xc,%esp
801001eb:	8d 73 0c             	lea    0xc(%ebx),%esi
801001ee:	56                   	push   %esi
801001ef:	e8 4c 42 00 00       	call   80104440 <holdingsleep>
801001f4:	83 c4 10             	add    $0x10,%esp
801001f7:	85 c0                	test   %eax,%eax
801001f9:	74 66                	je     80100261 <brelse+0x81>
    panic("brelse");

  releasesleep(&b->lock);
801001fb:	83 ec 0c             	sub    $0xc,%esp
801001fe:	56                   	push   %esi
801001ff:	e8 fc 41 00 00       	call   80104400 <releasesleep>

  acquire(&bcache.lock);
80100204:	c7 04 24 c0 b5 10 80 	movl   $0x8010b5c0,(%esp)
8010020b:	e8 c0 43 00 00       	call   801045d0 <acquire>
  b->refcnt--;
80100210:	8b 43 4c             	mov    0x4c(%ebx),%eax
  if (b->refcnt == 0) {
80100213:	83 c4 10             	add    $0x10,%esp
  b->refcnt--;
80100216:	83 e8 01             	sub    $0x1,%eax
  if (b->refcnt == 0) {
80100219:	85 c0                	test   %eax,%eax
  b->refcnt--;
8010021b:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
8010021e:	75 2f                	jne    8010024f <brelse+0x6f>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100220:	8b 43 54             	mov    0x54(%ebx),%eax
80100223:	8b 53 50             	mov    0x50(%ebx),%edx
80100226:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
80100229:	8b 43 50             	mov    0x50(%ebx),%eax
8010022c:	8b 53 54             	mov    0x54(%ebx),%edx
8010022f:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100232:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
    b->prev = &bcache.head;
80100237:	c7 43 50 bc fc 10 80 	movl   $0x8010fcbc,0x50(%ebx)
    b->next = bcache.head.next;
8010023e:	89 43 54             	mov    %eax,0x54(%ebx)
    bcache.head.next->prev = b;
80100241:	a1 10 fd 10 80       	mov    0x8010fd10,%eax
80100246:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
80100249:	89 1d 10 fd 10 80    	mov    %ebx,0x8010fd10
  }
  
  release(&bcache.lock);
8010024f:	c7 45 08 c0 b5 10 80 	movl   $0x8010b5c0,0x8(%ebp)
}
80100256:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100259:	5b                   	pop    %ebx
8010025a:	5e                   	pop    %esi
8010025b:	5d                   	pop    %ebp
  release(&bcache.lock);
8010025c:	e9 2f 44 00 00       	jmp    80104690 <release>
    panic("brelse");
80100261:	83 ec 0c             	sub    $0xc,%esp
80100264:	68 66 71 10 80       	push   $0x80107166
80100269:	e8 22 01 00 00       	call   80100390 <panic>
8010026e:	66 90                	xchg   %ax,%ax

80100270 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100270:	55                   	push   %ebp
80100271:	89 e5                	mov    %esp,%ebp
80100273:	57                   	push   %edi
80100274:	56                   	push   %esi
80100275:	53                   	push   %ebx
80100276:	83 ec 28             	sub    $0x28,%esp
80100279:	8b 7d 08             	mov    0x8(%ebp),%edi
8010027c:	8b 75 0c             	mov    0xc(%ebp),%esi
  uint target;
  int c;

  iunlock(ip);
8010027f:	57                   	push   %edi
80100280:	e8 6b 17 00 00       	call   801019f0 <iunlock>
  target = n;
  acquire(&cons.lock);
80100285:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
8010028c:	e8 3f 43 00 00       	call   801045d0 <acquire>
  while(n > 0){
80100291:	8b 5d 10             	mov    0x10(%ebp),%ebx
80100294:	83 c4 10             	add    $0x10,%esp
80100297:	31 c0                	xor    %eax,%eax
80100299:	85 db                	test   %ebx,%ebx
8010029b:	0f 8e a1 00 00 00    	jle    80100342 <consoleread+0xd2>
    while(input.r == input.w){
801002a1:	8b 15 a0 ff 10 80    	mov    0x8010ffa0,%edx
801002a7:	39 15 a4 ff 10 80    	cmp    %edx,0x8010ffa4
801002ad:	74 2c                	je     801002db <consoleread+0x6b>
801002af:	eb 5f                	jmp    80100310 <consoleread+0xa0>
801002b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      if(myproc()->killed){               
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
801002b8:	83 ec 08             	sub    $0x8,%esp
801002bb:	68 20 a5 10 80       	push   $0x8010a520
801002c0:	68 a0 ff 10 80       	push   $0x8010ffa0
801002c5:	e8 46 3d 00 00       	call   80104010 <sleep>
    while(input.r == input.w){
801002ca:	8b 15 a0 ff 10 80    	mov    0x8010ffa0,%edx
801002d0:	83 c4 10             	add    $0x10,%esp
801002d3:	3b 15 a4 ff 10 80    	cmp    0x8010ffa4,%edx
801002d9:	75 35                	jne    80100310 <consoleread+0xa0>
      if(myproc()->killed){               
801002db:	e8 90 37 00 00       	call   80103a70 <myproc>
801002e0:	8b 40 24             	mov    0x24(%eax),%eax
801002e3:	85 c0                	test   %eax,%eax
801002e5:	74 d1                	je     801002b8 <consoleread+0x48>
        release(&cons.lock);
801002e7:	83 ec 0c             	sub    $0xc,%esp
801002ea:	68 20 a5 10 80       	push   $0x8010a520
801002ef:	e8 9c 43 00 00       	call   80104690 <release>
        ilock(ip);
801002f4:	89 3c 24             	mov    %edi,(%esp)
801002f7:	e8 14 16 00 00       	call   80101910 <ilock>
        return -1;
801002fc:	83 c4 10             	add    $0x10,%esp
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
801002ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return -1;
80100302:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100307:	5b                   	pop    %ebx
80100308:	5e                   	pop    %esi
80100309:	5f                   	pop    %edi
8010030a:	5d                   	pop    %ebp
8010030b:	c3                   	ret    
8010030c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c = input.buf[input.r++ % INPUT_BUF];
80100310:	8d 42 01             	lea    0x1(%edx),%eax
80100313:	a3 a0 ff 10 80       	mov    %eax,0x8010ffa0
80100318:	89 d0                	mov    %edx,%eax
8010031a:	83 e0 7f             	and    $0x7f,%eax
8010031d:	0f be 80 20 ff 10 80 	movsbl -0x7fef00e0(%eax),%eax
    if(c == C('D')){  // EOF
80100324:	83 f8 04             	cmp    $0x4,%eax
80100327:	74 3f                	je     80100368 <consoleread+0xf8>
    *dst++ = c;
80100329:	83 c6 01             	add    $0x1,%esi
    --n;
8010032c:	83 eb 01             	sub    $0x1,%ebx
    if(c == '\n')
8010032f:	83 f8 0a             	cmp    $0xa,%eax
    *dst++ = c;
80100332:	88 46 ff             	mov    %al,-0x1(%esi)
    if(c == '\n')
80100335:	74 43                	je     8010037a <consoleread+0x10a>
  while(n > 0){
80100337:	85 db                	test   %ebx,%ebx
80100339:	0f 85 62 ff ff ff    	jne    801002a1 <consoleread+0x31>
8010033f:	8b 45 10             	mov    0x10(%ebp),%eax
  release(&cons.lock);
80100342:	83 ec 0c             	sub    $0xc,%esp
80100345:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80100348:	68 20 a5 10 80       	push   $0x8010a520
8010034d:	e8 3e 43 00 00       	call   80104690 <release>
  ilock(ip);
80100352:	89 3c 24             	mov    %edi,(%esp)
80100355:	e8 b6 15 00 00       	call   80101910 <ilock>
  return target - n;
8010035a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010035d:	83 c4 10             	add    $0x10,%esp
}
80100360:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100363:	5b                   	pop    %ebx
80100364:	5e                   	pop    %esi
80100365:	5f                   	pop    %edi
80100366:	5d                   	pop    %ebp
80100367:	c3                   	ret    
80100368:	8b 45 10             	mov    0x10(%ebp),%eax
8010036b:	29 d8                	sub    %ebx,%eax
      if(n < target){
8010036d:	3b 5d 10             	cmp    0x10(%ebp),%ebx
80100370:	73 d0                	jae    80100342 <consoleread+0xd2>
        input.r--;
80100372:	89 15 a0 ff 10 80    	mov    %edx,0x8010ffa0
80100378:	eb c8                	jmp    80100342 <consoleread+0xd2>
8010037a:	8b 45 10             	mov    0x10(%ebp),%eax
8010037d:	29 d8                	sub    %ebx,%eax
8010037f:	eb c1                	jmp    80100342 <consoleread+0xd2>
80100381:	eb 0d                	jmp    80100390 <panic>
80100383:	90                   	nop
80100384:	90                   	nop
80100385:	90                   	nop
80100386:	90                   	nop
80100387:	90                   	nop
80100388:	90                   	nop
80100389:	90                   	nop
8010038a:	90                   	nop
8010038b:	90                   	nop
8010038c:	90                   	nop
8010038d:	90                   	nop
8010038e:	90                   	nop
8010038f:	90                   	nop

80100390 <panic>:
{
80100390:	55                   	push   %ebp
80100391:	89 e5                	mov    %esp,%ebp
80100393:	56                   	push   %esi
80100394:	53                   	push   %ebx
80100395:	83 ec 30             	sub    $0x30,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
80100398:	fa                   	cli    
  cons.locking = 0;
80100399:	c7 05 54 a5 10 80 00 	movl   $0x0,0x8010a554
801003a0:	00 00 00 
  getcallerpcs(&s, pcs);
801003a3:	8d 5d d0             	lea    -0x30(%ebp),%ebx
801003a6:	8d 75 f8             	lea    -0x8(%ebp),%esi
  cprintf("lapicid %d: panic: ", lapicid());
801003a9:	e8 12 26 00 00       	call   801029c0 <lapicid>
801003ae:	83 ec 08             	sub    $0x8,%esp
801003b1:	50                   	push   %eax
801003b2:	68 6d 71 10 80       	push   $0x8010716d
801003b7:	e8 b4 03 00 00       	call   80100770 <cprintf>
  cprintf(s);
801003bc:	58                   	pop    %eax
801003bd:	ff 75 08             	pushl  0x8(%ebp)
801003c0:	e8 ab 03 00 00       	call   80100770 <cprintf>
  cprintf("\n");
801003c5:	c7 04 24 b7 7a 10 80 	movl   $0x80107ab7,(%esp)
801003cc:	e8 9f 03 00 00       	call   80100770 <cprintf>
  getcallerpcs(&s, pcs);
801003d1:	5a                   	pop    %edx
801003d2:	8d 45 08             	lea    0x8(%ebp),%eax
801003d5:	59                   	pop    %ecx
801003d6:	53                   	push   %ebx
801003d7:	50                   	push   %eax
801003d8:	e8 d3 40 00 00       	call   801044b0 <getcallerpcs>
801003dd:	83 c4 10             	add    $0x10,%esp
    cprintf(" %p", pcs[i]);
801003e0:	83 ec 08             	sub    $0x8,%esp
801003e3:	ff 33                	pushl  (%ebx)
801003e5:	83 c3 04             	add    $0x4,%ebx
801003e8:	68 81 71 10 80       	push   $0x80107181
801003ed:	e8 7e 03 00 00       	call   80100770 <cprintf>
  for(i=0; i<10; i++)
801003f2:	83 c4 10             	add    $0x10,%esp
801003f5:	39 f3                	cmp    %esi,%ebx
801003f7:	75 e7                	jne    801003e0 <panic+0x50>
  panicked = 1; // freeze other CPU
801003f9:	c7 05 58 a5 10 80 01 	movl   $0x1,0x8010a558
80100400:	00 00 00 
80100403:	eb fe                	jmp    80100403 <panic+0x73>
80100405:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100409:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80100410 <moveCursor>:
void moveCursor(int size, int c){
80100410:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100411:	b8 0e 00 00 00       	mov    $0xe,%eax
80100416:	89 e5                	mov    %esp,%ebp
80100418:	57                   	push   %edi
80100419:	56                   	push   %esi
8010041a:	53                   	push   %ebx
8010041b:	bb d4 03 00 00       	mov    $0x3d4,%ebx
80100420:	89 da                	mov    %ebx,%edx
80100422:	83 ec 0c             	sub    $0xc,%esp
80100425:	8b 75 0c             	mov    0xc(%ebp),%esi
80100428:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100429:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
8010042e:	89 ca                	mov    %ecx,%edx
80100430:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100431:	0f b6 f8             	movzbl %al,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100434:	89 da                	mov    %ebx,%edx
80100436:	b8 0f 00 00 00       	mov    $0xf,%eax
8010043b:	c1 e7 08             	shl    $0x8,%edi
8010043e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010043f:	89 ca                	mov    %ecx,%edx
80100441:	ec                   	in     (%dx),%al
  pos |= inb(CRTPORT+1);
80100442:	0f b6 d8             	movzbl %al,%ebx
80100445:	09 fb                	or     %edi,%ebx
  pos += size;
80100447:	03 5d 08             	add    0x8(%ebp),%ebx
  if(c == '\n')
8010044a:	83 fe 0a             	cmp    $0xa,%esi
8010044d:	0f 84 95 00 00 00    	je     801004e8 <moveCursor+0xd8>
    if(pos > 0) --pos;
80100453:	85 db                	test   %ebx,%ebx
80100455:	7e 0e                	jle    80100465 <moveCursor+0x55>
  else if(c == BACKSPACE){
80100457:	81 fe 00 01 00 00    	cmp    $0x100,%esi
8010045d:	0f 94 c0             	sete   %al
    if(pos > 0) --pos;
80100460:	3c 01                	cmp    $0x1,%al
80100462:	83 d3 ff             	adc    $0xffffffff,%ebx
  if(pos < 0 || pos > 25*80)
80100465:	81 fb d0 07 00 00    	cmp    $0x7d0,%ebx
8010046b:	0f 87 96 00 00 00    	ja     80100507 <moveCursor+0xf7>
  if((pos/80) >= 24){  // Scroll up.
80100471:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
80100477:	7e 39                	jle    801004b2 <moveCursor+0xa2>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100479:	83 ec 04             	sub    $0x4,%esp
    pos -= 80;
8010047c:	83 eb 50             	sub    $0x50,%ebx
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
8010047f:	68 60 0e 00 00       	push   $0xe60
80100484:	68 a0 80 0b 80       	push   $0x800b80a0
80100489:	68 00 80 0b 80       	push   $0x800b8000
8010048e:	e8 fd 42 00 00       	call   80104790 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100493:	b8 80 07 00 00       	mov    $0x780,%eax
80100498:	83 c4 0c             	add    $0xc,%esp
8010049b:	29 d8                	sub    %ebx,%eax
8010049d:	01 c0                	add    %eax,%eax
8010049f:	50                   	push   %eax
801004a0:	8d 84 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%eax
801004a7:	6a 00                	push   $0x0
801004a9:	50                   	push   %eax
801004aa:	e8 31 42 00 00       	call   801046e0 <memset>
801004af:	83 c4 10             	add    $0x10,%esp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801004b2:	be d4 03 00 00       	mov    $0x3d4,%esi
801004b7:	b8 0e 00 00 00       	mov    $0xe,%eax
801004bc:	89 f2                	mov    %esi,%edx
801004be:	ee                   	out    %al,(%dx)
801004bf:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
  outb(CRTPORT+1, pos>>8);
801004c4:	89 d8                	mov    %ebx,%eax
801004c6:	c1 f8 08             	sar    $0x8,%eax
801004c9:	89 ca                	mov    %ecx,%edx
801004cb:	ee                   	out    %al,(%dx)
801004cc:	b8 0f 00 00 00       	mov    $0xf,%eax
801004d1:	89 f2                	mov    %esi,%edx
801004d3:	ee                   	out    %al,(%dx)
801004d4:	89 d8                	mov    %ebx,%eax
801004d6:	89 ca                	mov    %ecx,%edx
801004d8:	ee                   	out    %al,(%dx)
}
801004d9:	8d 65 f4             	lea    -0xc(%ebp),%esp
801004dc:	5b                   	pop    %ebx
801004dd:	5e                   	pop    %esi
801004de:	5f                   	pop    %edi
801004df:	5d                   	pop    %ebp
801004e0:	c3                   	ret    
801004e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    pos += 80 - pos%80;
801004e8:	89 d8                	mov    %ebx,%eax
801004ea:	ba 67 66 66 66       	mov    $0x66666667,%edx
801004ef:	c1 fb 1f             	sar    $0x1f,%ebx
801004f2:	f7 ea                	imul   %edx
801004f4:	c1 fa 05             	sar    $0x5,%edx
801004f7:	29 da                	sub    %ebx,%edx
801004f9:	8d 04 92             	lea    (%edx,%edx,4),%eax
801004fc:	c1 e0 04             	shl    $0x4,%eax
801004ff:	8d 58 50             	lea    0x50(%eax),%ebx
80100502:	e9 5e ff ff ff       	jmp    80100465 <moveCursor+0x55>
    panic("pos under/overflow");
80100507:	83 ec 0c             	sub    $0xc,%esp
8010050a:	68 85 71 10 80       	push   $0x80107185
8010050f:	e8 7c fe ff ff       	call   80100390 <panic>
80100514:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010051a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80100520 <consputc>:
  if(panicked){
80100520:	8b 0d 58 a5 10 80    	mov    0x8010a558,%ecx
80100526:	85 c9                	test   %ecx,%ecx
80100528:	74 06                	je     80100530 <consputc+0x10>
  asm volatile("cli");
8010052a:	fa                   	cli    
8010052b:	eb fe                	jmp    8010052b <consputc+0xb>
8010052d:	8d 76 00             	lea    0x0(%esi),%esi
{
80100530:	55                   	push   %ebp
80100531:	89 e5                	mov    %esp,%ebp
80100533:	57                   	push   %edi
80100534:	56                   	push   %esi
80100535:	53                   	push   %ebx
80100536:	89 c6                	mov    %eax,%esi
80100538:	83 ec 0c             	sub    $0xc,%esp
  if(c == BACKSPACE){
8010053b:	3d 00 01 00 00       	cmp    $0x100,%eax
80100540:	0f 84 b1 00 00 00    	je     801005f7 <consputc+0xd7>
    uartputc(c);
80100546:	83 ec 0c             	sub    $0xc,%esp
80100549:	50                   	push   %eax
8010054a:	e8 01 58 00 00       	call   80105d50 <uartputc>
8010054f:	83 c4 10             	add    $0x10,%esp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100552:	bb d4 03 00 00       	mov    $0x3d4,%ebx
80100557:	b8 0e 00 00 00       	mov    $0xe,%eax
8010055c:	89 da                	mov    %ebx,%edx
8010055e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010055f:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
80100564:	89 ca                	mov    %ecx,%edx
80100566:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100567:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010056a:	89 da                	mov    %ebx,%edx
8010056c:	c1 e0 08             	shl    $0x8,%eax
8010056f:	89 c7                	mov    %eax,%edi
80100571:	b8 0f 00 00 00       	mov    $0xf,%eax
80100576:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100577:	89 ca                	mov    %ecx,%edx
80100579:	ec                   	in     (%dx),%al
8010057a:	0f b6 d8             	movzbl %al,%ebx
  pos |= inb(CRTPORT+1);
8010057d:	09 fb                	or     %edi,%ebx
  if(c == '\n')
8010057f:	83 fe 0a             	cmp    $0xa,%esi
80100582:	0f 84 f3 00 00 00    	je     8010067b <consputc+0x15b>
  else if(c == BACKSPACE){
80100588:	81 fe 00 01 00 00    	cmp    $0x100,%esi
8010058e:	0f 84 d7 00 00 00    	je     8010066b <consputc+0x14b>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100594:	89 f0                	mov    %esi,%eax
80100596:	0f b6 c0             	movzbl %al,%eax
80100599:	80 cc 07             	or     $0x7,%ah
8010059c:	66 89 84 1b 00 80 0b 	mov    %ax,-0x7ff48000(%ebx,%ebx,1)
801005a3:	80 
801005a4:	83 c3 01             	add    $0x1,%ebx
  if(pos < 0 || pos > 25*80)
801005a7:	81 fb d0 07 00 00    	cmp    $0x7d0,%ebx
801005ad:	0f 8f ab 00 00 00    	jg     8010065e <consputc+0x13e>
  if((pos/80) >= 24){  // Scroll up.
801005b3:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
801005b9:	7f 66                	jg     80100621 <consputc+0x101>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801005bb:	be d4 03 00 00       	mov    $0x3d4,%esi
801005c0:	b8 0e 00 00 00       	mov    $0xe,%eax
801005c5:	89 f2                	mov    %esi,%edx
801005c7:	ee                   	out    %al,(%dx)
801005c8:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
  outb(CRTPORT+1, pos>>8);
801005cd:	89 d8                	mov    %ebx,%eax
801005cf:	c1 f8 08             	sar    $0x8,%eax
801005d2:	89 ca                	mov    %ecx,%edx
801005d4:	ee                   	out    %al,(%dx)
801005d5:	b8 0f 00 00 00       	mov    $0xf,%eax
801005da:	89 f2                	mov    %esi,%edx
801005dc:	ee                   	out    %al,(%dx)
801005dd:	89 d8                	mov    %ebx,%eax
801005df:	89 ca                	mov    %ecx,%edx
801005e1:	ee                   	out    %al,(%dx)
  crt[pos] = ' ' | 0x0700;
801005e2:	b8 20 07 00 00       	mov    $0x720,%eax
801005e7:	66 89 84 1b 00 80 0b 	mov    %ax,-0x7ff48000(%ebx,%ebx,1)
801005ee:	80 
}
801005ef:	8d 65 f4             	lea    -0xc(%ebp),%esp
801005f2:	5b                   	pop    %ebx
801005f3:	5e                   	pop    %esi
801005f4:	5f                   	pop    %edi
801005f5:	5d                   	pop    %ebp
801005f6:	c3                   	ret    
    uartputc('\b'); uartputc(' '); uartputc('\b');
801005f7:	83 ec 0c             	sub    $0xc,%esp
801005fa:	6a 08                	push   $0x8
801005fc:	e8 4f 57 00 00       	call   80105d50 <uartputc>
80100601:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100608:	e8 43 57 00 00       	call   80105d50 <uartputc>
8010060d:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100614:	e8 37 57 00 00       	call   80105d50 <uartputc>
80100619:	83 c4 10             	add    $0x10,%esp
8010061c:	e9 31 ff ff ff       	jmp    80100552 <consputc+0x32>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100621:	52                   	push   %edx
80100622:	68 60 0e 00 00       	push   $0xe60
    pos -= 80;
80100627:	83 eb 50             	sub    $0x50,%ebx
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
8010062a:	68 a0 80 0b 80       	push   $0x800b80a0
8010062f:	68 00 80 0b 80       	push   $0x800b8000
80100634:	e8 57 41 00 00       	call   80104790 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100639:	b8 80 07 00 00       	mov    $0x780,%eax
8010063e:	83 c4 0c             	add    $0xc,%esp
80100641:	29 d8                	sub    %ebx,%eax
80100643:	01 c0                	add    %eax,%eax
80100645:	50                   	push   %eax
80100646:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
80100649:	6a 00                	push   $0x0
8010064b:	2d 00 80 f4 7f       	sub    $0x7ff48000,%eax
80100650:	50                   	push   %eax
80100651:	e8 8a 40 00 00       	call   801046e0 <memset>
80100656:	83 c4 10             	add    $0x10,%esp
80100659:	e9 5d ff ff ff       	jmp    801005bb <consputc+0x9b>
    panic("pos under/overflow");
8010065e:	83 ec 0c             	sub    $0xc,%esp
80100661:	68 85 71 10 80       	push   $0x80107185
80100666:	e8 25 fd ff ff       	call   80100390 <panic>
    if(pos > 0) --pos;
8010066b:	85 db                	test   %ebx,%ebx
8010066d:	0f 84 48 ff ff ff    	je     801005bb <consputc+0x9b>
80100673:	83 eb 01             	sub    $0x1,%ebx
80100676:	e9 2c ff ff ff       	jmp    801005a7 <consputc+0x87>
    pos += 80 - pos%80;
8010067b:	89 d8                	mov    %ebx,%eax
8010067d:	b9 50 00 00 00       	mov    $0x50,%ecx
80100682:	99                   	cltd   
80100683:	f7 f9                	idiv   %ecx
80100685:	29 d1                	sub    %edx,%ecx
80100687:	01 cb                	add    %ecx,%ebx
80100689:	e9 19 ff ff ff       	jmp    801005a7 <consputc+0x87>
8010068e:	66 90                	xchg   %ax,%ax

80100690 <printint>:
{
80100690:	55                   	push   %ebp
80100691:	89 e5                	mov    %esp,%ebp
80100693:	57                   	push   %edi
80100694:	56                   	push   %esi
80100695:	53                   	push   %ebx
80100696:	89 d3                	mov    %edx,%ebx
80100698:	83 ec 2c             	sub    $0x2c,%esp
  if(sign && (sign = xx < 0))
8010069b:	85 c9                	test   %ecx,%ecx
{
8010069d:	89 4d d4             	mov    %ecx,-0x2c(%ebp)
  if(sign && (sign = xx < 0))
801006a0:	74 04                	je     801006a6 <printint+0x16>
801006a2:	85 c0                	test   %eax,%eax
801006a4:	78 5a                	js     80100700 <printint+0x70>
    x = xx;
801006a6:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
  i = 0;
801006ad:	31 c9                	xor    %ecx,%ecx
801006af:	8d 75 d7             	lea    -0x29(%ebp),%esi
801006b2:	eb 06                	jmp    801006ba <printint+0x2a>
801006b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    buf[i++] = digits[x % base];
801006b8:	89 f9                	mov    %edi,%ecx
801006ba:	31 d2                	xor    %edx,%edx
801006bc:	8d 79 01             	lea    0x1(%ecx),%edi
801006bf:	f7 f3                	div    %ebx
801006c1:	0f b6 92 b0 71 10 80 	movzbl -0x7fef8e50(%edx),%edx
  }while((x /= base) != 0);
801006c8:	85 c0                	test   %eax,%eax
    buf[i++] = digits[x % base];
801006ca:	88 14 3e             	mov    %dl,(%esi,%edi,1)
  }while((x /= base) != 0);
801006cd:	75 e9                	jne    801006b8 <printint+0x28>
  if(sign)
801006cf:	8b 45 d4             	mov    -0x2c(%ebp),%eax
801006d2:	85 c0                	test   %eax,%eax
801006d4:	74 08                	je     801006de <printint+0x4e>
    buf[i++] = '-';
801006d6:	c6 44 3d d8 2d       	movb   $0x2d,-0x28(%ebp,%edi,1)
801006db:	8d 79 02             	lea    0x2(%ecx),%edi
801006de:	8d 5c 3d d7          	lea    -0x29(%ebp,%edi,1),%ebx
801006e2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    consputc(buf[i]);
801006e8:	0f be 03             	movsbl (%ebx),%eax
801006eb:	83 eb 01             	sub    $0x1,%ebx
801006ee:	e8 2d fe ff ff       	call   80100520 <consputc>
  while(--i >= 0)
801006f3:	39 f3                	cmp    %esi,%ebx
801006f5:	75 f1                	jne    801006e8 <printint+0x58>
}
801006f7:	83 c4 2c             	add    $0x2c,%esp
801006fa:	5b                   	pop    %ebx
801006fb:	5e                   	pop    %esi
801006fc:	5f                   	pop    %edi
801006fd:	5d                   	pop    %ebp
801006fe:	c3                   	ret    
801006ff:	90                   	nop
    x = -xx;
80100700:	f7 d8                	neg    %eax
80100702:	eb a9                	jmp    801006ad <printint+0x1d>
80100704:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010070a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80100710 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100710:	55                   	push   %ebp
80100711:	89 e5                	mov    %esp,%ebp
80100713:	57                   	push   %edi
80100714:	56                   	push   %esi
80100715:	53                   	push   %ebx
80100716:	83 ec 18             	sub    $0x18,%esp
80100719:	8b 75 10             	mov    0x10(%ebp),%esi
  int i;

  iunlock(ip);
8010071c:	ff 75 08             	pushl  0x8(%ebp)
8010071f:	e8 cc 12 00 00       	call   801019f0 <iunlock>
  acquire(&cons.lock);
80100724:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
8010072b:	e8 a0 3e 00 00       	call   801045d0 <acquire>
  for(i = 0; i < n; i++){
80100730:	83 c4 10             	add    $0x10,%esp
80100733:	85 f6                	test   %esi,%esi
80100735:	7e 18                	jle    8010074f <consolewrite+0x3f>
80100737:	8b 7d 0c             	mov    0xc(%ebp),%edi
8010073a:	8d 1c 37             	lea    (%edi,%esi,1),%ebx
8010073d:	8d 76 00             	lea    0x0(%esi),%esi
    consputc(buf[i] & 0xff);
80100740:	0f b6 07             	movzbl (%edi),%eax
80100743:	83 c7 01             	add    $0x1,%edi
80100746:	e8 d5 fd ff ff       	call   80100520 <consputc>
  for(i = 0; i < n; i++){
8010074b:	39 fb                	cmp    %edi,%ebx
8010074d:	75 f1                	jne    80100740 <consolewrite+0x30>
  }
  release(&cons.lock);
8010074f:	83 ec 0c             	sub    $0xc,%esp
80100752:	68 20 a5 10 80       	push   $0x8010a520
80100757:	e8 34 3f 00 00       	call   80104690 <release>
  ilock(ip);
8010075c:	58                   	pop    %eax
8010075d:	ff 75 08             	pushl  0x8(%ebp)
80100760:	e8 ab 11 00 00       	call   80101910 <ilock>

  return n;
}
80100765:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100768:	89 f0                	mov    %esi,%eax
8010076a:	5b                   	pop    %ebx
8010076b:	5e                   	pop    %esi
8010076c:	5f                   	pop    %edi
8010076d:	5d                   	pop    %ebp
8010076e:	c3                   	ret    
8010076f:	90                   	nop

80100770 <cprintf>:
{
80100770:	55                   	push   %ebp
80100771:	89 e5                	mov    %esp,%ebp
80100773:	57                   	push   %edi
80100774:	56                   	push   %esi
80100775:	53                   	push   %ebx
80100776:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
80100779:	a1 54 a5 10 80       	mov    0x8010a554,%eax
  if(locking)
8010077e:	85 c0                	test   %eax,%eax
  locking = cons.locking;
80100780:	89 45 dc             	mov    %eax,-0x24(%ebp)
  if(locking)
80100783:	0f 85 6f 01 00 00    	jne    801008f8 <cprintf+0x188>
  if (fmt == 0)
80100789:	8b 45 08             	mov    0x8(%ebp),%eax
8010078c:	85 c0                	test   %eax,%eax
8010078e:	89 c7                	mov    %eax,%edi
80100790:	0f 84 77 01 00 00    	je     8010090d <cprintf+0x19d>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100796:	0f b6 00             	movzbl (%eax),%eax
  argp = (uint*)(void*)(&fmt + 1);
80100799:	8d 4d 0c             	lea    0xc(%ebp),%ecx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010079c:	31 db                	xor    %ebx,%ebx
  argp = (uint*)(void*)(&fmt + 1);
8010079e:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801007a1:	85 c0                	test   %eax,%eax
801007a3:	75 56                	jne    801007fb <cprintf+0x8b>
801007a5:	eb 79                	jmp    80100820 <cprintf+0xb0>
801007a7:	89 f6                	mov    %esi,%esi
801007a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    c = fmt[++i] & 0xff;
801007b0:	0f b6 16             	movzbl (%esi),%edx
    if(c == 0)
801007b3:	85 d2                	test   %edx,%edx
801007b5:	74 69                	je     80100820 <cprintf+0xb0>
801007b7:	83 c3 02             	add    $0x2,%ebx
    switch(c){
801007ba:	83 fa 70             	cmp    $0x70,%edx
801007bd:	8d 34 1f             	lea    (%edi,%ebx,1),%esi
801007c0:	0f 84 84 00 00 00    	je     8010084a <cprintf+0xda>
801007c6:	7f 78                	jg     80100840 <cprintf+0xd0>
801007c8:	83 fa 25             	cmp    $0x25,%edx
801007cb:	0f 84 ff 00 00 00    	je     801008d0 <cprintf+0x160>
801007d1:	83 fa 64             	cmp    $0x64,%edx
801007d4:	0f 85 8e 00 00 00    	jne    80100868 <cprintf+0xf8>
      printint(*argp++, 10, 1);
801007da:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801007dd:	ba 0a 00 00 00       	mov    $0xa,%edx
801007e2:	8d 48 04             	lea    0x4(%eax),%ecx
801007e5:	8b 00                	mov    (%eax),%eax
801007e7:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
801007ea:	b9 01 00 00 00       	mov    $0x1,%ecx
801007ef:	e8 9c fe ff ff       	call   80100690 <printint>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801007f4:	0f b6 06             	movzbl (%esi),%eax
801007f7:	85 c0                	test   %eax,%eax
801007f9:	74 25                	je     80100820 <cprintf+0xb0>
801007fb:	8d 53 01             	lea    0x1(%ebx),%edx
    if(c != '%'){
801007fe:	83 f8 25             	cmp    $0x25,%eax
80100801:	8d 34 17             	lea    (%edi,%edx,1),%esi
80100804:	74 aa                	je     801007b0 <cprintf+0x40>
80100806:	89 55 e0             	mov    %edx,-0x20(%ebp)
      consputc(c);
80100809:	e8 12 fd ff ff       	call   80100520 <consputc>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
8010080e:	0f b6 06             	movzbl (%esi),%eax
      continue;
80100811:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100814:	89 d3                	mov    %edx,%ebx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100816:	85 c0                	test   %eax,%eax
80100818:	75 e1                	jne    801007fb <cprintf+0x8b>
8010081a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  if(locking)
80100820:	8b 45 dc             	mov    -0x24(%ebp),%eax
80100823:	85 c0                	test   %eax,%eax
80100825:	74 10                	je     80100837 <cprintf+0xc7>
    release(&cons.lock);
80100827:	83 ec 0c             	sub    $0xc,%esp
8010082a:	68 20 a5 10 80       	push   $0x8010a520
8010082f:	e8 5c 3e 00 00       	call   80104690 <release>
80100834:	83 c4 10             	add    $0x10,%esp
}
80100837:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010083a:	5b                   	pop    %ebx
8010083b:	5e                   	pop    %esi
8010083c:	5f                   	pop    %edi
8010083d:	5d                   	pop    %ebp
8010083e:	c3                   	ret    
8010083f:	90                   	nop
    switch(c){
80100840:	83 fa 73             	cmp    $0x73,%edx
80100843:	74 43                	je     80100888 <cprintf+0x118>
80100845:	83 fa 78             	cmp    $0x78,%edx
80100848:	75 1e                	jne    80100868 <cprintf+0xf8>
      printint(*argp++, 16, 0);
8010084a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010084d:	ba 10 00 00 00       	mov    $0x10,%edx
80100852:	8d 48 04             	lea    0x4(%eax),%ecx
80100855:	8b 00                	mov    (%eax),%eax
80100857:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
8010085a:	31 c9                	xor    %ecx,%ecx
8010085c:	e8 2f fe ff ff       	call   80100690 <printint>
      break;
80100861:	eb 91                	jmp    801007f4 <cprintf+0x84>
80100863:	90                   	nop
80100864:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      consputc('%');
80100868:	b8 25 00 00 00       	mov    $0x25,%eax
8010086d:	89 55 e0             	mov    %edx,-0x20(%ebp)
80100870:	e8 ab fc ff ff       	call   80100520 <consputc>
      consputc(c);
80100875:	8b 55 e0             	mov    -0x20(%ebp),%edx
80100878:	89 d0                	mov    %edx,%eax
8010087a:	e8 a1 fc ff ff       	call   80100520 <consputc>
      break;
8010087f:	e9 70 ff ff ff       	jmp    801007f4 <cprintf+0x84>
80100884:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if((s = (char*)*argp++) == 0)
80100888:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010088b:	8b 10                	mov    (%eax),%edx
8010088d:	8d 48 04             	lea    0x4(%eax),%ecx
80100890:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80100893:	85 d2                	test   %edx,%edx
80100895:	74 49                	je     801008e0 <cprintf+0x170>
      for(; *s; s++)
80100897:	0f be 02             	movsbl (%edx),%eax
      if((s = (char*)*argp++) == 0)
8010089a:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
      for(; *s; s++)
8010089d:	84 c0                	test   %al,%al
8010089f:	0f 84 4f ff ff ff    	je     801007f4 <cprintf+0x84>
801008a5:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
801008a8:	89 d3                	mov    %edx,%ebx
801008aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801008b0:	83 c3 01             	add    $0x1,%ebx
        consputc(*s);
801008b3:	e8 68 fc ff ff       	call   80100520 <consputc>
      for(; *s; s++)
801008b8:	0f be 03             	movsbl (%ebx),%eax
801008bb:	84 c0                	test   %al,%al
801008bd:	75 f1                	jne    801008b0 <cprintf+0x140>
      if((s = (char*)*argp++) == 0)
801008bf:	8b 45 e0             	mov    -0x20(%ebp),%eax
801008c2:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801008c5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801008c8:	e9 27 ff ff ff       	jmp    801007f4 <cprintf+0x84>
801008cd:	8d 76 00             	lea    0x0(%esi),%esi
      consputc('%');
801008d0:	b8 25 00 00 00       	mov    $0x25,%eax
801008d5:	e8 46 fc ff ff       	call   80100520 <consputc>
      break;
801008da:	e9 15 ff ff ff       	jmp    801007f4 <cprintf+0x84>
801008df:	90                   	nop
        s = "(null)";
801008e0:	ba 98 71 10 80       	mov    $0x80107198,%edx
      for(; *s; s++)
801008e5:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
801008e8:	b8 28 00 00 00       	mov    $0x28,%eax
801008ed:	89 d3                	mov    %edx,%ebx
801008ef:	eb bf                	jmp    801008b0 <cprintf+0x140>
801008f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    acquire(&cons.lock);
801008f8:	83 ec 0c             	sub    $0xc,%esp
801008fb:	68 20 a5 10 80       	push   $0x8010a520
80100900:	e8 cb 3c 00 00       	call   801045d0 <acquire>
80100905:	83 c4 10             	add    $0x10,%esp
80100908:	e9 7c fe ff ff       	jmp    80100789 <cprintf+0x19>
    panic("null fmt");
8010090d:	83 ec 0c             	sub    $0xc,%esp
80100910:	68 9f 71 10 80       	push   $0x8010719f
80100915:	e8 76 fa ff ff       	call   80100390 <panic>
8010091a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100920 <consoleintr>:
{ 
80100920:	55                   	push   %ebp
80100921:	89 e5                	mov    %esp,%ebp
80100923:	57                   	push   %edi
80100924:	56                   	push   %esi
80100925:	53                   	push   %ebx
80100926:	83 ec 28             	sub    $0x28,%esp
80100929:	8b 7d 08             	mov    0x8(%ebp),%edi
  acquire(&cons.lock);
8010092c:	68 20 a5 10 80       	push   $0x8010a520
80100931:	e8 9a 3c 00 00       	call   801045d0 <acquire>
  while((c = getc()) >= 0){
80100936:	83 c4 10             	add    $0x10,%esp
  int c, doprocdump = 0;
80100939:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
  while((c = getc()) >= 0){
80100940:	ff d7                	call   *%edi
80100942:	85 c0                	test   %eax,%eax
80100944:	89 c3                	mov    %eax,%ebx
80100946:	0f 88 9c 01 00 00    	js     80100ae8 <consoleintr+0x1c8>
    switch(c){
8010094c:	83 fb 15             	cmp    $0x15,%ebx
8010094f:	0f 84 8b 02 00 00    	je     80100be0 <consoleintr+0x2c0>
80100955:	0f 8f 45 01 00 00    	jg     80100aa0 <consoleintr+0x180>
8010095b:	83 fb 08             	cmp    $0x8,%ebx
8010095e:	0f 84 ac 01 00 00    	je     80100b10 <consoleintr+0x1f0>
80100964:	83 fb 10             	cmp    $0x10,%ebx
80100967:	0f 84 33 02 00 00    	je     80100ba0 <consoleintr+0x280>
8010096d:	83 fb 03             	cmp    $0x3,%ebx
80100970:	0f 84 ca 01 00 00    	je     80100b40 <consoleintr+0x220>
      if(input.flag == 1){
80100976:	83 3d b0 ff 10 80 01 	cmpl   $0x1,0x8010ffb0
8010097d:	75 1e                	jne    8010099d <consoleintr+0x7d>
        input.w = 0;
8010097f:	c7 05 a4 ff 10 80 00 	movl   $0x0,0x8010ffa4
80100986:	00 00 00 
        input.r = 0;
80100989:	c7 05 a0 ff 10 80 00 	movl   $0x0,0x8010ffa0
80100990:	00 00 00 
        input.flag = 0;
80100993:	c7 05 b0 ff 10 80 00 	movl   $0x0,0x8010ffb0
8010099a:	00 00 00 
      if(c != 0 && input.e -input.r < INPUT_BUF){
8010099d:	85 db                	test   %ebx,%ebx
8010099f:	74 9f                	je     80100940 <consoleintr+0x20>
801009a1:	8b 35 a8 ff 10 80    	mov    0x8010ffa8,%esi
801009a7:	89 f0                	mov    %esi,%eax
801009a9:	2b 05 a0 ff 10 80    	sub    0x8010ffa0,%eax
801009af:	83 f8 7f             	cmp    $0x7f,%eax
801009b2:	77 8c                	ja     80100940 <consoleintr+0x20>
        c = (c == '\r') ? '\n' : c;
801009b4:	83 fb 0d             	cmp    $0xd,%ebx
801009b7:	0f 84 77 02 00 00    	je     80100c34 <consoleintr+0x314>
801009bd:	83 fb 0a             	cmp    $0xa,%ebx
801009c0:	89 d9                	mov    %ebx,%ecx
801009c2:	0f 94 c2             	sete   %dl
801009c5:	83 fb 04             	cmp    $0x4,%ebx
801009c8:	0f 94 c0             	sete   %al
801009cb:	09 c2                	or     %eax,%edx
801009cd:	88 55 e3             	mov    %dl,-0x1d(%ebp)
        for(int i = input.num; i > (input.e); i--){
801009d0:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
801009d5:	39 c6                	cmp    %eax,%esi
801009d7:	89 c2                	mov    %eax,%edx
801009d9:	73 1c                	jae    801009f7 <consoleintr+0xd7>
801009db:	89 45 dc             	mov    %eax,-0x24(%ebp)
801009de:	66 90                	xchg   %ax,%ax
          input.buf[i] = input.buf[i - 1];
801009e0:	83 ea 01             	sub    $0x1,%edx
801009e3:	0f b6 82 20 ff 10 80 	movzbl -0x7fef00e0(%edx),%eax
        for(int i = input.num; i > (input.e); i--){
801009ea:	39 d6                	cmp    %edx,%esi
          input.buf[i] = input.buf[i - 1];
801009ec:	88 82 21 ff 10 80    	mov    %al,-0x7fef00df(%edx)
        for(int i = input.num; i > (input.e); i--){
801009f2:	72 ec                	jb     801009e0 <consoleintr+0xc0>
801009f4:	8b 45 dc             	mov    -0x24(%ebp),%eax
        input.num++;
801009f7:	83 c0 01             	add    $0x1,%eax
        input.buf[input.e++] = c;
801009fa:	8d 56 01             	lea    0x1(%esi),%edx
801009fd:	88 8e 20 ff 10 80    	mov    %cl,-0x7fef00e0(%esi)
        for(int i = (input.e) - 1; i < input.num; i++){
80100a03:	39 c6                	cmp    %eax,%esi
        input.num++;
80100a05:	a3 ac ff 10 80       	mov    %eax,0x8010ffac
        input.buf[input.e++] = c;
80100a0a:	89 15 a8 ff 10 80    	mov    %edx,0x8010ffa8
        for(int i = (input.e) - 1; i < input.num; i++){
80100a10:	73 24                	jae    80100a36 <consoleintr+0x116>
80100a12:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
          consputc(input.buf[i]);
80100a18:	0f be 86 20 ff 10 80 	movsbl -0x7fef00e0(%esi),%eax
        for(int i = (input.e) - 1; i < input.num; i++){
80100a1f:	83 c6 01             	add    $0x1,%esi
          consputc(input.buf[i]);
80100a22:	e8 f9 fa ff ff       	call   80100520 <consputc>
        for(int i = (input.e) - 1; i < input.num; i++){
80100a27:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100a2c:	39 f0                	cmp    %esi,%eax
80100a2e:	77 e8                	ja     80100a18 <consoleintr+0xf8>
80100a30:	8b 15 a8 ff 10 80    	mov    0x8010ffa8,%edx
        moveCursor((input.e) - input.num, c);
80100a36:	83 ec 08             	sub    $0x8,%esp
80100a39:	29 c2                	sub    %eax,%edx
80100a3b:	53                   	push   %ebx
80100a3c:	52                   	push   %edx
80100a3d:	e8 ce f9 ff ff       	call   80100410 <moveCursor>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
80100a42:	83 c4 10             	add    $0x10,%esp
80100a45:	80 7d e3 00          	cmpb   $0x0,-0x1d(%ebp)
80100a49:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100a4e:	75 14                	jne    80100a64 <consoleintr+0x144>
80100a50:	a1 a0 ff 10 80       	mov    0x8010ffa0,%eax
80100a55:	83 e8 80             	sub    $0xffffff80,%eax
80100a58:	39 05 a8 ff 10 80    	cmp    %eax,0x8010ffa8
80100a5e:	0f 85 dc fe ff ff    	jne    80100940 <consoleintr+0x20>
          wakeup(&input.r);
80100a64:	83 ec 0c             	sub    $0xc,%esp
          input.num = 0;
80100a67:	c7 05 ac ff 10 80 00 	movl   $0x0,0x8010ffac
80100a6e:	00 00 00 
          input.w = input.e;
80100a71:	a3 a4 ff 10 80       	mov    %eax,0x8010ffa4
          wakeup(&input.r);
80100a76:	68 a0 ff 10 80       	push   $0x8010ffa0
          input.e = 0;
80100a7b:	c7 05 a8 ff 10 80 00 	movl   $0x0,0x8010ffa8
80100a82:	00 00 00 
          input.flag = 1;
80100a85:	c7 05 b0 ff 10 80 01 	movl   $0x1,0x8010ffb0
80100a8c:	00 00 00 
          wakeup(&input.r);
80100a8f:	e8 2c 37 00 00       	call   801041c0 <wakeup>
80100a94:	83 c4 10             	add    $0x10,%esp
80100a97:	e9 a4 fe ff ff       	jmp    80100940 <consoleintr+0x20>
80100a9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    switch(c){
80100aa0:	83 fb 7d             	cmp    $0x7d,%ebx
80100aa3:	0f 84 07 01 00 00    	je     80100bb0 <consoleintr+0x290>
80100aa9:	83 fb 7f             	cmp    $0x7f,%ebx
80100aac:	74 62                	je     80100b10 <consoleintr+0x1f0>
80100aae:	83 fb 7b             	cmp    $0x7b,%ebx
80100ab1:	0f 85 bf fe ff ff    	jne    80100976 <consoleintr+0x56>
        moveCursor(-(input.e), c);
80100ab7:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100abc:	83 ec 08             	sub    $0x8,%esp
80100abf:	6a 7b                	push   $0x7b
80100ac1:	f7 d8                	neg    %eax
80100ac3:	50                   	push   %eax
80100ac4:	e8 47 f9 ff ff       	call   80100410 <moveCursor>
      break;
80100ac9:	83 c4 10             	add    $0x10,%esp
        input.e = 0;
80100acc:	c7 05 a8 ff 10 80 00 	movl   $0x0,0x8010ffa8
80100ad3:	00 00 00 
  while((c = getc()) >= 0){
80100ad6:	ff d7                	call   *%edi
80100ad8:	85 c0                	test   %eax,%eax
80100ada:	89 c3                	mov    %eax,%ebx
80100adc:	0f 89 6a fe ff ff    	jns    8010094c <consoleintr+0x2c>
80100ae2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&cons.lock);
80100ae8:	83 ec 0c             	sub    $0xc,%esp
80100aeb:	68 20 a5 10 80       	push   $0x8010a520
80100af0:	e8 9b 3b 00 00       	call   80104690 <release>
  if(doprocdump) {
80100af5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100af8:	83 c4 10             	add    $0x10,%esp
80100afb:	85 c0                	test   %eax,%eax
80100afd:	0f 85 25 01 00 00    	jne    80100c28 <consoleintr+0x308>
}
80100b03:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100b06:	5b                   	pop    %ebx
80100b07:	5e                   	pop    %esi
80100b08:	5f                   	pop    %edi
80100b09:	5d                   	pop    %ebp
80100b0a:	c3                   	ret    
80100b0b:	90                   	nop
80100b0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(input.e != input.w){
80100b10:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100b15:	3b 05 a4 ff 10 80    	cmp    0x8010ffa4,%eax
80100b1b:	0f 84 1f fe ff ff    	je     80100940 <consoleintr+0x20>
        input.e--;
80100b21:	83 e8 01             	sub    $0x1,%eax
        input.num--;
80100b24:	83 2d ac ff 10 80 01 	subl   $0x1,0x8010ffac
        input.e--;
80100b2b:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
        consputc(BACKSPACE);
80100b30:	b8 00 01 00 00       	mov    $0x100,%eax
80100b35:	e8 e6 f9 ff ff       	call   80100520 <consputc>
80100b3a:	e9 01 fe ff ff       	jmp    80100940 <consoleintr+0x20>
80100b3f:	90                   	nop
        moveCursor(input.num - (input.e) , c);    
80100b40:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100b45:	2b 05 a8 ff 10 80    	sub    0x8010ffa8,%eax
80100b4b:	83 ec 08             	sub    $0x8,%esp
80100b4e:	6a 03                	push   $0x3
80100b50:	50                   	push   %eax
80100b51:	e8 ba f8 ff ff       	call   80100410 <moveCursor>
        while(input.num != 0 && input.buf[(input.num-1) % INPUT_BUF] != '\n'){
80100b56:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100b5b:	83 c4 10             	add    $0x10,%esp
80100b5e:	85 c0                	test   %eax,%eax
80100b60:	75 1e                	jne    80100b80 <consoleintr+0x260>
80100b62:	eb 2d                	jmp    80100b91 <consoleintr+0x271>
80100b64:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
          input.num--;
80100b68:	a3 ac ff 10 80       	mov    %eax,0x8010ffac
          consputc(BACKSPACE);
80100b6d:	b8 00 01 00 00       	mov    $0x100,%eax
80100b72:	e8 a9 f9 ff ff       	call   80100520 <consputc>
        while(input.num != 0 && input.buf[(input.num-1) % INPUT_BUF] != '\n'){
80100b77:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100b7c:	85 c0                	test   %eax,%eax
80100b7e:	74 11                	je     80100b91 <consoleintr+0x271>
80100b80:	83 e8 01             	sub    $0x1,%eax
80100b83:	89 c2                	mov    %eax,%edx
80100b85:	83 e2 7f             	and    $0x7f,%edx
80100b88:	80 ba 20 ff 10 80 0a 	cmpb   $0xa,-0x7fef00e0(%edx)
80100b8f:	75 d7                	jne    80100b68 <consoleintr+0x248>
        input.e = 0;
80100b91:	c7 05 a8 ff 10 80 00 	movl   $0x0,0x8010ffa8
80100b98:	00 00 00 
      break;
80100b9b:	e9 a0 fd ff ff       	jmp    80100940 <consoleintr+0x20>
      doprocdump = 1;
80100ba0:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
80100ba7:	e9 94 fd ff ff       	jmp    80100940 <consoleintr+0x20>
80100bac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        moveCursor(input.num - (input.e), c);
80100bb0:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
80100bb5:	2b 05 a8 ff 10 80    	sub    0x8010ffa8,%eax
80100bbb:	83 ec 08             	sub    $0x8,%esp
80100bbe:	6a 7d                	push   $0x7d
80100bc0:	50                   	push   %eax
80100bc1:	e8 4a f8 ff ff       	call   80100410 <moveCursor>
        input.e = input.num;
80100bc6:	a1 ac ff 10 80       	mov    0x8010ffac,%eax
      break;
80100bcb:	83 c4 10             	add    $0x10,%esp
        input.e = input.num;
80100bce:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
      break;
80100bd3:	e9 68 fd ff ff       	jmp    80100940 <consoleintr+0x20>
80100bd8:	90                   	nop
80100bd9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
        while(input.e != 0 && input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100be0:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100be5:	85 c0                	test   %eax,%eax
80100be7:	75 23                	jne    80100c0c <consoleintr+0x2ec>
80100be9:	e9 52 fd ff ff       	jmp    80100940 <consoleintr+0x20>
80100bee:	66 90                	xchg   %ax,%ax
          input.e--;
80100bf0:	a3 a8 ff 10 80       	mov    %eax,0x8010ffa8
          consputc(BACKSPACE);
80100bf5:	b8 00 01 00 00       	mov    $0x100,%eax
80100bfa:	e8 21 f9 ff ff       	call   80100520 <consputc>
        while(input.e != 0 && input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100bff:	a1 a8 ff 10 80       	mov    0x8010ffa8,%eax
80100c04:	85 c0                	test   %eax,%eax
80100c06:	0f 84 34 fd ff ff    	je     80100940 <consoleintr+0x20>
80100c0c:	83 e8 01             	sub    $0x1,%eax
80100c0f:	89 c2                	mov    %eax,%edx
80100c11:	83 e2 7f             	and    $0x7f,%edx
80100c14:	80 ba 20 ff 10 80 0a 	cmpb   $0xa,-0x7fef00e0(%edx)
80100c1b:	75 d3                	jne    80100bf0 <consoleintr+0x2d0>
80100c1d:	e9 1e fd ff ff       	jmp    80100940 <consoleintr+0x20>
80100c22:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
}
80100c28:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100c2b:	5b                   	pop    %ebx
80100c2c:	5e                   	pop    %esi
80100c2d:	5f                   	pop    %edi
80100c2e:	5d                   	pop    %ebp
    procdump();  // now call procdump() wo. cons.lock held
80100c2f:	e9 6c 36 00 00       	jmp    801042a0 <procdump>
80100c34:	c6 45 e3 01          	movb   $0x1,-0x1d(%ebp)
80100c38:	b9 0a 00 00 00       	mov    $0xa,%ecx
        c = (c == '\r') ? '\n' : c;
80100c3d:	bb 0a 00 00 00       	mov    $0xa,%ebx
80100c42:	e9 89 fd ff ff       	jmp    801009d0 <consoleintr+0xb0>
80100c47:	89 f6                	mov    %esi,%esi
80100c49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80100c50 <consoleinit>:

void
consoleinit(void)
{
80100c50:	55                   	push   %ebp
80100c51:	89 e5                	mov    %esp,%ebp
80100c53:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
80100c56:	68 a8 71 10 80       	push   $0x801071a8
80100c5b:	68 20 a5 10 80       	push   $0x8010a520
80100c60:	e8 2b 38 00 00       	call   80104490 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  ioapicenable(IRQ_KBD, 0);
80100c65:	58                   	pop    %eax
80100c66:	5a                   	pop    %edx
80100c67:	6a 00                	push   $0x0
80100c69:	6a 01                	push   $0x1
  devsw[CONSOLE].write = consolewrite;
80100c6b:	c7 05 6c 09 11 80 10 	movl   $0x80100710,0x8011096c
80100c72:	07 10 80 
  devsw[CONSOLE].read = consoleread;
80100c75:	c7 05 68 09 11 80 70 	movl   $0x80100270,0x80110968
80100c7c:	02 10 80 
  cons.locking = 1;
80100c7f:	c7 05 54 a5 10 80 01 	movl   $0x1,0x8010a554
80100c86:	00 00 00 
  ioapicenable(IRQ_KBD, 0);
80100c89:	e8 d2 18 00 00       	call   80102560 <ioapicenable>
}
80100c8e:	83 c4 10             	add    $0x10,%esp
80100c91:	c9                   	leave  
80100c92:	c3                   	ret    
80100c93:	66 90                	xchg   %ax,%ax
80100c95:	66 90                	xchg   %ax,%ax
80100c97:	66 90                	xchg   %ax,%ax
80100c99:	66 90                	xchg   %ax,%ax
80100c9b:	66 90                	xchg   %ax,%ax
80100c9d:	66 90                	xchg   %ax,%ax
80100c9f:	90                   	nop

80100ca0 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100ca0:	55                   	push   %ebp
80100ca1:	89 e5                	mov    %esp,%ebp
80100ca3:	57                   	push   %edi
80100ca4:	56                   	push   %esi
80100ca5:	53                   	push   %ebx
80100ca6:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
80100cac:	e8 bf 2d 00 00       	call   80103a70 <myproc>
80100cb1:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)

  begin_op();
80100cb7:	e8 74 21 00 00       	call   80102e30 <begin_op>

  if((ip = namei(path)) == 0){
80100cbc:	83 ec 0c             	sub    $0xc,%esp
80100cbf:	ff 75 08             	pushl  0x8(%ebp)
80100cc2:	e8 a9 14 00 00       	call   80102170 <namei>
80100cc7:	83 c4 10             	add    $0x10,%esp
80100cca:	85 c0                	test   %eax,%eax
80100ccc:	0f 84 91 01 00 00    	je     80100e63 <exec+0x1c3>
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80100cd2:	83 ec 0c             	sub    $0xc,%esp
80100cd5:	89 c3                	mov    %eax,%ebx
80100cd7:	50                   	push   %eax
80100cd8:	e8 33 0c 00 00       	call   80101910 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
80100cdd:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80100ce3:	6a 34                	push   $0x34
80100ce5:	6a 00                	push   $0x0
80100ce7:	50                   	push   %eax
80100ce8:	53                   	push   %ebx
80100ce9:	e8 02 0f 00 00       	call   80101bf0 <readi>
80100cee:	83 c4 20             	add    $0x20,%esp
80100cf1:	83 f8 34             	cmp    $0x34,%eax
80100cf4:	74 22                	je     80100d18 <exec+0x78>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
80100cf6:	83 ec 0c             	sub    $0xc,%esp
80100cf9:	53                   	push   %ebx
80100cfa:	e8 a1 0e 00 00       	call   80101ba0 <iunlockput>
    end_op();
80100cff:	e8 9c 21 00 00       	call   80102ea0 <end_op>
80100d04:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
80100d07:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100d0c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100d0f:	5b                   	pop    %ebx
80100d10:	5e                   	pop    %esi
80100d11:	5f                   	pop    %edi
80100d12:	5d                   	pop    %ebp
80100d13:	c3                   	ret    
80100d14:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(elf.magic != ELF_MAGIC)
80100d18:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
80100d1f:	45 4c 46 
80100d22:	75 d2                	jne    80100cf6 <exec+0x56>
  if((pgdir = setupkvm()) == 0)
80100d24:	e8 77 61 00 00       	call   80106ea0 <setupkvm>
80100d29:	85 c0                	test   %eax,%eax
80100d2b:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80100d31:	74 c3                	je     80100cf6 <exec+0x56>
  sz = 0;
80100d33:	31 ff                	xor    %edi,%edi
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100d35:	66 83 bd 50 ff ff ff 	cmpw   $0x0,-0xb0(%ebp)
80100d3c:	00 
80100d3d:	8b 85 40 ff ff ff    	mov    -0xc0(%ebp),%eax
80100d43:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)
80100d49:	0f 84 8c 02 00 00    	je     80100fdb <exec+0x33b>
80100d4f:	31 f6                	xor    %esi,%esi
80100d51:	eb 7f                	jmp    80100dd2 <exec+0x132>
80100d53:	90                   	nop
80100d54:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ph.type != ELF_PROG_LOAD)
80100d58:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
80100d5f:	75 63                	jne    80100dc4 <exec+0x124>
    if(ph.memsz < ph.filesz)
80100d61:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
80100d67:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
80100d6d:	0f 82 86 00 00 00    	jb     80100df9 <exec+0x159>
80100d73:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
80100d79:	72 7e                	jb     80100df9 <exec+0x159>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100d7b:	83 ec 04             	sub    $0x4,%esp
80100d7e:	50                   	push   %eax
80100d7f:	57                   	push   %edi
80100d80:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100d86:	e8 35 5f 00 00       	call   80106cc0 <allocuvm>
80100d8b:	83 c4 10             	add    $0x10,%esp
80100d8e:	85 c0                	test   %eax,%eax
80100d90:	89 c7                	mov    %eax,%edi
80100d92:	74 65                	je     80100df9 <exec+0x159>
    if(ph.vaddr % PGSIZE != 0)
80100d94:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100d9a:	a9 ff 0f 00 00       	test   $0xfff,%eax
80100d9f:	75 58                	jne    80100df9 <exec+0x159>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100da1:	83 ec 0c             	sub    $0xc,%esp
80100da4:	ff b5 14 ff ff ff    	pushl  -0xec(%ebp)
80100daa:	ff b5 08 ff ff ff    	pushl  -0xf8(%ebp)
80100db0:	53                   	push   %ebx
80100db1:	50                   	push   %eax
80100db2:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100db8:	e8 43 5e 00 00       	call   80106c00 <loaduvm>
80100dbd:	83 c4 20             	add    $0x20,%esp
80100dc0:	85 c0                	test   %eax,%eax
80100dc2:	78 35                	js     80100df9 <exec+0x159>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100dc4:	0f b7 85 50 ff ff ff 	movzwl -0xb0(%ebp),%eax
80100dcb:	83 c6 01             	add    $0x1,%esi
80100dce:	39 f0                	cmp    %esi,%eax
80100dd0:	7e 3d                	jle    80100e0f <exec+0x16f>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100dd2:	89 f0                	mov    %esi,%eax
80100dd4:	6a 20                	push   $0x20
80100dd6:	c1 e0 05             	shl    $0x5,%eax
80100dd9:	03 85 ec fe ff ff    	add    -0x114(%ebp),%eax
80100ddf:	50                   	push   %eax
80100de0:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
80100de6:	50                   	push   %eax
80100de7:	53                   	push   %ebx
80100de8:	e8 03 0e 00 00       	call   80101bf0 <readi>
80100ded:	83 c4 10             	add    $0x10,%esp
80100df0:	83 f8 20             	cmp    $0x20,%eax
80100df3:	0f 84 5f ff ff ff    	je     80100d58 <exec+0xb8>
    freevm(pgdir);
80100df9:	83 ec 0c             	sub    $0xc,%esp
80100dfc:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e02:	e8 19 60 00 00       	call   80106e20 <freevm>
80100e07:	83 c4 10             	add    $0x10,%esp
80100e0a:	e9 e7 fe ff ff       	jmp    80100cf6 <exec+0x56>
80100e0f:	81 c7 ff 0f 00 00    	add    $0xfff,%edi
80100e15:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
80100e1b:	8d b7 00 20 00 00    	lea    0x2000(%edi),%esi
  iunlockput(ip);
80100e21:	83 ec 0c             	sub    $0xc,%esp
80100e24:	53                   	push   %ebx
80100e25:	e8 76 0d 00 00       	call   80101ba0 <iunlockput>
  end_op();
80100e2a:	e8 71 20 00 00       	call   80102ea0 <end_op>
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100e2f:	83 c4 0c             	add    $0xc,%esp
80100e32:	56                   	push   %esi
80100e33:	57                   	push   %edi
80100e34:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e3a:	e8 81 5e 00 00       	call   80106cc0 <allocuvm>
80100e3f:	83 c4 10             	add    $0x10,%esp
80100e42:	85 c0                	test   %eax,%eax
80100e44:	89 c6                	mov    %eax,%esi
80100e46:	75 3a                	jne    80100e82 <exec+0x1e2>
    freevm(pgdir);
80100e48:	83 ec 0c             	sub    $0xc,%esp
80100e4b:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e51:	e8 ca 5f 00 00       	call   80106e20 <freevm>
80100e56:	83 c4 10             	add    $0x10,%esp
  return -1;
80100e59:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100e5e:	e9 a9 fe ff ff       	jmp    80100d0c <exec+0x6c>
    end_op();
80100e63:	e8 38 20 00 00       	call   80102ea0 <end_op>
    cprintf("exec: fail\n");
80100e68:	83 ec 0c             	sub    $0xc,%esp
80100e6b:	68 c1 71 10 80       	push   $0x801071c1
80100e70:	e8 fb f8 ff ff       	call   80100770 <cprintf>
    return -1;
80100e75:	83 c4 10             	add    $0x10,%esp
80100e78:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100e7d:	e9 8a fe ff ff       	jmp    80100d0c <exec+0x6c>
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100e82:	8d 80 00 e0 ff ff    	lea    -0x2000(%eax),%eax
80100e88:	83 ec 08             	sub    $0x8,%esp
  for(argc = 0; argv[argc]; argc++) {
80100e8b:	31 ff                	xor    %edi,%edi
80100e8d:	89 f3                	mov    %esi,%ebx
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100e8f:	50                   	push   %eax
80100e90:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100e96:	e8 a5 60 00 00       	call   80106f40 <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80100e9b:	8b 45 0c             	mov    0xc(%ebp),%eax
80100e9e:	83 c4 10             	add    $0x10,%esp
80100ea1:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
80100ea7:	8b 00                	mov    (%eax),%eax
80100ea9:	85 c0                	test   %eax,%eax
80100eab:	74 70                	je     80100f1d <exec+0x27d>
80100ead:	89 b5 ec fe ff ff    	mov    %esi,-0x114(%ebp)
80100eb3:	8b b5 f0 fe ff ff    	mov    -0x110(%ebp),%esi
80100eb9:	eb 0a                	jmp    80100ec5 <exec+0x225>
80100ebb:	90                   	nop
80100ebc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(argc >= MAXARG)
80100ec0:	83 ff 20             	cmp    $0x20,%edi
80100ec3:	74 83                	je     80100e48 <exec+0x1a8>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100ec5:	83 ec 0c             	sub    $0xc,%esp
80100ec8:	50                   	push   %eax
80100ec9:	e8 32 3a 00 00       	call   80104900 <strlen>
80100ece:	f7 d0                	not    %eax
80100ed0:	01 c3                	add    %eax,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100ed2:	8b 45 0c             	mov    0xc(%ebp),%eax
80100ed5:	5a                   	pop    %edx
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100ed6:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100ed9:	ff 34 b8             	pushl  (%eax,%edi,4)
80100edc:	e8 1f 3a 00 00       	call   80104900 <strlen>
80100ee1:	83 c0 01             	add    $0x1,%eax
80100ee4:	50                   	push   %eax
80100ee5:	8b 45 0c             	mov    0xc(%ebp),%eax
80100ee8:	ff 34 b8             	pushl  (%eax,%edi,4)
80100eeb:	53                   	push   %ebx
80100eec:	56                   	push   %esi
80100eed:	e8 ae 61 00 00       	call   801070a0 <copyout>
80100ef2:	83 c4 20             	add    $0x20,%esp
80100ef5:	85 c0                	test   %eax,%eax
80100ef7:	0f 88 4b ff ff ff    	js     80100e48 <exec+0x1a8>
  for(argc = 0; argv[argc]; argc++) {
80100efd:	8b 45 0c             	mov    0xc(%ebp),%eax
    ustack[3+argc] = sp;
80100f00:	89 9c bd 64 ff ff ff 	mov    %ebx,-0x9c(%ebp,%edi,4)
  for(argc = 0; argv[argc]; argc++) {
80100f07:	83 c7 01             	add    $0x1,%edi
    ustack[3+argc] = sp;
80100f0a:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
  for(argc = 0; argv[argc]; argc++) {
80100f10:	8b 04 b8             	mov    (%eax,%edi,4),%eax
80100f13:	85 c0                	test   %eax,%eax
80100f15:	75 a9                	jne    80100ec0 <exec+0x220>
80100f17:	8b b5 ec fe ff ff    	mov    -0x114(%ebp),%esi
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100f1d:	8d 04 bd 04 00 00 00 	lea    0x4(,%edi,4),%eax
80100f24:	89 d9                	mov    %ebx,%ecx
  ustack[3+argc] = 0;
80100f26:	c7 84 bd 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%edi,4)
80100f2d:	00 00 00 00 
  ustack[0] = 0xffffffff;  // fake return PC
80100f31:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
80100f38:	ff ff ff 
  ustack[1] = argc;
80100f3b:	89 bd 5c ff ff ff    	mov    %edi,-0xa4(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100f41:	29 c1                	sub    %eax,%ecx
  sp -= (3+argc+1) * 4;
80100f43:	83 c0 0c             	add    $0xc,%eax
80100f46:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100f48:	50                   	push   %eax
80100f49:	52                   	push   %edx
80100f4a:	53                   	push   %ebx
80100f4b:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100f51:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100f57:	e8 44 61 00 00       	call   801070a0 <copyout>
80100f5c:	83 c4 10             	add    $0x10,%esp
80100f5f:	85 c0                	test   %eax,%eax
80100f61:	0f 88 e1 fe ff ff    	js     80100e48 <exec+0x1a8>
  for(last=s=path; *s; s++)
80100f67:	8b 45 08             	mov    0x8(%ebp),%eax
80100f6a:	0f b6 00             	movzbl (%eax),%eax
80100f6d:	84 c0                	test   %al,%al
80100f6f:	74 17                	je     80100f88 <exec+0x2e8>
80100f71:	8b 55 08             	mov    0x8(%ebp),%edx
80100f74:	89 d1                	mov    %edx,%ecx
80100f76:	83 c1 01             	add    $0x1,%ecx
80100f79:	3c 2f                	cmp    $0x2f,%al
80100f7b:	0f b6 01             	movzbl (%ecx),%eax
80100f7e:	0f 44 d1             	cmove  %ecx,%edx
80100f81:	84 c0                	test   %al,%al
80100f83:	75 f1                	jne    80100f76 <exec+0x2d6>
80100f85:	89 55 08             	mov    %edx,0x8(%ebp)
  safestrcpy(curproc->name, last, sizeof(curproc->name));
80100f88:	8b bd f4 fe ff ff    	mov    -0x10c(%ebp),%edi
80100f8e:	50                   	push   %eax
80100f8f:	6a 10                	push   $0x10
80100f91:	ff 75 08             	pushl  0x8(%ebp)
80100f94:	89 f8                	mov    %edi,%eax
80100f96:	83 c0 6c             	add    $0x6c,%eax
80100f99:	50                   	push   %eax
80100f9a:	e8 21 39 00 00       	call   801048c0 <safestrcpy>
  curproc->pgdir = pgdir;
80100f9f:	8b 95 f0 fe ff ff    	mov    -0x110(%ebp),%edx
  oldpgdir = curproc->pgdir;
80100fa5:	89 f9                	mov    %edi,%ecx
80100fa7:	8b 7f 04             	mov    0x4(%edi),%edi
  curproc->tf->eip = elf.entry;  // main
80100faa:	8b 41 18             	mov    0x18(%ecx),%eax
  curproc->sz = sz;
80100fad:	89 31                	mov    %esi,(%ecx)
  curproc->pgdir = pgdir;
80100faf:	89 51 04             	mov    %edx,0x4(%ecx)
  curproc->tf->eip = elf.entry;  // main
80100fb2:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
80100fb8:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
80100fbb:	8b 41 18             	mov    0x18(%ecx),%eax
80100fbe:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(curproc);
80100fc1:	89 0c 24             	mov    %ecx,(%esp)
80100fc4:	e8 a7 5a 00 00       	call   80106a70 <switchuvm>
  freevm(oldpgdir);
80100fc9:	89 3c 24             	mov    %edi,(%esp)
80100fcc:	e8 4f 5e 00 00       	call   80106e20 <freevm>
  return 0;
80100fd1:	83 c4 10             	add    $0x10,%esp
80100fd4:	31 c0                	xor    %eax,%eax
80100fd6:	e9 31 fd ff ff       	jmp    80100d0c <exec+0x6c>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100fdb:	be 00 20 00 00       	mov    $0x2000,%esi
80100fe0:	e9 3c fe ff ff       	jmp    80100e21 <exec+0x181>
80100fe5:	66 90                	xchg   %ax,%ax
80100fe7:	66 90                	xchg   %ax,%ax
80100fe9:	66 90                	xchg   %ax,%ax
80100feb:	66 90                	xchg   %ax,%ax
80100fed:	66 90                	xchg   %ax,%ax
80100fef:	90                   	nop

80100ff0 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80100ff0:	55                   	push   %ebp
80100ff1:	89 e5                	mov    %esp,%ebp
80100ff3:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
80100ff6:	68 cd 71 10 80       	push   $0x801071cd
80100ffb:	68 c0 ff 10 80       	push   $0x8010ffc0
80101000:	e8 8b 34 00 00       	call   80104490 <initlock>
}
80101005:	83 c4 10             	add    $0x10,%esp
80101008:	c9                   	leave  
80101009:	c3                   	ret    
8010100a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101010 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80101010:	55                   	push   %ebp
80101011:	89 e5                	mov    %esp,%ebp
80101013:	53                   	push   %ebx
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101014:	bb f4 ff 10 80       	mov    $0x8010fff4,%ebx
{
80101019:	83 ec 10             	sub    $0x10,%esp
  acquire(&ftable.lock);
8010101c:	68 c0 ff 10 80       	push   $0x8010ffc0
80101021:	e8 aa 35 00 00       	call   801045d0 <acquire>
80101026:	83 c4 10             	add    $0x10,%esp
80101029:	eb 10                	jmp    8010103b <filealloc+0x2b>
8010102b:	90                   	nop
8010102c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80101030:	83 c3 18             	add    $0x18,%ebx
80101033:	81 fb 54 09 11 80    	cmp    $0x80110954,%ebx
80101039:	73 25                	jae    80101060 <filealloc+0x50>
    if(f->ref == 0){
8010103b:	8b 43 04             	mov    0x4(%ebx),%eax
8010103e:	85 c0                	test   %eax,%eax
80101040:	75 ee                	jne    80101030 <filealloc+0x20>
      f->ref = 1;
      release(&ftable.lock);
80101042:	83 ec 0c             	sub    $0xc,%esp
      f->ref = 1;
80101045:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
8010104c:	68 c0 ff 10 80       	push   $0x8010ffc0
80101051:	e8 3a 36 00 00       	call   80104690 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
80101056:	89 d8                	mov    %ebx,%eax
      return f;
80101058:	83 c4 10             	add    $0x10,%esp
}
8010105b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010105e:	c9                   	leave  
8010105f:	c3                   	ret    
  release(&ftable.lock);
80101060:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80101063:	31 db                	xor    %ebx,%ebx
  release(&ftable.lock);
80101065:	68 c0 ff 10 80       	push   $0x8010ffc0
8010106a:	e8 21 36 00 00       	call   80104690 <release>
}
8010106f:	89 d8                	mov    %ebx,%eax
  return 0;
80101071:	83 c4 10             	add    $0x10,%esp
}
80101074:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101077:	c9                   	leave  
80101078:	c3                   	ret    
80101079:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101080 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80101080:	55                   	push   %ebp
80101081:	89 e5                	mov    %esp,%ebp
80101083:	53                   	push   %ebx
80101084:	83 ec 10             	sub    $0x10,%esp
80101087:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
8010108a:	68 c0 ff 10 80       	push   $0x8010ffc0
8010108f:	e8 3c 35 00 00       	call   801045d0 <acquire>
  if(f->ref < 1)
80101094:	8b 43 04             	mov    0x4(%ebx),%eax
80101097:	83 c4 10             	add    $0x10,%esp
8010109a:	85 c0                	test   %eax,%eax
8010109c:	7e 1a                	jle    801010b8 <filedup+0x38>
    panic("filedup");
  f->ref++;
8010109e:	83 c0 01             	add    $0x1,%eax
  release(&ftable.lock);
801010a1:	83 ec 0c             	sub    $0xc,%esp
  f->ref++;
801010a4:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
801010a7:	68 c0 ff 10 80       	push   $0x8010ffc0
801010ac:	e8 df 35 00 00       	call   80104690 <release>
  return f;
}
801010b1:	89 d8                	mov    %ebx,%eax
801010b3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801010b6:	c9                   	leave  
801010b7:	c3                   	ret    
    panic("filedup");
801010b8:	83 ec 0c             	sub    $0xc,%esp
801010bb:	68 d4 71 10 80       	push   $0x801071d4
801010c0:	e8 cb f2 ff ff       	call   80100390 <panic>
801010c5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801010c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801010d0 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
801010d0:	55                   	push   %ebp
801010d1:	89 e5                	mov    %esp,%ebp
801010d3:	57                   	push   %edi
801010d4:	56                   	push   %esi
801010d5:	53                   	push   %ebx
801010d6:	83 ec 28             	sub    $0x28,%esp
801010d9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
801010dc:	68 c0 ff 10 80       	push   $0x8010ffc0
801010e1:	e8 ea 34 00 00       	call   801045d0 <acquire>
  if(f->ref < 1)
801010e6:	8b 43 04             	mov    0x4(%ebx),%eax
801010e9:	83 c4 10             	add    $0x10,%esp
801010ec:	85 c0                	test   %eax,%eax
801010ee:	0f 8e 9b 00 00 00    	jle    8010118f <fileclose+0xbf>
    panic("fileclose");
  if(--f->ref > 0){
801010f4:	83 e8 01             	sub    $0x1,%eax
801010f7:	85 c0                	test   %eax,%eax
801010f9:	89 43 04             	mov    %eax,0x4(%ebx)
801010fc:	74 1a                	je     80101118 <fileclose+0x48>
    release(&ftable.lock);
801010fe:	c7 45 08 c0 ff 10 80 	movl   $0x8010ffc0,0x8(%ebp)
  else if(ff.type == FD_INODE){
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
80101105:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101108:	5b                   	pop    %ebx
80101109:	5e                   	pop    %esi
8010110a:	5f                   	pop    %edi
8010110b:	5d                   	pop    %ebp
    release(&ftable.lock);
8010110c:	e9 7f 35 00 00       	jmp    80104690 <release>
80101111:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  ff = *f;
80101118:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
8010111c:	8b 3b                	mov    (%ebx),%edi
  release(&ftable.lock);
8010111e:	83 ec 0c             	sub    $0xc,%esp
  ff = *f;
80101121:	8b 73 0c             	mov    0xc(%ebx),%esi
  f->type = FD_NONE;
80101124:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  ff = *f;
8010112a:	88 45 e7             	mov    %al,-0x19(%ebp)
8010112d:	8b 43 10             	mov    0x10(%ebx),%eax
  release(&ftable.lock);
80101130:	68 c0 ff 10 80       	push   $0x8010ffc0
  ff = *f;
80101135:	89 45 e0             	mov    %eax,-0x20(%ebp)
  release(&ftable.lock);
80101138:	e8 53 35 00 00       	call   80104690 <release>
  if(ff.type == FD_PIPE)
8010113d:	83 c4 10             	add    $0x10,%esp
80101140:	83 ff 01             	cmp    $0x1,%edi
80101143:	74 13                	je     80101158 <fileclose+0x88>
  else if(ff.type == FD_INODE){
80101145:	83 ff 02             	cmp    $0x2,%edi
80101148:	74 26                	je     80101170 <fileclose+0xa0>
}
8010114a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010114d:	5b                   	pop    %ebx
8010114e:	5e                   	pop    %esi
8010114f:	5f                   	pop    %edi
80101150:	5d                   	pop    %ebp
80101151:	c3                   	ret    
80101152:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    pipeclose(ff.pipe, ff.writable);
80101158:	0f be 5d e7          	movsbl -0x19(%ebp),%ebx
8010115c:	83 ec 08             	sub    $0x8,%esp
8010115f:	53                   	push   %ebx
80101160:	56                   	push   %esi
80101161:	e8 7a 24 00 00       	call   801035e0 <pipeclose>
80101166:	83 c4 10             	add    $0x10,%esp
80101169:	eb df                	jmp    8010114a <fileclose+0x7a>
8010116b:	90                   	nop
8010116c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    begin_op();
80101170:	e8 bb 1c 00 00       	call   80102e30 <begin_op>
    iput(ff.ip);
80101175:	83 ec 0c             	sub    $0xc,%esp
80101178:	ff 75 e0             	pushl  -0x20(%ebp)
8010117b:	e8 c0 08 00 00       	call   80101a40 <iput>
    end_op();
80101180:	83 c4 10             	add    $0x10,%esp
}
80101183:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101186:	5b                   	pop    %ebx
80101187:	5e                   	pop    %esi
80101188:	5f                   	pop    %edi
80101189:	5d                   	pop    %ebp
    end_op();
8010118a:	e9 11 1d 00 00       	jmp    80102ea0 <end_op>
    panic("fileclose");
8010118f:	83 ec 0c             	sub    $0xc,%esp
80101192:	68 dc 71 10 80       	push   $0x801071dc
80101197:	e8 f4 f1 ff ff       	call   80100390 <panic>
8010119c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801011a0 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
801011a0:	55                   	push   %ebp
801011a1:	89 e5                	mov    %esp,%ebp
801011a3:	53                   	push   %ebx
801011a4:	83 ec 04             	sub    $0x4,%esp
801011a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
801011aa:	83 3b 02             	cmpl   $0x2,(%ebx)
801011ad:	75 31                	jne    801011e0 <filestat+0x40>
    ilock(f->ip);
801011af:	83 ec 0c             	sub    $0xc,%esp
801011b2:	ff 73 10             	pushl  0x10(%ebx)
801011b5:	e8 56 07 00 00       	call   80101910 <ilock>
    stati(f->ip, st);
801011ba:	58                   	pop    %eax
801011bb:	5a                   	pop    %edx
801011bc:	ff 75 0c             	pushl  0xc(%ebp)
801011bf:	ff 73 10             	pushl  0x10(%ebx)
801011c2:	e8 f9 09 00 00       	call   80101bc0 <stati>
    iunlock(f->ip);
801011c7:	59                   	pop    %ecx
801011c8:	ff 73 10             	pushl  0x10(%ebx)
801011cb:	e8 20 08 00 00       	call   801019f0 <iunlock>
    return 0;
801011d0:	83 c4 10             	add    $0x10,%esp
801011d3:	31 c0                	xor    %eax,%eax
  }
  return -1;
}
801011d5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801011d8:	c9                   	leave  
801011d9:	c3                   	ret    
801011da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return -1;
801011e0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801011e5:	eb ee                	jmp    801011d5 <filestat+0x35>
801011e7:	89 f6                	mov    %esi,%esi
801011e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801011f0 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
801011f0:	55                   	push   %ebp
801011f1:	89 e5                	mov    %esp,%ebp
801011f3:	57                   	push   %edi
801011f4:	56                   	push   %esi
801011f5:	53                   	push   %ebx
801011f6:	83 ec 0c             	sub    $0xc,%esp
801011f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
801011fc:	8b 75 0c             	mov    0xc(%ebp),%esi
801011ff:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
80101202:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
80101206:	74 60                	je     80101268 <fileread+0x78>
    return -1;
  if(f->type == FD_PIPE)
80101208:	8b 03                	mov    (%ebx),%eax
8010120a:	83 f8 01             	cmp    $0x1,%eax
8010120d:	74 41                	je     80101250 <fileread+0x60>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
8010120f:	83 f8 02             	cmp    $0x2,%eax
80101212:	75 5b                	jne    8010126f <fileread+0x7f>
    ilock(f->ip);
80101214:	83 ec 0c             	sub    $0xc,%esp
80101217:	ff 73 10             	pushl  0x10(%ebx)
8010121a:	e8 f1 06 00 00       	call   80101910 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
8010121f:	57                   	push   %edi
80101220:	ff 73 14             	pushl  0x14(%ebx)
80101223:	56                   	push   %esi
80101224:	ff 73 10             	pushl  0x10(%ebx)
80101227:	e8 c4 09 00 00       	call   80101bf0 <readi>
8010122c:	83 c4 20             	add    $0x20,%esp
8010122f:	85 c0                	test   %eax,%eax
80101231:	89 c6                	mov    %eax,%esi
80101233:	7e 03                	jle    80101238 <fileread+0x48>
      f->off += r;
80101235:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
80101238:	83 ec 0c             	sub    $0xc,%esp
8010123b:	ff 73 10             	pushl  0x10(%ebx)
8010123e:	e8 ad 07 00 00       	call   801019f0 <iunlock>
    return r;
80101243:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
80101246:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101249:	89 f0                	mov    %esi,%eax
8010124b:	5b                   	pop    %ebx
8010124c:	5e                   	pop    %esi
8010124d:	5f                   	pop    %edi
8010124e:	5d                   	pop    %ebp
8010124f:	c3                   	ret    
    return piperead(f->pipe, addr, n);
80101250:	8b 43 0c             	mov    0xc(%ebx),%eax
80101253:	89 45 08             	mov    %eax,0x8(%ebp)
}
80101256:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101259:	5b                   	pop    %ebx
8010125a:	5e                   	pop    %esi
8010125b:	5f                   	pop    %edi
8010125c:	5d                   	pop    %ebp
    return piperead(f->pipe, addr, n);
8010125d:	e9 2e 25 00 00       	jmp    80103790 <piperead>
80101262:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
80101268:	be ff ff ff ff       	mov    $0xffffffff,%esi
8010126d:	eb d7                	jmp    80101246 <fileread+0x56>
  panic("fileread");
8010126f:	83 ec 0c             	sub    $0xc,%esp
80101272:	68 e6 71 10 80       	push   $0x801071e6
80101277:	e8 14 f1 ff ff       	call   80100390 <panic>
8010127c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101280 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80101280:	55                   	push   %ebp
80101281:	89 e5                	mov    %esp,%ebp
80101283:	57                   	push   %edi
80101284:	56                   	push   %esi
80101285:	53                   	push   %ebx
80101286:	83 ec 1c             	sub    $0x1c,%esp
80101289:	8b 75 08             	mov    0x8(%ebp),%esi
8010128c:	8b 45 0c             	mov    0xc(%ebp),%eax
  int r;

  if(f->writable == 0)
8010128f:	80 7e 09 00          	cmpb   $0x0,0x9(%esi)
{
80101293:	89 45 dc             	mov    %eax,-0x24(%ebp)
80101296:	8b 45 10             	mov    0x10(%ebp),%eax
80101299:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(f->writable == 0)
8010129c:	0f 84 aa 00 00 00    	je     8010134c <filewrite+0xcc>
    return -1;
  if(f->type == FD_PIPE)
801012a2:	8b 06                	mov    (%esi),%eax
801012a4:	83 f8 01             	cmp    $0x1,%eax
801012a7:	0f 84 c3 00 00 00    	je     80101370 <filewrite+0xf0>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
801012ad:	83 f8 02             	cmp    $0x2,%eax
801012b0:	0f 85 d9 00 00 00    	jne    8010138f <filewrite+0x10f>
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
801012b6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    int i = 0;
801012b9:	31 ff                	xor    %edi,%edi
    while(i < n){
801012bb:	85 c0                	test   %eax,%eax
801012bd:	7f 34                	jg     801012f3 <filewrite+0x73>
801012bf:	e9 9c 00 00 00       	jmp    80101360 <filewrite+0xe0>
801012c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        n1 = max;

      begin_op();
      ilock(f->ip);
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
        f->off += r;
801012c8:	01 46 14             	add    %eax,0x14(%esi)
      iunlock(f->ip);
801012cb:	83 ec 0c             	sub    $0xc,%esp
801012ce:	ff 76 10             	pushl  0x10(%esi)
        f->off += r;
801012d1:	89 45 e0             	mov    %eax,-0x20(%ebp)
      iunlock(f->ip);
801012d4:	e8 17 07 00 00       	call   801019f0 <iunlock>
      end_op();
801012d9:	e8 c2 1b 00 00       	call   80102ea0 <end_op>
801012de:	8b 45 e0             	mov    -0x20(%ebp),%eax
801012e1:	83 c4 10             	add    $0x10,%esp

      if(r < 0)
        break;
      if(r != n1)
801012e4:	39 c3                	cmp    %eax,%ebx
801012e6:	0f 85 96 00 00 00    	jne    80101382 <filewrite+0x102>
        panic("short filewrite");
      i += r;
801012ec:	01 df                	add    %ebx,%edi
    while(i < n){
801012ee:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
801012f1:	7e 6d                	jle    80101360 <filewrite+0xe0>
      int n1 = n - i;
801012f3:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801012f6:	b8 00 06 00 00       	mov    $0x600,%eax
801012fb:	29 fb                	sub    %edi,%ebx
801012fd:	81 fb 00 06 00 00    	cmp    $0x600,%ebx
80101303:	0f 4f d8             	cmovg  %eax,%ebx
      begin_op();
80101306:	e8 25 1b 00 00       	call   80102e30 <begin_op>
      ilock(f->ip);
8010130b:	83 ec 0c             	sub    $0xc,%esp
8010130e:	ff 76 10             	pushl  0x10(%esi)
80101311:	e8 fa 05 00 00       	call   80101910 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80101316:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101319:	53                   	push   %ebx
8010131a:	ff 76 14             	pushl  0x14(%esi)
8010131d:	01 f8                	add    %edi,%eax
8010131f:	50                   	push   %eax
80101320:	ff 76 10             	pushl  0x10(%esi)
80101323:	e8 c8 09 00 00       	call   80101cf0 <writei>
80101328:	83 c4 20             	add    $0x20,%esp
8010132b:	85 c0                	test   %eax,%eax
8010132d:	7f 99                	jg     801012c8 <filewrite+0x48>
      iunlock(f->ip);
8010132f:	83 ec 0c             	sub    $0xc,%esp
80101332:	ff 76 10             	pushl  0x10(%esi)
80101335:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101338:	e8 b3 06 00 00       	call   801019f0 <iunlock>
      end_op();
8010133d:	e8 5e 1b 00 00       	call   80102ea0 <end_op>
      if(r < 0)
80101342:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101345:	83 c4 10             	add    $0x10,%esp
80101348:	85 c0                	test   %eax,%eax
8010134a:	74 98                	je     801012e4 <filewrite+0x64>
    }
    return i == n ? n : -1;
  }
  panic("filewrite");
}
8010134c:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
8010134f:	bf ff ff ff ff       	mov    $0xffffffff,%edi
}
80101354:	89 f8                	mov    %edi,%eax
80101356:	5b                   	pop    %ebx
80101357:	5e                   	pop    %esi
80101358:	5f                   	pop    %edi
80101359:	5d                   	pop    %ebp
8010135a:	c3                   	ret    
8010135b:	90                   	nop
8010135c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return i == n ? n : -1;
80101360:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101363:	75 e7                	jne    8010134c <filewrite+0xcc>
}
80101365:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101368:	89 f8                	mov    %edi,%eax
8010136a:	5b                   	pop    %ebx
8010136b:	5e                   	pop    %esi
8010136c:	5f                   	pop    %edi
8010136d:	5d                   	pop    %ebp
8010136e:	c3                   	ret    
8010136f:	90                   	nop
    return pipewrite(f->pipe, addr, n);
80101370:	8b 46 0c             	mov    0xc(%esi),%eax
80101373:	89 45 08             	mov    %eax,0x8(%ebp)
}
80101376:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101379:	5b                   	pop    %ebx
8010137a:	5e                   	pop    %esi
8010137b:	5f                   	pop    %edi
8010137c:	5d                   	pop    %ebp
    return pipewrite(f->pipe, addr, n);
8010137d:	e9 fe 22 00 00       	jmp    80103680 <pipewrite>
        panic("short filewrite");
80101382:	83 ec 0c             	sub    $0xc,%esp
80101385:	68 ef 71 10 80       	push   $0x801071ef
8010138a:	e8 01 f0 ff ff       	call   80100390 <panic>
  panic("filewrite");
8010138f:	83 ec 0c             	sub    $0xc,%esp
80101392:	68 f5 71 10 80       	push   $0x801071f5
80101397:	e8 f4 ef ff ff       	call   80100390 <panic>
8010139c:	66 90                	xchg   %ax,%ax
8010139e:	66 90                	xchg   %ax,%ax

801013a0 <bfree>:
}

// Free a disk block.
static void
bfree(int dev, uint b)
{
801013a0:	55                   	push   %ebp
801013a1:	89 e5                	mov    %esp,%ebp
801013a3:	56                   	push   %esi
801013a4:	53                   	push   %ebx
801013a5:	89 d3                	mov    %edx,%ebx
  struct buf *bp;
  int bi, m;

  bp = bread(dev, BBLOCK(b, sb));
801013a7:	c1 ea 0c             	shr    $0xc,%edx
801013aa:	03 15 d8 09 11 80    	add    0x801109d8,%edx
801013b0:	83 ec 08             	sub    $0x8,%esp
801013b3:	52                   	push   %edx
801013b4:	50                   	push   %eax
801013b5:	e8 16 ed ff ff       	call   801000d0 <bread>
  bi = b % BPB;
  m = 1 << (bi % 8);
801013ba:	89 d9                	mov    %ebx,%ecx
  if((bp->data[bi/8] & m) == 0)
801013bc:	c1 fb 03             	sar    $0x3,%ebx
  m = 1 << (bi % 8);
801013bf:	ba 01 00 00 00       	mov    $0x1,%edx
801013c4:	83 e1 07             	and    $0x7,%ecx
  if((bp->data[bi/8] & m) == 0)
801013c7:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
801013cd:	83 c4 10             	add    $0x10,%esp
  m = 1 << (bi % 8);
801013d0:	d3 e2                	shl    %cl,%edx
  if((bp->data[bi/8] & m) == 0)
801013d2:	0f b6 4c 18 5c       	movzbl 0x5c(%eax,%ebx,1),%ecx
801013d7:	85 d1                	test   %edx,%ecx
801013d9:	74 25                	je     80101400 <bfree+0x60>
    panic("freeing free block");
  bp->data[bi/8] &= ~m;
801013db:	f7 d2                	not    %edx
801013dd:	89 c6                	mov    %eax,%esi
  log_write(bp);
801013df:	83 ec 0c             	sub    $0xc,%esp
  bp->data[bi/8] &= ~m;
801013e2:	21 ca                	and    %ecx,%edx
801013e4:	88 54 1e 5c          	mov    %dl,0x5c(%esi,%ebx,1)
  log_write(bp);
801013e8:	56                   	push   %esi
801013e9:	e8 12 1c 00 00       	call   80103000 <log_write>
  brelse(bp);
801013ee:	89 34 24             	mov    %esi,(%esp)
801013f1:	e8 ea ed ff ff       	call   801001e0 <brelse>
}
801013f6:	83 c4 10             	add    $0x10,%esp
801013f9:	8d 65 f8             	lea    -0x8(%ebp),%esp
801013fc:	5b                   	pop    %ebx
801013fd:	5e                   	pop    %esi
801013fe:	5d                   	pop    %ebp
801013ff:	c3                   	ret    
    panic("freeing free block");
80101400:	83 ec 0c             	sub    $0xc,%esp
80101403:	68 ff 71 10 80       	push   $0x801071ff
80101408:	e8 83 ef ff ff       	call   80100390 <panic>
8010140d:	8d 76 00             	lea    0x0(%esi),%esi

80101410 <balloc>:
{
80101410:	55                   	push   %ebp
80101411:	89 e5                	mov    %esp,%ebp
80101413:	57                   	push   %edi
80101414:	56                   	push   %esi
80101415:	53                   	push   %ebx
80101416:	83 ec 1c             	sub    $0x1c,%esp
  for(b = 0; b < sb.size; b += BPB){
80101419:	8b 0d c0 09 11 80    	mov    0x801109c0,%ecx
{
8010141f:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101422:	85 c9                	test   %ecx,%ecx
80101424:	0f 84 87 00 00 00    	je     801014b1 <balloc+0xa1>
8010142a:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    bp = bread(dev, BBLOCK(b, sb));
80101431:	8b 75 dc             	mov    -0x24(%ebp),%esi
80101434:	83 ec 08             	sub    $0x8,%esp
80101437:	89 f0                	mov    %esi,%eax
80101439:	c1 f8 0c             	sar    $0xc,%eax
8010143c:	03 05 d8 09 11 80    	add    0x801109d8,%eax
80101442:	50                   	push   %eax
80101443:	ff 75 d8             	pushl  -0x28(%ebp)
80101446:	e8 85 ec ff ff       	call   801000d0 <bread>
8010144b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010144e:	a1 c0 09 11 80       	mov    0x801109c0,%eax
80101453:	83 c4 10             	add    $0x10,%esp
80101456:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101459:	31 c0                	xor    %eax,%eax
8010145b:	eb 2f                	jmp    8010148c <balloc+0x7c>
8010145d:	8d 76 00             	lea    0x0(%esi),%esi
      m = 1 << (bi % 8);
80101460:	89 c1                	mov    %eax,%ecx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101462:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      m = 1 << (bi % 8);
80101465:	bb 01 00 00 00       	mov    $0x1,%ebx
8010146a:	83 e1 07             	and    $0x7,%ecx
8010146d:	d3 e3                	shl    %cl,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
8010146f:	89 c1                	mov    %eax,%ecx
80101471:	c1 f9 03             	sar    $0x3,%ecx
80101474:	0f b6 7c 0a 5c       	movzbl 0x5c(%edx,%ecx,1),%edi
80101479:	85 df                	test   %ebx,%edi
8010147b:	89 fa                	mov    %edi,%edx
8010147d:	74 41                	je     801014c0 <balloc+0xb0>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010147f:	83 c0 01             	add    $0x1,%eax
80101482:	83 c6 01             	add    $0x1,%esi
80101485:	3d 00 10 00 00       	cmp    $0x1000,%eax
8010148a:	74 05                	je     80101491 <balloc+0x81>
8010148c:	39 75 e0             	cmp    %esi,-0x20(%ebp)
8010148f:	77 cf                	ja     80101460 <balloc+0x50>
    brelse(bp);
80101491:	83 ec 0c             	sub    $0xc,%esp
80101494:	ff 75 e4             	pushl  -0x1c(%ebp)
80101497:	e8 44 ed ff ff       	call   801001e0 <brelse>
  for(b = 0; b < sb.size; b += BPB){
8010149c:	81 45 dc 00 10 00 00 	addl   $0x1000,-0x24(%ebp)
801014a3:	83 c4 10             	add    $0x10,%esp
801014a6:	8b 45 dc             	mov    -0x24(%ebp),%eax
801014a9:	39 05 c0 09 11 80    	cmp    %eax,0x801109c0
801014af:	77 80                	ja     80101431 <balloc+0x21>
  panic("balloc: out of blocks");
801014b1:	83 ec 0c             	sub    $0xc,%esp
801014b4:	68 12 72 10 80       	push   $0x80107212
801014b9:	e8 d2 ee ff ff       	call   80100390 <panic>
801014be:	66 90                	xchg   %ax,%ax
        bp->data[bi/8] |= m;  // Mark block in use.
801014c0:	8b 7d e4             	mov    -0x1c(%ebp),%edi
        log_write(bp);
801014c3:	83 ec 0c             	sub    $0xc,%esp
        bp->data[bi/8] |= m;  // Mark block in use.
801014c6:	09 da                	or     %ebx,%edx
801014c8:	88 54 0f 5c          	mov    %dl,0x5c(%edi,%ecx,1)
        log_write(bp);
801014cc:	57                   	push   %edi
801014cd:	e8 2e 1b 00 00       	call   80103000 <log_write>
        brelse(bp);
801014d2:	89 3c 24             	mov    %edi,(%esp)
801014d5:	e8 06 ed ff ff       	call   801001e0 <brelse>
  bp = bread(dev, bno);
801014da:	58                   	pop    %eax
801014db:	5a                   	pop    %edx
801014dc:	56                   	push   %esi
801014dd:	ff 75 d8             	pushl  -0x28(%ebp)
801014e0:	e8 eb eb ff ff       	call   801000d0 <bread>
801014e5:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
801014e7:	8d 40 5c             	lea    0x5c(%eax),%eax
801014ea:	83 c4 0c             	add    $0xc,%esp
801014ed:	68 00 02 00 00       	push   $0x200
801014f2:	6a 00                	push   $0x0
801014f4:	50                   	push   %eax
801014f5:	e8 e6 31 00 00       	call   801046e0 <memset>
  log_write(bp);
801014fa:	89 1c 24             	mov    %ebx,(%esp)
801014fd:	e8 fe 1a 00 00       	call   80103000 <log_write>
  brelse(bp);
80101502:	89 1c 24             	mov    %ebx,(%esp)
80101505:	e8 d6 ec ff ff       	call   801001e0 <brelse>
}
8010150a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010150d:	89 f0                	mov    %esi,%eax
8010150f:	5b                   	pop    %ebx
80101510:	5e                   	pop    %esi
80101511:	5f                   	pop    %edi
80101512:	5d                   	pop    %ebp
80101513:	c3                   	ret    
80101514:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010151a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80101520 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
80101520:	55                   	push   %ebp
80101521:	89 e5                	mov    %esp,%ebp
80101523:	57                   	push   %edi
80101524:	56                   	push   %esi
80101525:	53                   	push   %ebx
80101526:	89 c7                	mov    %eax,%edi
  struct inode *ip, *empty;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
80101528:	31 f6                	xor    %esi,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010152a:	bb 14 0a 11 80       	mov    $0x80110a14,%ebx
{
8010152f:	83 ec 28             	sub    $0x28,%esp
80101532:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
80101535:	68 e0 09 11 80       	push   $0x801109e0
8010153a:	e8 91 30 00 00       	call   801045d0 <acquire>
8010153f:	83 c4 10             	add    $0x10,%esp
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101542:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101545:	eb 17                	jmp    8010155e <iget+0x3e>
80101547:	89 f6                	mov    %esi,%esi
80101549:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80101550:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101556:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
8010155c:	73 22                	jae    80101580 <iget+0x60>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
8010155e:	8b 4b 08             	mov    0x8(%ebx),%ecx
80101561:	85 c9                	test   %ecx,%ecx
80101563:	7e 04                	jle    80101569 <iget+0x49>
80101565:	39 3b                	cmp    %edi,(%ebx)
80101567:	74 4f                	je     801015b8 <iget+0x98>
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101569:	85 f6                	test   %esi,%esi
8010156b:	75 e3                	jne    80101550 <iget+0x30>
8010156d:	85 c9                	test   %ecx,%ecx
8010156f:	0f 44 f3             	cmove  %ebx,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101572:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101578:	81 fb 34 26 11 80    	cmp    $0x80112634,%ebx
8010157e:	72 de                	jb     8010155e <iget+0x3e>
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101580:	85 f6                	test   %esi,%esi
80101582:	74 5b                	je     801015df <iget+0xbf>
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  release(&icache.lock);
80101584:	83 ec 0c             	sub    $0xc,%esp
  ip->dev = dev;
80101587:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
80101589:	89 56 04             	mov    %edx,0x4(%esi)
  ip->ref = 1;
8010158c:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
80101593:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
8010159a:	68 e0 09 11 80       	push   $0x801109e0
8010159f:	e8 ec 30 00 00       	call   80104690 <release>

  return ip;
801015a4:	83 c4 10             	add    $0x10,%esp
}
801015a7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801015aa:	89 f0                	mov    %esi,%eax
801015ac:	5b                   	pop    %ebx
801015ad:	5e                   	pop    %esi
801015ae:	5f                   	pop    %edi
801015af:	5d                   	pop    %ebp
801015b0:	c3                   	ret    
801015b1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
801015b8:	39 53 04             	cmp    %edx,0x4(%ebx)
801015bb:	75 ac                	jne    80101569 <iget+0x49>
      release(&icache.lock);
801015bd:	83 ec 0c             	sub    $0xc,%esp
      ip->ref++;
801015c0:	83 c1 01             	add    $0x1,%ecx
      return ip;
801015c3:	89 de                	mov    %ebx,%esi
      release(&icache.lock);
801015c5:	68 e0 09 11 80       	push   $0x801109e0
      ip->ref++;
801015ca:	89 4b 08             	mov    %ecx,0x8(%ebx)
      release(&icache.lock);
801015cd:	e8 be 30 00 00       	call   80104690 <release>
      return ip;
801015d2:	83 c4 10             	add    $0x10,%esp
}
801015d5:	8d 65 f4             	lea    -0xc(%ebp),%esp
801015d8:	89 f0                	mov    %esi,%eax
801015da:	5b                   	pop    %ebx
801015db:	5e                   	pop    %esi
801015dc:	5f                   	pop    %edi
801015dd:	5d                   	pop    %ebp
801015de:	c3                   	ret    
    panic("iget: no inodes");
801015df:	83 ec 0c             	sub    $0xc,%esp
801015e2:	68 28 72 10 80       	push   $0x80107228
801015e7:	e8 a4 ed ff ff       	call   80100390 <panic>
801015ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801015f0 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
801015f0:	55                   	push   %ebp
801015f1:	89 e5                	mov    %esp,%ebp
801015f3:	57                   	push   %edi
801015f4:	56                   	push   %esi
801015f5:	53                   	push   %ebx
801015f6:	89 c6                	mov    %eax,%esi
801015f8:	83 ec 1c             	sub    $0x1c,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
801015fb:	83 fa 0b             	cmp    $0xb,%edx
801015fe:	77 18                	ja     80101618 <bmap+0x28>
80101600:	8d 3c 90             	lea    (%eax,%edx,4),%edi
    if((addr = ip->addrs[bn]) == 0)
80101603:	8b 5f 5c             	mov    0x5c(%edi),%ebx
80101606:	85 db                	test   %ebx,%ebx
80101608:	74 76                	je     80101680 <bmap+0x90>
    brelse(bp);
    return addr;
  }

  panic("bmap: out of range");
}
8010160a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010160d:	89 d8                	mov    %ebx,%eax
8010160f:	5b                   	pop    %ebx
80101610:	5e                   	pop    %esi
80101611:	5f                   	pop    %edi
80101612:	5d                   	pop    %ebp
80101613:	c3                   	ret    
80101614:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  bn -= NDIRECT;
80101618:	8d 5a f4             	lea    -0xc(%edx),%ebx
  if(bn < NINDIRECT){
8010161b:	83 fb 7f             	cmp    $0x7f,%ebx
8010161e:	0f 87 90 00 00 00    	ja     801016b4 <bmap+0xc4>
    if((addr = ip->addrs[NDIRECT]) == 0)
80101624:	8b 90 8c 00 00 00    	mov    0x8c(%eax),%edx
8010162a:	8b 00                	mov    (%eax),%eax
8010162c:	85 d2                	test   %edx,%edx
8010162e:	74 70                	je     801016a0 <bmap+0xb0>
    bp = bread(ip->dev, addr);
80101630:	83 ec 08             	sub    $0x8,%esp
80101633:	52                   	push   %edx
80101634:	50                   	push   %eax
80101635:	e8 96 ea ff ff       	call   801000d0 <bread>
    if((addr = a[bn]) == 0){
8010163a:	8d 54 98 5c          	lea    0x5c(%eax,%ebx,4),%edx
8010163e:	83 c4 10             	add    $0x10,%esp
    bp = bread(ip->dev, addr);
80101641:	89 c7                	mov    %eax,%edi
    if((addr = a[bn]) == 0){
80101643:	8b 1a                	mov    (%edx),%ebx
80101645:	85 db                	test   %ebx,%ebx
80101647:	75 1d                	jne    80101666 <bmap+0x76>
      a[bn] = addr = balloc(ip->dev);
80101649:	8b 06                	mov    (%esi),%eax
8010164b:	89 55 e4             	mov    %edx,-0x1c(%ebp)
8010164e:	e8 bd fd ff ff       	call   80101410 <balloc>
80101653:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      log_write(bp);
80101656:	83 ec 0c             	sub    $0xc,%esp
      a[bn] = addr = balloc(ip->dev);
80101659:	89 c3                	mov    %eax,%ebx
8010165b:	89 02                	mov    %eax,(%edx)
      log_write(bp);
8010165d:	57                   	push   %edi
8010165e:	e8 9d 19 00 00       	call   80103000 <log_write>
80101663:	83 c4 10             	add    $0x10,%esp
    brelse(bp);
80101666:	83 ec 0c             	sub    $0xc,%esp
80101669:	57                   	push   %edi
8010166a:	e8 71 eb ff ff       	call   801001e0 <brelse>
8010166f:	83 c4 10             	add    $0x10,%esp
}
80101672:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101675:	89 d8                	mov    %ebx,%eax
80101677:	5b                   	pop    %ebx
80101678:	5e                   	pop    %esi
80101679:	5f                   	pop    %edi
8010167a:	5d                   	pop    %ebp
8010167b:	c3                   	ret    
8010167c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ip->addrs[bn] = addr = balloc(ip->dev);
80101680:	8b 00                	mov    (%eax),%eax
80101682:	e8 89 fd ff ff       	call   80101410 <balloc>
80101687:	89 47 5c             	mov    %eax,0x5c(%edi)
}
8010168a:	8d 65 f4             	lea    -0xc(%ebp),%esp
      ip->addrs[bn] = addr = balloc(ip->dev);
8010168d:	89 c3                	mov    %eax,%ebx
}
8010168f:	89 d8                	mov    %ebx,%eax
80101691:	5b                   	pop    %ebx
80101692:	5e                   	pop    %esi
80101693:	5f                   	pop    %edi
80101694:	5d                   	pop    %ebp
80101695:	c3                   	ret    
80101696:	8d 76 00             	lea    0x0(%esi),%esi
80101699:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
801016a0:	e8 6b fd ff ff       	call   80101410 <balloc>
801016a5:	89 c2                	mov    %eax,%edx
801016a7:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
801016ad:	8b 06                	mov    (%esi),%eax
801016af:	e9 7c ff ff ff       	jmp    80101630 <bmap+0x40>
  panic("bmap: out of range");
801016b4:	83 ec 0c             	sub    $0xc,%esp
801016b7:	68 38 72 10 80       	push   $0x80107238
801016bc:	e8 cf ec ff ff       	call   80100390 <panic>
801016c1:	eb 0d                	jmp    801016d0 <readsb>
801016c3:	90                   	nop
801016c4:	90                   	nop
801016c5:	90                   	nop
801016c6:	90                   	nop
801016c7:	90                   	nop
801016c8:	90                   	nop
801016c9:	90                   	nop
801016ca:	90                   	nop
801016cb:	90                   	nop
801016cc:	90                   	nop
801016cd:	90                   	nop
801016ce:	90                   	nop
801016cf:	90                   	nop

801016d0 <readsb>:
{
801016d0:	55                   	push   %ebp
801016d1:	89 e5                	mov    %esp,%ebp
801016d3:	56                   	push   %esi
801016d4:	53                   	push   %ebx
801016d5:	8b 75 0c             	mov    0xc(%ebp),%esi
  bp = bread(dev, 1);
801016d8:	83 ec 08             	sub    $0x8,%esp
801016db:	6a 01                	push   $0x1
801016dd:	ff 75 08             	pushl  0x8(%ebp)
801016e0:	e8 eb e9 ff ff       	call   801000d0 <bread>
801016e5:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
801016e7:	8d 40 5c             	lea    0x5c(%eax),%eax
801016ea:	83 c4 0c             	add    $0xc,%esp
801016ed:	6a 1c                	push   $0x1c
801016ef:	50                   	push   %eax
801016f0:	56                   	push   %esi
801016f1:	e8 9a 30 00 00       	call   80104790 <memmove>
  brelse(bp);
801016f6:	89 5d 08             	mov    %ebx,0x8(%ebp)
801016f9:	83 c4 10             	add    $0x10,%esp
}
801016fc:	8d 65 f8             	lea    -0x8(%ebp),%esp
801016ff:	5b                   	pop    %ebx
80101700:	5e                   	pop    %esi
80101701:	5d                   	pop    %ebp
  brelse(bp);
80101702:	e9 d9 ea ff ff       	jmp    801001e0 <brelse>
80101707:	89 f6                	mov    %esi,%esi
80101709:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101710 <iinit>:
{
80101710:	55                   	push   %ebp
80101711:	89 e5                	mov    %esp,%ebp
80101713:	53                   	push   %ebx
80101714:	bb 20 0a 11 80       	mov    $0x80110a20,%ebx
80101719:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
8010171c:	68 4b 72 10 80       	push   $0x8010724b
80101721:	68 e0 09 11 80       	push   $0x801109e0
80101726:	e8 65 2d 00 00       	call   80104490 <initlock>
8010172b:	83 c4 10             	add    $0x10,%esp
8010172e:	66 90                	xchg   %ax,%ax
    initsleeplock(&icache.inode[i].lock, "inode");
80101730:	83 ec 08             	sub    $0x8,%esp
80101733:	68 52 72 10 80       	push   $0x80107252
80101738:	53                   	push   %ebx
80101739:	81 c3 90 00 00 00    	add    $0x90,%ebx
8010173f:	e8 1c 2c 00 00       	call   80104360 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
80101744:	83 c4 10             	add    $0x10,%esp
80101747:	81 fb 40 26 11 80    	cmp    $0x80112640,%ebx
8010174d:	75 e1                	jne    80101730 <iinit+0x20>
  readsb(dev, &sb);
8010174f:	83 ec 08             	sub    $0x8,%esp
80101752:	68 c0 09 11 80       	push   $0x801109c0
80101757:	ff 75 08             	pushl  0x8(%ebp)
8010175a:	e8 71 ff ff ff       	call   801016d0 <readsb>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
8010175f:	ff 35 d8 09 11 80    	pushl  0x801109d8
80101765:	ff 35 d4 09 11 80    	pushl  0x801109d4
8010176b:	ff 35 d0 09 11 80    	pushl  0x801109d0
80101771:	ff 35 cc 09 11 80    	pushl  0x801109cc
80101777:	ff 35 c8 09 11 80    	pushl  0x801109c8
8010177d:	ff 35 c4 09 11 80    	pushl  0x801109c4
80101783:	ff 35 c0 09 11 80    	pushl  0x801109c0
80101789:	68 b8 72 10 80       	push   $0x801072b8
8010178e:	e8 dd ef ff ff       	call   80100770 <cprintf>
}
80101793:	83 c4 30             	add    $0x30,%esp
80101796:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101799:	c9                   	leave  
8010179a:	c3                   	ret    
8010179b:	90                   	nop
8010179c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801017a0 <ialloc>:
{
801017a0:	55                   	push   %ebp
801017a1:	89 e5                	mov    %esp,%ebp
801017a3:	57                   	push   %edi
801017a4:	56                   	push   %esi
801017a5:	53                   	push   %ebx
801017a6:	83 ec 1c             	sub    $0x1c,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
801017a9:	83 3d c8 09 11 80 01 	cmpl   $0x1,0x801109c8
{
801017b0:	8b 45 0c             	mov    0xc(%ebp),%eax
801017b3:	8b 75 08             	mov    0x8(%ebp),%esi
801017b6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
801017b9:	0f 86 91 00 00 00    	jbe    80101850 <ialloc+0xb0>
801017bf:	bb 01 00 00 00       	mov    $0x1,%ebx
801017c4:	eb 21                	jmp    801017e7 <ialloc+0x47>
801017c6:	8d 76 00             	lea    0x0(%esi),%esi
801017c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    brelse(bp);
801017d0:	83 ec 0c             	sub    $0xc,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
801017d3:	83 c3 01             	add    $0x1,%ebx
    brelse(bp);
801017d6:	57                   	push   %edi
801017d7:	e8 04 ea ff ff       	call   801001e0 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
801017dc:	83 c4 10             	add    $0x10,%esp
801017df:	39 1d c8 09 11 80    	cmp    %ebx,0x801109c8
801017e5:	76 69                	jbe    80101850 <ialloc+0xb0>
    bp = bread(dev, IBLOCK(inum, sb));
801017e7:	89 d8                	mov    %ebx,%eax
801017e9:	83 ec 08             	sub    $0x8,%esp
801017ec:	c1 e8 03             	shr    $0x3,%eax
801017ef:	03 05 d4 09 11 80    	add    0x801109d4,%eax
801017f5:	50                   	push   %eax
801017f6:	56                   	push   %esi
801017f7:	e8 d4 e8 ff ff       	call   801000d0 <bread>
801017fc:	89 c7                	mov    %eax,%edi
    dip = (struct dinode*)bp->data + inum%IPB;
801017fe:	89 d8                	mov    %ebx,%eax
    if(dip->type == 0){  // a free inode
80101800:	83 c4 10             	add    $0x10,%esp
    dip = (struct dinode*)bp->data + inum%IPB;
80101803:	83 e0 07             	and    $0x7,%eax
80101806:	c1 e0 06             	shl    $0x6,%eax
80101809:	8d 4c 07 5c          	lea    0x5c(%edi,%eax,1),%ecx
    if(dip->type == 0){  // a free inode
8010180d:	66 83 39 00          	cmpw   $0x0,(%ecx)
80101811:	75 bd                	jne    801017d0 <ialloc+0x30>
      memset(dip, 0, sizeof(*dip));
80101813:	83 ec 04             	sub    $0x4,%esp
80101816:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80101819:	6a 40                	push   $0x40
8010181b:	6a 00                	push   $0x0
8010181d:	51                   	push   %ecx
8010181e:	e8 bd 2e 00 00       	call   801046e0 <memset>
      dip->type = type;
80101823:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80101827:	8b 4d e0             	mov    -0x20(%ebp),%ecx
8010182a:	66 89 01             	mov    %ax,(%ecx)
      log_write(bp);   // mark it allocated on the disk
8010182d:	89 3c 24             	mov    %edi,(%esp)
80101830:	e8 cb 17 00 00       	call   80103000 <log_write>
      brelse(bp);
80101835:	89 3c 24             	mov    %edi,(%esp)
80101838:	e8 a3 e9 ff ff       	call   801001e0 <brelse>
      return iget(dev, inum);
8010183d:	83 c4 10             	add    $0x10,%esp
}
80101840:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return iget(dev, inum);
80101843:	89 da                	mov    %ebx,%edx
80101845:	89 f0                	mov    %esi,%eax
}
80101847:	5b                   	pop    %ebx
80101848:	5e                   	pop    %esi
80101849:	5f                   	pop    %edi
8010184a:	5d                   	pop    %ebp
      return iget(dev, inum);
8010184b:	e9 d0 fc ff ff       	jmp    80101520 <iget>
  panic("ialloc: no inodes");
80101850:	83 ec 0c             	sub    $0xc,%esp
80101853:	68 58 72 10 80       	push   $0x80107258
80101858:	e8 33 eb ff ff       	call   80100390 <panic>
8010185d:	8d 76 00             	lea    0x0(%esi),%esi

80101860 <iupdate>:
{
80101860:	55                   	push   %ebp
80101861:	89 e5                	mov    %esp,%ebp
80101863:	56                   	push   %esi
80101864:	53                   	push   %ebx
80101865:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101868:	83 ec 08             	sub    $0x8,%esp
8010186b:	8b 43 04             	mov    0x4(%ebx),%eax
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
8010186e:	83 c3 5c             	add    $0x5c,%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101871:	c1 e8 03             	shr    $0x3,%eax
80101874:	03 05 d4 09 11 80    	add    0x801109d4,%eax
8010187a:	50                   	push   %eax
8010187b:	ff 73 a4             	pushl  -0x5c(%ebx)
8010187e:	e8 4d e8 ff ff       	call   801000d0 <bread>
80101883:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
80101885:	8b 43 a8             	mov    -0x58(%ebx),%eax
  dip->type = ip->type;
80101888:	0f b7 53 f4          	movzwl -0xc(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
8010188c:	83 c4 0c             	add    $0xc,%esp
  dip = (struct dinode*)bp->data + ip->inum%IPB;
8010188f:	83 e0 07             	and    $0x7,%eax
80101892:	c1 e0 06             	shl    $0x6,%eax
80101895:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
80101899:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
8010189c:	0f b7 53 f6          	movzwl -0xa(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018a0:	83 c0 0c             	add    $0xc,%eax
  dip->major = ip->major;
801018a3:	66 89 50 f6          	mov    %dx,-0xa(%eax)
  dip->minor = ip->minor;
801018a7:	0f b7 53 f8          	movzwl -0x8(%ebx),%edx
801018ab:	66 89 50 f8          	mov    %dx,-0x8(%eax)
  dip->nlink = ip->nlink;
801018af:	0f b7 53 fa          	movzwl -0x6(%ebx),%edx
801018b3:	66 89 50 fa          	mov    %dx,-0x6(%eax)
  dip->size = ip->size;
801018b7:	8b 53 fc             	mov    -0x4(%ebx),%edx
801018ba:	89 50 fc             	mov    %edx,-0x4(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801018bd:	6a 34                	push   $0x34
801018bf:	53                   	push   %ebx
801018c0:	50                   	push   %eax
801018c1:	e8 ca 2e 00 00       	call   80104790 <memmove>
  log_write(bp);
801018c6:	89 34 24             	mov    %esi,(%esp)
801018c9:	e8 32 17 00 00       	call   80103000 <log_write>
  brelse(bp);
801018ce:	89 75 08             	mov    %esi,0x8(%ebp)
801018d1:	83 c4 10             	add    $0x10,%esp
}
801018d4:	8d 65 f8             	lea    -0x8(%ebp),%esp
801018d7:	5b                   	pop    %ebx
801018d8:	5e                   	pop    %esi
801018d9:	5d                   	pop    %ebp
  brelse(bp);
801018da:	e9 01 e9 ff ff       	jmp    801001e0 <brelse>
801018df:	90                   	nop

801018e0 <idup>:
{
801018e0:	55                   	push   %ebp
801018e1:	89 e5                	mov    %esp,%ebp
801018e3:	53                   	push   %ebx
801018e4:	83 ec 10             	sub    $0x10,%esp
801018e7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
801018ea:	68 e0 09 11 80       	push   $0x801109e0
801018ef:	e8 dc 2c 00 00       	call   801045d0 <acquire>
  ip->ref++;
801018f4:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
801018f8:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
801018ff:	e8 8c 2d 00 00       	call   80104690 <release>
}
80101904:	89 d8                	mov    %ebx,%eax
80101906:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101909:	c9                   	leave  
8010190a:	c3                   	ret    
8010190b:	90                   	nop
8010190c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101910 <ilock>:
{
80101910:	55                   	push   %ebp
80101911:	89 e5                	mov    %esp,%ebp
80101913:	56                   	push   %esi
80101914:	53                   	push   %ebx
80101915:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
80101918:	85 db                	test   %ebx,%ebx
8010191a:	0f 84 b7 00 00 00    	je     801019d7 <ilock+0xc7>
80101920:	8b 53 08             	mov    0x8(%ebx),%edx
80101923:	85 d2                	test   %edx,%edx
80101925:	0f 8e ac 00 00 00    	jle    801019d7 <ilock+0xc7>
  acquiresleep(&ip->lock);
8010192b:	8d 43 0c             	lea    0xc(%ebx),%eax
8010192e:	83 ec 0c             	sub    $0xc,%esp
80101931:	50                   	push   %eax
80101932:	e8 69 2a 00 00       	call   801043a0 <acquiresleep>
  if(ip->valid == 0){
80101937:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010193a:	83 c4 10             	add    $0x10,%esp
8010193d:	85 c0                	test   %eax,%eax
8010193f:	74 0f                	je     80101950 <ilock+0x40>
}
80101941:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101944:	5b                   	pop    %ebx
80101945:	5e                   	pop    %esi
80101946:	5d                   	pop    %ebp
80101947:	c3                   	ret    
80101948:	90                   	nop
80101949:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101950:	8b 43 04             	mov    0x4(%ebx),%eax
80101953:	83 ec 08             	sub    $0x8,%esp
80101956:	c1 e8 03             	shr    $0x3,%eax
80101959:	03 05 d4 09 11 80    	add    0x801109d4,%eax
8010195f:	50                   	push   %eax
80101960:	ff 33                	pushl  (%ebx)
80101962:	e8 69 e7 ff ff       	call   801000d0 <bread>
80101967:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
80101969:	8b 43 04             	mov    0x4(%ebx),%eax
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
8010196c:	83 c4 0c             	add    $0xc,%esp
    dip = (struct dinode*)bp->data + ip->inum%IPB;
8010196f:	83 e0 07             	and    $0x7,%eax
80101972:	c1 e0 06             	shl    $0x6,%eax
80101975:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
80101979:	0f b7 10             	movzwl (%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
8010197c:	83 c0 0c             	add    $0xc,%eax
    ip->type = dip->type;
8010197f:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
80101983:	0f b7 50 f6          	movzwl -0xa(%eax),%edx
80101987:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
8010198b:	0f b7 50 f8          	movzwl -0x8(%eax),%edx
8010198f:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
80101993:	0f b7 50 fa          	movzwl -0x6(%eax),%edx
80101997:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
8010199b:	8b 50 fc             	mov    -0x4(%eax),%edx
8010199e:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801019a1:	6a 34                	push   $0x34
801019a3:	50                   	push   %eax
801019a4:	8d 43 5c             	lea    0x5c(%ebx),%eax
801019a7:	50                   	push   %eax
801019a8:	e8 e3 2d 00 00       	call   80104790 <memmove>
    brelse(bp);
801019ad:	89 34 24             	mov    %esi,(%esp)
801019b0:	e8 2b e8 ff ff       	call   801001e0 <brelse>
    if(ip->type == 0)
801019b5:	83 c4 10             	add    $0x10,%esp
801019b8:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
    ip->valid = 1;
801019bd:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
801019c4:	0f 85 77 ff ff ff    	jne    80101941 <ilock+0x31>
      panic("ilock: no type");
801019ca:	83 ec 0c             	sub    $0xc,%esp
801019cd:	68 70 72 10 80       	push   $0x80107270
801019d2:	e8 b9 e9 ff ff       	call   80100390 <panic>
    panic("ilock");
801019d7:	83 ec 0c             	sub    $0xc,%esp
801019da:	68 6a 72 10 80       	push   $0x8010726a
801019df:	e8 ac e9 ff ff       	call   80100390 <panic>
801019e4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801019ea:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

801019f0 <iunlock>:
{
801019f0:	55                   	push   %ebp
801019f1:	89 e5                	mov    %esp,%ebp
801019f3:	56                   	push   %esi
801019f4:	53                   	push   %ebx
801019f5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
801019f8:	85 db                	test   %ebx,%ebx
801019fa:	74 28                	je     80101a24 <iunlock+0x34>
801019fc:	8d 73 0c             	lea    0xc(%ebx),%esi
801019ff:	83 ec 0c             	sub    $0xc,%esp
80101a02:	56                   	push   %esi
80101a03:	e8 38 2a 00 00       	call   80104440 <holdingsleep>
80101a08:	83 c4 10             	add    $0x10,%esp
80101a0b:	85 c0                	test   %eax,%eax
80101a0d:	74 15                	je     80101a24 <iunlock+0x34>
80101a0f:	8b 43 08             	mov    0x8(%ebx),%eax
80101a12:	85 c0                	test   %eax,%eax
80101a14:	7e 0e                	jle    80101a24 <iunlock+0x34>
  releasesleep(&ip->lock);
80101a16:	89 75 08             	mov    %esi,0x8(%ebp)
}
80101a19:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101a1c:	5b                   	pop    %ebx
80101a1d:	5e                   	pop    %esi
80101a1e:	5d                   	pop    %ebp
  releasesleep(&ip->lock);
80101a1f:	e9 dc 29 00 00       	jmp    80104400 <releasesleep>
    panic("iunlock");
80101a24:	83 ec 0c             	sub    $0xc,%esp
80101a27:	68 7f 72 10 80       	push   $0x8010727f
80101a2c:	e8 5f e9 ff ff       	call   80100390 <panic>
80101a31:	eb 0d                	jmp    80101a40 <iput>
80101a33:	90                   	nop
80101a34:	90                   	nop
80101a35:	90                   	nop
80101a36:	90                   	nop
80101a37:	90                   	nop
80101a38:	90                   	nop
80101a39:	90                   	nop
80101a3a:	90                   	nop
80101a3b:	90                   	nop
80101a3c:	90                   	nop
80101a3d:	90                   	nop
80101a3e:	90                   	nop
80101a3f:	90                   	nop

80101a40 <iput>:
{
80101a40:	55                   	push   %ebp
80101a41:	89 e5                	mov    %esp,%ebp
80101a43:	57                   	push   %edi
80101a44:	56                   	push   %esi
80101a45:	53                   	push   %ebx
80101a46:	83 ec 28             	sub    $0x28,%esp
80101a49:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
80101a4c:	8d 7b 0c             	lea    0xc(%ebx),%edi
80101a4f:	57                   	push   %edi
80101a50:	e8 4b 29 00 00       	call   801043a0 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
80101a55:	8b 53 4c             	mov    0x4c(%ebx),%edx
80101a58:	83 c4 10             	add    $0x10,%esp
80101a5b:	85 d2                	test   %edx,%edx
80101a5d:	74 07                	je     80101a66 <iput+0x26>
80101a5f:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80101a64:	74 32                	je     80101a98 <iput+0x58>
  releasesleep(&ip->lock);
80101a66:	83 ec 0c             	sub    $0xc,%esp
80101a69:	57                   	push   %edi
80101a6a:	e8 91 29 00 00       	call   80104400 <releasesleep>
  acquire(&icache.lock);
80101a6f:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101a76:	e8 55 2b 00 00       	call   801045d0 <acquire>
  ip->ref--;
80101a7b:	83 6b 08 01          	subl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101a7f:	83 c4 10             	add    $0x10,%esp
80101a82:	c7 45 08 e0 09 11 80 	movl   $0x801109e0,0x8(%ebp)
}
80101a89:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101a8c:	5b                   	pop    %ebx
80101a8d:	5e                   	pop    %esi
80101a8e:	5f                   	pop    %edi
80101a8f:	5d                   	pop    %ebp
  release(&icache.lock);
80101a90:	e9 fb 2b 00 00       	jmp    80104690 <release>
80101a95:	8d 76 00             	lea    0x0(%esi),%esi
    acquire(&icache.lock);
80101a98:	83 ec 0c             	sub    $0xc,%esp
80101a9b:	68 e0 09 11 80       	push   $0x801109e0
80101aa0:	e8 2b 2b 00 00       	call   801045d0 <acquire>
    int r = ip->ref;
80101aa5:	8b 73 08             	mov    0x8(%ebx),%esi
    release(&icache.lock);
80101aa8:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101aaf:	e8 dc 2b 00 00       	call   80104690 <release>
    if(r == 1){
80101ab4:	83 c4 10             	add    $0x10,%esp
80101ab7:	83 fe 01             	cmp    $0x1,%esi
80101aba:	75 aa                	jne    80101a66 <iput+0x26>
80101abc:	8d 8b 8c 00 00 00    	lea    0x8c(%ebx),%ecx
80101ac2:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80101ac5:	8d 73 5c             	lea    0x5c(%ebx),%esi
80101ac8:	89 cf                	mov    %ecx,%edi
80101aca:	eb 0b                	jmp    80101ad7 <iput+0x97>
80101acc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101ad0:	83 c6 04             	add    $0x4,%esi
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101ad3:	39 fe                	cmp    %edi,%esi
80101ad5:	74 19                	je     80101af0 <iput+0xb0>
    if(ip->addrs[i]){
80101ad7:	8b 16                	mov    (%esi),%edx
80101ad9:	85 d2                	test   %edx,%edx
80101adb:	74 f3                	je     80101ad0 <iput+0x90>
      bfree(ip->dev, ip->addrs[i]);
80101add:	8b 03                	mov    (%ebx),%eax
80101adf:	e8 bc f8 ff ff       	call   801013a0 <bfree>
      ip->addrs[i] = 0;
80101ae4:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80101aea:	eb e4                	jmp    80101ad0 <iput+0x90>
80101aec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    }
  }

  if(ip->addrs[NDIRECT]){
80101af0:	8b 83 8c 00 00 00    	mov    0x8c(%ebx),%eax
80101af6:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101af9:	85 c0                	test   %eax,%eax
80101afb:	75 33                	jne    80101b30 <iput+0xf0>
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  iupdate(ip);
80101afd:	83 ec 0c             	sub    $0xc,%esp
  ip->size = 0;
80101b00:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  iupdate(ip);
80101b07:	53                   	push   %ebx
80101b08:	e8 53 fd ff ff       	call   80101860 <iupdate>
      ip->type = 0;
80101b0d:	31 c0                	xor    %eax,%eax
80101b0f:	66 89 43 50          	mov    %ax,0x50(%ebx)
      iupdate(ip);
80101b13:	89 1c 24             	mov    %ebx,(%esp)
80101b16:	e8 45 fd ff ff       	call   80101860 <iupdate>
      ip->valid = 0;
80101b1b:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
80101b22:	83 c4 10             	add    $0x10,%esp
80101b25:	e9 3c ff ff ff       	jmp    80101a66 <iput+0x26>
80101b2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80101b30:	83 ec 08             	sub    $0x8,%esp
80101b33:	50                   	push   %eax
80101b34:	ff 33                	pushl  (%ebx)
80101b36:	e8 95 e5 ff ff       	call   801000d0 <bread>
80101b3b:	8d 88 5c 02 00 00    	lea    0x25c(%eax),%ecx
80101b41:	89 7d e0             	mov    %edi,-0x20(%ebp)
80101b44:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    a = (uint*)bp->data;
80101b47:	8d 70 5c             	lea    0x5c(%eax),%esi
80101b4a:	83 c4 10             	add    $0x10,%esp
80101b4d:	89 cf                	mov    %ecx,%edi
80101b4f:	eb 0e                	jmp    80101b5f <iput+0x11f>
80101b51:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101b58:	83 c6 04             	add    $0x4,%esi
    for(j = 0; j < NINDIRECT; j++){
80101b5b:	39 fe                	cmp    %edi,%esi
80101b5d:	74 0f                	je     80101b6e <iput+0x12e>
      if(a[j])
80101b5f:	8b 16                	mov    (%esi),%edx
80101b61:	85 d2                	test   %edx,%edx
80101b63:	74 f3                	je     80101b58 <iput+0x118>
        bfree(ip->dev, a[j]);
80101b65:	8b 03                	mov    (%ebx),%eax
80101b67:	e8 34 f8 ff ff       	call   801013a0 <bfree>
80101b6c:	eb ea                	jmp    80101b58 <iput+0x118>
    brelse(bp);
80101b6e:	83 ec 0c             	sub    $0xc,%esp
80101b71:	ff 75 e4             	pushl  -0x1c(%ebp)
80101b74:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101b77:	e8 64 e6 ff ff       	call   801001e0 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
80101b7c:	8b 93 8c 00 00 00    	mov    0x8c(%ebx),%edx
80101b82:	8b 03                	mov    (%ebx),%eax
80101b84:	e8 17 f8 ff ff       	call   801013a0 <bfree>
    ip->addrs[NDIRECT] = 0;
80101b89:	c7 83 8c 00 00 00 00 	movl   $0x0,0x8c(%ebx)
80101b90:	00 00 00 
80101b93:	83 c4 10             	add    $0x10,%esp
80101b96:	e9 62 ff ff ff       	jmp    80101afd <iput+0xbd>
80101b9b:	90                   	nop
80101b9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101ba0 <iunlockput>:
{
80101ba0:	55                   	push   %ebp
80101ba1:	89 e5                	mov    %esp,%ebp
80101ba3:	53                   	push   %ebx
80101ba4:	83 ec 10             	sub    $0x10,%esp
80101ba7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
80101baa:	53                   	push   %ebx
80101bab:	e8 40 fe ff ff       	call   801019f0 <iunlock>
  iput(ip);
80101bb0:	89 5d 08             	mov    %ebx,0x8(%ebp)
80101bb3:	83 c4 10             	add    $0x10,%esp
}
80101bb6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101bb9:	c9                   	leave  
  iput(ip);
80101bba:	e9 81 fe ff ff       	jmp    80101a40 <iput>
80101bbf:	90                   	nop

80101bc0 <stati>:

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
80101bc0:	55                   	push   %ebp
80101bc1:	89 e5                	mov    %esp,%ebp
80101bc3:	8b 55 08             	mov    0x8(%ebp),%edx
80101bc6:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
80101bc9:	8b 0a                	mov    (%edx),%ecx
80101bcb:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
80101bce:	8b 4a 04             	mov    0x4(%edx),%ecx
80101bd1:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
80101bd4:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
80101bd8:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
80101bdb:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
80101bdf:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80101be3:	8b 52 58             	mov    0x58(%edx),%edx
80101be6:	89 50 10             	mov    %edx,0x10(%eax)
}
80101be9:	5d                   	pop    %ebp
80101bea:	c3                   	ret    
80101beb:	90                   	nop
80101bec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101bf0 <readi>:
//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101bf0:	55                   	push   %ebp
80101bf1:	89 e5                	mov    %esp,%ebp
80101bf3:	57                   	push   %edi
80101bf4:	56                   	push   %esi
80101bf5:	53                   	push   %ebx
80101bf6:	83 ec 1c             	sub    $0x1c,%esp
80101bf9:	8b 45 08             	mov    0x8(%ebp),%eax
80101bfc:	8b 75 0c             	mov    0xc(%ebp),%esi
80101bff:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101c02:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101c07:	89 75 e0             	mov    %esi,-0x20(%ebp)
80101c0a:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101c0d:	8b 75 10             	mov    0x10(%ebp),%esi
80101c10:	89 7d e4             	mov    %edi,-0x1c(%ebp)
  if(ip->type == T_DEV){
80101c13:	0f 84 a7 00 00 00    	je     80101cc0 <readi+0xd0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
80101c19:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101c1c:	8b 40 58             	mov    0x58(%eax),%eax
80101c1f:	39 c6                	cmp    %eax,%esi
80101c21:	0f 87 ba 00 00 00    	ja     80101ce1 <readi+0xf1>
80101c27:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101c2a:	89 f9                	mov    %edi,%ecx
80101c2c:	01 f1                	add    %esi,%ecx
80101c2e:	0f 82 ad 00 00 00    	jb     80101ce1 <readi+0xf1>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
80101c34:	89 c2                	mov    %eax,%edx
80101c36:	29 f2                	sub    %esi,%edx
80101c38:	39 c8                	cmp    %ecx,%eax
80101c3a:	0f 43 d7             	cmovae %edi,%edx

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101c3d:	31 ff                	xor    %edi,%edi
80101c3f:	85 d2                	test   %edx,%edx
    n = ip->size - off;
80101c41:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101c44:	74 6c                	je     80101cb2 <readi+0xc2>
80101c46:	8d 76 00             	lea    0x0(%esi),%esi
80101c49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101c50:	8b 5d d8             	mov    -0x28(%ebp),%ebx
80101c53:	89 f2                	mov    %esi,%edx
80101c55:	c1 ea 09             	shr    $0x9,%edx
80101c58:	89 d8                	mov    %ebx,%eax
80101c5a:	e8 91 f9 ff ff       	call   801015f0 <bmap>
80101c5f:	83 ec 08             	sub    $0x8,%esp
80101c62:	50                   	push   %eax
80101c63:	ff 33                	pushl  (%ebx)
80101c65:	e8 66 e4 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101c6a:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101c6d:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
80101c6f:	89 f0                	mov    %esi,%eax
80101c71:	25 ff 01 00 00       	and    $0x1ff,%eax
80101c76:	b9 00 02 00 00       	mov    $0x200,%ecx
80101c7b:	83 c4 0c             	add    $0xc,%esp
80101c7e:	29 c1                	sub    %eax,%ecx
    memmove(dst, bp->data + off%BSIZE, m);
80101c80:	8d 44 02 5c          	lea    0x5c(%edx,%eax,1),%eax
80101c84:	89 55 dc             	mov    %edx,-0x24(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
80101c87:	29 fb                	sub    %edi,%ebx
80101c89:	39 d9                	cmp    %ebx,%ecx
80101c8b:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80101c8e:	53                   	push   %ebx
80101c8f:	50                   	push   %eax
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101c90:	01 df                	add    %ebx,%edi
    memmove(dst, bp->data + off%BSIZE, m);
80101c92:	ff 75 e0             	pushl  -0x20(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101c95:	01 de                	add    %ebx,%esi
    memmove(dst, bp->data + off%BSIZE, m);
80101c97:	e8 f4 2a 00 00       	call   80104790 <memmove>
    brelse(bp);
80101c9c:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101c9f:	89 14 24             	mov    %edx,(%esp)
80101ca2:	e8 39 e5 ff ff       	call   801001e0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101ca7:	01 5d e0             	add    %ebx,-0x20(%ebp)
80101caa:	83 c4 10             	add    $0x10,%esp
80101cad:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101cb0:	77 9e                	ja     80101c50 <readi+0x60>
  }
  return n;
80101cb2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
}
80101cb5:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101cb8:	5b                   	pop    %ebx
80101cb9:	5e                   	pop    %esi
80101cba:	5f                   	pop    %edi
80101cbb:	5d                   	pop    %ebp
80101cbc:	c3                   	ret    
80101cbd:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101cc0:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101cc4:	66 83 f8 09          	cmp    $0x9,%ax
80101cc8:	77 17                	ja     80101ce1 <readi+0xf1>
80101cca:	8b 04 c5 60 09 11 80 	mov    -0x7feef6a0(,%eax,8),%eax
80101cd1:	85 c0                	test   %eax,%eax
80101cd3:	74 0c                	je     80101ce1 <readi+0xf1>
    return devsw[ip->major].read(ip, dst, n);
80101cd5:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101cd8:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101cdb:	5b                   	pop    %ebx
80101cdc:	5e                   	pop    %esi
80101cdd:	5f                   	pop    %edi
80101cde:	5d                   	pop    %ebp
    return devsw[ip->major].read(ip, dst, n);
80101cdf:	ff e0                	jmp    *%eax
      return -1;
80101ce1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101ce6:	eb cd                	jmp    80101cb5 <readi+0xc5>
80101ce8:	90                   	nop
80101ce9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101cf0 <writei>:
// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80101cf0:	55                   	push   %ebp
80101cf1:	89 e5                	mov    %esp,%ebp
80101cf3:	57                   	push   %edi
80101cf4:	56                   	push   %esi
80101cf5:	53                   	push   %ebx
80101cf6:	83 ec 1c             	sub    $0x1c,%esp
80101cf9:	8b 45 08             	mov    0x8(%ebp),%eax
80101cfc:	8b 75 0c             	mov    0xc(%ebp),%esi
80101cff:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101d02:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101d07:	89 75 dc             	mov    %esi,-0x24(%ebp)
80101d0a:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101d0d:	8b 75 10             	mov    0x10(%ebp),%esi
80101d10:	89 7d e0             	mov    %edi,-0x20(%ebp)
  if(ip->type == T_DEV){
80101d13:	0f 84 b7 00 00 00    	je     80101dd0 <writei+0xe0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
80101d19:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101d1c:	39 70 58             	cmp    %esi,0x58(%eax)
80101d1f:	0f 82 eb 00 00 00    	jb     80101e10 <writei+0x120>
80101d25:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101d28:	31 d2                	xor    %edx,%edx
80101d2a:	89 f8                	mov    %edi,%eax
80101d2c:	01 f0                	add    %esi,%eax
80101d2e:	0f 92 c2             	setb   %dl
    return -1;
  if(off + n > MAXFILE*BSIZE)
80101d31:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101d36:	0f 87 d4 00 00 00    	ja     80101e10 <writei+0x120>
80101d3c:	85 d2                	test   %edx,%edx
80101d3e:	0f 85 cc 00 00 00    	jne    80101e10 <writei+0x120>
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101d44:	85 ff                	test   %edi,%edi
80101d46:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80101d4d:	74 72                	je     80101dc1 <writei+0xd1>
80101d4f:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101d50:	8b 7d d8             	mov    -0x28(%ebp),%edi
80101d53:	89 f2                	mov    %esi,%edx
80101d55:	c1 ea 09             	shr    $0x9,%edx
80101d58:	89 f8                	mov    %edi,%eax
80101d5a:	e8 91 f8 ff ff       	call   801015f0 <bmap>
80101d5f:	83 ec 08             	sub    $0x8,%esp
80101d62:	50                   	push   %eax
80101d63:	ff 37                	pushl  (%edi)
80101d65:	e8 66 e3 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101d6a:	8b 5d e0             	mov    -0x20(%ebp),%ebx
80101d6d:	2b 5d e4             	sub    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101d70:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
80101d72:	89 f0                	mov    %esi,%eax
80101d74:	b9 00 02 00 00       	mov    $0x200,%ecx
80101d79:	83 c4 0c             	add    $0xc,%esp
80101d7c:	25 ff 01 00 00       	and    $0x1ff,%eax
80101d81:	29 c1                	sub    %eax,%ecx
    memmove(bp->data + off%BSIZE, src, m);
80101d83:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101d87:	39 d9                	cmp    %ebx,%ecx
80101d89:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
80101d8c:	53                   	push   %ebx
80101d8d:	ff 75 dc             	pushl  -0x24(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101d90:	01 de                	add    %ebx,%esi
    memmove(bp->data + off%BSIZE, src, m);
80101d92:	50                   	push   %eax
80101d93:	e8 f8 29 00 00       	call   80104790 <memmove>
    log_write(bp);
80101d98:	89 3c 24             	mov    %edi,(%esp)
80101d9b:	e8 60 12 00 00       	call   80103000 <log_write>
    brelse(bp);
80101da0:	89 3c 24             	mov    %edi,(%esp)
80101da3:	e8 38 e4 ff ff       	call   801001e0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101da8:	01 5d e4             	add    %ebx,-0x1c(%ebp)
80101dab:	01 5d dc             	add    %ebx,-0x24(%ebp)
80101dae:	83 c4 10             	add    $0x10,%esp
80101db1:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101db4:	39 45 e0             	cmp    %eax,-0x20(%ebp)
80101db7:	77 97                	ja     80101d50 <writei+0x60>
  }

  if(n > 0 && off > ip->size){
80101db9:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101dbc:	3b 70 58             	cmp    0x58(%eax),%esi
80101dbf:	77 37                	ja     80101df8 <writei+0x108>
    ip->size = off;
    iupdate(ip);
  }
  return n;
80101dc1:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80101dc4:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101dc7:	5b                   	pop    %ebx
80101dc8:	5e                   	pop    %esi
80101dc9:	5f                   	pop    %edi
80101dca:	5d                   	pop    %ebp
80101dcb:	c3                   	ret    
80101dcc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80101dd0:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101dd4:	66 83 f8 09          	cmp    $0x9,%ax
80101dd8:	77 36                	ja     80101e10 <writei+0x120>
80101dda:	8b 04 c5 64 09 11 80 	mov    -0x7feef69c(,%eax,8),%eax
80101de1:	85 c0                	test   %eax,%eax
80101de3:	74 2b                	je     80101e10 <writei+0x120>
    return devsw[ip->major].write(ip, src, n);
80101de5:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101de8:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101deb:	5b                   	pop    %ebx
80101dec:	5e                   	pop    %esi
80101ded:	5f                   	pop    %edi
80101dee:	5d                   	pop    %ebp
    return devsw[ip->major].write(ip, src, n);
80101def:	ff e0                	jmp    *%eax
80101df1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    ip->size = off;
80101df8:	8b 45 d8             	mov    -0x28(%ebp),%eax
    iupdate(ip);
80101dfb:	83 ec 0c             	sub    $0xc,%esp
    ip->size = off;
80101dfe:	89 70 58             	mov    %esi,0x58(%eax)
    iupdate(ip);
80101e01:	50                   	push   %eax
80101e02:	e8 59 fa ff ff       	call   80101860 <iupdate>
80101e07:	83 c4 10             	add    $0x10,%esp
80101e0a:	eb b5                	jmp    80101dc1 <writei+0xd1>
80101e0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return -1;
80101e10:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101e15:	eb ad                	jmp    80101dc4 <writei+0xd4>
80101e17:	89 f6                	mov    %esi,%esi
80101e19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101e20 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80101e20:	55                   	push   %ebp
80101e21:	89 e5                	mov    %esp,%ebp
80101e23:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
80101e26:	6a 0e                	push   $0xe
80101e28:	ff 75 0c             	pushl  0xc(%ebp)
80101e2b:	ff 75 08             	pushl  0x8(%ebp)
80101e2e:	e8 cd 29 00 00       	call   80104800 <strncmp>
}
80101e33:	c9                   	leave  
80101e34:	c3                   	ret    
80101e35:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101e39:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80101e40 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80101e40:	55                   	push   %ebp
80101e41:	89 e5                	mov    %esp,%ebp
80101e43:	57                   	push   %edi
80101e44:	56                   	push   %esi
80101e45:	53                   	push   %ebx
80101e46:	83 ec 1c             	sub    $0x1c,%esp
80101e49:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80101e4c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80101e51:	0f 85 85 00 00 00    	jne    80101edc <dirlookup+0x9c>
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80101e57:	8b 53 58             	mov    0x58(%ebx),%edx
80101e5a:	31 ff                	xor    %edi,%edi
80101e5c:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101e5f:	85 d2                	test   %edx,%edx
80101e61:	74 3e                	je     80101ea1 <dirlookup+0x61>
80101e63:	90                   	nop
80101e64:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101e68:	6a 10                	push   $0x10
80101e6a:	57                   	push   %edi
80101e6b:	56                   	push   %esi
80101e6c:	53                   	push   %ebx
80101e6d:	e8 7e fd ff ff       	call   80101bf0 <readi>
80101e72:	83 c4 10             	add    $0x10,%esp
80101e75:	83 f8 10             	cmp    $0x10,%eax
80101e78:	75 55                	jne    80101ecf <dirlookup+0x8f>
      panic("dirlookup read");
    if(de.inum == 0)
80101e7a:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101e7f:	74 18                	je     80101e99 <dirlookup+0x59>
  return strncmp(s, t, DIRSIZ);
80101e81:	8d 45 da             	lea    -0x26(%ebp),%eax
80101e84:	83 ec 04             	sub    $0x4,%esp
80101e87:	6a 0e                	push   $0xe
80101e89:	50                   	push   %eax
80101e8a:	ff 75 0c             	pushl  0xc(%ebp)
80101e8d:	e8 6e 29 00 00       	call   80104800 <strncmp>
      continue;
    if(namecmp(name, de.name) == 0){
80101e92:	83 c4 10             	add    $0x10,%esp
80101e95:	85 c0                	test   %eax,%eax
80101e97:	74 17                	je     80101eb0 <dirlookup+0x70>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101e99:	83 c7 10             	add    $0x10,%edi
80101e9c:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101e9f:	72 c7                	jb     80101e68 <dirlookup+0x28>
      return iget(dp->dev, inum);
    }
  }

  return 0;
}
80101ea1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80101ea4:	31 c0                	xor    %eax,%eax
}
80101ea6:	5b                   	pop    %ebx
80101ea7:	5e                   	pop    %esi
80101ea8:	5f                   	pop    %edi
80101ea9:	5d                   	pop    %ebp
80101eaa:	c3                   	ret    
80101eab:	90                   	nop
80101eac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      if(poff)
80101eb0:	8b 45 10             	mov    0x10(%ebp),%eax
80101eb3:	85 c0                	test   %eax,%eax
80101eb5:	74 05                	je     80101ebc <dirlookup+0x7c>
        *poff = off;
80101eb7:	8b 45 10             	mov    0x10(%ebp),%eax
80101eba:	89 38                	mov    %edi,(%eax)
      inum = de.inum;
80101ebc:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80101ec0:	8b 03                	mov    (%ebx),%eax
80101ec2:	e8 59 f6 ff ff       	call   80101520 <iget>
}
80101ec7:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101eca:	5b                   	pop    %ebx
80101ecb:	5e                   	pop    %esi
80101ecc:	5f                   	pop    %edi
80101ecd:	5d                   	pop    %ebp
80101ece:	c3                   	ret    
      panic("dirlookup read");
80101ecf:	83 ec 0c             	sub    $0xc,%esp
80101ed2:	68 99 72 10 80       	push   $0x80107299
80101ed7:	e8 b4 e4 ff ff       	call   80100390 <panic>
    panic("dirlookup not DIR");
80101edc:	83 ec 0c             	sub    $0xc,%esp
80101edf:	68 87 72 10 80       	push   $0x80107287
80101ee4:	e8 a7 e4 ff ff       	call   80100390 <panic>
80101ee9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101ef0 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80101ef0:	55                   	push   %ebp
80101ef1:	89 e5                	mov    %esp,%ebp
80101ef3:	57                   	push   %edi
80101ef4:	56                   	push   %esi
80101ef5:	53                   	push   %ebx
80101ef6:	89 cf                	mov    %ecx,%edi
80101ef8:	89 c3                	mov    %eax,%ebx
80101efa:	83 ec 1c             	sub    $0x1c,%esp
  struct inode *ip, *next;

  if(*path == '/')
80101efd:	80 38 2f             	cmpb   $0x2f,(%eax)
{
80101f00:	89 55 e0             	mov    %edx,-0x20(%ebp)
  if(*path == '/')
80101f03:	0f 84 67 01 00 00    	je     80102070 <namex+0x180>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
80101f09:	e8 62 1b 00 00       	call   80103a70 <myproc>
  acquire(&icache.lock);
80101f0e:	83 ec 0c             	sub    $0xc,%esp
    ip = idup(myproc()->cwd);
80101f11:	8b 70 68             	mov    0x68(%eax),%esi
  acquire(&icache.lock);
80101f14:	68 e0 09 11 80       	push   $0x801109e0
80101f19:	e8 b2 26 00 00       	call   801045d0 <acquire>
  ip->ref++;
80101f1e:	83 46 08 01          	addl   $0x1,0x8(%esi)
  release(&icache.lock);
80101f22:	c7 04 24 e0 09 11 80 	movl   $0x801109e0,(%esp)
80101f29:	e8 62 27 00 00       	call   80104690 <release>
80101f2e:	83 c4 10             	add    $0x10,%esp
80101f31:	eb 08                	jmp    80101f3b <namex+0x4b>
80101f33:	90                   	nop
80101f34:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    path++;
80101f38:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80101f3b:	0f b6 03             	movzbl (%ebx),%eax
80101f3e:	3c 2f                	cmp    $0x2f,%al
80101f40:	74 f6                	je     80101f38 <namex+0x48>
  if(*path == 0)
80101f42:	84 c0                	test   %al,%al
80101f44:	0f 84 ee 00 00 00    	je     80102038 <namex+0x148>
  while(*path != '/' && *path != 0)
80101f4a:	0f b6 03             	movzbl (%ebx),%eax
80101f4d:	3c 2f                	cmp    $0x2f,%al
80101f4f:	0f 84 b3 00 00 00    	je     80102008 <namex+0x118>
80101f55:	84 c0                	test   %al,%al
80101f57:	89 da                	mov    %ebx,%edx
80101f59:	75 09                	jne    80101f64 <namex+0x74>
80101f5b:	e9 a8 00 00 00       	jmp    80102008 <namex+0x118>
80101f60:	84 c0                	test   %al,%al
80101f62:	74 0a                	je     80101f6e <namex+0x7e>
    path++;
80101f64:	83 c2 01             	add    $0x1,%edx
  while(*path != '/' && *path != 0)
80101f67:	0f b6 02             	movzbl (%edx),%eax
80101f6a:	3c 2f                	cmp    $0x2f,%al
80101f6c:	75 f2                	jne    80101f60 <namex+0x70>
80101f6e:	89 d1                	mov    %edx,%ecx
80101f70:	29 d9                	sub    %ebx,%ecx
  if(len >= DIRSIZ)
80101f72:	83 f9 0d             	cmp    $0xd,%ecx
80101f75:	0f 8e 91 00 00 00    	jle    8010200c <namex+0x11c>
    memmove(name, s, DIRSIZ);
80101f7b:	83 ec 04             	sub    $0x4,%esp
80101f7e:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80101f81:	6a 0e                	push   $0xe
80101f83:	53                   	push   %ebx
80101f84:	57                   	push   %edi
80101f85:	e8 06 28 00 00       	call   80104790 <memmove>
    path++;
80101f8a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
    memmove(name, s, DIRSIZ);
80101f8d:	83 c4 10             	add    $0x10,%esp
    path++;
80101f90:	89 d3                	mov    %edx,%ebx
  while(*path == '/')
80101f92:	80 3a 2f             	cmpb   $0x2f,(%edx)
80101f95:	75 11                	jne    80101fa8 <namex+0xb8>
80101f97:	89 f6                	mov    %esi,%esi
80101f99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    path++;
80101fa0:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80101fa3:	80 3b 2f             	cmpb   $0x2f,(%ebx)
80101fa6:	74 f8                	je     80101fa0 <namex+0xb0>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
80101fa8:	83 ec 0c             	sub    $0xc,%esp
80101fab:	56                   	push   %esi
80101fac:	e8 5f f9 ff ff       	call   80101910 <ilock>
    if(ip->type != T_DIR){
80101fb1:	83 c4 10             	add    $0x10,%esp
80101fb4:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80101fb9:	0f 85 91 00 00 00    	jne    80102050 <namex+0x160>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
80101fbf:	8b 55 e0             	mov    -0x20(%ebp),%edx
80101fc2:	85 d2                	test   %edx,%edx
80101fc4:	74 09                	je     80101fcf <namex+0xdf>
80101fc6:	80 3b 00             	cmpb   $0x0,(%ebx)
80101fc9:	0f 84 b7 00 00 00    	je     80102086 <namex+0x196>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80101fcf:	83 ec 04             	sub    $0x4,%esp
80101fd2:	6a 00                	push   $0x0
80101fd4:	57                   	push   %edi
80101fd5:	56                   	push   %esi
80101fd6:	e8 65 fe ff ff       	call   80101e40 <dirlookup>
80101fdb:	83 c4 10             	add    $0x10,%esp
80101fde:	85 c0                	test   %eax,%eax
80101fe0:	74 6e                	je     80102050 <namex+0x160>
  iunlock(ip);
80101fe2:	83 ec 0c             	sub    $0xc,%esp
80101fe5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101fe8:	56                   	push   %esi
80101fe9:	e8 02 fa ff ff       	call   801019f0 <iunlock>
  iput(ip);
80101fee:	89 34 24             	mov    %esi,(%esp)
80101ff1:	e8 4a fa ff ff       	call   80101a40 <iput>
80101ff6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101ff9:	83 c4 10             	add    $0x10,%esp
80101ffc:	89 c6                	mov    %eax,%esi
80101ffe:	e9 38 ff ff ff       	jmp    80101f3b <namex+0x4b>
80102003:	90                   	nop
80102004:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  while(*path != '/' && *path != 0)
80102008:	89 da                	mov    %ebx,%edx
8010200a:	31 c9                	xor    %ecx,%ecx
    memmove(name, s, len);
8010200c:	83 ec 04             	sub    $0x4,%esp
8010200f:	89 55 dc             	mov    %edx,-0x24(%ebp)
80102012:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80102015:	51                   	push   %ecx
80102016:	53                   	push   %ebx
80102017:	57                   	push   %edi
80102018:	e8 73 27 00 00       	call   80104790 <memmove>
    name[len] = 0;
8010201d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80102020:	8b 55 dc             	mov    -0x24(%ebp),%edx
80102023:	83 c4 10             	add    $0x10,%esp
80102026:	c6 04 0f 00          	movb   $0x0,(%edi,%ecx,1)
8010202a:	89 d3                	mov    %edx,%ebx
8010202c:	e9 61 ff ff ff       	jmp    80101f92 <namex+0xa2>
80102031:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80102038:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010203b:	85 c0                	test   %eax,%eax
8010203d:	75 5d                	jne    8010209c <namex+0x1ac>
    iput(ip);
    return 0;
  }
  return ip;
}
8010203f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102042:	89 f0                	mov    %esi,%eax
80102044:	5b                   	pop    %ebx
80102045:	5e                   	pop    %esi
80102046:	5f                   	pop    %edi
80102047:	5d                   	pop    %ebp
80102048:	c3                   	ret    
80102049:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  iunlock(ip);
80102050:	83 ec 0c             	sub    $0xc,%esp
80102053:	56                   	push   %esi
80102054:	e8 97 f9 ff ff       	call   801019f0 <iunlock>
  iput(ip);
80102059:	89 34 24             	mov    %esi,(%esp)
      return 0;
8010205c:	31 f6                	xor    %esi,%esi
  iput(ip);
8010205e:	e8 dd f9 ff ff       	call   80101a40 <iput>
      return 0;
80102063:	83 c4 10             	add    $0x10,%esp
}
80102066:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102069:	89 f0                	mov    %esi,%eax
8010206b:	5b                   	pop    %ebx
8010206c:	5e                   	pop    %esi
8010206d:	5f                   	pop    %edi
8010206e:	5d                   	pop    %ebp
8010206f:	c3                   	ret    
    ip = iget(ROOTDEV, ROOTINO);
80102070:	ba 01 00 00 00       	mov    $0x1,%edx
80102075:	b8 01 00 00 00       	mov    $0x1,%eax
8010207a:	e8 a1 f4 ff ff       	call   80101520 <iget>
8010207f:	89 c6                	mov    %eax,%esi
80102081:	e9 b5 fe ff ff       	jmp    80101f3b <namex+0x4b>
      iunlock(ip);
80102086:	83 ec 0c             	sub    $0xc,%esp
80102089:	56                   	push   %esi
8010208a:	e8 61 f9 ff ff       	call   801019f0 <iunlock>
      return ip;
8010208f:	83 c4 10             	add    $0x10,%esp
}
80102092:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102095:	89 f0                	mov    %esi,%eax
80102097:	5b                   	pop    %ebx
80102098:	5e                   	pop    %esi
80102099:	5f                   	pop    %edi
8010209a:	5d                   	pop    %ebp
8010209b:	c3                   	ret    
    iput(ip);
8010209c:	83 ec 0c             	sub    $0xc,%esp
8010209f:	56                   	push   %esi
    return 0;
801020a0:	31 f6                	xor    %esi,%esi
    iput(ip);
801020a2:	e8 99 f9 ff ff       	call   80101a40 <iput>
    return 0;
801020a7:	83 c4 10             	add    $0x10,%esp
801020aa:	eb 93                	jmp    8010203f <namex+0x14f>
801020ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801020b0 <dirlink>:
{
801020b0:	55                   	push   %ebp
801020b1:	89 e5                	mov    %esp,%ebp
801020b3:	57                   	push   %edi
801020b4:	56                   	push   %esi
801020b5:	53                   	push   %ebx
801020b6:	83 ec 20             	sub    $0x20,%esp
801020b9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((ip = dirlookup(dp, name, 0)) != 0){
801020bc:	6a 00                	push   $0x0
801020be:	ff 75 0c             	pushl  0xc(%ebp)
801020c1:	53                   	push   %ebx
801020c2:	e8 79 fd ff ff       	call   80101e40 <dirlookup>
801020c7:	83 c4 10             	add    $0x10,%esp
801020ca:	85 c0                	test   %eax,%eax
801020cc:	75 67                	jne    80102135 <dirlink+0x85>
  for(off = 0; off < dp->size; off += sizeof(de)){
801020ce:	8b 7b 58             	mov    0x58(%ebx),%edi
801020d1:	8d 75 d8             	lea    -0x28(%ebp),%esi
801020d4:	85 ff                	test   %edi,%edi
801020d6:	74 29                	je     80102101 <dirlink+0x51>
801020d8:	31 ff                	xor    %edi,%edi
801020da:	8d 75 d8             	lea    -0x28(%ebp),%esi
801020dd:	eb 09                	jmp    801020e8 <dirlink+0x38>
801020df:	90                   	nop
801020e0:	83 c7 10             	add    $0x10,%edi
801020e3:	3b 7b 58             	cmp    0x58(%ebx),%edi
801020e6:	73 19                	jae    80102101 <dirlink+0x51>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801020e8:	6a 10                	push   $0x10
801020ea:	57                   	push   %edi
801020eb:	56                   	push   %esi
801020ec:	53                   	push   %ebx
801020ed:	e8 fe fa ff ff       	call   80101bf0 <readi>
801020f2:	83 c4 10             	add    $0x10,%esp
801020f5:	83 f8 10             	cmp    $0x10,%eax
801020f8:	75 4e                	jne    80102148 <dirlink+0x98>
    if(de.inum == 0)
801020fa:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
801020ff:	75 df                	jne    801020e0 <dirlink+0x30>
  strncpy(de.name, name, DIRSIZ);
80102101:	8d 45 da             	lea    -0x26(%ebp),%eax
80102104:	83 ec 04             	sub    $0x4,%esp
80102107:	6a 0e                	push   $0xe
80102109:	ff 75 0c             	pushl  0xc(%ebp)
8010210c:	50                   	push   %eax
8010210d:	e8 4e 27 00 00       	call   80104860 <strncpy>
  de.inum = inum;
80102112:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80102115:	6a 10                	push   $0x10
80102117:	57                   	push   %edi
80102118:	56                   	push   %esi
80102119:	53                   	push   %ebx
  de.inum = inum;
8010211a:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010211e:	e8 cd fb ff ff       	call   80101cf0 <writei>
80102123:	83 c4 20             	add    $0x20,%esp
80102126:	83 f8 10             	cmp    $0x10,%eax
80102129:	75 2a                	jne    80102155 <dirlink+0xa5>
  return 0;
8010212b:	31 c0                	xor    %eax,%eax
}
8010212d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102130:	5b                   	pop    %ebx
80102131:	5e                   	pop    %esi
80102132:	5f                   	pop    %edi
80102133:	5d                   	pop    %ebp
80102134:	c3                   	ret    
    iput(ip);
80102135:	83 ec 0c             	sub    $0xc,%esp
80102138:	50                   	push   %eax
80102139:	e8 02 f9 ff ff       	call   80101a40 <iput>
    return -1;
8010213e:	83 c4 10             	add    $0x10,%esp
80102141:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102146:	eb e5                	jmp    8010212d <dirlink+0x7d>
      panic("dirlink read");
80102148:	83 ec 0c             	sub    $0xc,%esp
8010214b:	68 a8 72 10 80       	push   $0x801072a8
80102150:	e8 3b e2 ff ff       	call   80100390 <panic>
    panic("dirlink");
80102155:	83 ec 0c             	sub    $0xc,%esp
80102158:	68 9e 78 10 80       	push   $0x8010789e
8010215d:	e8 2e e2 ff ff       	call   80100390 <panic>
80102162:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102169:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102170 <namei>:

struct inode*
namei(char *path)
{
80102170:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
80102171:	31 d2                	xor    %edx,%edx
{
80102173:	89 e5                	mov    %esp,%ebp
80102175:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 0, name);
80102178:	8b 45 08             	mov    0x8(%ebp),%eax
8010217b:	8d 4d ea             	lea    -0x16(%ebp),%ecx
8010217e:	e8 6d fd ff ff       	call   80101ef0 <namex>
}
80102183:	c9                   	leave  
80102184:	c3                   	ret    
80102185:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102189:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102190 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80102190:	55                   	push   %ebp
  return namex(path, 1, name);
80102191:	ba 01 00 00 00       	mov    $0x1,%edx
{
80102196:	89 e5                	mov    %esp,%ebp
  return namex(path, 1, name);
80102198:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010219b:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010219e:	5d                   	pop    %ebp
  return namex(path, 1, name);
8010219f:	e9 4c fd ff ff       	jmp    80101ef0 <namex>
801021a4:	66 90                	xchg   %ax,%ax
801021a6:	66 90                	xchg   %ax,%ax
801021a8:	66 90                	xchg   %ax,%ax
801021aa:	66 90                	xchg   %ax,%ax
801021ac:	66 90                	xchg   %ax,%ax
801021ae:	66 90                	xchg   %ax,%ax

801021b0 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
801021b0:	55                   	push   %ebp
801021b1:	89 e5                	mov    %esp,%ebp
801021b3:	57                   	push   %edi
801021b4:	56                   	push   %esi
801021b5:	53                   	push   %ebx
801021b6:	83 ec 0c             	sub    $0xc,%esp
  if(b == 0)
801021b9:	85 c0                	test   %eax,%eax
801021bb:	0f 84 b4 00 00 00    	je     80102275 <idestart+0xc5>
    panic("idestart");
  if(b->blockno >= FSSIZE)
801021c1:	8b 58 08             	mov    0x8(%eax),%ebx
801021c4:	89 c6                	mov    %eax,%esi
801021c6:	81 fb e7 03 00 00    	cmp    $0x3e7,%ebx
801021cc:	0f 87 96 00 00 00    	ja     80102268 <idestart+0xb8>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801021d2:	b9 f7 01 00 00       	mov    $0x1f7,%ecx
801021d7:	89 f6                	mov    %esi,%esi
801021d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801021e0:	89 ca                	mov    %ecx,%edx
801021e2:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801021e3:	83 e0 c0             	and    $0xffffffc0,%eax
801021e6:	3c 40                	cmp    $0x40,%al
801021e8:	75 f6                	jne    801021e0 <idestart+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801021ea:	31 ff                	xor    %edi,%edi
801021ec:	ba f6 03 00 00       	mov    $0x3f6,%edx
801021f1:	89 f8                	mov    %edi,%eax
801021f3:	ee                   	out    %al,(%dx)
801021f4:	b8 01 00 00 00       	mov    $0x1,%eax
801021f9:	ba f2 01 00 00       	mov    $0x1f2,%edx
801021fe:	ee                   	out    %al,(%dx)
801021ff:	ba f3 01 00 00       	mov    $0x1f3,%edx
80102204:	89 d8                	mov    %ebx,%eax
80102206:	ee                   	out    %al,(%dx)

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
80102207:	89 d8                	mov    %ebx,%eax
80102209:	ba f4 01 00 00       	mov    $0x1f4,%edx
8010220e:	c1 f8 08             	sar    $0x8,%eax
80102211:	ee                   	out    %al,(%dx)
80102212:	ba f5 01 00 00       	mov    $0x1f5,%edx
80102217:	89 f8                	mov    %edi,%eax
80102219:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
8010221a:	0f b6 46 04          	movzbl 0x4(%esi),%eax
8010221e:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102223:	c1 e0 04             	shl    $0x4,%eax
80102226:	83 e0 10             	and    $0x10,%eax
80102229:	83 c8 e0             	or     $0xffffffe0,%eax
8010222c:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
8010222d:	f6 06 04             	testb  $0x4,(%esi)
80102230:	75 16                	jne    80102248 <idestart+0x98>
80102232:	b8 20 00 00 00       	mov    $0x20,%eax
80102237:	89 ca                	mov    %ecx,%edx
80102239:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
8010223a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010223d:	5b                   	pop    %ebx
8010223e:	5e                   	pop    %esi
8010223f:	5f                   	pop    %edi
80102240:	5d                   	pop    %ebp
80102241:	c3                   	ret    
80102242:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80102248:	b8 30 00 00 00       	mov    $0x30,%eax
8010224d:	89 ca                	mov    %ecx,%edx
8010224f:	ee                   	out    %al,(%dx)
  asm volatile("cld; rep outsl" :
80102250:	b9 80 00 00 00       	mov    $0x80,%ecx
    outsl(0x1f0, b->data, BSIZE/4);
80102255:	83 c6 5c             	add    $0x5c,%esi
80102258:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010225d:	fc                   	cld    
8010225e:	f3 6f                	rep outsl %ds:(%esi),(%dx)
}
80102260:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102263:	5b                   	pop    %ebx
80102264:	5e                   	pop    %esi
80102265:	5f                   	pop    %edi
80102266:	5d                   	pop    %ebp
80102267:	c3                   	ret    
    panic("incorrect blockno");
80102268:	83 ec 0c             	sub    $0xc,%esp
8010226b:	68 14 73 10 80       	push   $0x80107314
80102270:	e8 1b e1 ff ff       	call   80100390 <panic>
    panic("idestart");
80102275:	83 ec 0c             	sub    $0xc,%esp
80102278:	68 0b 73 10 80       	push   $0x8010730b
8010227d:	e8 0e e1 ff ff       	call   80100390 <panic>
80102282:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102289:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102290 <ideinit>:
{
80102290:	55                   	push   %ebp
80102291:	89 e5                	mov    %esp,%ebp
80102293:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
80102296:	68 26 73 10 80       	push   $0x80107326
8010229b:	68 80 a5 10 80       	push   $0x8010a580
801022a0:	e8 eb 21 00 00       	call   80104490 <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
801022a5:	58                   	pop    %eax
801022a6:	a1 00 2d 11 80       	mov    0x80112d00,%eax
801022ab:	5a                   	pop    %edx
801022ac:	83 e8 01             	sub    $0x1,%eax
801022af:	50                   	push   %eax
801022b0:	6a 0e                	push   $0xe
801022b2:	e8 a9 02 00 00       	call   80102560 <ioapicenable>
801022b7:	83 c4 10             	add    $0x10,%esp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801022ba:	ba f7 01 00 00       	mov    $0x1f7,%edx
801022bf:	90                   	nop
801022c0:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801022c1:	83 e0 c0             	and    $0xffffffc0,%eax
801022c4:	3c 40                	cmp    $0x40,%al
801022c6:	75 f8                	jne    801022c0 <ideinit+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801022c8:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
801022cd:	ba f6 01 00 00       	mov    $0x1f6,%edx
801022d2:	ee                   	out    %al,(%dx)
801022d3:	b9 e8 03 00 00       	mov    $0x3e8,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801022d8:	ba f7 01 00 00       	mov    $0x1f7,%edx
801022dd:	eb 06                	jmp    801022e5 <ideinit+0x55>
801022df:	90                   	nop
  for(i=0; i<1000; i++){
801022e0:	83 e9 01             	sub    $0x1,%ecx
801022e3:	74 0f                	je     801022f4 <ideinit+0x64>
801022e5:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
801022e6:	84 c0                	test   %al,%al
801022e8:	74 f6                	je     801022e0 <ideinit+0x50>
      havedisk1 = 1;
801022ea:	c7 05 60 a5 10 80 01 	movl   $0x1,0x8010a560
801022f1:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801022f4:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
801022f9:	ba f6 01 00 00       	mov    $0x1f6,%edx
801022fe:	ee                   	out    %al,(%dx)
}
801022ff:	c9                   	leave  
80102300:	c3                   	ret    
80102301:	eb 0d                	jmp    80102310 <ideintr>
80102303:	90                   	nop
80102304:	90                   	nop
80102305:	90                   	nop
80102306:	90                   	nop
80102307:	90                   	nop
80102308:	90                   	nop
80102309:	90                   	nop
8010230a:	90                   	nop
8010230b:	90                   	nop
8010230c:	90                   	nop
8010230d:	90                   	nop
8010230e:	90                   	nop
8010230f:	90                   	nop

80102310 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102310:	55                   	push   %ebp
80102311:	89 e5                	mov    %esp,%ebp
80102313:	57                   	push   %edi
80102314:	56                   	push   %esi
80102315:	53                   	push   %ebx
80102316:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102319:	68 80 a5 10 80       	push   $0x8010a580
8010231e:	e8 ad 22 00 00       	call   801045d0 <acquire>

  if((b = idequeue) == 0){
80102323:	8b 1d 64 a5 10 80    	mov    0x8010a564,%ebx
80102329:	83 c4 10             	add    $0x10,%esp
8010232c:	85 db                	test   %ebx,%ebx
8010232e:	74 67                	je     80102397 <ideintr+0x87>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
80102330:	8b 43 58             	mov    0x58(%ebx),%eax
80102333:	a3 64 a5 10 80       	mov    %eax,0x8010a564

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80102338:	8b 3b                	mov    (%ebx),%edi
8010233a:	f7 c7 04 00 00 00    	test   $0x4,%edi
80102340:	75 31                	jne    80102373 <ideintr+0x63>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102342:	ba f7 01 00 00       	mov    $0x1f7,%edx
80102347:	89 f6                	mov    %esi,%esi
80102349:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80102350:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102351:	89 c6                	mov    %eax,%esi
80102353:	83 e6 c0             	and    $0xffffffc0,%esi
80102356:	89 f1                	mov    %esi,%ecx
80102358:	80 f9 40             	cmp    $0x40,%cl
8010235b:	75 f3                	jne    80102350 <ideintr+0x40>
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
8010235d:	a8 21                	test   $0x21,%al
8010235f:	75 12                	jne    80102373 <ideintr+0x63>
    insl(0x1f0, b->data, BSIZE/4);
80102361:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
80102364:	b9 80 00 00 00       	mov    $0x80,%ecx
80102369:	ba f0 01 00 00       	mov    $0x1f0,%edx
8010236e:	fc                   	cld    
8010236f:	f3 6d                	rep insl (%dx),%es:(%edi)
80102371:	8b 3b                	mov    (%ebx),%edi

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
  b->flags &= ~B_DIRTY;
80102373:	83 e7 fb             	and    $0xfffffffb,%edi
  wakeup(b);
80102376:	83 ec 0c             	sub    $0xc,%esp
  b->flags &= ~B_DIRTY;
80102379:	89 f9                	mov    %edi,%ecx
8010237b:	83 c9 02             	or     $0x2,%ecx
8010237e:	89 0b                	mov    %ecx,(%ebx)
  wakeup(b);
80102380:	53                   	push   %ebx
80102381:	e8 3a 1e 00 00       	call   801041c0 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102386:	a1 64 a5 10 80       	mov    0x8010a564,%eax
8010238b:	83 c4 10             	add    $0x10,%esp
8010238e:	85 c0                	test   %eax,%eax
80102390:	74 05                	je     80102397 <ideintr+0x87>
    idestart(idequeue);
80102392:	e8 19 fe ff ff       	call   801021b0 <idestart>
    release(&idelock);
80102397:	83 ec 0c             	sub    $0xc,%esp
8010239a:	68 80 a5 10 80       	push   $0x8010a580
8010239f:	e8 ec 22 00 00       	call   80104690 <release>

  release(&idelock);
}
801023a4:	8d 65 f4             	lea    -0xc(%ebp),%esp
801023a7:	5b                   	pop    %ebx
801023a8:	5e                   	pop    %esi
801023a9:	5f                   	pop    %edi
801023aa:	5d                   	pop    %ebp
801023ab:	c3                   	ret    
801023ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801023b0 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
801023b0:	55                   	push   %ebp
801023b1:	89 e5                	mov    %esp,%ebp
801023b3:	53                   	push   %ebx
801023b4:	83 ec 10             	sub    $0x10,%esp
801023b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
801023ba:	8d 43 0c             	lea    0xc(%ebx),%eax
801023bd:	50                   	push   %eax
801023be:	e8 7d 20 00 00       	call   80104440 <holdingsleep>
801023c3:	83 c4 10             	add    $0x10,%esp
801023c6:	85 c0                	test   %eax,%eax
801023c8:	0f 84 c6 00 00 00    	je     80102494 <iderw+0xe4>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
801023ce:	8b 03                	mov    (%ebx),%eax
801023d0:	83 e0 06             	and    $0x6,%eax
801023d3:	83 f8 02             	cmp    $0x2,%eax
801023d6:	0f 84 ab 00 00 00    	je     80102487 <iderw+0xd7>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
801023dc:	8b 53 04             	mov    0x4(%ebx),%edx
801023df:	85 d2                	test   %edx,%edx
801023e1:	74 0d                	je     801023f0 <iderw+0x40>
801023e3:	a1 60 a5 10 80       	mov    0x8010a560,%eax
801023e8:	85 c0                	test   %eax,%eax
801023ea:	0f 84 b1 00 00 00    	je     801024a1 <iderw+0xf1>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
801023f0:	83 ec 0c             	sub    $0xc,%esp
801023f3:	68 80 a5 10 80       	push   $0x8010a580
801023f8:	e8 d3 21 00 00       	call   801045d0 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
801023fd:	8b 15 64 a5 10 80    	mov    0x8010a564,%edx
80102403:	83 c4 10             	add    $0x10,%esp
  b->qnext = 0;
80102406:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010240d:	85 d2                	test   %edx,%edx
8010240f:	75 09                	jne    8010241a <iderw+0x6a>
80102411:	eb 6d                	jmp    80102480 <iderw+0xd0>
80102413:	90                   	nop
80102414:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102418:	89 c2                	mov    %eax,%edx
8010241a:	8b 42 58             	mov    0x58(%edx),%eax
8010241d:	85 c0                	test   %eax,%eax
8010241f:	75 f7                	jne    80102418 <iderw+0x68>
80102421:	83 c2 58             	add    $0x58,%edx
    ;
  *pp = b;
80102424:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
80102426:	39 1d 64 a5 10 80    	cmp    %ebx,0x8010a564
8010242c:	74 42                	je     80102470 <iderw+0xc0>
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010242e:	8b 03                	mov    (%ebx),%eax
80102430:	83 e0 06             	and    $0x6,%eax
80102433:	83 f8 02             	cmp    $0x2,%eax
80102436:	74 23                	je     8010245b <iderw+0xab>
80102438:	90                   	nop
80102439:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    sleep(b, &idelock);
80102440:	83 ec 08             	sub    $0x8,%esp
80102443:	68 80 a5 10 80       	push   $0x8010a580
80102448:	53                   	push   %ebx
80102449:	e8 c2 1b 00 00       	call   80104010 <sleep>
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
8010244e:	8b 03                	mov    (%ebx),%eax
80102450:	83 c4 10             	add    $0x10,%esp
80102453:	83 e0 06             	and    $0x6,%eax
80102456:	83 f8 02             	cmp    $0x2,%eax
80102459:	75 e5                	jne    80102440 <iderw+0x90>
  }


  release(&idelock);
8010245b:	c7 45 08 80 a5 10 80 	movl   $0x8010a580,0x8(%ebp)
}
80102462:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102465:	c9                   	leave  
  release(&idelock);
80102466:	e9 25 22 00 00       	jmp    80104690 <release>
8010246b:	90                   	nop
8010246c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    idestart(b);
80102470:	89 d8                	mov    %ebx,%eax
80102472:	e8 39 fd ff ff       	call   801021b0 <idestart>
80102477:	eb b5                	jmp    8010242e <iderw+0x7e>
80102479:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102480:	ba 64 a5 10 80       	mov    $0x8010a564,%edx
80102485:	eb 9d                	jmp    80102424 <iderw+0x74>
    panic("iderw: nothing to do");
80102487:	83 ec 0c             	sub    $0xc,%esp
8010248a:	68 40 73 10 80       	push   $0x80107340
8010248f:	e8 fc de ff ff       	call   80100390 <panic>
    panic("iderw: buf not locked");
80102494:	83 ec 0c             	sub    $0xc,%esp
80102497:	68 2a 73 10 80       	push   $0x8010732a
8010249c:	e8 ef de ff ff       	call   80100390 <panic>
    panic("iderw: ide disk 1 not present");
801024a1:	83 ec 0c             	sub    $0xc,%esp
801024a4:	68 55 73 10 80       	push   $0x80107355
801024a9:	e8 e2 de ff ff       	call   80100390 <panic>
801024ae:	66 90                	xchg   %ax,%ax

801024b0 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
801024b0:	55                   	push   %ebp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
801024b1:	c7 05 34 26 11 80 00 	movl   $0xfec00000,0x80112634
801024b8:	00 c0 fe 
{
801024bb:	89 e5                	mov    %esp,%ebp
801024bd:	56                   	push   %esi
801024be:	53                   	push   %ebx
  ioapic->reg = reg;
801024bf:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
801024c6:	00 00 00 
  return ioapic->data;
801024c9:	a1 34 26 11 80       	mov    0x80112634,%eax
801024ce:	8b 58 10             	mov    0x10(%eax),%ebx
  ioapic->reg = reg;
801024d1:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  return ioapic->data;
801024d7:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
801024dd:	0f b6 15 60 27 11 80 	movzbl 0x80112760,%edx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
801024e4:	c1 eb 10             	shr    $0x10,%ebx
  return ioapic->data;
801024e7:	8b 41 10             	mov    0x10(%ecx),%eax
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
801024ea:	0f b6 db             	movzbl %bl,%ebx
  id = ioapicread(REG_ID) >> 24;
801024ed:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
801024f0:	39 c2                	cmp    %eax,%edx
801024f2:	74 16                	je     8010250a <ioapicinit+0x5a>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
801024f4:	83 ec 0c             	sub    $0xc,%esp
801024f7:	68 74 73 10 80       	push   $0x80107374
801024fc:	e8 6f e2 ff ff       	call   80100770 <cprintf>
80102501:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102507:	83 c4 10             	add    $0x10,%esp
8010250a:	83 c3 21             	add    $0x21,%ebx
{
8010250d:	ba 10 00 00 00       	mov    $0x10,%edx
80102512:	b8 20 00 00 00       	mov    $0x20,%eax
80102517:	89 f6                	mov    %esi,%esi
80102519:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  ioapic->reg = reg;
80102520:	89 11                	mov    %edx,(%ecx)
  ioapic->data = data;
80102522:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80102528:	89 c6                	mov    %eax,%esi
8010252a:	81 ce 00 00 01 00    	or     $0x10000,%esi
80102530:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
80102533:	89 71 10             	mov    %esi,0x10(%ecx)
80102536:	8d 72 01             	lea    0x1(%edx),%esi
80102539:	83 c2 02             	add    $0x2,%edx
  for(i = 0; i <= maxintr; i++){
8010253c:	39 d8                	cmp    %ebx,%eax
  ioapic->reg = reg;
8010253e:	89 31                	mov    %esi,(%ecx)
  ioapic->data = data;
80102540:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
80102546:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  for(i = 0; i <= maxintr; i++){
8010254d:	75 d1                	jne    80102520 <ioapicinit+0x70>
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
8010254f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102552:	5b                   	pop    %ebx
80102553:	5e                   	pop    %esi
80102554:	5d                   	pop    %ebp
80102555:	c3                   	ret    
80102556:	8d 76 00             	lea    0x0(%esi),%esi
80102559:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102560 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80102560:	55                   	push   %ebp
  ioapic->reg = reg;
80102561:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
{
80102567:	89 e5                	mov    %esp,%ebp
80102569:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
8010256c:	8d 50 20             	lea    0x20(%eax),%edx
8010256f:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
  ioapic->reg = reg;
80102573:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102575:	8b 0d 34 26 11 80    	mov    0x80112634,%ecx
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010257b:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
8010257e:	89 51 10             	mov    %edx,0x10(%ecx)
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102581:	8b 55 0c             	mov    0xc(%ebp),%edx
  ioapic->reg = reg;
80102584:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102586:	a1 34 26 11 80       	mov    0x80112634,%eax
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010258b:	c1 e2 18             	shl    $0x18,%edx
  ioapic->data = data;
8010258e:	89 50 10             	mov    %edx,0x10(%eax)
}
80102591:	5d                   	pop    %ebp
80102592:	c3                   	ret    
80102593:	66 90                	xchg   %ax,%ax
80102595:	66 90                	xchg   %ax,%ax
80102597:	66 90                	xchg   %ax,%ax
80102599:	66 90                	xchg   %ax,%ax
8010259b:	66 90                	xchg   %ax,%ax
8010259d:	66 90                	xchg   %ax,%ax
8010259f:	90                   	nop

801025a0 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
801025a0:	55                   	push   %ebp
801025a1:	89 e5                	mov    %esp,%ebp
801025a3:	53                   	push   %ebx
801025a4:	83 ec 04             	sub    $0x4,%esp
801025a7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
801025aa:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
801025b0:	75 70                	jne    80102622 <kfree+0x82>
801025b2:	81 fb a8 54 11 80    	cmp    $0x801154a8,%ebx
801025b8:	72 68                	jb     80102622 <kfree+0x82>
801025ba:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
801025c0:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
801025c5:	77 5b                	ja     80102622 <kfree+0x82>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
801025c7:	83 ec 04             	sub    $0x4,%esp
801025ca:	68 00 10 00 00       	push   $0x1000
801025cf:	6a 01                	push   $0x1
801025d1:	53                   	push   %ebx
801025d2:	e8 09 21 00 00       	call   801046e0 <memset>

  if(kmem.use_lock)
801025d7:	8b 15 74 26 11 80    	mov    0x80112674,%edx
801025dd:	83 c4 10             	add    $0x10,%esp
801025e0:	85 d2                	test   %edx,%edx
801025e2:	75 2c                	jne    80102610 <kfree+0x70>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
801025e4:	a1 78 26 11 80       	mov    0x80112678,%eax
801025e9:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  if(kmem.use_lock)
801025eb:	a1 74 26 11 80       	mov    0x80112674,%eax
  kmem.freelist = r;
801025f0:	89 1d 78 26 11 80    	mov    %ebx,0x80112678
  if(kmem.use_lock)
801025f6:	85 c0                	test   %eax,%eax
801025f8:	75 06                	jne    80102600 <kfree+0x60>
    release(&kmem.lock);
}
801025fa:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801025fd:	c9                   	leave  
801025fe:	c3                   	ret    
801025ff:	90                   	nop
    release(&kmem.lock);
80102600:	c7 45 08 40 26 11 80 	movl   $0x80112640,0x8(%ebp)
}
80102607:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010260a:	c9                   	leave  
    release(&kmem.lock);
8010260b:	e9 80 20 00 00       	jmp    80104690 <release>
    acquire(&kmem.lock);
80102610:	83 ec 0c             	sub    $0xc,%esp
80102613:	68 40 26 11 80       	push   $0x80112640
80102618:	e8 b3 1f 00 00       	call   801045d0 <acquire>
8010261d:	83 c4 10             	add    $0x10,%esp
80102620:	eb c2                	jmp    801025e4 <kfree+0x44>
    panic("kfree");
80102622:	83 ec 0c             	sub    $0xc,%esp
80102625:	68 a6 73 10 80       	push   $0x801073a6
8010262a:	e8 61 dd ff ff       	call   80100390 <panic>
8010262f:	90                   	nop

80102630 <freerange>:
{
80102630:	55                   	push   %ebp
80102631:	89 e5                	mov    %esp,%ebp
80102633:	56                   	push   %esi
80102634:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
80102635:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102638:	8b 75 0c             	mov    0xc(%ebp),%esi
  p = (char*)PGROUNDUP((uint)vstart);
8010263b:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102641:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102647:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010264d:	39 de                	cmp    %ebx,%esi
8010264f:	72 23                	jb     80102674 <freerange+0x44>
80102651:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
80102658:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
8010265e:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102661:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102667:	50                   	push   %eax
80102668:	e8 33 ff ff ff       	call   801025a0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010266d:	83 c4 10             	add    $0x10,%esp
80102670:	39 f3                	cmp    %esi,%ebx
80102672:	76 e4                	jbe    80102658 <freerange+0x28>
}
80102674:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102677:	5b                   	pop    %ebx
80102678:	5e                   	pop    %esi
80102679:	5d                   	pop    %ebp
8010267a:	c3                   	ret    
8010267b:	90                   	nop
8010267c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102680 <kinit1>:
{
80102680:	55                   	push   %ebp
80102681:	89 e5                	mov    %esp,%ebp
80102683:	56                   	push   %esi
80102684:	53                   	push   %ebx
80102685:	8b 75 0c             	mov    0xc(%ebp),%esi
  initlock(&kmem.lock, "kmem");
80102688:	83 ec 08             	sub    $0x8,%esp
8010268b:	68 ac 73 10 80       	push   $0x801073ac
80102690:	68 40 26 11 80       	push   $0x80112640
80102695:	e8 f6 1d 00 00       	call   80104490 <initlock>
  p = (char*)PGROUNDUP((uint)vstart);
8010269a:	8b 45 08             	mov    0x8(%ebp),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010269d:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
801026a0:	c7 05 74 26 11 80 00 	movl   $0x0,0x80112674
801026a7:	00 00 00 
  p = (char*)PGROUNDUP((uint)vstart);
801026aa:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
801026b0:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801026b6:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801026bc:	39 de                	cmp    %ebx,%esi
801026be:	72 1c                	jb     801026dc <kinit1+0x5c>
    kfree(p);
801026c0:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
801026c6:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801026c9:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
801026cf:	50                   	push   %eax
801026d0:	e8 cb fe ff ff       	call   801025a0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801026d5:	83 c4 10             	add    $0x10,%esp
801026d8:	39 de                	cmp    %ebx,%esi
801026da:	73 e4                	jae    801026c0 <kinit1+0x40>
}
801026dc:	8d 65 f8             	lea    -0x8(%ebp),%esp
801026df:	5b                   	pop    %ebx
801026e0:	5e                   	pop    %esi
801026e1:	5d                   	pop    %ebp
801026e2:	c3                   	ret    
801026e3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801026e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801026f0 <kinit2>:
{
801026f0:	55                   	push   %ebp
801026f1:	89 e5                	mov    %esp,%ebp
801026f3:	56                   	push   %esi
801026f4:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
801026f5:	8b 45 08             	mov    0x8(%ebp),%eax
{
801026f8:	8b 75 0c             	mov    0xc(%ebp),%esi
  p = (char*)PGROUNDUP((uint)vstart);
801026fb:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102701:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102707:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010270d:	39 de                	cmp    %ebx,%esi
8010270f:	72 23                	jb     80102734 <kinit2+0x44>
80102711:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
80102718:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
8010271e:	83 ec 0c             	sub    $0xc,%esp
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102721:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
80102727:	50                   	push   %eax
80102728:	e8 73 fe ff ff       	call   801025a0 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010272d:	83 c4 10             	add    $0x10,%esp
80102730:	39 de                	cmp    %ebx,%esi
80102732:	73 e4                	jae    80102718 <kinit2+0x28>
  kmem.use_lock = 1;
80102734:	c7 05 74 26 11 80 01 	movl   $0x1,0x80112674
8010273b:	00 00 00 
}
8010273e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102741:	5b                   	pop    %ebx
80102742:	5e                   	pop    %esi
80102743:	5d                   	pop    %ebp
80102744:	c3                   	ret    
80102745:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102749:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102750 <kalloc>:
char*
kalloc(void)
{
  struct run *r;

  if(kmem.use_lock)
80102750:	a1 74 26 11 80       	mov    0x80112674,%eax
80102755:	85 c0                	test   %eax,%eax
80102757:	75 1f                	jne    80102778 <kalloc+0x28>
    acquire(&kmem.lock);
  r = kmem.freelist;
80102759:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
8010275e:	85 c0                	test   %eax,%eax
80102760:	74 0e                	je     80102770 <kalloc+0x20>
    kmem.freelist = r->next;
80102762:	8b 10                	mov    (%eax),%edx
80102764:	89 15 78 26 11 80    	mov    %edx,0x80112678
8010276a:	c3                   	ret    
8010276b:	90                   	nop
8010276c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(kmem.use_lock)
    release(&kmem.lock);
  return (char*)r;
}
80102770:	f3 c3                	repz ret 
80102772:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
{
80102778:	55                   	push   %ebp
80102779:	89 e5                	mov    %esp,%ebp
8010277b:	83 ec 24             	sub    $0x24,%esp
    acquire(&kmem.lock);
8010277e:	68 40 26 11 80       	push   $0x80112640
80102783:	e8 48 1e 00 00       	call   801045d0 <acquire>
  r = kmem.freelist;
80102788:	a1 78 26 11 80       	mov    0x80112678,%eax
  if(r)
8010278d:	83 c4 10             	add    $0x10,%esp
80102790:	8b 15 74 26 11 80    	mov    0x80112674,%edx
80102796:	85 c0                	test   %eax,%eax
80102798:	74 08                	je     801027a2 <kalloc+0x52>
    kmem.freelist = r->next;
8010279a:	8b 08                	mov    (%eax),%ecx
8010279c:	89 0d 78 26 11 80    	mov    %ecx,0x80112678
  if(kmem.use_lock)
801027a2:	85 d2                	test   %edx,%edx
801027a4:	74 16                	je     801027bc <kalloc+0x6c>
    release(&kmem.lock);
801027a6:	83 ec 0c             	sub    $0xc,%esp
801027a9:	89 45 f4             	mov    %eax,-0xc(%ebp)
801027ac:	68 40 26 11 80       	push   $0x80112640
801027b1:	e8 da 1e 00 00       	call   80104690 <release>
  return (char*)r;
801027b6:	8b 45 f4             	mov    -0xc(%ebp),%eax
    release(&kmem.lock);
801027b9:	83 c4 10             	add    $0x10,%esp
}
801027bc:	c9                   	leave  
801027bd:	c3                   	ret    
801027be:	66 90                	xchg   %ax,%ax

801027c0 <kbdgetc>:
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801027c0:	ba 64 00 00 00       	mov    $0x64,%edx
801027c5:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
801027c6:	a8 01                	test   $0x1,%al
801027c8:	0f 84 c2 00 00 00    	je     80102890 <kbdgetc+0xd0>
801027ce:	ba 60 00 00 00       	mov    $0x60,%edx
801027d3:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);
801027d4:	0f b6 d0             	movzbl %al,%edx
801027d7:	8b 0d b4 a5 10 80    	mov    0x8010a5b4,%ecx

  if(data == 0xE0){
801027dd:	81 fa e0 00 00 00    	cmp    $0xe0,%edx
801027e3:	0f 84 7f 00 00 00    	je     80102868 <kbdgetc+0xa8>
{
801027e9:	55                   	push   %ebp
801027ea:	89 e5                	mov    %esp,%ebp
801027ec:	53                   	push   %ebx
801027ed:	89 cb                	mov    %ecx,%ebx
801027ef:	83 e3 40             	and    $0x40,%ebx
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
801027f2:	84 c0                	test   %al,%al
801027f4:	78 4a                	js     80102840 <kbdgetc+0x80>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
801027f6:	85 db                	test   %ebx,%ebx
801027f8:	74 09                	je     80102803 <kbdgetc+0x43>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
801027fa:	83 c8 80             	or     $0xffffff80,%eax
    shift &= ~E0ESC;
801027fd:	83 e1 bf             	and    $0xffffffbf,%ecx
    data |= 0x80;
80102800:	0f b6 d0             	movzbl %al,%edx
  }

  shift |= shiftcode[data];
80102803:	0f b6 82 e0 74 10 80 	movzbl -0x7fef8b20(%edx),%eax
8010280a:	09 c1                	or     %eax,%ecx
  shift ^= togglecode[data];
8010280c:	0f b6 82 e0 73 10 80 	movzbl -0x7fef8c20(%edx),%eax
80102813:	31 c1                	xor    %eax,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102815:	89 c8                	mov    %ecx,%eax
  shift ^= togglecode[data];
80102817:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
  c = charcode[shift & (CTL | SHIFT)][data];
8010281d:	83 e0 03             	and    $0x3,%eax
  if(shift & CAPSLOCK){
80102820:	83 e1 08             	and    $0x8,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
80102823:	8b 04 85 c0 73 10 80 	mov    -0x7fef8c40(,%eax,4),%eax
8010282a:	0f b6 04 10          	movzbl (%eax,%edx,1),%eax
  if(shift & CAPSLOCK){
8010282e:	74 31                	je     80102861 <kbdgetc+0xa1>
    if('a' <= c && c <= 'z')
80102830:	8d 50 9f             	lea    -0x61(%eax),%edx
80102833:	83 fa 19             	cmp    $0x19,%edx
80102836:	77 40                	ja     80102878 <kbdgetc+0xb8>
      c += 'A' - 'a';
80102838:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
8010283b:	5b                   	pop    %ebx
8010283c:	5d                   	pop    %ebp
8010283d:	c3                   	ret    
8010283e:	66 90                	xchg   %ax,%ax
    data = (shift & E0ESC ? data : data & 0x7F);
80102840:	83 e0 7f             	and    $0x7f,%eax
80102843:	85 db                	test   %ebx,%ebx
80102845:	0f 44 d0             	cmove  %eax,%edx
    shift &= ~(shiftcode[data] | E0ESC);
80102848:	0f b6 82 e0 74 10 80 	movzbl -0x7fef8b20(%edx),%eax
8010284f:	83 c8 40             	or     $0x40,%eax
80102852:	0f b6 c0             	movzbl %al,%eax
80102855:	f7 d0                	not    %eax
80102857:	21 c1                	and    %eax,%ecx
    return 0;
80102859:	31 c0                	xor    %eax,%eax
    shift &= ~(shiftcode[data] | E0ESC);
8010285b:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
}
80102861:	5b                   	pop    %ebx
80102862:	5d                   	pop    %ebp
80102863:	c3                   	ret    
80102864:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    shift |= E0ESC;
80102868:	83 c9 40             	or     $0x40,%ecx
    return 0;
8010286b:	31 c0                	xor    %eax,%eax
    shift |= E0ESC;
8010286d:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
    return 0;
80102873:	c3                   	ret    
80102874:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    else if('A' <= c && c <= 'Z')
80102878:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
8010287b:	8d 50 20             	lea    0x20(%eax),%edx
}
8010287e:	5b                   	pop    %ebx
      c += 'a' - 'A';
8010287f:	83 f9 1a             	cmp    $0x1a,%ecx
80102882:	0f 42 c2             	cmovb  %edx,%eax
}
80102885:	5d                   	pop    %ebp
80102886:	c3                   	ret    
80102887:	89 f6                	mov    %esi,%esi
80102889:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80102890:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80102895:	c3                   	ret    
80102896:	8d 76 00             	lea    0x0(%esi),%esi
80102899:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801028a0 <kbdintr>:

void
kbdintr(void)
{
801028a0:	55                   	push   %ebp
801028a1:	89 e5                	mov    %esp,%ebp
801028a3:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
801028a6:	68 c0 27 10 80       	push   $0x801027c0
801028ab:	e8 70 e0 ff ff       	call   80100920 <consoleintr>
}
801028b0:	83 c4 10             	add    $0x10,%esp
801028b3:	c9                   	leave  
801028b4:	c3                   	ret    
801028b5:	66 90                	xchg   %ax,%ax
801028b7:	66 90                	xchg   %ax,%ax
801028b9:	66 90                	xchg   %ax,%ax
801028bb:	66 90                	xchg   %ax,%ax
801028bd:	66 90                	xchg   %ax,%ax
801028bf:	90                   	nop

801028c0 <lapicinit>:
}

void
lapicinit(void)
{
  if(!lapic)
801028c0:	a1 7c 26 11 80       	mov    0x8011267c,%eax
{
801028c5:	55                   	push   %ebp
801028c6:	89 e5                	mov    %esp,%ebp
  if(!lapic)
801028c8:	85 c0                	test   %eax,%eax
801028ca:	0f 84 c8 00 00 00    	je     80102998 <lapicinit+0xd8>
  lapic[index] = value;
801028d0:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
801028d7:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
801028da:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801028dd:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
801028e4:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801028e7:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801028ea:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
801028f1:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
801028f4:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801028f7:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
801028fe:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
80102901:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102904:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
8010290b:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010290e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102911:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
80102918:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010291b:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
8010291e:	8b 50 30             	mov    0x30(%eax),%edx
80102921:	c1 ea 10             	shr    $0x10,%edx
80102924:	80 fa 03             	cmp    $0x3,%dl
80102927:	77 77                	ja     801029a0 <lapicinit+0xe0>
  lapic[index] = value;
80102929:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
80102930:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102933:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102936:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
8010293d:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102940:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102943:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
8010294a:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010294d:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102950:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80102957:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
8010295a:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010295d:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
80102964:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102967:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010296a:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
80102971:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
80102974:	8b 50 20             	mov    0x20(%eax),%edx
80102977:	89 f6                	mov    %esi,%esi
80102979:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
80102980:	8b 90 00 03 00 00    	mov    0x300(%eax),%edx
80102986:	80 e6 10             	and    $0x10,%dh
80102989:	75 f5                	jne    80102980 <lapicinit+0xc0>
  lapic[index] = value;
8010298b:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80102992:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102995:	8b 40 20             	mov    0x20(%eax),%eax
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80102998:	5d                   	pop    %ebp
80102999:	c3                   	ret    
8010299a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  lapic[index] = value;
801029a0:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
801029a7:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
801029aa:	8b 50 20             	mov    0x20(%eax),%edx
801029ad:	e9 77 ff ff ff       	jmp    80102929 <lapicinit+0x69>
801029b2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801029b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801029c0 <lapicid>:

int
lapicid(void)
{
  if (!lapic)
801029c0:	8b 15 7c 26 11 80    	mov    0x8011267c,%edx
{
801029c6:	55                   	push   %ebp
801029c7:	31 c0                	xor    %eax,%eax
801029c9:	89 e5                	mov    %esp,%ebp
  if (!lapic)
801029cb:	85 d2                	test   %edx,%edx
801029cd:	74 06                	je     801029d5 <lapicid+0x15>
    return 0;
  return lapic[ID] >> 24;
801029cf:	8b 42 20             	mov    0x20(%edx),%eax
801029d2:	c1 e8 18             	shr    $0x18,%eax
}
801029d5:	5d                   	pop    %ebp
801029d6:	c3                   	ret    
801029d7:	89 f6                	mov    %esi,%esi
801029d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801029e0 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
  if(lapic)
801029e0:	a1 7c 26 11 80       	mov    0x8011267c,%eax
{
801029e5:	55                   	push   %ebp
801029e6:	89 e5                	mov    %esp,%ebp
  if(lapic)
801029e8:	85 c0                	test   %eax,%eax
801029ea:	74 0d                	je     801029f9 <lapiceoi+0x19>
  lapic[index] = value;
801029ec:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
801029f3:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801029f6:	8b 40 20             	mov    0x20(%eax),%eax
    lapicw(EOI, 0);
}
801029f9:	5d                   	pop    %ebp
801029fa:	c3                   	ret    
801029fb:	90                   	nop
801029fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102a00 <microdelay>:

// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
80102a00:	55                   	push   %ebp
80102a01:	89 e5                	mov    %esp,%ebp
}
80102a03:	5d                   	pop    %ebp
80102a04:	c3                   	ret    
80102a05:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102a09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102a10 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
80102a10:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a11:	b8 0f 00 00 00       	mov    $0xf,%eax
80102a16:	ba 70 00 00 00       	mov    $0x70,%edx
80102a1b:	89 e5                	mov    %esp,%ebp
80102a1d:	53                   	push   %ebx
80102a1e:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80102a21:	8b 5d 08             	mov    0x8(%ebp),%ebx
80102a24:	ee                   	out    %al,(%dx)
80102a25:	b8 0a 00 00 00       	mov    $0xa,%eax
80102a2a:	ba 71 00 00 00       	mov    $0x71,%edx
80102a2f:	ee                   	out    %al,(%dx)
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
80102a30:	31 c0                	xor    %eax,%eax
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
80102a32:	c1 e3 18             	shl    $0x18,%ebx
  wrv[0] = 0;
80102a35:	66 a3 67 04 00 80    	mov    %ax,0x80000467
  wrv[1] = addr >> 4;
80102a3b:	89 c8                	mov    %ecx,%eax
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
80102a3d:	c1 e9 0c             	shr    $0xc,%ecx
  wrv[1] = addr >> 4;
80102a40:	c1 e8 04             	shr    $0x4,%eax
  lapicw(ICRHI, apicid<<24);
80102a43:	89 da                	mov    %ebx,%edx
    lapicw(ICRLO, STARTUP | (addr>>12));
80102a45:	80 cd 06             	or     $0x6,%ch
  wrv[1] = addr >> 4;
80102a48:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapic[index] = value;
80102a4e:	a1 7c 26 11 80       	mov    0x8011267c,%eax
80102a53:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102a59:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102a5c:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
80102a63:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102a66:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102a69:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
80102a70:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102a73:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102a76:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102a7c:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102a7f:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102a85:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102a88:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102a8e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102a91:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102a97:	8b 40 20             	mov    0x20(%eax),%eax
    microdelay(200);
  }
}
80102a9a:	5b                   	pop    %ebx
80102a9b:	5d                   	pop    %ebp
80102a9c:	c3                   	ret    
80102a9d:	8d 76 00             	lea    0x0(%esi),%esi

80102aa0 <cmostime>:
}

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
80102aa0:	55                   	push   %ebp
80102aa1:	b8 0b 00 00 00       	mov    $0xb,%eax
80102aa6:	ba 70 00 00 00       	mov    $0x70,%edx
80102aab:	89 e5                	mov    %esp,%ebp
80102aad:	57                   	push   %edi
80102aae:	56                   	push   %esi
80102aaf:	53                   	push   %ebx
80102ab0:	83 ec 4c             	sub    $0x4c,%esp
80102ab3:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102ab4:	ba 71 00 00 00       	mov    $0x71,%edx
80102ab9:	ec                   	in     (%dx),%al
80102aba:	83 e0 04             	and    $0x4,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102abd:	bb 70 00 00 00       	mov    $0x70,%ebx
80102ac2:	88 45 b3             	mov    %al,-0x4d(%ebp)
80102ac5:	8d 76 00             	lea    0x0(%esi),%esi
80102ac8:	31 c0                	xor    %eax,%eax
80102aca:	89 da                	mov    %ebx,%edx
80102acc:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102acd:	b9 71 00 00 00       	mov    $0x71,%ecx
80102ad2:	89 ca                	mov    %ecx,%edx
80102ad4:	ec                   	in     (%dx),%al
80102ad5:	88 45 b7             	mov    %al,-0x49(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102ad8:	89 da                	mov    %ebx,%edx
80102ada:	b8 02 00 00 00       	mov    $0x2,%eax
80102adf:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102ae0:	89 ca                	mov    %ecx,%edx
80102ae2:	ec                   	in     (%dx),%al
80102ae3:	88 45 b6             	mov    %al,-0x4a(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102ae6:	89 da                	mov    %ebx,%edx
80102ae8:	b8 04 00 00 00       	mov    $0x4,%eax
80102aed:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102aee:	89 ca                	mov    %ecx,%edx
80102af0:	ec                   	in     (%dx),%al
80102af1:	88 45 b5             	mov    %al,-0x4b(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102af4:	89 da                	mov    %ebx,%edx
80102af6:	b8 07 00 00 00       	mov    $0x7,%eax
80102afb:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102afc:	89 ca                	mov    %ecx,%edx
80102afe:	ec                   	in     (%dx),%al
80102aff:	88 45 b4             	mov    %al,-0x4c(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b02:	89 da                	mov    %ebx,%edx
80102b04:	b8 08 00 00 00       	mov    $0x8,%eax
80102b09:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b0a:	89 ca                	mov    %ecx,%edx
80102b0c:	ec                   	in     (%dx),%al
80102b0d:	89 c7                	mov    %eax,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b0f:	89 da                	mov    %ebx,%edx
80102b11:	b8 09 00 00 00       	mov    $0x9,%eax
80102b16:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b17:	89 ca                	mov    %ecx,%edx
80102b19:	ec                   	in     (%dx),%al
80102b1a:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b1c:	89 da                	mov    %ebx,%edx
80102b1e:	b8 0a 00 00 00       	mov    $0xa,%eax
80102b23:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b24:	89 ca                	mov    %ecx,%edx
80102b26:	ec                   	in     (%dx),%al
  bcd = (sb & (1 << 2)) == 0;

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
80102b27:	84 c0                	test   %al,%al
80102b29:	78 9d                	js     80102ac8 <cmostime+0x28>
  return inb(CMOS_RETURN);
80102b2b:	0f b6 45 b7          	movzbl -0x49(%ebp),%eax
80102b2f:	89 fa                	mov    %edi,%edx
80102b31:	0f b6 fa             	movzbl %dl,%edi
80102b34:	89 f2                	mov    %esi,%edx
80102b36:	0f b6 f2             	movzbl %dl,%esi
80102b39:	89 7d c8             	mov    %edi,-0x38(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b3c:	89 da                	mov    %ebx,%edx
80102b3e:	89 75 cc             	mov    %esi,-0x34(%ebp)
80102b41:	89 45 b8             	mov    %eax,-0x48(%ebp)
80102b44:	0f b6 45 b6          	movzbl -0x4a(%ebp),%eax
80102b48:	89 45 bc             	mov    %eax,-0x44(%ebp)
80102b4b:	0f b6 45 b5          	movzbl -0x4b(%ebp),%eax
80102b4f:	89 45 c0             	mov    %eax,-0x40(%ebp)
80102b52:	0f b6 45 b4          	movzbl -0x4c(%ebp),%eax
80102b56:	89 45 c4             	mov    %eax,-0x3c(%ebp)
80102b59:	31 c0                	xor    %eax,%eax
80102b5b:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b5c:	89 ca                	mov    %ecx,%edx
80102b5e:	ec                   	in     (%dx),%al
80102b5f:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b62:	89 da                	mov    %ebx,%edx
80102b64:	89 45 d0             	mov    %eax,-0x30(%ebp)
80102b67:	b8 02 00 00 00       	mov    $0x2,%eax
80102b6c:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b6d:	89 ca                	mov    %ecx,%edx
80102b6f:	ec                   	in     (%dx),%al
80102b70:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b73:	89 da                	mov    %ebx,%edx
80102b75:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80102b78:	b8 04 00 00 00       	mov    $0x4,%eax
80102b7d:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b7e:	89 ca                	mov    %ecx,%edx
80102b80:	ec                   	in     (%dx),%al
80102b81:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b84:	89 da                	mov    %ebx,%edx
80102b86:	89 45 d8             	mov    %eax,-0x28(%ebp)
80102b89:	b8 07 00 00 00       	mov    $0x7,%eax
80102b8e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102b8f:	89 ca                	mov    %ecx,%edx
80102b91:	ec                   	in     (%dx),%al
80102b92:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102b95:	89 da                	mov    %ebx,%edx
80102b97:	89 45 dc             	mov    %eax,-0x24(%ebp)
80102b9a:	b8 08 00 00 00       	mov    $0x8,%eax
80102b9f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102ba0:	89 ca                	mov    %ecx,%edx
80102ba2:	ec                   	in     (%dx),%al
80102ba3:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102ba6:	89 da                	mov    %ebx,%edx
80102ba8:	89 45 e0             	mov    %eax,-0x20(%ebp)
80102bab:	b8 09 00 00 00       	mov    $0x9,%eax
80102bb0:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102bb1:	89 ca                	mov    %ecx,%edx
80102bb3:	ec                   	in     (%dx),%al
80102bb4:	0f b6 c0             	movzbl %al,%eax
        continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102bb7:	83 ec 04             	sub    $0x4,%esp
  return inb(CMOS_RETURN);
80102bba:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102bbd:	8d 45 d0             	lea    -0x30(%ebp),%eax
80102bc0:	6a 18                	push   $0x18
80102bc2:	50                   	push   %eax
80102bc3:	8d 45 b8             	lea    -0x48(%ebp),%eax
80102bc6:	50                   	push   %eax
80102bc7:	e8 64 1b 00 00       	call   80104730 <memcmp>
80102bcc:	83 c4 10             	add    $0x10,%esp
80102bcf:	85 c0                	test   %eax,%eax
80102bd1:	0f 85 f1 fe ff ff    	jne    80102ac8 <cmostime+0x28>
      break;
  }

  // convert
  if(bcd) {
80102bd7:	80 7d b3 00          	cmpb   $0x0,-0x4d(%ebp)
80102bdb:	75 78                	jne    80102c55 <cmostime+0x1b5>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
80102bdd:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102be0:	89 c2                	mov    %eax,%edx
80102be2:	83 e0 0f             	and    $0xf,%eax
80102be5:	c1 ea 04             	shr    $0x4,%edx
80102be8:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102beb:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102bee:	89 45 b8             	mov    %eax,-0x48(%ebp)
    CONV(minute);
80102bf1:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102bf4:	89 c2                	mov    %eax,%edx
80102bf6:	83 e0 0f             	and    $0xf,%eax
80102bf9:	c1 ea 04             	shr    $0x4,%edx
80102bfc:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102bff:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c02:	89 45 bc             	mov    %eax,-0x44(%ebp)
    CONV(hour  );
80102c05:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102c08:	89 c2                	mov    %eax,%edx
80102c0a:	83 e0 0f             	and    $0xf,%eax
80102c0d:	c1 ea 04             	shr    $0x4,%edx
80102c10:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c13:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c16:	89 45 c0             	mov    %eax,-0x40(%ebp)
    CONV(day   );
80102c19:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102c1c:	89 c2                	mov    %eax,%edx
80102c1e:	83 e0 0f             	and    $0xf,%eax
80102c21:	c1 ea 04             	shr    $0x4,%edx
80102c24:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c27:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c2a:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    CONV(month );
80102c2d:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102c30:	89 c2                	mov    %eax,%edx
80102c32:	83 e0 0f             	and    $0xf,%eax
80102c35:	c1 ea 04             	shr    $0x4,%edx
80102c38:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c3b:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c3e:	89 45 c8             	mov    %eax,-0x38(%ebp)
    CONV(year  );
80102c41:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102c44:	89 c2                	mov    %eax,%edx
80102c46:	83 e0 0f             	and    $0xf,%eax
80102c49:	c1 ea 04             	shr    $0x4,%edx
80102c4c:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102c4f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102c52:	89 45 cc             	mov    %eax,-0x34(%ebp)
#undef     CONV
  }

  *r = t1;
80102c55:	8b 75 08             	mov    0x8(%ebp),%esi
80102c58:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102c5b:	89 06                	mov    %eax,(%esi)
80102c5d:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102c60:	89 46 04             	mov    %eax,0x4(%esi)
80102c63:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102c66:	89 46 08             	mov    %eax,0x8(%esi)
80102c69:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102c6c:	89 46 0c             	mov    %eax,0xc(%esi)
80102c6f:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102c72:	89 46 10             	mov    %eax,0x10(%esi)
80102c75:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102c78:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
80102c7b:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
80102c82:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102c85:	5b                   	pop    %ebx
80102c86:	5e                   	pop    %esi
80102c87:	5f                   	pop    %edi
80102c88:	5d                   	pop    %ebp
80102c89:	c3                   	ret    
80102c8a:	66 90                	xchg   %ax,%ax
80102c8c:	66 90                	xchg   %ax,%ax
80102c8e:	66 90                	xchg   %ax,%ax

80102c90 <install_trans>:
static void
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80102c90:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80102c96:	85 c9                	test   %ecx,%ecx
80102c98:	0f 8e 8a 00 00 00    	jle    80102d28 <install_trans+0x98>
{
80102c9e:	55                   	push   %ebp
80102c9f:	89 e5                	mov    %esp,%ebp
80102ca1:	57                   	push   %edi
80102ca2:	56                   	push   %esi
80102ca3:	53                   	push   %ebx
  for (tail = 0; tail < log.lh.n; tail++) {
80102ca4:	31 db                	xor    %ebx,%ebx
{
80102ca6:	83 ec 0c             	sub    $0xc,%esp
80102ca9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80102cb0:	a1 b4 26 11 80       	mov    0x801126b4,%eax
80102cb5:	83 ec 08             	sub    $0x8,%esp
80102cb8:	01 d8                	add    %ebx,%eax
80102cba:	83 c0 01             	add    $0x1,%eax
80102cbd:	50                   	push   %eax
80102cbe:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102cc4:	e8 07 d4 ff ff       	call   801000d0 <bread>
80102cc9:	89 c7                	mov    %eax,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102ccb:	58                   	pop    %eax
80102ccc:	5a                   	pop    %edx
80102ccd:	ff 34 9d cc 26 11 80 	pushl  -0x7feed934(,%ebx,4)
80102cd4:	ff 35 c4 26 11 80    	pushl  0x801126c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102cda:	83 c3 01             	add    $0x1,%ebx
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102cdd:	e8 ee d3 ff ff       	call   801000d0 <bread>
80102ce2:	89 c6                	mov    %eax,%esi
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102ce4:	8d 47 5c             	lea    0x5c(%edi),%eax
80102ce7:	83 c4 0c             	add    $0xc,%esp
80102cea:	68 00 02 00 00       	push   $0x200
80102cef:	50                   	push   %eax
80102cf0:	8d 46 5c             	lea    0x5c(%esi),%eax
80102cf3:	50                   	push   %eax
80102cf4:	e8 97 1a 00 00       	call   80104790 <memmove>
    bwrite(dbuf);  // write dst to disk
80102cf9:	89 34 24             	mov    %esi,(%esp)
80102cfc:	e8 9f d4 ff ff       	call   801001a0 <bwrite>
    brelse(lbuf);
80102d01:	89 3c 24             	mov    %edi,(%esp)
80102d04:	e8 d7 d4 ff ff       	call   801001e0 <brelse>
    brelse(dbuf);
80102d09:	89 34 24             	mov    %esi,(%esp)
80102d0c:	e8 cf d4 ff ff       	call   801001e0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102d11:	83 c4 10             	add    $0x10,%esp
80102d14:	39 1d c8 26 11 80    	cmp    %ebx,0x801126c8
80102d1a:	7f 94                	jg     80102cb0 <install_trans+0x20>
  }
}
80102d1c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102d1f:	5b                   	pop    %ebx
80102d20:	5e                   	pop    %esi
80102d21:	5f                   	pop    %edi
80102d22:	5d                   	pop    %ebp
80102d23:	c3                   	ret    
80102d24:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102d28:	f3 c3                	repz ret 
80102d2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80102d30 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80102d30:	55                   	push   %ebp
80102d31:	89 e5                	mov    %esp,%ebp
80102d33:	56                   	push   %esi
80102d34:	53                   	push   %ebx
  struct buf *buf = bread(log.dev, log.start);
80102d35:	83 ec 08             	sub    $0x8,%esp
80102d38:	ff 35 b4 26 11 80    	pushl  0x801126b4
80102d3e:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102d44:	e8 87 d3 ff ff       	call   801000d0 <bread>
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
80102d49:	8b 1d c8 26 11 80    	mov    0x801126c8,%ebx
  for (i = 0; i < log.lh.n; i++) {
80102d4f:	83 c4 10             	add    $0x10,%esp
  struct buf *buf = bread(log.dev, log.start);
80102d52:	89 c6                	mov    %eax,%esi
  for (i = 0; i < log.lh.n; i++) {
80102d54:	85 db                	test   %ebx,%ebx
  hb->n = log.lh.n;
80102d56:	89 58 5c             	mov    %ebx,0x5c(%eax)
  for (i = 0; i < log.lh.n; i++) {
80102d59:	7e 16                	jle    80102d71 <write_head+0x41>
80102d5b:	c1 e3 02             	shl    $0x2,%ebx
80102d5e:	31 d2                	xor    %edx,%edx
    hb->block[i] = log.lh.block[i];
80102d60:	8b 8a cc 26 11 80    	mov    -0x7feed934(%edx),%ecx
80102d66:	89 4c 16 60          	mov    %ecx,0x60(%esi,%edx,1)
80102d6a:	83 c2 04             	add    $0x4,%edx
  for (i = 0; i < log.lh.n; i++) {
80102d6d:	39 da                	cmp    %ebx,%edx
80102d6f:	75 ef                	jne    80102d60 <write_head+0x30>
  }
  bwrite(buf);
80102d71:	83 ec 0c             	sub    $0xc,%esp
80102d74:	56                   	push   %esi
80102d75:	e8 26 d4 ff ff       	call   801001a0 <bwrite>
  brelse(buf);
80102d7a:	89 34 24             	mov    %esi,(%esp)
80102d7d:	e8 5e d4 ff ff       	call   801001e0 <brelse>
}
80102d82:	83 c4 10             	add    $0x10,%esp
80102d85:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102d88:	5b                   	pop    %ebx
80102d89:	5e                   	pop    %esi
80102d8a:	5d                   	pop    %ebp
80102d8b:	c3                   	ret    
80102d8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102d90 <initlog>:
{
80102d90:	55                   	push   %ebp
80102d91:	89 e5                	mov    %esp,%ebp
80102d93:	53                   	push   %ebx
80102d94:	83 ec 2c             	sub    $0x2c,%esp
80102d97:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
80102d9a:	68 e0 75 10 80       	push   $0x801075e0
80102d9f:	68 80 26 11 80       	push   $0x80112680
80102da4:	e8 e7 16 00 00       	call   80104490 <initlock>
  readsb(dev, &sb);
80102da9:	58                   	pop    %eax
80102daa:	8d 45 dc             	lea    -0x24(%ebp),%eax
80102dad:	5a                   	pop    %edx
80102dae:	50                   	push   %eax
80102daf:	53                   	push   %ebx
80102db0:	e8 1b e9 ff ff       	call   801016d0 <readsb>
  log.size = sb.nlog;
80102db5:	8b 55 e8             	mov    -0x18(%ebp),%edx
  log.start = sb.logstart;
80102db8:	8b 45 ec             	mov    -0x14(%ebp),%eax
  struct buf *buf = bread(log.dev, log.start);
80102dbb:	59                   	pop    %ecx
  log.dev = dev;
80102dbc:	89 1d c4 26 11 80    	mov    %ebx,0x801126c4
  log.size = sb.nlog;
80102dc2:	89 15 b8 26 11 80    	mov    %edx,0x801126b8
  log.start = sb.logstart;
80102dc8:	a3 b4 26 11 80       	mov    %eax,0x801126b4
  struct buf *buf = bread(log.dev, log.start);
80102dcd:	5a                   	pop    %edx
80102dce:	50                   	push   %eax
80102dcf:	53                   	push   %ebx
80102dd0:	e8 fb d2 ff ff       	call   801000d0 <bread>
  log.lh.n = lh->n;
80102dd5:	8b 58 5c             	mov    0x5c(%eax),%ebx
  for (i = 0; i < log.lh.n; i++) {
80102dd8:	83 c4 10             	add    $0x10,%esp
80102ddb:	85 db                	test   %ebx,%ebx
  log.lh.n = lh->n;
80102ddd:	89 1d c8 26 11 80    	mov    %ebx,0x801126c8
  for (i = 0; i < log.lh.n; i++) {
80102de3:	7e 1c                	jle    80102e01 <initlog+0x71>
80102de5:	c1 e3 02             	shl    $0x2,%ebx
80102de8:	31 d2                	xor    %edx,%edx
80102dea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    log.lh.block[i] = lh->block[i];
80102df0:	8b 4c 10 60          	mov    0x60(%eax,%edx,1),%ecx
80102df4:	83 c2 04             	add    $0x4,%edx
80102df7:	89 8a c8 26 11 80    	mov    %ecx,-0x7feed938(%edx)
  for (i = 0; i < log.lh.n; i++) {
80102dfd:	39 d3                	cmp    %edx,%ebx
80102dff:	75 ef                	jne    80102df0 <initlog+0x60>
  brelse(buf);
80102e01:	83 ec 0c             	sub    $0xc,%esp
80102e04:	50                   	push   %eax
80102e05:	e8 d6 d3 ff ff       	call   801001e0 <brelse>

static void
recover_from_log(void)
{
  read_head();
  install_trans(); // if committed, copy from log to disk
80102e0a:	e8 81 fe ff ff       	call   80102c90 <install_trans>
  log.lh.n = 0;
80102e0f:	c7 05 c8 26 11 80 00 	movl   $0x0,0x801126c8
80102e16:	00 00 00 
  write_head(); // clear the log
80102e19:	e8 12 ff ff ff       	call   80102d30 <write_head>
}
80102e1e:	83 c4 10             	add    $0x10,%esp
80102e21:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102e24:	c9                   	leave  
80102e25:	c3                   	ret    
80102e26:	8d 76 00             	lea    0x0(%esi),%esi
80102e29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102e30 <begin_op>:
}

// called at the start of each FS system call.
void
begin_op(void)
{
80102e30:	55                   	push   %ebp
80102e31:	89 e5                	mov    %esp,%ebp
80102e33:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
80102e36:	68 80 26 11 80       	push   $0x80112680
80102e3b:	e8 90 17 00 00       	call   801045d0 <acquire>
80102e40:	83 c4 10             	add    $0x10,%esp
80102e43:	eb 18                	jmp    80102e5d <begin_op+0x2d>
80102e45:	8d 76 00             	lea    0x0(%esi),%esi
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
80102e48:	83 ec 08             	sub    $0x8,%esp
80102e4b:	68 80 26 11 80       	push   $0x80112680
80102e50:	68 80 26 11 80       	push   $0x80112680
80102e55:	e8 b6 11 00 00       	call   80104010 <sleep>
80102e5a:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
80102e5d:	a1 c0 26 11 80       	mov    0x801126c0,%eax
80102e62:	85 c0                	test   %eax,%eax
80102e64:	75 e2                	jne    80102e48 <begin_op+0x18>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80102e66:	a1 bc 26 11 80       	mov    0x801126bc,%eax
80102e6b:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
80102e71:	83 c0 01             	add    $0x1,%eax
80102e74:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102e77:	8d 14 4a             	lea    (%edx,%ecx,2),%edx
80102e7a:	83 fa 1e             	cmp    $0x1e,%edx
80102e7d:	7f c9                	jg     80102e48 <begin_op+0x18>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
      release(&log.lock);
80102e7f:	83 ec 0c             	sub    $0xc,%esp
      log.outstanding += 1;
80102e82:	a3 bc 26 11 80       	mov    %eax,0x801126bc
      release(&log.lock);
80102e87:	68 80 26 11 80       	push   $0x80112680
80102e8c:	e8 ff 17 00 00       	call   80104690 <release>
      break;
    }
  }
}
80102e91:	83 c4 10             	add    $0x10,%esp
80102e94:	c9                   	leave  
80102e95:	c3                   	ret    
80102e96:	8d 76 00             	lea    0x0(%esi),%esi
80102e99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80102ea0 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80102ea0:	55                   	push   %ebp
80102ea1:	89 e5                	mov    %esp,%ebp
80102ea3:	57                   	push   %edi
80102ea4:	56                   	push   %esi
80102ea5:	53                   	push   %ebx
80102ea6:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;

  acquire(&log.lock);
80102ea9:	68 80 26 11 80       	push   $0x80112680
80102eae:	e8 1d 17 00 00       	call   801045d0 <acquire>
  log.outstanding -= 1;
80102eb3:	a1 bc 26 11 80       	mov    0x801126bc,%eax
  if(log.committing)
80102eb8:	8b 35 c0 26 11 80    	mov    0x801126c0,%esi
80102ebe:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80102ec1:	8d 58 ff             	lea    -0x1(%eax),%ebx
  if(log.committing)
80102ec4:	85 f6                	test   %esi,%esi
  log.outstanding -= 1;
80102ec6:	89 1d bc 26 11 80    	mov    %ebx,0x801126bc
  if(log.committing)
80102ecc:	0f 85 1a 01 00 00    	jne    80102fec <end_op+0x14c>
    panic("log.committing");
  if(log.outstanding == 0){
80102ed2:	85 db                	test   %ebx,%ebx
80102ed4:	0f 85 ee 00 00 00    	jne    80102fc8 <end_op+0x128>
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    wakeup(&log);
  }
  release(&log.lock);
80102eda:	83 ec 0c             	sub    $0xc,%esp
    log.committing = 1;
80102edd:	c7 05 c0 26 11 80 01 	movl   $0x1,0x801126c0
80102ee4:	00 00 00 
  release(&log.lock);
80102ee7:	68 80 26 11 80       	push   $0x80112680
80102eec:	e8 9f 17 00 00       	call   80104690 <release>
}

static void
commit()
{
  if (log.lh.n > 0) {
80102ef1:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80102ef7:	83 c4 10             	add    $0x10,%esp
80102efa:	85 c9                	test   %ecx,%ecx
80102efc:	0f 8e 85 00 00 00    	jle    80102f87 <end_op+0xe7>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80102f02:	a1 b4 26 11 80       	mov    0x801126b4,%eax
80102f07:	83 ec 08             	sub    $0x8,%esp
80102f0a:	01 d8                	add    %ebx,%eax
80102f0c:	83 c0 01             	add    $0x1,%eax
80102f0f:	50                   	push   %eax
80102f10:	ff 35 c4 26 11 80    	pushl  0x801126c4
80102f16:	e8 b5 d1 ff ff       	call   801000d0 <bread>
80102f1b:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102f1d:	58                   	pop    %eax
80102f1e:	5a                   	pop    %edx
80102f1f:	ff 34 9d cc 26 11 80 	pushl  -0x7feed934(,%ebx,4)
80102f26:	ff 35 c4 26 11 80    	pushl  0x801126c4
  for (tail = 0; tail < log.lh.n; tail++) {
80102f2c:	83 c3 01             	add    $0x1,%ebx
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102f2f:	e8 9c d1 ff ff       	call   801000d0 <bread>
80102f34:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
80102f36:	8d 40 5c             	lea    0x5c(%eax),%eax
80102f39:	83 c4 0c             	add    $0xc,%esp
80102f3c:	68 00 02 00 00       	push   $0x200
80102f41:	50                   	push   %eax
80102f42:	8d 46 5c             	lea    0x5c(%esi),%eax
80102f45:	50                   	push   %eax
80102f46:	e8 45 18 00 00       	call   80104790 <memmove>
    bwrite(to);  // write the log
80102f4b:	89 34 24             	mov    %esi,(%esp)
80102f4e:	e8 4d d2 ff ff       	call   801001a0 <bwrite>
    brelse(from);
80102f53:	89 3c 24             	mov    %edi,(%esp)
80102f56:	e8 85 d2 ff ff       	call   801001e0 <brelse>
    brelse(to);
80102f5b:	89 34 24             	mov    %esi,(%esp)
80102f5e:	e8 7d d2 ff ff       	call   801001e0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102f63:	83 c4 10             	add    $0x10,%esp
80102f66:	3b 1d c8 26 11 80    	cmp    0x801126c8,%ebx
80102f6c:	7c 94                	jl     80102f02 <end_op+0x62>
    write_log();     // Write modified blocks from cache to log
    write_head();    // Write header to disk -- the real commit
80102f6e:	e8 bd fd ff ff       	call   80102d30 <write_head>
    install_trans(); // Now install writes to home locations
80102f73:	e8 18 fd ff ff       	call   80102c90 <install_trans>
    log.lh.n = 0;
80102f78:	c7 05 c8 26 11 80 00 	movl   $0x0,0x801126c8
80102f7f:	00 00 00 
    write_head();    // Erase the transaction from the log
80102f82:	e8 a9 fd ff ff       	call   80102d30 <write_head>
    acquire(&log.lock);
80102f87:	83 ec 0c             	sub    $0xc,%esp
80102f8a:	68 80 26 11 80       	push   $0x80112680
80102f8f:	e8 3c 16 00 00       	call   801045d0 <acquire>
    wakeup(&log);
80102f94:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
    log.committing = 0;
80102f9b:	c7 05 c0 26 11 80 00 	movl   $0x0,0x801126c0
80102fa2:	00 00 00 
    wakeup(&log);
80102fa5:	e8 16 12 00 00       	call   801041c0 <wakeup>
    release(&log.lock);
80102faa:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
80102fb1:	e8 da 16 00 00       	call   80104690 <release>
80102fb6:	83 c4 10             	add    $0x10,%esp
}
80102fb9:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102fbc:	5b                   	pop    %ebx
80102fbd:	5e                   	pop    %esi
80102fbe:	5f                   	pop    %edi
80102fbf:	5d                   	pop    %ebp
80102fc0:	c3                   	ret    
80102fc1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    wakeup(&log);
80102fc8:	83 ec 0c             	sub    $0xc,%esp
80102fcb:	68 80 26 11 80       	push   $0x80112680
80102fd0:	e8 eb 11 00 00       	call   801041c0 <wakeup>
  release(&log.lock);
80102fd5:	c7 04 24 80 26 11 80 	movl   $0x80112680,(%esp)
80102fdc:	e8 af 16 00 00       	call   80104690 <release>
80102fe1:	83 c4 10             	add    $0x10,%esp
}
80102fe4:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102fe7:	5b                   	pop    %ebx
80102fe8:	5e                   	pop    %esi
80102fe9:	5f                   	pop    %edi
80102fea:	5d                   	pop    %ebp
80102feb:	c3                   	ret    
    panic("log.committing");
80102fec:	83 ec 0c             	sub    $0xc,%esp
80102fef:	68 e4 75 10 80       	push   $0x801075e4
80102ff4:	e8 97 d3 ff ff       	call   80100390 <panic>
80102ff9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103000 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80103000:	55                   	push   %ebp
80103001:	89 e5                	mov    %esp,%ebp
80103003:	53                   	push   %ebx
80103004:	83 ec 04             	sub    $0x4,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103007:	8b 15 c8 26 11 80    	mov    0x801126c8,%edx
{
8010300d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80103010:	83 fa 1d             	cmp    $0x1d,%edx
80103013:	0f 8f 9d 00 00 00    	jg     801030b6 <log_write+0xb6>
80103019:	a1 b8 26 11 80       	mov    0x801126b8,%eax
8010301e:	83 e8 01             	sub    $0x1,%eax
80103021:	39 c2                	cmp    %eax,%edx
80103023:	0f 8d 8d 00 00 00    	jge    801030b6 <log_write+0xb6>
    panic("too big a transaction");
  if (log.outstanding < 1)
80103029:	a1 bc 26 11 80       	mov    0x801126bc,%eax
8010302e:	85 c0                	test   %eax,%eax
80103030:	0f 8e 8d 00 00 00    	jle    801030c3 <log_write+0xc3>
    panic("log_write outside of trans");

  acquire(&log.lock);
80103036:	83 ec 0c             	sub    $0xc,%esp
80103039:	68 80 26 11 80       	push   $0x80112680
8010303e:	e8 8d 15 00 00       	call   801045d0 <acquire>
  for (i = 0; i < log.lh.n; i++) {
80103043:	8b 0d c8 26 11 80    	mov    0x801126c8,%ecx
80103049:	83 c4 10             	add    $0x10,%esp
8010304c:	83 f9 00             	cmp    $0x0,%ecx
8010304f:	7e 57                	jle    801030a8 <log_write+0xa8>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103051:	8b 53 08             	mov    0x8(%ebx),%edx
  for (i = 0; i < log.lh.n; i++) {
80103054:	31 c0                	xor    %eax,%eax
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80103056:	3b 15 cc 26 11 80    	cmp    0x801126cc,%edx
8010305c:	75 0b                	jne    80103069 <log_write+0x69>
8010305e:	eb 38                	jmp    80103098 <log_write+0x98>
80103060:	39 14 85 cc 26 11 80 	cmp    %edx,-0x7feed934(,%eax,4)
80103067:	74 2f                	je     80103098 <log_write+0x98>
  for (i = 0; i < log.lh.n; i++) {
80103069:	83 c0 01             	add    $0x1,%eax
8010306c:	39 c1                	cmp    %eax,%ecx
8010306e:	75 f0                	jne    80103060 <log_write+0x60>
      break;
  }
  log.lh.block[i] = b->blockno;
80103070:	89 14 85 cc 26 11 80 	mov    %edx,-0x7feed934(,%eax,4)
  if (i == log.lh.n)
    log.lh.n++;
80103077:	83 c0 01             	add    $0x1,%eax
8010307a:	a3 c8 26 11 80       	mov    %eax,0x801126c8
  b->flags |= B_DIRTY; // prevent eviction
8010307f:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
80103082:	c7 45 08 80 26 11 80 	movl   $0x80112680,0x8(%ebp)
}
80103089:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010308c:	c9                   	leave  
  release(&log.lock);
8010308d:	e9 fe 15 00 00       	jmp    80104690 <release>
80103092:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  log.lh.block[i] = b->blockno;
80103098:	89 14 85 cc 26 11 80 	mov    %edx,-0x7feed934(,%eax,4)
8010309f:	eb de                	jmp    8010307f <log_write+0x7f>
801030a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801030a8:	8b 43 08             	mov    0x8(%ebx),%eax
801030ab:	a3 cc 26 11 80       	mov    %eax,0x801126cc
  if (i == log.lh.n)
801030b0:	75 cd                	jne    8010307f <log_write+0x7f>
801030b2:	31 c0                	xor    %eax,%eax
801030b4:	eb c1                	jmp    80103077 <log_write+0x77>
    panic("too big a transaction");
801030b6:	83 ec 0c             	sub    $0xc,%esp
801030b9:	68 f3 75 10 80       	push   $0x801075f3
801030be:	e8 cd d2 ff ff       	call   80100390 <panic>
    panic("log_write outside of trans");
801030c3:	83 ec 0c             	sub    $0xc,%esp
801030c6:	68 09 76 10 80       	push   $0x80107609
801030cb:	e8 c0 d2 ff ff       	call   80100390 <panic>

801030d0 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
801030d0:	55                   	push   %ebp
801030d1:	89 e5                	mov    %esp,%ebp
801030d3:	53                   	push   %ebx
801030d4:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
801030d7:	e8 74 09 00 00       	call   80103a50 <cpuid>
801030dc:	89 c3                	mov    %eax,%ebx
801030de:	e8 6d 09 00 00       	call   80103a50 <cpuid>
801030e3:	83 ec 04             	sub    $0x4,%esp
801030e6:	53                   	push   %ebx
801030e7:	50                   	push   %eax
801030e8:	68 24 76 10 80       	push   $0x80107624
801030ed:	e8 7e d6 ff ff       	call   80100770 <cprintf>
  idtinit();       // load idt register
801030f2:	e8 69 28 00 00       	call   80105960 <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
801030f7:	e8 d4 08 00 00       	call   801039d0 <mycpu>
801030fc:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
801030fe:	b8 01 00 00 00       	mov    $0x1,%eax
80103103:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
8010310a:	e8 21 0c 00 00       	call   80103d30 <scheduler>
8010310f:	90                   	nop

80103110 <mpenter>:
{
80103110:	55                   	push   %ebp
80103111:	89 e5                	mov    %esp,%ebp
80103113:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80103116:	e8 35 39 00 00       	call   80106a50 <switchkvm>
  seginit();
8010311b:	e8 a0 38 00 00       	call   801069c0 <seginit>
  lapicinit();
80103120:	e8 9b f7 ff ff       	call   801028c0 <lapicinit>
  mpmain();
80103125:	e8 a6 ff ff ff       	call   801030d0 <mpmain>
8010312a:	66 90                	xchg   %ax,%ax
8010312c:	66 90                	xchg   %ax,%ax
8010312e:	66 90                	xchg   %ax,%ax

80103130 <main>:
{
80103130:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80103134:	83 e4 f0             	and    $0xfffffff0,%esp
80103137:	ff 71 fc             	pushl  -0x4(%ecx)
8010313a:	55                   	push   %ebp
8010313b:	89 e5                	mov    %esp,%ebp
8010313d:	53                   	push   %ebx
8010313e:	51                   	push   %ecx
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
8010313f:	83 ec 08             	sub    $0x8,%esp
80103142:	68 00 00 40 80       	push   $0x80400000
80103147:	68 a8 54 11 80       	push   $0x801154a8
8010314c:	e8 2f f5 ff ff       	call   80102680 <kinit1>
  kvmalloc();      // kernel page table
80103151:	e8 ca 3d 00 00       	call   80106f20 <kvmalloc>
  mpinit();        // detect other processors
80103156:	e8 75 01 00 00       	call   801032d0 <mpinit>
  lapicinit();     // interrupt controller
8010315b:	e8 60 f7 ff ff       	call   801028c0 <lapicinit>
  seginit();       // segment descriptors
80103160:	e8 5b 38 00 00       	call   801069c0 <seginit>
  picinit();       // disable pic
80103165:	e8 46 03 00 00       	call   801034b0 <picinit>
  ioapicinit();    // another interrupt controller
8010316a:	e8 41 f3 ff ff       	call   801024b0 <ioapicinit>
  consoleinit();   // console hardware
8010316f:	e8 dc da ff ff       	call   80100c50 <consoleinit>
  uartinit();      // serial port
80103174:	e8 17 2b 00 00       	call   80105c90 <uartinit>
  pinit();         // process table
80103179:	e8 32 08 00 00       	call   801039b0 <pinit>
  tvinit();        // trap vectors
8010317e:	e8 5d 27 00 00       	call   801058e0 <tvinit>
  binit();         // buffer cache
80103183:	e8 b8 ce ff ff       	call   80100040 <binit>
  fileinit();      // file table
80103188:	e8 63 de ff ff       	call   80100ff0 <fileinit>
  ideinit();       // disk 
8010318d:	e8 fe f0 ff ff       	call   80102290 <ideinit>

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103192:	83 c4 0c             	add    $0xc,%esp
80103195:	68 8a 00 00 00       	push   $0x8a
8010319a:	68 8c a4 10 80       	push   $0x8010a48c
8010319f:	68 00 70 00 80       	push   $0x80007000
801031a4:	e8 e7 15 00 00       	call   80104790 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
801031a9:	69 05 00 2d 11 80 b0 	imul   $0xb0,0x80112d00,%eax
801031b0:	00 00 00 
801031b3:	83 c4 10             	add    $0x10,%esp
801031b6:	05 80 27 11 80       	add    $0x80112780,%eax
801031bb:	3d 80 27 11 80       	cmp    $0x80112780,%eax
801031c0:	76 71                	jbe    80103233 <main+0x103>
801031c2:	bb 80 27 11 80       	mov    $0x80112780,%ebx
801031c7:	89 f6                	mov    %esi,%esi
801031c9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    if(c == mycpu())  // We've started already.
801031d0:	e8 fb 07 00 00       	call   801039d0 <mycpu>
801031d5:	39 d8                	cmp    %ebx,%eax
801031d7:	74 41                	je     8010321a <main+0xea>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
801031d9:	e8 72 f5 ff ff       	call   80102750 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
801031de:	05 00 10 00 00       	add    $0x1000,%eax
    *(void(**)(void))(code-8) = mpenter;
801031e3:	c7 05 f8 6f 00 80 10 	movl   $0x80103110,0x80006ff8
801031ea:	31 10 80 
    *(int**)(code-12) = (void *) V2P(entrypgdir);
801031ed:	c7 05 f4 6f 00 80 00 	movl   $0x109000,0x80006ff4
801031f4:	90 10 00 
    *(void**)(code-4) = stack + KSTACKSIZE;
801031f7:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc

    lapicstartap(c->apicid, V2P(code));
801031fc:	0f b6 03             	movzbl (%ebx),%eax
801031ff:	83 ec 08             	sub    $0x8,%esp
80103202:	68 00 70 00 00       	push   $0x7000
80103207:	50                   	push   %eax
80103208:	e8 03 f8 ff ff       	call   80102a10 <lapicstartap>
8010320d:	83 c4 10             	add    $0x10,%esp

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80103210:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
80103216:	85 c0                	test   %eax,%eax
80103218:	74 f6                	je     80103210 <main+0xe0>
  for(c = cpus; c < cpus+ncpu; c++){
8010321a:	69 05 00 2d 11 80 b0 	imul   $0xb0,0x80112d00,%eax
80103221:	00 00 00 
80103224:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
8010322a:	05 80 27 11 80       	add    $0x80112780,%eax
8010322f:	39 c3                	cmp    %eax,%ebx
80103231:	72 9d                	jb     801031d0 <main+0xa0>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80103233:	83 ec 08             	sub    $0x8,%esp
80103236:	68 00 00 00 8e       	push   $0x8e000000
8010323b:	68 00 00 40 80       	push   $0x80400000
80103240:	e8 ab f4 ff ff       	call   801026f0 <kinit2>
  userinit();      // first user process
80103245:	e8 56 08 00 00       	call   80103aa0 <userinit>
  mpmain();        // finish this processor's setup
8010324a:	e8 81 fe ff ff       	call   801030d0 <mpmain>
8010324f:	90                   	nop

80103250 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103250:	55                   	push   %ebp
80103251:	89 e5                	mov    %esp,%ebp
80103253:	57                   	push   %edi
80103254:	56                   	push   %esi
  uchar *e, *p, *addr;

  addr = P2V(a);
80103255:	8d b0 00 00 00 80    	lea    -0x80000000(%eax),%esi
{
8010325b:	53                   	push   %ebx
  e = addr+len;
8010325c:	8d 1c 16             	lea    (%esi,%edx,1),%ebx
{
8010325f:	83 ec 0c             	sub    $0xc,%esp
  for(p = addr; p < e; p += sizeof(struct mp))
80103262:	39 de                	cmp    %ebx,%esi
80103264:	72 10                	jb     80103276 <mpsearch1+0x26>
80103266:	eb 50                	jmp    801032b8 <mpsearch1+0x68>
80103268:	90                   	nop
80103269:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103270:	39 fb                	cmp    %edi,%ebx
80103272:	89 fe                	mov    %edi,%esi
80103274:	76 42                	jbe    801032b8 <mpsearch1+0x68>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103276:	83 ec 04             	sub    $0x4,%esp
80103279:	8d 7e 10             	lea    0x10(%esi),%edi
8010327c:	6a 04                	push   $0x4
8010327e:	68 38 76 10 80       	push   $0x80107638
80103283:	56                   	push   %esi
80103284:	e8 a7 14 00 00       	call   80104730 <memcmp>
80103289:	83 c4 10             	add    $0x10,%esp
8010328c:	85 c0                	test   %eax,%eax
8010328e:	75 e0                	jne    80103270 <mpsearch1+0x20>
80103290:	89 f1                	mov    %esi,%ecx
80103292:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    sum += addr[i];
80103298:	0f b6 11             	movzbl (%ecx),%edx
8010329b:	83 c1 01             	add    $0x1,%ecx
8010329e:	01 d0                	add    %edx,%eax
  for(i=0; i<len; i++)
801032a0:	39 f9                	cmp    %edi,%ecx
801032a2:	75 f4                	jne    80103298 <mpsearch1+0x48>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
801032a4:	84 c0                	test   %al,%al
801032a6:	75 c8                	jne    80103270 <mpsearch1+0x20>
      return (struct mp*)p;
  return 0;
}
801032a8:	8d 65 f4             	lea    -0xc(%ebp),%esp
801032ab:	89 f0                	mov    %esi,%eax
801032ad:	5b                   	pop    %ebx
801032ae:	5e                   	pop    %esi
801032af:	5f                   	pop    %edi
801032b0:	5d                   	pop    %ebp
801032b1:	c3                   	ret    
801032b2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801032b8:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801032bb:	31 f6                	xor    %esi,%esi
}
801032bd:	89 f0                	mov    %esi,%eax
801032bf:	5b                   	pop    %ebx
801032c0:	5e                   	pop    %esi
801032c1:	5f                   	pop    %edi
801032c2:	5d                   	pop    %ebp
801032c3:	c3                   	ret    
801032c4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801032ca:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

801032d0 <mpinit>:
  return conf;
}

void
mpinit(void)
{
801032d0:	55                   	push   %ebp
801032d1:	89 e5                	mov    %esp,%ebp
801032d3:	57                   	push   %edi
801032d4:	56                   	push   %esi
801032d5:	53                   	push   %ebx
801032d6:	83 ec 1c             	sub    $0x1c,%esp
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
801032d9:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
801032e0:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
801032e7:	c1 e0 08             	shl    $0x8,%eax
801032ea:	09 d0                	or     %edx,%eax
801032ec:	c1 e0 04             	shl    $0x4,%eax
801032ef:	85 c0                	test   %eax,%eax
801032f1:	75 1b                	jne    8010330e <mpinit+0x3e>
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
801032f3:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
801032fa:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
80103301:	c1 e0 08             	shl    $0x8,%eax
80103304:	09 d0                	or     %edx,%eax
80103306:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
80103309:	2d 00 04 00 00       	sub    $0x400,%eax
    if((mp = mpsearch1(p, 1024)))
8010330e:	ba 00 04 00 00       	mov    $0x400,%edx
80103313:	e8 38 ff ff ff       	call   80103250 <mpsearch1>
80103318:	85 c0                	test   %eax,%eax
8010331a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010331d:	0f 84 3d 01 00 00    	je     80103460 <mpinit+0x190>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103323:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80103326:	8b 58 04             	mov    0x4(%eax),%ebx
80103329:	85 db                	test   %ebx,%ebx
8010332b:	0f 84 4f 01 00 00    	je     80103480 <mpinit+0x1b0>
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
80103331:	8d b3 00 00 00 80    	lea    -0x80000000(%ebx),%esi
  if(memcmp(conf, "PCMP", 4) != 0)
80103337:	83 ec 04             	sub    $0x4,%esp
8010333a:	6a 04                	push   $0x4
8010333c:	68 55 76 10 80       	push   $0x80107655
80103341:	56                   	push   %esi
80103342:	e8 e9 13 00 00       	call   80104730 <memcmp>
80103347:	83 c4 10             	add    $0x10,%esp
8010334a:	85 c0                	test   %eax,%eax
8010334c:	0f 85 2e 01 00 00    	jne    80103480 <mpinit+0x1b0>
  if(conf->version != 1 && conf->version != 4)
80103352:	0f b6 83 06 00 00 80 	movzbl -0x7ffffffa(%ebx),%eax
80103359:	3c 01                	cmp    $0x1,%al
8010335b:	0f 95 c2             	setne  %dl
8010335e:	3c 04                	cmp    $0x4,%al
80103360:	0f 95 c0             	setne  %al
80103363:	20 c2                	and    %al,%dl
80103365:	0f 85 15 01 00 00    	jne    80103480 <mpinit+0x1b0>
  if(sum((uchar*)conf, conf->length) != 0)
8010336b:	0f b7 bb 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edi
  for(i=0; i<len; i++)
80103372:	66 85 ff             	test   %di,%di
80103375:	74 1a                	je     80103391 <mpinit+0xc1>
80103377:	89 f0                	mov    %esi,%eax
80103379:	01 f7                	add    %esi,%edi
  sum = 0;
8010337b:	31 d2                	xor    %edx,%edx
8010337d:	8d 76 00             	lea    0x0(%esi),%esi
    sum += addr[i];
80103380:	0f b6 08             	movzbl (%eax),%ecx
80103383:	83 c0 01             	add    $0x1,%eax
80103386:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
80103388:	39 c7                	cmp    %eax,%edi
8010338a:	75 f4                	jne    80103380 <mpinit+0xb0>
8010338c:	84 d2                	test   %dl,%dl
8010338e:	0f 95 c2             	setne  %dl
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
80103391:	85 f6                	test   %esi,%esi
80103393:	0f 84 e7 00 00 00    	je     80103480 <mpinit+0x1b0>
80103399:	84 d2                	test   %dl,%dl
8010339b:	0f 85 df 00 00 00    	jne    80103480 <mpinit+0x1b0>
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
801033a1:	8b 83 24 00 00 80    	mov    -0x7fffffdc(%ebx),%eax
801033a7:	a3 7c 26 11 80       	mov    %eax,0x8011267c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801033ac:	0f b7 93 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edx
801033b3:	8d 83 2c 00 00 80    	lea    -0x7fffffd4(%ebx),%eax
  ismp = 1;
801033b9:	bb 01 00 00 00       	mov    $0x1,%ebx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801033be:	01 d6                	add    %edx,%esi
801033c0:	39 c6                	cmp    %eax,%esi
801033c2:	76 23                	jbe    801033e7 <mpinit+0x117>
    switch(*p){
801033c4:	0f b6 10             	movzbl (%eax),%edx
801033c7:	80 fa 04             	cmp    $0x4,%dl
801033ca:	0f 87 ca 00 00 00    	ja     8010349a <mpinit+0x1ca>
801033d0:	ff 24 95 7c 76 10 80 	jmp    *-0x7fef8984(,%edx,4)
801033d7:	89 f6                	mov    %esi,%esi
801033d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
801033e0:	83 c0 08             	add    $0x8,%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801033e3:	39 c6                	cmp    %eax,%esi
801033e5:	77 dd                	ja     801033c4 <mpinit+0xf4>
    default:
      ismp = 0;
      break;
    }
  }
  if(!ismp)
801033e7:	85 db                	test   %ebx,%ebx
801033e9:	0f 84 9e 00 00 00    	je     8010348d <mpinit+0x1bd>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
801033ef:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801033f2:	80 78 0c 00          	cmpb   $0x0,0xc(%eax)
801033f6:	74 15                	je     8010340d <mpinit+0x13d>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801033f8:	b8 70 00 00 00       	mov    $0x70,%eax
801033fd:	ba 22 00 00 00       	mov    $0x22,%edx
80103402:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80103403:	ba 23 00 00 00       	mov    $0x23,%edx
80103408:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80103409:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010340c:	ee                   	out    %al,(%dx)
  }
}
8010340d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103410:	5b                   	pop    %ebx
80103411:	5e                   	pop    %esi
80103412:	5f                   	pop    %edi
80103413:	5d                   	pop    %ebp
80103414:	c3                   	ret    
80103415:	8d 76 00             	lea    0x0(%esi),%esi
      if(ncpu < NCPU) {
80103418:	8b 0d 00 2d 11 80    	mov    0x80112d00,%ecx
8010341e:	83 f9 07             	cmp    $0x7,%ecx
80103421:	7f 19                	jg     8010343c <mpinit+0x16c>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103423:	0f b6 50 01          	movzbl 0x1(%eax),%edx
80103427:	69 f9 b0 00 00 00    	imul   $0xb0,%ecx,%edi
        ncpu++;
8010342d:	83 c1 01             	add    $0x1,%ecx
80103430:	89 0d 00 2d 11 80    	mov    %ecx,0x80112d00
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80103436:	88 97 80 27 11 80    	mov    %dl,-0x7feed880(%edi)
      p += sizeof(struct mpproc);
8010343c:	83 c0 14             	add    $0x14,%eax
      continue;
8010343f:	e9 7c ff ff ff       	jmp    801033c0 <mpinit+0xf0>
80103444:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      ioapicid = ioapic->apicno;
80103448:	0f b6 50 01          	movzbl 0x1(%eax),%edx
      p += sizeof(struct mpioapic);
8010344c:	83 c0 08             	add    $0x8,%eax
      ioapicid = ioapic->apicno;
8010344f:	88 15 60 27 11 80    	mov    %dl,0x80112760
      continue;
80103455:	e9 66 ff ff ff       	jmp    801033c0 <mpinit+0xf0>
8010345a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return mpsearch1(0xF0000, 0x10000);
80103460:	ba 00 00 01 00       	mov    $0x10000,%edx
80103465:	b8 00 00 0f 00       	mov    $0xf0000,%eax
8010346a:	e8 e1 fd ff ff       	call   80103250 <mpsearch1>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
8010346f:	85 c0                	test   %eax,%eax
  return mpsearch1(0xF0000, 0x10000);
80103471:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80103474:	0f 85 a9 fe ff ff    	jne    80103323 <mpinit+0x53>
8010347a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    panic("Expect to run on an SMP");
80103480:	83 ec 0c             	sub    $0xc,%esp
80103483:	68 3d 76 10 80       	push   $0x8010763d
80103488:	e8 03 cf ff ff       	call   80100390 <panic>
    panic("Didn't find a suitable machine");
8010348d:	83 ec 0c             	sub    $0xc,%esp
80103490:	68 5c 76 10 80       	push   $0x8010765c
80103495:	e8 f6 ce ff ff       	call   80100390 <panic>
      ismp = 0;
8010349a:	31 db                	xor    %ebx,%ebx
8010349c:	e9 26 ff ff ff       	jmp    801033c7 <mpinit+0xf7>
801034a1:	66 90                	xchg   %ax,%ax
801034a3:	66 90                	xchg   %ax,%ax
801034a5:	66 90                	xchg   %ax,%ax
801034a7:	66 90                	xchg   %ax,%ax
801034a9:	66 90                	xchg   %ax,%ax
801034ab:	66 90                	xchg   %ax,%ax
801034ad:	66 90                	xchg   %ax,%ax
801034af:	90                   	nop

801034b0 <picinit>:
#define IO_PIC2         0xA0    // Slave (IRQs 8-15)

// Don't use the 8259A interrupt controllers.  Xv6 assumes SMP hardware.
void
picinit(void)
{
801034b0:	55                   	push   %ebp
801034b1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801034b6:	ba 21 00 00 00       	mov    $0x21,%edx
801034bb:	89 e5                	mov    %esp,%ebp
801034bd:	ee                   	out    %al,(%dx)
801034be:	ba a1 00 00 00       	mov    $0xa1,%edx
801034c3:	ee                   	out    %al,(%dx)
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
  outb(IO_PIC2+1, 0xFF);
}
801034c4:	5d                   	pop    %ebp
801034c5:	c3                   	ret    
801034c6:	66 90                	xchg   %ax,%ax
801034c8:	66 90                	xchg   %ax,%ax
801034ca:	66 90                	xchg   %ax,%ax
801034cc:	66 90                	xchg   %ax,%ax
801034ce:	66 90                	xchg   %ax,%ax

801034d0 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
801034d0:	55                   	push   %ebp
801034d1:	89 e5                	mov    %esp,%ebp
801034d3:	57                   	push   %edi
801034d4:	56                   	push   %esi
801034d5:	53                   	push   %ebx
801034d6:	83 ec 0c             	sub    $0xc,%esp
801034d9:	8b 5d 08             	mov    0x8(%ebp),%ebx
801034dc:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
801034df:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
801034e5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
801034eb:	e8 20 db ff ff       	call   80101010 <filealloc>
801034f0:	85 c0                	test   %eax,%eax
801034f2:	89 03                	mov    %eax,(%ebx)
801034f4:	74 22                	je     80103518 <pipealloc+0x48>
801034f6:	e8 15 db ff ff       	call   80101010 <filealloc>
801034fb:	85 c0                	test   %eax,%eax
801034fd:	89 06                	mov    %eax,(%esi)
801034ff:	74 3f                	je     80103540 <pipealloc+0x70>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80103501:	e8 4a f2 ff ff       	call   80102750 <kalloc>
80103506:	85 c0                	test   %eax,%eax
80103508:	89 c7                	mov    %eax,%edi
8010350a:	75 54                	jne    80103560 <pipealloc+0x90>

//PAGEBREAK: 20
 bad:
  if(p)
    kfree((char*)p);
  if(*f0)
8010350c:	8b 03                	mov    (%ebx),%eax
8010350e:	85 c0                	test   %eax,%eax
80103510:	75 34                	jne    80103546 <pipealloc+0x76>
80103512:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    fileclose(*f0);
  if(*f1)
80103518:	8b 06                	mov    (%esi),%eax
8010351a:	85 c0                	test   %eax,%eax
8010351c:	74 0c                	je     8010352a <pipealloc+0x5a>
    fileclose(*f1);
8010351e:	83 ec 0c             	sub    $0xc,%esp
80103521:	50                   	push   %eax
80103522:	e8 a9 db ff ff       	call   801010d0 <fileclose>
80103527:	83 c4 10             	add    $0x10,%esp
  return -1;
}
8010352a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
8010352d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80103532:	5b                   	pop    %ebx
80103533:	5e                   	pop    %esi
80103534:	5f                   	pop    %edi
80103535:	5d                   	pop    %ebp
80103536:	c3                   	ret    
80103537:	89 f6                	mov    %esi,%esi
80103539:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  if(*f0)
80103540:	8b 03                	mov    (%ebx),%eax
80103542:	85 c0                	test   %eax,%eax
80103544:	74 e4                	je     8010352a <pipealloc+0x5a>
    fileclose(*f0);
80103546:	83 ec 0c             	sub    $0xc,%esp
80103549:	50                   	push   %eax
8010354a:	e8 81 db ff ff       	call   801010d0 <fileclose>
  if(*f1)
8010354f:	8b 06                	mov    (%esi),%eax
    fileclose(*f0);
80103551:	83 c4 10             	add    $0x10,%esp
  if(*f1)
80103554:	85 c0                	test   %eax,%eax
80103556:	75 c6                	jne    8010351e <pipealloc+0x4e>
80103558:	eb d0                	jmp    8010352a <pipealloc+0x5a>
8010355a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  initlock(&p->lock, "pipe");
80103560:	83 ec 08             	sub    $0x8,%esp
  p->readopen = 1;
80103563:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
8010356a:	00 00 00 
  p->writeopen = 1;
8010356d:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
80103574:	00 00 00 
  p->nwrite = 0;
80103577:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
8010357e:	00 00 00 
  p->nread = 0;
80103581:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80103588:	00 00 00 
  initlock(&p->lock, "pipe");
8010358b:	68 90 76 10 80       	push   $0x80107690
80103590:	50                   	push   %eax
80103591:	e8 fa 0e 00 00       	call   80104490 <initlock>
  (*f0)->type = FD_PIPE;
80103596:	8b 03                	mov    (%ebx),%eax
  return 0;
80103598:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
8010359b:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
801035a1:	8b 03                	mov    (%ebx),%eax
801035a3:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
801035a7:	8b 03                	mov    (%ebx),%eax
801035a9:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
801035ad:	8b 03                	mov    (%ebx),%eax
801035af:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
801035b2:	8b 06                	mov    (%esi),%eax
801035b4:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
801035ba:	8b 06                	mov    (%esi),%eax
801035bc:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
801035c0:	8b 06                	mov    (%esi),%eax
801035c2:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
801035c6:	8b 06                	mov    (%esi),%eax
801035c8:	89 78 0c             	mov    %edi,0xc(%eax)
}
801035cb:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
801035ce:	31 c0                	xor    %eax,%eax
}
801035d0:	5b                   	pop    %ebx
801035d1:	5e                   	pop    %esi
801035d2:	5f                   	pop    %edi
801035d3:	5d                   	pop    %ebp
801035d4:	c3                   	ret    
801035d5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801035d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801035e0 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
801035e0:	55                   	push   %ebp
801035e1:	89 e5                	mov    %esp,%ebp
801035e3:	56                   	push   %esi
801035e4:	53                   	push   %ebx
801035e5:	8b 5d 08             	mov    0x8(%ebp),%ebx
801035e8:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
801035eb:	83 ec 0c             	sub    $0xc,%esp
801035ee:	53                   	push   %ebx
801035ef:	e8 dc 0f 00 00       	call   801045d0 <acquire>
  if(writable){
801035f4:	83 c4 10             	add    $0x10,%esp
801035f7:	85 f6                	test   %esi,%esi
801035f9:	74 45                	je     80103640 <pipeclose+0x60>
    p->writeopen = 0;
    wakeup(&p->nread);
801035fb:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80103601:	83 ec 0c             	sub    $0xc,%esp
    p->writeopen = 0;
80103604:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
8010360b:	00 00 00 
    wakeup(&p->nread);
8010360e:	50                   	push   %eax
8010360f:	e8 ac 0b 00 00       	call   801041c0 <wakeup>
80103614:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
80103617:	8b 93 3c 02 00 00    	mov    0x23c(%ebx),%edx
8010361d:	85 d2                	test   %edx,%edx
8010361f:	75 0a                	jne    8010362b <pipeclose+0x4b>
80103621:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
80103627:	85 c0                	test   %eax,%eax
80103629:	74 35                	je     80103660 <pipeclose+0x80>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
8010362b:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
8010362e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103631:	5b                   	pop    %ebx
80103632:	5e                   	pop    %esi
80103633:	5d                   	pop    %ebp
    release(&p->lock);
80103634:	e9 57 10 00 00       	jmp    80104690 <release>
80103639:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    wakeup(&p->nwrite);
80103640:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
80103646:	83 ec 0c             	sub    $0xc,%esp
    p->readopen = 0;
80103649:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
80103650:	00 00 00 
    wakeup(&p->nwrite);
80103653:	50                   	push   %eax
80103654:	e8 67 0b 00 00       	call   801041c0 <wakeup>
80103659:	83 c4 10             	add    $0x10,%esp
8010365c:	eb b9                	jmp    80103617 <pipeclose+0x37>
8010365e:	66 90                	xchg   %ax,%ax
    release(&p->lock);
80103660:	83 ec 0c             	sub    $0xc,%esp
80103663:	53                   	push   %ebx
80103664:	e8 27 10 00 00       	call   80104690 <release>
    kfree((char*)p);
80103669:	89 5d 08             	mov    %ebx,0x8(%ebp)
8010366c:	83 c4 10             	add    $0x10,%esp
}
8010366f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103672:	5b                   	pop    %ebx
80103673:	5e                   	pop    %esi
80103674:	5d                   	pop    %ebp
    kfree((char*)p);
80103675:	e9 26 ef ff ff       	jmp    801025a0 <kfree>
8010367a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80103680 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
80103680:	55                   	push   %ebp
80103681:	89 e5                	mov    %esp,%ebp
80103683:	57                   	push   %edi
80103684:	56                   	push   %esi
80103685:	53                   	push   %ebx
80103686:	83 ec 28             	sub    $0x28,%esp
80103689:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
8010368c:	53                   	push   %ebx
8010368d:	e8 3e 0f 00 00       	call   801045d0 <acquire>
  for(i = 0; i < n; i++){
80103692:	8b 45 10             	mov    0x10(%ebp),%eax
80103695:	83 c4 10             	add    $0x10,%esp
80103698:	85 c0                	test   %eax,%eax
8010369a:	0f 8e c9 00 00 00    	jle    80103769 <pipewrite+0xe9>
801036a0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801036a3:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || myproc()->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
801036a9:	8d bb 34 02 00 00    	lea    0x234(%ebx),%edi
801036af:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
801036b2:	03 4d 10             	add    0x10(%ebp),%ecx
801036b5:	89 4d e0             	mov    %ecx,-0x20(%ebp)
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
801036b8:	8b 8b 34 02 00 00    	mov    0x234(%ebx),%ecx
801036be:	8d 91 00 02 00 00    	lea    0x200(%ecx),%edx
801036c4:	39 d0                	cmp    %edx,%eax
801036c6:	75 71                	jne    80103739 <pipewrite+0xb9>
      if(p->readopen == 0 || myproc()->killed){
801036c8:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
801036ce:	85 c0                	test   %eax,%eax
801036d0:	74 4e                	je     80103720 <pipewrite+0xa0>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
801036d2:	8d b3 38 02 00 00    	lea    0x238(%ebx),%esi
801036d8:	eb 3a                	jmp    80103714 <pipewrite+0x94>
801036da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      wakeup(&p->nread);
801036e0:	83 ec 0c             	sub    $0xc,%esp
801036e3:	57                   	push   %edi
801036e4:	e8 d7 0a 00 00       	call   801041c0 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
801036e9:	5a                   	pop    %edx
801036ea:	59                   	pop    %ecx
801036eb:	53                   	push   %ebx
801036ec:	56                   	push   %esi
801036ed:	e8 1e 09 00 00       	call   80104010 <sleep>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
801036f2:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
801036f8:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
801036fe:	83 c4 10             	add    $0x10,%esp
80103701:	05 00 02 00 00       	add    $0x200,%eax
80103706:	39 c2                	cmp    %eax,%edx
80103708:	75 36                	jne    80103740 <pipewrite+0xc0>
      if(p->readopen == 0 || myproc()->killed){
8010370a:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
80103710:	85 c0                	test   %eax,%eax
80103712:	74 0c                	je     80103720 <pipewrite+0xa0>
80103714:	e8 57 03 00 00       	call   80103a70 <myproc>
80103719:	8b 40 24             	mov    0x24(%eax),%eax
8010371c:	85 c0                	test   %eax,%eax
8010371e:	74 c0                	je     801036e0 <pipewrite+0x60>
        release(&p->lock);
80103720:	83 ec 0c             	sub    $0xc,%esp
80103723:	53                   	push   %ebx
80103724:	e8 67 0f 00 00       	call   80104690 <release>
        return -1;
80103729:	83 c4 10             	add    $0x10,%esp
8010372c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
80103731:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103734:	5b                   	pop    %ebx
80103735:	5e                   	pop    %esi
80103736:	5f                   	pop    %edi
80103737:	5d                   	pop    %ebp
80103738:	c3                   	ret    
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103739:	89 c2                	mov    %eax,%edx
8010373b:	90                   	nop
8010373c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80103740:	8b 75 e4             	mov    -0x1c(%ebp),%esi
80103743:	8d 42 01             	lea    0x1(%edx),%eax
80103746:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
8010374c:	89 83 38 02 00 00    	mov    %eax,0x238(%ebx)
80103752:	83 c6 01             	add    $0x1,%esi
80103755:	0f b6 4e ff          	movzbl -0x1(%esi),%ecx
  for(i = 0; i < n; i++){
80103759:	3b 75 e0             	cmp    -0x20(%ebp),%esi
8010375c:	89 75 e4             	mov    %esi,-0x1c(%ebp)
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
8010375f:	88 4c 13 34          	mov    %cl,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
80103763:	0f 85 4f ff ff ff    	jne    801036b8 <pipewrite+0x38>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
80103769:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
8010376f:	83 ec 0c             	sub    $0xc,%esp
80103772:	50                   	push   %eax
80103773:	e8 48 0a 00 00       	call   801041c0 <wakeup>
  release(&p->lock);
80103778:	89 1c 24             	mov    %ebx,(%esp)
8010377b:	e8 10 0f 00 00       	call   80104690 <release>
  return n;
80103780:	83 c4 10             	add    $0x10,%esp
80103783:	8b 45 10             	mov    0x10(%ebp),%eax
80103786:	eb a9                	jmp    80103731 <pipewrite+0xb1>
80103788:	90                   	nop
80103789:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103790 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80103790:	55                   	push   %ebp
80103791:	89 e5                	mov    %esp,%ebp
80103793:	57                   	push   %edi
80103794:	56                   	push   %esi
80103795:	53                   	push   %ebx
80103796:	83 ec 18             	sub    $0x18,%esp
80103799:	8b 75 08             	mov    0x8(%ebp),%esi
8010379c:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  acquire(&p->lock);
8010379f:	56                   	push   %esi
801037a0:	e8 2b 0e 00 00       	call   801045d0 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801037a5:	83 c4 10             	add    $0x10,%esp
801037a8:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
801037ae:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
801037b4:	75 6a                	jne    80103820 <piperead+0x90>
801037b6:	8b 9e 40 02 00 00    	mov    0x240(%esi),%ebx
801037bc:	85 db                	test   %ebx,%ebx
801037be:	0f 84 c4 00 00 00    	je     80103888 <piperead+0xf8>
    if(myproc()->killed){
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
801037c4:	8d 9e 34 02 00 00    	lea    0x234(%esi),%ebx
801037ca:	eb 2d                	jmp    801037f9 <piperead+0x69>
801037cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801037d0:	83 ec 08             	sub    $0x8,%esp
801037d3:	56                   	push   %esi
801037d4:	53                   	push   %ebx
801037d5:	e8 36 08 00 00       	call   80104010 <sleep>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
801037da:	83 c4 10             	add    $0x10,%esp
801037dd:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
801037e3:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
801037e9:	75 35                	jne    80103820 <piperead+0x90>
801037eb:	8b 96 40 02 00 00    	mov    0x240(%esi),%edx
801037f1:	85 d2                	test   %edx,%edx
801037f3:	0f 84 8f 00 00 00    	je     80103888 <piperead+0xf8>
    if(myproc()->killed){
801037f9:	e8 72 02 00 00       	call   80103a70 <myproc>
801037fe:	8b 48 24             	mov    0x24(%eax),%ecx
80103801:	85 c9                	test   %ecx,%ecx
80103803:	74 cb                	je     801037d0 <piperead+0x40>
      release(&p->lock);
80103805:	83 ec 0c             	sub    $0xc,%esp
      return -1;
80103808:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
      release(&p->lock);
8010380d:	56                   	push   %esi
8010380e:	e8 7d 0e 00 00       	call   80104690 <release>
      return -1;
80103813:	83 c4 10             	add    $0x10,%esp
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
  release(&p->lock);
  return i;
}
80103816:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103819:	89 d8                	mov    %ebx,%eax
8010381b:	5b                   	pop    %ebx
8010381c:	5e                   	pop    %esi
8010381d:	5f                   	pop    %edi
8010381e:	5d                   	pop    %ebp
8010381f:	c3                   	ret    
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103820:	8b 45 10             	mov    0x10(%ebp),%eax
80103823:	85 c0                	test   %eax,%eax
80103825:	7e 61                	jle    80103888 <piperead+0xf8>
    if(p->nread == p->nwrite)
80103827:	31 db                	xor    %ebx,%ebx
80103829:	eb 13                	jmp    8010383e <piperead+0xae>
8010382b:	90                   	nop
8010382c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103830:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
80103836:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
8010383c:	74 1f                	je     8010385d <piperead+0xcd>
    addr[i] = p->data[p->nread++ % PIPESIZE];
8010383e:	8d 41 01             	lea    0x1(%ecx),%eax
80103841:	81 e1 ff 01 00 00    	and    $0x1ff,%ecx
80103847:	89 86 34 02 00 00    	mov    %eax,0x234(%esi)
8010384d:	0f b6 44 0e 34       	movzbl 0x34(%esi,%ecx,1),%eax
80103852:	88 04 1f             	mov    %al,(%edi,%ebx,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103855:	83 c3 01             	add    $0x1,%ebx
80103858:	39 5d 10             	cmp    %ebx,0x10(%ebp)
8010385b:	75 d3                	jne    80103830 <piperead+0xa0>
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
8010385d:	8d 86 38 02 00 00    	lea    0x238(%esi),%eax
80103863:	83 ec 0c             	sub    $0xc,%esp
80103866:	50                   	push   %eax
80103867:	e8 54 09 00 00       	call   801041c0 <wakeup>
  release(&p->lock);
8010386c:	89 34 24             	mov    %esi,(%esp)
8010386f:	e8 1c 0e 00 00       	call   80104690 <release>
  return i;
80103874:	83 c4 10             	add    $0x10,%esp
}
80103877:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010387a:	89 d8                	mov    %ebx,%eax
8010387c:	5b                   	pop    %ebx
8010387d:	5e                   	pop    %esi
8010387e:	5f                   	pop    %edi
8010387f:	5d                   	pop    %ebp
80103880:	c3                   	ret    
80103881:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103888:	31 db                	xor    %ebx,%ebx
8010388a:	eb d1                	jmp    8010385d <piperead+0xcd>
8010388c:	66 90                	xchg   %ax,%ax
8010388e:	66 90                	xchg   %ax,%ax

80103890 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
80103890:	55                   	push   %ebp
80103891:	89 e5                	mov    %esp,%ebp
80103893:	53                   	push   %ebx
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103894:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
80103899:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
8010389c:	68 20 2d 11 80       	push   $0x80112d20
801038a1:	e8 2a 0d 00 00       	call   801045d0 <acquire>
801038a6:	83 c4 10             	add    $0x10,%esp
801038a9:	eb 10                	jmp    801038bb <allocproc+0x2b>
801038ab:	90                   	nop
801038ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801038b0:	83 c3 7c             	add    $0x7c,%ebx
801038b3:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
801038b9:	73 75                	jae    80103930 <allocproc+0xa0>
    if(p->state == UNUSED)
801038bb:	8b 43 0c             	mov    0xc(%ebx),%eax
801038be:	85 c0                	test   %eax,%eax
801038c0:	75 ee                	jne    801038b0 <allocproc+0x20>
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
801038c2:	a1 04 a0 10 80       	mov    0x8010a004,%eax

  release(&ptable.lock);
801038c7:	83 ec 0c             	sub    $0xc,%esp
  p->state = EMBRYO;
801038ca:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
801038d1:	8d 50 01             	lea    0x1(%eax),%edx
801038d4:	89 43 10             	mov    %eax,0x10(%ebx)
  release(&ptable.lock);
801038d7:	68 20 2d 11 80       	push   $0x80112d20
  p->pid = nextpid++;
801038dc:	89 15 04 a0 10 80    	mov    %edx,0x8010a004
  release(&ptable.lock);
801038e2:	e8 a9 0d 00 00       	call   80104690 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
801038e7:	e8 64 ee ff ff       	call   80102750 <kalloc>
801038ec:	83 c4 10             	add    $0x10,%esp
801038ef:	85 c0                	test   %eax,%eax
801038f1:	89 43 08             	mov    %eax,0x8(%ebx)
801038f4:	74 53                	je     80103949 <allocproc+0xb9>
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
801038f6:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
801038fc:	83 ec 04             	sub    $0x4,%esp
  sp -= sizeof *p->context;
801038ff:	05 9c 0f 00 00       	add    $0xf9c,%eax
  sp -= sizeof *p->tf;
80103904:	89 53 18             	mov    %edx,0x18(%ebx)
  *(uint*)sp = (uint)trapret;
80103907:	c7 40 14 d2 58 10 80 	movl   $0x801058d2,0x14(%eax)
  p->context = (struct context*)sp;
8010390e:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
80103911:	6a 14                	push   $0x14
80103913:	6a 00                	push   $0x0
80103915:	50                   	push   %eax
80103916:	e8 c5 0d 00 00       	call   801046e0 <memset>
  p->context->eip = (uint)forkret;
8010391b:	8b 43 1c             	mov    0x1c(%ebx),%eax

  return p;
8010391e:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
80103921:	c7 40 10 60 39 10 80 	movl   $0x80103960,0x10(%eax)
}
80103928:	89 d8                	mov    %ebx,%eax
8010392a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010392d:	c9                   	leave  
8010392e:	c3                   	ret    
8010392f:	90                   	nop
  release(&ptable.lock);
80103930:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80103933:	31 db                	xor    %ebx,%ebx
  release(&ptable.lock);
80103935:	68 20 2d 11 80       	push   $0x80112d20
8010393a:	e8 51 0d 00 00       	call   80104690 <release>
}
8010393f:	89 d8                	mov    %ebx,%eax
  return 0;
80103941:	83 c4 10             	add    $0x10,%esp
}
80103944:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103947:	c9                   	leave  
80103948:	c3                   	ret    
    p->state = UNUSED;
80103949:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
80103950:	31 db                	xor    %ebx,%ebx
80103952:	eb d4                	jmp    80103928 <allocproc+0x98>
80103954:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010395a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103960 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
80103960:	55                   	push   %ebp
80103961:	89 e5                	mov    %esp,%ebp
80103963:	83 ec 14             	sub    $0x14,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
80103966:	68 20 2d 11 80       	push   $0x80112d20
8010396b:	e8 20 0d 00 00       	call   80104690 <release>

  if (first) {
80103970:	a1 00 a0 10 80       	mov    0x8010a000,%eax
80103975:	83 c4 10             	add    $0x10,%esp
80103978:	85 c0                	test   %eax,%eax
8010397a:	75 04                	jne    80103980 <forkret+0x20>
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}
8010397c:	c9                   	leave  
8010397d:	c3                   	ret    
8010397e:	66 90                	xchg   %ax,%ax
    iinit(ROOTDEV);
80103980:	83 ec 0c             	sub    $0xc,%esp
    first = 0;
80103983:	c7 05 00 a0 10 80 00 	movl   $0x0,0x8010a000
8010398a:	00 00 00 
    iinit(ROOTDEV);
8010398d:	6a 01                	push   $0x1
8010398f:	e8 7c dd ff ff       	call   80101710 <iinit>
    initlog(ROOTDEV);
80103994:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010399b:	e8 f0 f3 ff ff       	call   80102d90 <initlog>
801039a0:	83 c4 10             	add    $0x10,%esp
}
801039a3:	c9                   	leave  
801039a4:	c3                   	ret    
801039a5:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801039a9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801039b0 <pinit>:
{
801039b0:	55                   	push   %ebp
801039b1:	89 e5                	mov    %esp,%ebp
801039b3:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
801039b6:	68 95 76 10 80       	push   $0x80107695
801039bb:	68 20 2d 11 80       	push   $0x80112d20
801039c0:	e8 cb 0a 00 00       	call   80104490 <initlock>
}
801039c5:	83 c4 10             	add    $0x10,%esp
801039c8:	c9                   	leave  
801039c9:	c3                   	ret    
801039ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801039d0 <mycpu>:
{
801039d0:	55                   	push   %ebp
801039d1:	89 e5                	mov    %esp,%ebp
801039d3:	56                   	push   %esi
801039d4:	53                   	push   %ebx
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801039d5:	9c                   	pushf  
801039d6:	58                   	pop    %eax
  if(readeflags()&FL_IF)
801039d7:	f6 c4 02             	test   $0x2,%ah
801039da:	75 5e                	jne    80103a3a <mycpu+0x6a>
  apicid = lapicid();
801039dc:	e8 df ef ff ff       	call   801029c0 <lapicid>
  for (i = 0; i < ncpu; ++i) {
801039e1:	8b 35 00 2d 11 80    	mov    0x80112d00,%esi
801039e7:	85 f6                	test   %esi,%esi
801039e9:	7e 42                	jle    80103a2d <mycpu+0x5d>
    if (cpus[i].apicid == apicid)
801039eb:	0f b6 15 80 27 11 80 	movzbl 0x80112780,%edx
801039f2:	39 d0                	cmp    %edx,%eax
801039f4:	74 30                	je     80103a26 <mycpu+0x56>
801039f6:	b9 30 28 11 80       	mov    $0x80112830,%ecx
  for (i = 0; i < ncpu; ++i) {
801039fb:	31 d2                	xor    %edx,%edx
801039fd:	8d 76 00             	lea    0x0(%esi),%esi
80103a00:	83 c2 01             	add    $0x1,%edx
80103a03:	39 f2                	cmp    %esi,%edx
80103a05:	74 26                	je     80103a2d <mycpu+0x5d>
    if (cpus[i].apicid == apicid)
80103a07:	0f b6 19             	movzbl (%ecx),%ebx
80103a0a:	81 c1 b0 00 00 00    	add    $0xb0,%ecx
80103a10:	39 c3                	cmp    %eax,%ebx
80103a12:	75 ec                	jne    80103a00 <mycpu+0x30>
80103a14:	69 c2 b0 00 00 00    	imul   $0xb0,%edx,%eax
80103a1a:	05 80 27 11 80       	add    $0x80112780,%eax
}
80103a1f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103a22:	5b                   	pop    %ebx
80103a23:	5e                   	pop    %esi
80103a24:	5d                   	pop    %ebp
80103a25:	c3                   	ret    
    if (cpus[i].apicid == apicid)
80103a26:	b8 80 27 11 80       	mov    $0x80112780,%eax
      return &cpus[i];
80103a2b:	eb f2                	jmp    80103a1f <mycpu+0x4f>
  panic("unknown apicid\n");
80103a2d:	83 ec 0c             	sub    $0xc,%esp
80103a30:	68 9c 76 10 80       	push   $0x8010769c
80103a35:	e8 56 c9 ff ff       	call   80100390 <panic>
    panic("mycpu called with interrupts enabled\n");
80103a3a:	83 ec 0c             	sub    $0xc,%esp
80103a3d:	68 78 77 10 80       	push   $0x80107778
80103a42:	e8 49 c9 ff ff       	call   80100390 <panic>
80103a47:	89 f6                	mov    %esi,%esi
80103a49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103a50 <cpuid>:
cpuid() {
80103a50:	55                   	push   %ebp
80103a51:	89 e5                	mov    %esp,%ebp
80103a53:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
80103a56:	e8 75 ff ff ff       	call   801039d0 <mycpu>
80103a5b:	2d 80 27 11 80       	sub    $0x80112780,%eax
}
80103a60:	c9                   	leave  
  return mycpu()-cpus;
80103a61:	c1 f8 04             	sar    $0x4,%eax
80103a64:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
80103a6a:	c3                   	ret    
80103a6b:	90                   	nop
80103a6c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103a70 <myproc>:
myproc(void) {
80103a70:	55                   	push   %ebp
80103a71:	89 e5                	mov    %esp,%ebp
80103a73:	53                   	push   %ebx
80103a74:	83 ec 04             	sub    $0x4,%esp
  pushcli();
80103a77:	e8 84 0a 00 00       	call   80104500 <pushcli>
  c = mycpu();
80103a7c:	e8 4f ff ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80103a81:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103a87:	e8 b4 0a 00 00       	call   80104540 <popcli>
}
80103a8c:	83 c4 04             	add    $0x4,%esp
80103a8f:	89 d8                	mov    %ebx,%eax
80103a91:	5b                   	pop    %ebx
80103a92:	5d                   	pop    %ebp
80103a93:	c3                   	ret    
80103a94:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103a9a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103aa0 <userinit>:
{
80103aa0:	55                   	push   %ebp
80103aa1:	89 e5                	mov    %esp,%ebp
80103aa3:	53                   	push   %ebx
80103aa4:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
80103aa7:	e8 e4 fd ff ff       	call   80103890 <allocproc>
80103aac:	89 c3                	mov    %eax,%ebx
  initproc = p;
80103aae:	a3 b8 a5 10 80       	mov    %eax,0x8010a5b8
  if((p->pgdir = setupkvm()) == 0)
80103ab3:	e8 e8 33 00 00       	call   80106ea0 <setupkvm>
80103ab8:	85 c0                	test   %eax,%eax
80103aba:	89 43 04             	mov    %eax,0x4(%ebx)
80103abd:	0f 84 bd 00 00 00    	je     80103b80 <userinit+0xe0>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80103ac3:	83 ec 04             	sub    $0x4,%esp
80103ac6:	68 2c 00 00 00       	push   $0x2c
80103acb:	68 60 a4 10 80       	push   $0x8010a460
80103ad0:	50                   	push   %eax
80103ad1:	e8 aa 30 00 00       	call   80106b80 <inituvm>
  memset(p->tf, 0, sizeof(*p->tf));
80103ad6:	83 c4 0c             	add    $0xc,%esp
  p->sz = PGSIZE;
80103ad9:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
80103adf:	6a 4c                	push   $0x4c
80103ae1:	6a 00                	push   $0x0
80103ae3:	ff 73 18             	pushl  0x18(%ebx)
80103ae6:	e8 f5 0b 00 00       	call   801046e0 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103aeb:	8b 43 18             	mov    0x18(%ebx),%eax
80103aee:	ba 1b 00 00 00       	mov    $0x1b,%edx
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103af3:	b9 23 00 00 00       	mov    $0x23,%ecx
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103af8:	83 c4 0c             	add    $0xc,%esp
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
80103afb:	66 89 50 3c          	mov    %dx,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
80103aff:	8b 43 18             	mov    0x18(%ebx),%eax
80103b02:	66 89 48 2c          	mov    %cx,0x2c(%eax)
  p->tf->es = p->tf->ds;
80103b06:	8b 43 18             	mov    0x18(%ebx),%eax
80103b09:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103b0d:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
80103b11:	8b 43 18             	mov    0x18(%ebx),%eax
80103b14:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
80103b18:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
80103b1c:	8b 43 18             	mov    0x18(%ebx),%eax
80103b1f:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
80103b26:	8b 43 18             	mov    0x18(%ebx),%eax
80103b29:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80103b30:	8b 43 18             	mov    0x18(%ebx),%eax
80103b33:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  safestrcpy(p->name, "initcode", sizeof(p->name));
80103b3a:	8d 43 6c             	lea    0x6c(%ebx),%eax
80103b3d:	6a 10                	push   $0x10
80103b3f:	68 c5 76 10 80       	push   $0x801076c5
80103b44:	50                   	push   %eax
80103b45:	e8 76 0d 00 00       	call   801048c0 <safestrcpy>
  p->cwd = namei("/");
80103b4a:	c7 04 24 ce 76 10 80 	movl   $0x801076ce,(%esp)
80103b51:	e8 1a e6 ff ff       	call   80102170 <namei>
80103b56:	89 43 68             	mov    %eax,0x68(%ebx)
  acquire(&ptable.lock);
80103b59:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103b60:	e8 6b 0a 00 00       	call   801045d0 <acquire>
  p->state = RUNNABLE;
80103b65:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  release(&ptable.lock);
80103b6c:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103b73:	e8 18 0b 00 00       	call   80104690 <release>
}
80103b78:	83 c4 10             	add    $0x10,%esp
80103b7b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103b7e:	c9                   	leave  
80103b7f:	c3                   	ret    
    panic("userinit: out of memory?");
80103b80:	83 ec 0c             	sub    $0xc,%esp
80103b83:	68 ac 76 10 80       	push   $0x801076ac
80103b88:	e8 03 c8 ff ff       	call   80100390 <panic>
80103b8d:	8d 76 00             	lea    0x0(%esi),%esi

80103b90 <growproc>:
{
80103b90:	55                   	push   %ebp
80103b91:	89 e5                	mov    %esp,%ebp
80103b93:	56                   	push   %esi
80103b94:	53                   	push   %ebx
80103b95:	8b 75 08             	mov    0x8(%ebp),%esi
  pushcli();
80103b98:	e8 63 09 00 00       	call   80104500 <pushcli>
  c = mycpu();
80103b9d:	e8 2e fe ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80103ba2:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103ba8:	e8 93 09 00 00       	call   80104540 <popcli>
  if(n > 0){
80103bad:	83 fe 00             	cmp    $0x0,%esi
  sz = curproc->sz;
80103bb0:	8b 03                	mov    (%ebx),%eax
  if(n > 0){
80103bb2:	7f 1c                	jg     80103bd0 <growproc+0x40>
  } else if(n < 0){
80103bb4:	75 3a                	jne    80103bf0 <growproc+0x60>
  switchuvm(curproc);
80103bb6:	83 ec 0c             	sub    $0xc,%esp
  curproc->sz = sz;
80103bb9:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
80103bbb:	53                   	push   %ebx
80103bbc:	e8 af 2e 00 00       	call   80106a70 <switchuvm>
  return 0;
80103bc1:	83 c4 10             	add    $0x10,%esp
80103bc4:	31 c0                	xor    %eax,%eax
}
80103bc6:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103bc9:	5b                   	pop    %ebx
80103bca:	5e                   	pop    %esi
80103bcb:	5d                   	pop    %ebp
80103bcc:	c3                   	ret    
80103bcd:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103bd0:	83 ec 04             	sub    $0x4,%esp
80103bd3:	01 c6                	add    %eax,%esi
80103bd5:	56                   	push   %esi
80103bd6:	50                   	push   %eax
80103bd7:	ff 73 04             	pushl  0x4(%ebx)
80103bda:	e8 e1 30 00 00       	call   80106cc0 <allocuvm>
80103bdf:	83 c4 10             	add    $0x10,%esp
80103be2:	85 c0                	test   %eax,%eax
80103be4:	75 d0                	jne    80103bb6 <growproc+0x26>
      return -1;
80103be6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103beb:	eb d9                	jmp    80103bc6 <growproc+0x36>
80103bed:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103bf0:	83 ec 04             	sub    $0x4,%esp
80103bf3:	01 c6                	add    %eax,%esi
80103bf5:	56                   	push   %esi
80103bf6:	50                   	push   %eax
80103bf7:	ff 73 04             	pushl  0x4(%ebx)
80103bfa:	e8 f1 31 00 00       	call   80106df0 <deallocuvm>
80103bff:	83 c4 10             	add    $0x10,%esp
80103c02:	85 c0                	test   %eax,%eax
80103c04:	75 b0                	jne    80103bb6 <growproc+0x26>
80103c06:	eb de                	jmp    80103be6 <growproc+0x56>
80103c08:	90                   	nop
80103c09:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80103c10 <fork>:
{
80103c10:	55                   	push   %ebp
80103c11:	89 e5                	mov    %esp,%ebp
80103c13:	57                   	push   %edi
80103c14:	56                   	push   %esi
80103c15:	53                   	push   %ebx
80103c16:	83 ec 1c             	sub    $0x1c,%esp
  pushcli();
80103c19:	e8 e2 08 00 00       	call   80104500 <pushcli>
  c = mycpu();
80103c1e:	e8 ad fd ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80103c23:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103c29:	e8 12 09 00 00       	call   80104540 <popcli>
  if((np = allocproc()) == 0){
80103c2e:	e8 5d fc ff ff       	call   80103890 <allocproc>
80103c33:	85 c0                	test   %eax,%eax
80103c35:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80103c38:	0f 84 b7 00 00 00    	je     80103cf5 <fork+0xe5>
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
80103c3e:	83 ec 08             	sub    $0x8,%esp
80103c41:	ff 33                	pushl  (%ebx)
80103c43:	ff 73 04             	pushl  0x4(%ebx)
80103c46:	89 c7                	mov    %eax,%edi
80103c48:	e8 23 33 00 00       	call   80106f70 <copyuvm>
80103c4d:	83 c4 10             	add    $0x10,%esp
80103c50:	85 c0                	test   %eax,%eax
80103c52:	89 47 04             	mov    %eax,0x4(%edi)
80103c55:	0f 84 a1 00 00 00    	je     80103cfc <fork+0xec>
  np->sz = curproc->sz;
80103c5b:	8b 03                	mov    (%ebx),%eax
80103c5d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80103c60:	89 01                	mov    %eax,(%ecx)
  np->parent = curproc;
80103c62:	89 59 14             	mov    %ebx,0x14(%ecx)
80103c65:	89 c8                	mov    %ecx,%eax
  *np->tf = *curproc->tf;
80103c67:	8b 79 18             	mov    0x18(%ecx),%edi
80103c6a:	8b 73 18             	mov    0x18(%ebx),%esi
80103c6d:	b9 13 00 00 00       	mov    $0x13,%ecx
80103c72:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  for(i = 0; i < NOFILE; i++)
80103c74:	31 f6                	xor    %esi,%esi
  np->tf->eax = 0;
80103c76:	8b 40 18             	mov    0x18(%eax),%eax
80103c79:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
    if(curproc->ofile[i])
80103c80:	8b 44 b3 28          	mov    0x28(%ebx,%esi,4),%eax
80103c84:	85 c0                	test   %eax,%eax
80103c86:	74 13                	je     80103c9b <fork+0x8b>
      np->ofile[i] = filedup(curproc->ofile[i]);
80103c88:	83 ec 0c             	sub    $0xc,%esp
80103c8b:	50                   	push   %eax
80103c8c:	e8 ef d3 ff ff       	call   80101080 <filedup>
80103c91:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103c94:	83 c4 10             	add    $0x10,%esp
80103c97:	89 44 b2 28          	mov    %eax,0x28(%edx,%esi,4)
  for(i = 0; i < NOFILE; i++)
80103c9b:	83 c6 01             	add    $0x1,%esi
80103c9e:	83 fe 10             	cmp    $0x10,%esi
80103ca1:	75 dd                	jne    80103c80 <fork+0x70>
  np->cwd = idup(curproc->cwd);
80103ca3:	83 ec 0c             	sub    $0xc,%esp
80103ca6:	ff 73 68             	pushl  0x68(%ebx)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103ca9:	83 c3 6c             	add    $0x6c,%ebx
  np->cwd = idup(curproc->cwd);
80103cac:	e8 2f dc ff ff       	call   801018e0 <idup>
80103cb1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103cb4:	83 c4 0c             	add    $0xc,%esp
  np->cwd = idup(curproc->cwd);
80103cb7:	89 47 68             	mov    %eax,0x68(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103cba:	8d 47 6c             	lea    0x6c(%edi),%eax
80103cbd:	6a 10                	push   $0x10
80103cbf:	53                   	push   %ebx
80103cc0:	50                   	push   %eax
80103cc1:	e8 fa 0b 00 00       	call   801048c0 <safestrcpy>
  pid = np->pid;
80103cc6:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
80103cc9:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103cd0:	e8 fb 08 00 00       	call   801045d0 <acquire>
  np->state = RUNNABLE;
80103cd5:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
  release(&ptable.lock);
80103cdc:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103ce3:	e8 a8 09 00 00       	call   80104690 <release>
  return pid;
80103ce8:	83 c4 10             	add    $0x10,%esp
}
80103ceb:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103cee:	89 d8                	mov    %ebx,%eax
80103cf0:	5b                   	pop    %ebx
80103cf1:	5e                   	pop    %esi
80103cf2:	5f                   	pop    %edi
80103cf3:	5d                   	pop    %ebp
80103cf4:	c3                   	ret    
    return -1;
80103cf5:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103cfa:	eb ef                	jmp    80103ceb <fork+0xdb>
    kfree(np->kstack);
80103cfc:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103cff:	83 ec 0c             	sub    $0xc,%esp
80103d02:	ff 73 08             	pushl  0x8(%ebx)
80103d05:	e8 96 e8 ff ff       	call   801025a0 <kfree>
    np->kstack = 0;
80103d0a:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    np->state = UNUSED;
80103d11:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
80103d18:	83 c4 10             	add    $0x10,%esp
80103d1b:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103d20:	eb c9                	jmp    80103ceb <fork+0xdb>
80103d22:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103d29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103d30 <scheduler>:
{
80103d30:	55                   	push   %ebp
80103d31:	89 e5                	mov    %esp,%ebp
80103d33:	57                   	push   %edi
80103d34:	56                   	push   %esi
80103d35:	53                   	push   %ebx
80103d36:	83 ec 0c             	sub    $0xc,%esp
  struct cpu *c = mycpu();
80103d39:	e8 92 fc ff ff       	call   801039d0 <mycpu>
80103d3e:	8d 78 04             	lea    0x4(%eax),%edi
80103d41:	89 c6                	mov    %eax,%esi
  c->proc = 0;
80103d43:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80103d4a:	00 00 00 
80103d4d:	8d 76 00             	lea    0x0(%esi),%esi
  asm volatile("sti");
80103d50:	fb                   	sti    
    acquire(&ptable.lock);
80103d51:	83 ec 0c             	sub    $0xc,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103d54:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
    acquire(&ptable.lock);
80103d59:	68 20 2d 11 80       	push   $0x80112d20
80103d5e:	e8 6d 08 00 00       	call   801045d0 <acquire>
80103d63:	83 c4 10             	add    $0x10,%esp
80103d66:	8d 76 00             	lea    0x0(%esi),%esi
80103d69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
      if(p->state != RUNNABLE)
80103d70:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80103d74:	75 33                	jne    80103da9 <scheduler+0x79>
      switchuvm(p);
80103d76:	83 ec 0c             	sub    $0xc,%esp
      c->proc = p;
80103d79:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
80103d7f:	53                   	push   %ebx
80103d80:	e8 eb 2c 00 00       	call   80106a70 <switchuvm>
      swtch(&(c->scheduler), p->context);
80103d85:	58                   	pop    %eax
80103d86:	5a                   	pop    %edx
80103d87:	ff 73 1c             	pushl  0x1c(%ebx)
80103d8a:	57                   	push   %edi
      p->state = RUNNING;
80103d8b:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
      swtch(&(c->scheduler), p->context);
80103d92:	e8 84 0b 00 00       	call   8010491b <swtch>
      switchkvm();
80103d97:	e8 b4 2c 00 00       	call   80106a50 <switchkvm>
      c->proc = 0;
80103d9c:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
80103da3:	00 00 00 
80103da6:	83 c4 10             	add    $0x10,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103da9:	83 c3 7c             	add    $0x7c,%ebx
80103dac:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80103db2:	72 bc                	jb     80103d70 <scheduler+0x40>
    release(&ptable.lock);
80103db4:	83 ec 0c             	sub    $0xc,%esp
80103db7:	68 20 2d 11 80       	push   $0x80112d20
80103dbc:	e8 cf 08 00 00       	call   80104690 <release>
    sti();
80103dc1:	83 c4 10             	add    $0x10,%esp
80103dc4:	eb 8a                	jmp    80103d50 <scheduler+0x20>
80103dc6:	8d 76 00             	lea    0x0(%esi),%esi
80103dc9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80103dd0 <sched>:
{
80103dd0:	55                   	push   %ebp
80103dd1:	89 e5                	mov    %esp,%ebp
80103dd3:	56                   	push   %esi
80103dd4:	53                   	push   %ebx
  pushcli();
80103dd5:	e8 26 07 00 00       	call   80104500 <pushcli>
  c = mycpu();
80103dda:	e8 f1 fb ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80103ddf:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103de5:	e8 56 07 00 00       	call   80104540 <popcli>
  if(!holding(&ptable.lock))
80103dea:	83 ec 0c             	sub    $0xc,%esp
80103ded:	68 20 2d 11 80       	push   $0x80112d20
80103df2:	e8 a9 07 00 00       	call   801045a0 <holding>
80103df7:	83 c4 10             	add    $0x10,%esp
80103dfa:	85 c0                	test   %eax,%eax
80103dfc:	74 4f                	je     80103e4d <sched+0x7d>
  if(mycpu()->ncli != 1)
80103dfe:	e8 cd fb ff ff       	call   801039d0 <mycpu>
80103e03:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
80103e0a:	75 68                	jne    80103e74 <sched+0xa4>
  if(p->state == RUNNING)
80103e0c:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80103e10:	74 55                	je     80103e67 <sched+0x97>
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103e12:	9c                   	pushf  
80103e13:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103e14:	f6 c4 02             	test   $0x2,%ah
80103e17:	75 41                	jne    80103e5a <sched+0x8a>
  intena = mycpu()->intena;
80103e19:	e8 b2 fb ff ff       	call   801039d0 <mycpu>
  swtch(&p->context, mycpu()->scheduler);
80103e1e:	83 c3 1c             	add    $0x1c,%ebx
  intena = mycpu()->intena;
80103e21:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
80103e27:	e8 a4 fb ff ff       	call   801039d0 <mycpu>
80103e2c:	83 ec 08             	sub    $0x8,%esp
80103e2f:	ff 70 04             	pushl  0x4(%eax)
80103e32:	53                   	push   %ebx
80103e33:	e8 e3 0a 00 00       	call   8010491b <swtch>
  mycpu()->intena = intena;
80103e38:	e8 93 fb ff ff       	call   801039d0 <mycpu>
}
80103e3d:	83 c4 10             	add    $0x10,%esp
  mycpu()->intena = intena;
80103e40:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
80103e46:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103e49:	5b                   	pop    %ebx
80103e4a:	5e                   	pop    %esi
80103e4b:	5d                   	pop    %ebp
80103e4c:	c3                   	ret    
    panic("sched ptable.lock");
80103e4d:	83 ec 0c             	sub    $0xc,%esp
80103e50:	68 d0 76 10 80       	push   $0x801076d0
80103e55:	e8 36 c5 ff ff       	call   80100390 <panic>
    panic("sched interruptible");
80103e5a:	83 ec 0c             	sub    $0xc,%esp
80103e5d:	68 fc 76 10 80       	push   $0x801076fc
80103e62:	e8 29 c5 ff ff       	call   80100390 <panic>
    panic("sched running");
80103e67:	83 ec 0c             	sub    $0xc,%esp
80103e6a:	68 ee 76 10 80       	push   $0x801076ee
80103e6f:	e8 1c c5 ff ff       	call   80100390 <panic>
    panic("sched locks");
80103e74:	83 ec 0c             	sub    $0xc,%esp
80103e77:	68 e2 76 10 80       	push   $0x801076e2
80103e7c:	e8 0f c5 ff ff       	call   80100390 <panic>
80103e81:	eb 0d                	jmp    80103e90 <exit>
80103e83:	90                   	nop
80103e84:	90                   	nop
80103e85:	90                   	nop
80103e86:	90                   	nop
80103e87:	90                   	nop
80103e88:	90                   	nop
80103e89:	90                   	nop
80103e8a:	90                   	nop
80103e8b:	90                   	nop
80103e8c:	90                   	nop
80103e8d:	90                   	nop
80103e8e:	90                   	nop
80103e8f:	90                   	nop

80103e90 <exit>:
{
80103e90:	55                   	push   %ebp
80103e91:	89 e5                	mov    %esp,%ebp
80103e93:	57                   	push   %edi
80103e94:	56                   	push   %esi
80103e95:	53                   	push   %ebx
80103e96:	83 ec 0c             	sub    $0xc,%esp
  pushcli();
80103e99:	e8 62 06 00 00       	call   80104500 <pushcli>
  c = mycpu();
80103e9e:	e8 2d fb ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80103ea3:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
80103ea9:	e8 92 06 00 00       	call   80104540 <popcli>
  if(curproc == initproc)
80103eae:	39 35 b8 a5 10 80    	cmp    %esi,0x8010a5b8
80103eb4:	8d 5e 28             	lea    0x28(%esi),%ebx
80103eb7:	8d 7e 68             	lea    0x68(%esi),%edi
80103eba:	0f 84 e7 00 00 00    	je     80103fa7 <exit+0x117>
    if(curproc->ofile[fd]){
80103ec0:	8b 03                	mov    (%ebx),%eax
80103ec2:	85 c0                	test   %eax,%eax
80103ec4:	74 12                	je     80103ed8 <exit+0x48>
      fileclose(curproc->ofile[fd]);
80103ec6:	83 ec 0c             	sub    $0xc,%esp
80103ec9:	50                   	push   %eax
80103eca:	e8 01 d2 ff ff       	call   801010d0 <fileclose>
      curproc->ofile[fd] = 0;
80103ecf:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
80103ed5:	83 c4 10             	add    $0x10,%esp
80103ed8:	83 c3 04             	add    $0x4,%ebx
  for(fd = 0; fd < NOFILE; fd++){
80103edb:	39 fb                	cmp    %edi,%ebx
80103edd:	75 e1                	jne    80103ec0 <exit+0x30>
  begin_op();
80103edf:	e8 4c ef ff ff       	call   80102e30 <begin_op>
  iput(curproc->cwd);
80103ee4:	83 ec 0c             	sub    $0xc,%esp
80103ee7:	ff 76 68             	pushl  0x68(%esi)
80103eea:	e8 51 db ff ff       	call   80101a40 <iput>
  end_op();
80103eef:	e8 ac ef ff ff       	call   80102ea0 <end_op>
  curproc->cwd = 0;
80103ef4:	c7 46 68 00 00 00 00 	movl   $0x0,0x68(%esi)
  acquire(&ptable.lock);
80103efb:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103f02:	e8 c9 06 00 00       	call   801045d0 <acquire>
  wakeup1(curproc->parent);
80103f07:	8b 56 14             	mov    0x14(%esi),%edx
80103f0a:	83 c4 10             	add    $0x10,%esp
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f0d:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80103f12:	eb 0e                	jmp    80103f22 <exit+0x92>
80103f14:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103f18:	83 c0 7c             	add    $0x7c,%eax
80103f1b:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80103f20:	73 1c                	jae    80103f3e <exit+0xae>
    if(p->state == SLEEPING && p->chan == chan)
80103f22:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80103f26:	75 f0                	jne    80103f18 <exit+0x88>
80103f28:	3b 50 20             	cmp    0x20(%eax),%edx
80103f2b:	75 eb                	jne    80103f18 <exit+0x88>
      p->state = RUNNABLE;
80103f2d:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f34:	83 c0 7c             	add    $0x7c,%eax
80103f37:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80103f3c:	72 e4                	jb     80103f22 <exit+0x92>
      p->parent = initproc;
80103f3e:	8b 0d b8 a5 10 80    	mov    0x8010a5b8,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103f44:	ba 54 2d 11 80       	mov    $0x80112d54,%edx
80103f49:	eb 10                	jmp    80103f5b <exit+0xcb>
80103f4b:	90                   	nop
80103f4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103f50:	83 c2 7c             	add    $0x7c,%edx
80103f53:	81 fa 54 4c 11 80    	cmp    $0x80114c54,%edx
80103f59:	73 33                	jae    80103f8e <exit+0xfe>
    if(p->parent == curproc){
80103f5b:	39 72 14             	cmp    %esi,0x14(%edx)
80103f5e:	75 f0                	jne    80103f50 <exit+0xc0>
      if(p->state == ZOMBIE)
80103f60:	83 7a 0c 05          	cmpl   $0x5,0xc(%edx)
      p->parent = initproc;
80103f64:	89 4a 14             	mov    %ecx,0x14(%edx)
      if(p->state == ZOMBIE)
80103f67:	75 e7                	jne    80103f50 <exit+0xc0>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103f69:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
80103f6e:	eb 0a                	jmp    80103f7a <exit+0xea>
80103f70:	83 c0 7c             	add    $0x7c,%eax
80103f73:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80103f78:	73 d6                	jae    80103f50 <exit+0xc0>
    if(p->state == SLEEPING && p->chan == chan)
80103f7a:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80103f7e:	75 f0                	jne    80103f70 <exit+0xe0>
80103f80:	3b 48 20             	cmp    0x20(%eax),%ecx
80103f83:	75 eb                	jne    80103f70 <exit+0xe0>
      p->state = RUNNABLE;
80103f85:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
80103f8c:	eb e2                	jmp    80103f70 <exit+0xe0>
  curproc->state = ZOMBIE;
80103f8e:	c7 46 0c 05 00 00 00 	movl   $0x5,0xc(%esi)
  sched();
80103f95:	e8 36 fe ff ff       	call   80103dd0 <sched>
  panic("zombie exit");
80103f9a:	83 ec 0c             	sub    $0xc,%esp
80103f9d:	68 1d 77 10 80       	push   $0x8010771d
80103fa2:	e8 e9 c3 ff ff       	call   80100390 <panic>
    panic("init exiting");
80103fa7:	83 ec 0c             	sub    $0xc,%esp
80103faa:	68 10 77 10 80       	push   $0x80107710
80103faf:	e8 dc c3 ff ff       	call   80100390 <panic>
80103fb4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103fba:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80103fc0 <yield>:
{
80103fc0:	55                   	push   %ebp
80103fc1:	89 e5                	mov    %esp,%ebp
80103fc3:	53                   	push   %ebx
80103fc4:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
80103fc7:	68 20 2d 11 80       	push   $0x80112d20
80103fcc:	e8 ff 05 00 00       	call   801045d0 <acquire>
  pushcli();
80103fd1:	e8 2a 05 00 00       	call   80104500 <pushcli>
  c = mycpu();
80103fd6:	e8 f5 f9 ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80103fdb:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103fe1:	e8 5a 05 00 00       	call   80104540 <popcli>
  myproc()->state = RUNNABLE;
80103fe6:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  sched();
80103fed:	e8 de fd ff ff       	call   80103dd0 <sched>
  release(&ptable.lock);
80103ff2:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
80103ff9:	e8 92 06 00 00       	call   80104690 <release>
}
80103ffe:	83 c4 10             	add    $0x10,%esp
80104001:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104004:	c9                   	leave  
80104005:	c3                   	ret    
80104006:	8d 76 00             	lea    0x0(%esi),%esi
80104009:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104010 <sleep>:
{
80104010:	55                   	push   %ebp
80104011:	89 e5                	mov    %esp,%ebp
80104013:	57                   	push   %edi
80104014:	56                   	push   %esi
80104015:	53                   	push   %ebx
80104016:	83 ec 0c             	sub    $0xc,%esp
80104019:	8b 7d 08             	mov    0x8(%ebp),%edi
8010401c:	8b 75 0c             	mov    0xc(%ebp),%esi
  pushcli();
8010401f:	e8 dc 04 00 00       	call   80104500 <pushcli>
  c = mycpu();
80104024:	e8 a7 f9 ff ff       	call   801039d0 <mycpu>
  p = c->proc;
80104029:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
8010402f:	e8 0c 05 00 00       	call   80104540 <popcli>
  if(p == 0)
80104034:	85 db                	test   %ebx,%ebx
80104036:	0f 84 87 00 00 00    	je     801040c3 <sleep+0xb3>
  if(lk == 0)
8010403c:	85 f6                	test   %esi,%esi
8010403e:	74 76                	je     801040b6 <sleep+0xa6>
  if(lk != &ptable.lock){  //DOC: sleeplock0
80104040:	81 fe 20 2d 11 80    	cmp    $0x80112d20,%esi
80104046:	74 50                	je     80104098 <sleep+0x88>
    acquire(&ptable.lock);  //DOC: sleeplock1
80104048:	83 ec 0c             	sub    $0xc,%esp
8010404b:	68 20 2d 11 80       	push   $0x80112d20
80104050:	e8 7b 05 00 00       	call   801045d0 <acquire>
    release(lk);
80104055:	89 34 24             	mov    %esi,(%esp)
80104058:	e8 33 06 00 00       	call   80104690 <release>
  p->chan = chan;
8010405d:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
80104060:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80104067:	e8 64 fd ff ff       	call   80103dd0 <sched>
  p->chan = 0;
8010406c:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
    release(&ptable.lock);
80104073:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
8010407a:	e8 11 06 00 00       	call   80104690 <release>
    acquire(lk);
8010407f:	89 75 08             	mov    %esi,0x8(%ebp)
80104082:	83 c4 10             	add    $0x10,%esp
}
80104085:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104088:	5b                   	pop    %ebx
80104089:	5e                   	pop    %esi
8010408a:	5f                   	pop    %edi
8010408b:	5d                   	pop    %ebp
    acquire(lk);
8010408c:	e9 3f 05 00 00       	jmp    801045d0 <acquire>
80104091:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  p->chan = chan;
80104098:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
8010409b:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
801040a2:	e8 29 fd ff ff       	call   80103dd0 <sched>
  p->chan = 0;
801040a7:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
}
801040ae:	8d 65 f4             	lea    -0xc(%ebp),%esp
801040b1:	5b                   	pop    %ebx
801040b2:	5e                   	pop    %esi
801040b3:	5f                   	pop    %edi
801040b4:	5d                   	pop    %ebp
801040b5:	c3                   	ret    
    panic("sleep without lk");
801040b6:	83 ec 0c             	sub    $0xc,%esp
801040b9:	68 2f 77 10 80       	push   $0x8010772f
801040be:	e8 cd c2 ff ff       	call   80100390 <panic>
    panic("sleep");
801040c3:	83 ec 0c             	sub    $0xc,%esp
801040c6:	68 29 77 10 80       	push   $0x80107729
801040cb:	e8 c0 c2 ff ff       	call   80100390 <panic>

801040d0 <wait>:
{
801040d0:	55                   	push   %ebp
801040d1:	89 e5                	mov    %esp,%ebp
801040d3:	56                   	push   %esi
801040d4:	53                   	push   %ebx
  pushcli();
801040d5:	e8 26 04 00 00       	call   80104500 <pushcli>
  c = mycpu();
801040da:	e8 f1 f8 ff ff       	call   801039d0 <mycpu>
  p = c->proc;
801040df:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
801040e5:	e8 56 04 00 00       	call   80104540 <popcli>
  acquire(&ptable.lock);
801040ea:	83 ec 0c             	sub    $0xc,%esp
801040ed:	68 20 2d 11 80       	push   $0x80112d20
801040f2:	e8 d9 04 00 00       	call   801045d0 <acquire>
801040f7:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
801040fa:	31 c0                	xor    %eax,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801040fc:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
80104101:	eb 10                	jmp    80104113 <wait+0x43>
80104103:	90                   	nop
80104104:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104108:	83 c3 7c             	add    $0x7c,%ebx
8010410b:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
80104111:	73 1b                	jae    8010412e <wait+0x5e>
      if(p->parent != curproc)
80104113:	39 73 14             	cmp    %esi,0x14(%ebx)
80104116:	75 f0                	jne    80104108 <wait+0x38>
      if(p->state == ZOMBIE){
80104118:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
8010411c:	74 32                	je     80104150 <wait+0x80>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010411e:	83 c3 7c             	add    $0x7c,%ebx
      havekids = 1;
80104121:	b8 01 00 00 00       	mov    $0x1,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104126:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
8010412c:	72 e5                	jb     80104113 <wait+0x43>
    if(!havekids || curproc->killed){
8010412e:	85 c0                	test   %eax,%eax
80104130:	74 74                	je     801041a6 <wait+0xd6>
80104132:	8b 46 24             	mov    0x24(%esi),%eax
80104135:	85 c0                	test   %eax,%eax
80104137:	75 6d                	jne    801041a6 <wait+0xd6>
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
80104139:	83 ec 08             	sub    $0x8,%esp
8010413c:	68 20 2d 11 80       	push   $0x80112d20
80104141:	56                   	push   %esi
80104142:	e8 c9 fe ff ff       	call   80104010 <sleep>
    havekids = 0;
80104147:	83 c4 10             	add    $0x10,%esp
8010414a:	eb ae                	jmp    801040fa <wait+0x2a>
8010414c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        kfree(p->kstack);
80104150:	83 ec 0c             	sub    $0xc,%esp
80104153:	ff 73 08             	pushl  0x8(%ebx)
        pid = p->pid;
80104156:	8b 73 10             	mov    0x10(%ebx),%esi
        kfree(p->kstack);
80104159:	e8 42 e4 ff ff       	call   801025a0 <kfree>
        freevm(p->pgdir);
8010415e:	5a                   	pop    %edx
8010415f:	ff 73 04             	pushl  0x4(%ebx)
        p->kstack = 0;
80104162:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
80104169:	e8 b2 2c 00 00       	call   80106e20 <freevm>
        release(&ptable.lock);
8010416e:	c7 04 24 20 2d 11 80 	movl   $0x80112d20,(%esp)
        p->pid = 0;
80104175:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
8010417c:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
80104183:	c6 43 6c 00          	movb   $0x0,0x6c(%ebx)
        p->killed = 0;
80104187:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        p->state = UNUSED;
8010418e:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        release(&ptable.lock);
80104195:	e8 f6 04 00 00       	call   80104690 <release>
        return pid;
8010419a:	83 c4 10             	add    $0x10,%esp
}
8010419d:	8d 65 f8             	lea    -0x8(%ebp),%esp
801041a0:	89 f0                	mov    %esi,%eax
801041a2:	5b                   	pop    %ebx
801041a3:	5e                   	pop    %esi
801041a4:	5d                   	pop    %ebp
801041a5:	c3                   	ret    
      release(&ptable.lock);
801041a6:	83 ec 0c             	sub    $0xc,%esp
      return -1;
801041a9:	be ff ff ff ff       	mov    $0xffffffff,%esi
      release(&ptable.lock);
801041ae:	68 20 2d 11 80       	push   $0x80112d20
801041b3:	e8 d8 04 00 00       	call   80104690 <release>
      return -1;
801041b8:	83 c4 10             	add    $0x10,%esp
801041bb:	eb e0                	jmp    8010419d <wait+0xcd>
801041bd:	8d 76 00             	lea    0x0(%esi),%esi

801041c0 <wakeup>:
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
801041c0:	55                   	push   %ebp
801041c1:	89 e5                	mov    %esp,%ebp
801041c3:	53                   	push   %ebx
801041c4:	83 ec 10             	sub    $0x10,%esp
801041c7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
801041ca:	68 20 2d 11 80       	push   $0x80112d20
801041cf:	e8 fc 03 00 00       	call   801045d0 <acquire>
801041d4:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801041d7:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
801041dc:	eb 0c                	jmp    801041ea <wakeup+0x2a>
801041de:	66 90                	xchg   %ax,%ax
801041e0:	83 c0 7c             	add    $0x7c,%eax
801041e3:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
801041e8:	73 1c                	jae    80104206 <wakeup+0x46>
    if(p->state == SLEEPING && p->chan == chan)
801041ea:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
801041ee:	75 f0                	jne    801041e0 <wakeup+0x20>
801041f0:	3b 58 20             	cmp    0x20(%eax),%ebx
801041f3:	75 eb                	jne    801041e0 <wakeup+0x20>
      p->state = RUNNABLE;
801041f5:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801041fc:	83 c0 7c             	add    $0x7c,%eax
801041ff:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80104204:	72 e4                	jb     801041ea <wakeup+0x2a>
  wakeup1(chan);
  release(&ptable.lock);
80104206:	c7 45 08 20 2d 11 80 	movl   $0x80112d20,0x8(%ebp)
}
8010420d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104210:	c9                   	leave  
  release(&ptable.lock);
80104211:	e9 7a 04 00 00       	jmp    80104690 <release>
80104216:	8d 76 00             	lea    0x0(%esi),%esi
80104219:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104220 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104220:	55                   	push   %ebp
80104221:	89 e5                	mov    %esp,%ebp
80104223:	53                   	push   %ebx
80104224:	83 ec 10             	sub    $0x10,%esp
80104227:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
8010422a:	68 20 2d 11 80       	push   $0x80112d20
8010422f:	e8 9c 03 00 00       	call   801045d0 <acquire>
80104234:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104237:	b8 54 2d 11 80       	mov    $0x80112d54,%eax
8010423c:	eb 0c                	jmp    8010424a <kill+0x2a>
8010423e:	66 90                	xchg   %ax,%ax
80104240:	83 c0 7c             	add    $0x7c,%eax
80104243:	3d 54 4c 11 80       	cmp    $0x80114c54,%eax
80104248:	73 36                	jae    80104280 <kill+0x60>
    if(p->pid == pid){
8010424a:	39 58 10             	cmp    %ebx,0x10(%eax)
8010424d:	75 f1                	jne    80104240 <kill+0x20>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
8010424f:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
      p->killed = 1;
80104253:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      if(p->state == SLEEPING)
8010425a:	75 07                	jne    80104263 <kill+0x43>
        p->state = RUNNABLE;
8010425c:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
80104263:	83 ec 0c             	sub    $0xc,%esp
80104266:	68 20 2d 11 80       	push   $0x80112d20
8010426b:	e8 20 04 00 00       	call   80104690 <release>
      return 0;
80104270:	83 c4 10             	add    $0x10,%esp
80104273:	31 c0                	xor    %eax,%eax
    }
  }
  release(&ptable.lock);
  return -1;
}
80104275:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104278:	c9                   	leave  
80104279:	c3                   	ret    
8010427a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&ptable.lock);
80104280:	83 ec 0c             	sub    $0xc,%esp
80104283:	68 20 2d 11 80       	push   $0x80112d20
80104288:	e8 03 04 00 00       	call   80104690 <release>
  return -1;
8010428d:	83 c4 10             	add    $0x10,%esp
80104290:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104295:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104298:	c9                   	leave  
80104299:	c3                   	ret    
8010429a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801042a0 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801042a0:	55                   	push   %ebp
801042a1:	89 e5                	mov    %esp,%ebp
801042a3:	57                   	push   %edi
801042a4:	56                   	push   %esi
801042a5:	53                   	push   %ebx
801042a6:	8d 75 e8             	lea    -0x18(%ebp),%esi
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801042a9:	bb 54 2d 11 80       	mov    $0x80112d54,%ebx
{
801042ae:	83 ec 3c             	sub    $0x3c,%esp
801042b1:	eb 24                	jmp    801042d7 <procdump+0x37>
801042b3:	90                   	nop
801042b4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
801042b8:	83 ec 0c             	sub    $0xc,%esp
801042bb:	68 b7 7a 10 80       	push   $0x80107ab7
801042c0:	e8 ab c4 ff ff       	call   80100770 <cprintf>
801042c5:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801042c8:	83 c3 7c             	add    $0x7c,%ebx
801042cb:	81 fb 54 4c 11 80    	cmp    $0x80114c54,%ebx
801042d1:	0f 83 81 00 00 00    	jae    80104358 <procdump+0xb8>
    if(p->state == UNUSED)
801042d7:	8b 43 0c             	mov    0xc(%ebx),%eax
801042da:	85 c0                	test   %eax,%eax
801042dc:	74 ea                	je     801042c8 <procdump+0x28>
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801042de:	83 f8 05             	cmp    $0x5,%eax
      state = "???";
801042e1:	ba 40 77 10 80       	mov    $0x80107740,%edx
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801042e6:	77 11                	ja     801042f9 <procdump+0x59>
801042e8:	8b 14 85 a0 77 10 80 	mov    -0x7fef8860(,%eax,4),%edx
      state = "???";
801042ef:	b8 40 77 10 80       	mov    $0x80107740,%eax
801042f4:	85 d2                	test   %edx,%edx
801042f6:	0f 44 d0             	cmove  %eax,%edx
    cprintf("%d %s %s", p->pid, state, p->name);
801042f9:	8d 43 6c             	lea    0x6c(%ebx),%eax
801042fc:	50                   	push   %eax
801042fd:	52                   	push   %edx
801042fe:	ff 73 10             	pushl  0x10(%ebx)
80104301:	68 44 77 10 80       	push   $0x80107744
80104306:	e8 65 c4 ff ff       	call   80100770 <cprintf>
    if(p->state == SLEEPING){
8010430b:	83 c4 10             	add    $0x10,%esp
8010430e:	83 7b 0c 02          	cmpl   $0x2,0xc(%ebx)
80104312:	75 a4                	jne    801042b8 <procdump+0x18>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80104314:	8d 45 c0             	lea    -0x40(%ebp),%eax
80104317:	83 ec 08             	sub    $0x8,%esp
8010431a:	8d 7d c0             	lea    -0x40(%ebp),%edi
8010431d:	50                   	push   %eax
8010431e:	8b 43 1c             	mov    0x1c(%ebx),%eax
80104321:	8b 40 0c             	mov    0xc(%eax),%eax
80104324:	83 c0 08             	add    $0x8,%eax
80104327:	50                   	push   %eax
80104328:	e8 83 01 00 00       	call   801044b0 <getcallerpcs>
8010432d:	83 c4 10             	add    $0x10,%esp
      for(i=0; i<10 && pc[i] != 0; i++)
80104330:	8b 17                	mov    (%edi),%edx
80104332:	85 d2                	test   %edx,%edx
80104334:	74 82                	je     801042b8 <procdump+0x18>
        cprintf(" %p", pc[i]);
80104336:	83 ec 08             	sub    $0x8,%esp
80104339:	83 c7 04             	add    $0x4,%edi
8010433c:	52                   	push   %edx
8010433d:	68 81 71 10 80       	push   $0x80107181
80104342:	e8 29 c4 ff ff       	call   80100770 <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
80104347:	83 c4 10             	add    $0x10,%esp
8010434a:	39 fe                	cmp    %edi,%esi
8010434c:	75 e2                	jne    80104330 <procdump+0x90>
8010434e:	e9 65 ff ff ff       	jmp    801042b8 <procdump+0x18>
80104353:	90                   	nop
80104354:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  }
}
80104358:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010435b:	5b                   	pop    %ebx
8010435c:	5e                   	pop    %esi
8010435d:	5f                   	pop    %edi
8010435e:	5d                   	pop    %ebp
8010435f:	c3                   	ret    

80104360 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
80104360:	55                   	push   %ebp
80104361:	89 e5                	mov    %esp,%ebp
80104363:	53                   	push   %ebx
80104364:	83 ec 0c             	sub    $0xc,%esp
80104367:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
8010436a:	68 b8 77 10 80       	push   $0x801077b8
8010436f:	8d 43 04             	lea    0x4(%ebx),%eax
80104372:	50                   	push   %eax
80104373:	e8 18 01 00 00       	call   80104490 <initlock>
  lk->name = name;
80104378:	8b 45 0c             	mov    0xc(%ebp),%eax
  lk->locked = 0;
8010437b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
}
80104381:	83 c4 10             	add    $0x10,%esp
  lk->pid = 0;
80104384:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  lk->name = name;
8010438b:	89 43 38             	mov    %eax,0x38(%ebx)
}
8010438e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104391:	c9                   	leave  
80104392:	c3                   	ret    
80104393:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104399:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801043a0 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
801043a0:	55                   	push   %ebp
801043a1:	89 e5                	mov    %esp,%ebp
801043a3:	56                   	push   %esi
801043a4:	53                   	push   %ebx
801043a5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
801043a8:	83 ec 0c             	sub    $0xc,%esp
801043ab:	8d 73 04             	lea    0x4(%ebx),%esi
801043ae:	56                   	push   %esi
801043af:	e8 1c 02 00 00       	call   801045d0 <acquire>
  while (lk->locked) {
801043b4:	8b 13                	mov    (%ebx),%edx
801043b6:	83 c4 10             	add    $0x10,%esp
801043b9:	85 d2                	test   %edx,%edx
801043bb:	74 16                	je     801043d3 <acquiresleep+0x33>
801043bd:	8d 76 00             	lea    0x0(%esi),%esi
    sleep(lk, &lk->lk);
801043c0:	83 ec 08             	sub    $0x8,%esp
801043c3:	56                   	push   %esi
801043c4:	53                   	push   %ebx
801043c5:	e8 46 fc ff ff       	call   80104010 <sleep>
  while (lk->locked) {
801043ca:	8b 03                	mov    (%ebx),%eax
801043cc:	83 c4 10             	add    $0x10,%esp
801043cf:	85 c0                	test   %eax,%eax
801043d1:	75 ed                	jne    801043c0 <acquiresleep+0x20>
  }
  lk->locked = 1;
801043d3:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
801043d9:	e8 92 f6 ff ff       	call   80103a70 <myproc>
801043de:	8b 40 10             	mov    0x10(%eax),%eax
801043e1:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
801043e4:	89 75 08             	mov    %esi,0x8(%ebp)
}
801043e7:	8d 65 f8             	lea    -0x8(%ebp),%esp
801043ea:	5b                   	pop    %ebx
801043eb:	5e                   	pop    %esi
801043ec:	5d                   	pop    %ebp
  release(&lk->lk);
801043ed:	e9 9e 02 00 00       	jmp    80104690 <release>
801043f2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801043f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104400 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80104400:	55                   	push   %ebp
80104401:	89 e5                	mov    %esp,%ebp
80104403:	56                   	push   %esi
80104404:	53                   	push   %ebx
80104405:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80104408:	83 ec 0c             	sub    $0xc,%esp
8010440b:	8d 73 04             	lea    0x4(%ebx),%esi
8010440e:	56                   	push   %esi
8010440f:	e8 bc 01 00 00       	call   801045d0 <acquire>
  lk->locked = 0;
80104414:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
8010441a:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80104421:	89 1c 24             	mov    %ebx,(%esp)
80104424:	e8 97 fd ff ff       	call   801041c0 <wakeup>
  release(&lk->lk);
80104429:	89 75 08             	mov    %esi,0x8(%ebp)
8010442c:	83 c4 10             	add    $0x10,%esp
}
8010442f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104432:	5b                   	pop    %ebx
80104433:	5e                   	pop    %esi
80104434:	5d                   	pop    %ebp
  release(&lk->lk);
80104435:	e9 56 02 00 00       	jmp    80104690 <release>
8010443a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104440 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80104440:	55                   	push   %ebp
80104441:	89 e5                	mov    %esp,%ebp
80104443:	57                   	push   %edi
80104444:	56                   	push   %esi
80104445:	53                   	push   %ebx
80104446:	31 ff                	xor    %edi,%edi
80104448:	83 ec 18             	sub    $0x18,%esp
8010444b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
8010444e:	8d 73 04             	lea    0x4(%ebx),%esi
80104451:	56                   	push   %esi
80104452:	e8 79 01 00 00       	call   801045d0 <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
80104457:	8b 03                	mov    (%ebx),%eax
80104459:	83 c4 10             	add    $0x10,%esp
8010445c:	85 c0                	test   %eax,%eax
8010445e:	74 13                	je     80104473 <holdingsleep+0x33>
80104460:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
80104463:	e8 08 f6 ff ff       	call   80103a70 <myproc>
80104468:	39 58 10             	cmp    %ebx,0x10(%eax)
8010446b:	0f 94 c0             	sete   %al
8010446e:	0f b6 c0             	movzbl %al,%eax
80104471:	89 c7                	mov    %eax,%edi
  release(&lk->lk);
80104473:	83 ec 0c             	sub    $0xc,%esp
80104476:	56                   	push   %esi
80104477:	e8 14 02 00 00       	call   80104690 <release>
  return r;
}
8010447c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010447f:	89 f8                	mov    %edi,%eax
80104481:	5b                   	pop    %ebx
80104482:	5e                   	pop    %esi
80104483:	5f                   	pop    %edi
80104484:	5d                   	pop    %ebp
80104485:	c3                   	ret    
80104486:	66 90                	xchg   %ax,%ax
80104488:	66 90                	xchg   %ax,%ax
8010448a:	66 90                	xchg   %ax,%ax
8010448c:	66 90                	xchg   %ax,%ax
8010448e:	66 90                	xchg   %ax,%ax

80104490 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80104490:	55                   	push   %ebp
80104491:	89 e5                	mov    %esp,%ebp
80104493:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
80104496:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
80104499:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->name = name;
8010449f:	89 50 04             	mov    %edx,0x4(%eax)
  lk->cpu = 0;
801044a2:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
801044a9:	5d                   	pop    %ebp
801044aa:	c3                   	ret    
801044ab:	90                   	nop
801044ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801044b0 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
801044b0:	55                   	push   %ebp
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
801044b1:	31 d2                	xor    %edx,%edx
{
801044b3:	89 e5                	mov    %esp,%ebp
801044b5:	53                   	push   %ebx
  ebp = (uint*)v - 2;
801044b6:	8b 45 08             	mov    0x8(%ebp),%eax
{
801044b9:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  ebp = (uint*)v - 2;
801044bc:	83 e8 08             	sub    $0x8,%eax
801044bf:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
801044c0:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
801044c6:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
801044cc:	77 1a                	ja     801044e8 <getcallerpcs+0x38>
      break;
    pcs[i] = ebp[1];     // saved %eip
801044ce:	8b 58 04             	mov    0x4(%eax),%ebx
801044d1:	89 1c 91             	mov    %ebx,(%ecx,%edx,4)
  for(i = 0; i < 10; i++){
801044d4:	83 c2 01             	add    $0x1,%edx
    ebp = (uint*)ebp[0]; // saved %ebp
801044d7:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
801044d9:	83 fa 0a             	cmp    $0xa,%edx
801044dc:	75 e2                	jne    801044c0 <getcallerpcs+0x10>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
801044de:	5b                   	pop    %ebx
801044df:	5d                   	pop    %ebp
801044e0:	c3                   	ret    
801044e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801044e8:	8d 04 91             	lea    (%ecx,%edx,4),%eax
801044eb:	83 c1 28             	add    $0x28,%ecx
801044ee:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
801044f0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801044f6:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
801044f9:	39 c1                	cmp    %eax,%ecx
801044fb:	75 f3                	jne    801044f0 <getcallerpcs+0x40>
}
801044fd:	5b                   	pop    %ebx
801044fe:	5d                   	pop    %ebp
801044ff:	c3                   	ret    

80104500 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104500:	55                   	push   %ebp
80104501:	89 e5                	mov    %esp,%ebp
80104503:	53                   	push   %ebx
80104504:	83 ec 04             	sub    $0x4,%esp
80104507:	9c                   	pushf  
80104508:	5b                   	pop    %ebx
  asm volatile("cli");
80104509:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
8010450a:	e8 c1 f4 ff ff       	call   801039d0 <mycpu>
8010450f:	8b 80 a4 00 00 00    	mov    0xa4(%eax),%eax
80104515:	85 c0                	test   %eax,%eax
80104517:	75 11                	jne    8010452a <pushcli+0x2a>
    mycpu()->intena = eflags & FL_IF;
80104519:	81 e3 00 02 00 00    	and    $0x200,%ebx
8010451f:	e8 ac f4 ff ff       	call   801039d0 <mycpu>
80104524:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
  mycpu()->ncli += 1;
8010452a:	e8 a1 f4 ff ff       	call   801039d0 <mycpu>
8010452f:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80104536:	83 c4 04             	add    $0x4,%esp
80104539:	5b                   	pop    %ebx
8010453a:	5d                   	pop    %ebp
8010453b:	c3                   	ret    
8010453c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104540 <popcli>:

void
popcli(void)
{
80104540:	55                   	push   %ebp
80104541:	89 e5                	mov    %esp,%ebp
80104543:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80104546:	9c                   	pushf  
80104547:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80104548:	f6 c4 02             	test   $0x2,%ah
8010454b:	75 35                	jne    80104582 <popcli+0x42>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
8010454d:	e8 7e f4 ff ff       	call   801039d0 <mycpu>
80104552:	83 a8 a4 00 00 00 01 	subl   $0x1,0xa4(%eax)
80104559:	78 34                	js     8010458f <popcli+0x4f>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
8010455b:	e8 70 f4 ff ff       	call   801039d0 <mycpu>
80104560:	8b 90 a4 00 00 00    	mov    0xa4(%eax),%edx
80104566:	85 d2                	test   %edx,%edx
80104568:	74 06                	je     80104570 <popcli+0x30>
    sti();
}
8010456a:	c9                   	leave  
8010456b:	c3                   	ret    
8010456c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(mycpu()->ncli == 0 && mycpu()->intena)
80104570:	e8 5b f4 ff ff       	call   801039d0 <mycpu>
80104575:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
8010457b:	85 c0                	test   %eax,%eax
8010457d:	74 eb                	je     8010456a <popcli+0x2a>
  asm volatile("sti");
8010457f:	fb                   	sti    
}
80104580:	c9                   	leave  
80104581:	c3                   	ret    
    panic("popcli - interruptible");
80104582:	83 ec 0c             	sub    $0xc,%esp
80104585:	68 c3 77 10 80       	push   $0x801077c3
8010458a:	e8 01 be ff ff       	call   80100390 <panic>
    panic("popcli");
8010458f:	83 ec 0c             	sub    $0xc,%esp
80104592:	68 da 77 10 80       	push   $0x801077da
80104597:	e8 f4 bd ff ff       	call   80100390 <panic>
8010459c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801045a0 <holding>:
{
801045a0:	55                   	push   %ebp
801045a1:	89 e5                	mov    %esp,%ebp
801045a3:	56                   	push   %esi
801045a4:	53                   	push   %ebx
801045a5:	8b 75 08             	mov    0x8(%ebp),%esi
801045a8:	31 db                	xor    %ebx,%ebx
  pushcli();
801045aa:	e8 51 ff ff ff       	call   80104500 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
801045af:	8b 06                	mov    (%esi),%eax
801045b1:	85 c0                	test   %eax,%eax
801045b3:	74 10                	je     801045c5 <holding+0x25>
801045b5:	8b 5e 08             	mov    0x8(%esi),%ebx
801045b8:	e8 13 f4 ff ff       	call   801039d0 <mycpu>
801045bd:	39 c3                	cmp    %eax,%ebx
801045bf:	0f 94 c3             	sete   %bl
801045c2:	0f b6 db             	movzbl %bl,%ebx
  popcli();
801045c5:	e8 76 ff ff ff       	call   80104540 <popcli>
}
801045ca:	89 d8                	mov    %ebx,%eax
801045cc:	5b                   	pop    %ebx
801045cd:	5e                   	pop    %esi
801045ce:	5d                   	pop    %ebp
801045cf:	c3                   	ret    

801045d0 <acquire>:
{
801045d0:	55                   	push   %ebp
801045d1:	89 e5                	mov    %esp,%ebp
801045d3:	56                   	push   %esi
801045d4:	53                   	push   %ebx
  pushcli(); // disable interrupts to avoid deadlock.
801045d5:	e8 26 ff ff ff       	call   80104500 <pushcli>
  if(holding(lk))
801045da:	8b 5d 08             	mov    0x8(%ebp),%ebx
801045dd:	83 ec 0c             	sub    $0xc,%esp
801045e0:	53                   	push   %ebx
801045e1:	e8 ba ff ff ff       	call   801045a0 <holding>
801045e6:	83 c4 10             	add    $0x10,%esp
801045e9:	85 c0                	test   %eax,%eax
801045eb:	0f 85 83 00 00 00    	jne    80104674 <acquire+0xa4>
801045f1:	89 c6                	mov    %eax,%esi
  asm volatile("lock; xchgl %0, %1" :
801045f3:	ba 01 00 00 00       	mov    $0x1,%edx
801045f8:	eb 09                	jmp    80104603 <acquire+0x33>
801045fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104600:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104603:	89 d0                	mov    %edx,%eax
80104605:	f0 87 03             	lock xchg %eax,(%ebx)
  while(xchg(&lk->locked, 1) != 0)
80104608:	85 c0                	test   %eax,%eax
8010460a:	75 f4                	jne    80104600 <acquire+0x30>
  __sync_synchronize();
8010460c:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
80104611:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104614:	e8 b7 f3 ff ff       	call   801039d0 <mycpu>
  getcallerpcs(&lk, lk->pcs);
80104619:	8d 53 0c             	lea    0xc(%ebx),%edx
  lk->cpu = mycpu();
8010461c:	89 43 08             	mov    %eax,0x8(%ebx)
  ebp = (uint*)v - 2;
8010461f:	89 e8                	mov    %ebp,%eax
80104621:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104628:	8d 88 00 00 00 80    	lea    -0x80000000(%eax),%ecx
8010462e:	81 f9 fe ff ff 7f    	cmp    $0x7ffffffe,%ecx
80104634:	77 1a                	ja     80104650 <acquire+0x80>
    pcs[i] = ebp[1];     // saved %eip
80104636:	8b 48 04             	mov    0x4(%eax),%ecx
80104639:	89 0c b2             	mov    %ecx,(%edx,%esi,4)
  for(i = 0; i < 10; i++){
8010463c:	83 c6 01             	add    $0x1,%esi
    ebp = (uint*)ebp[0]; // saved %ebp
8010463f:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104641:	83 fe 0a             	cmp    $0xa,%esi
80104644:	75 e2                	jne    80104628 <acquire+0x58>
}
80104646:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104649:	5b                   	pop    %ebx
8010464a:	5e                   	pop    %esi
8010464b:	5d                   	pop    %ebp
8010464c:	c3                   	ret    
8010464d:	8d 76 00             	lea    0x0(%esi),%esi
80104650:	8d 04 b2             	lea    (%edx,%esi,4),%eax
80104653:	83 c2 28             	add    $0x28,%edx
80104656:	8d 76 00             	lea    0x0(%esi),%esi
80104659:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    pcs[i] = 0;
80104660:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80104666:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
80104669:	39 d0                	cmp    %edx,%eax
8010466b:	75 f3                	jne    80104660 <acquire+0x90>
}
8010466d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104670:	5b                   	pop    %ebx
80104671:	5e                   	pop    %esi
80104672:	5d                   	pop    %ebp
80104673:	c3                   	ret    
    panic("acquire");
80104674:	83 ec 0c             	sub    $0xc,%esp
80104677:	68 e1 77 10 80       	push   $0x801077e1
8010467c:	e8 0f bd ff ff       	call   80100390 <panic>
80104681:	eb 0d                	jmp    80104690 <release>
80104683:	90                   	nop
80104684:	90                   	nop
80104685:	90                   	nop
80104686:	90                   	nop
80104687:	90                   	nop
80104688:	90                   	nop
80104689:	90                   	nop
8010468a:	90                   	nop
8010468b:	90                   	nop
8010468c:	90                   	nop
8010468d:	90                   	nop
8010468e:	90                   	nop
8010468f:	90                   	nop

80104690 <release>:
{
80104690:	55                   	push   %ebp
80104691:	89 e5                	mov    %esp,%ebp
80104693:	53                   	push   %ebx
80104694:	83 ec 10             	sub    $0x10,%esp
80104697:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holding(lk))
8010469a:	53                   	push   %ebx
8010469b:	e8 00 ff ff ff       	call   801045a0 <holding>
801046a0:	83 c4 10             	add    $0x10,%esp
801046a3:	85 c0                	test   %eax,%eax
801046a5:	74 22                	je     801046c9 <release+0x39>
  lk->pcs[0] = 0;
801046a7:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
801046ae:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
801046b5:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
801046ba:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
}
801046c0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801046c3:	c9                   	leave  
  popcli();
801046c4:	e9 77 fe ff ff       	jmp    80104540 <popcli>
    panic("release");
801046c9:	83 ec 0c             	sub    $0xc,%esp
801046cc:	68 e9 77 10 80       	push   $0x801077e9
801046d1:	e8 ba bc ff ff       	call   80100390 <panic>
801046d6:	66 90                	xchg   %ax,%ax
801046d8:	66 90                	xchg   %ax,%ax
801046da:	66 90                	xchg   %ax,%ax
801046dc:	66 90                	xchg   %ax,%ax
801046de:	66 90                	xchg   %ax,%ax

801046e0 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
801046e0:	55                   	push   %ebp
801046e1:	89 e5                	mov    %esp,%ebp
801046e3:	57                   	push   %edi
801046e4:	53                   	push   %ebx
801046e5:	8b 55 08             	mov    0x8(%ebp),%edx
801046e8:	8b 4d 10             	mov    0x10(%ebp),%ecx
  if ((int)dst%4 == 0 && n%4 == 0){
801046eb:	f6 c2 03             	test   $0x3,%dl
801046ee:	75 05                	jne    801046f5 <memset+0x15>
801046f0:	f6 c1 03             	test   $0x3,%cl
801046f3:	74 13                	je     80104708 <memset+0x28>
  asm volatile("cld; rep stosb" :
801046f5:	89 d7                	mov    %edx,%edi
801046f7:	8b 45 0c             	mov    0xc(%ebp),%eax
801046fa:	fc                   	cld    
801046fb:	f3 aa                	rep stos %al,%es:(%edi)
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
  return dst;
}
801046fd:	5b                   	pop    %ebx
801046fe:	89 d0                	mov    %edx,%eax
80104700:	5f                   	pop    %edi
80104701:	5d                   	pop    %ebp
80104702:	c3                   	ret    
80104703:	90                   	nop
80104704:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    c &= 0xFF;
80104708:	0f b6 7d 0c          	movzbl 0xc(%ebp),%edi
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
8010470c:	c1 e9 02             	shr    $0x2,%ecx
8010470f:	89 f8                	mov    %edi,%eax
80104711:	89 fb                	mov    %edi,%ebx
80104713:	c1 e0 18             	shl    $0x18,%eax
80104716:	c1 e3 10             	shl    $0x10,%ebx
80104719:	09 d8                	or     %ebx,%eax
8010471b:	09 f8                	or     %edi,%eax
8010471d:	c1 e7 08             	shl    $0x8,%edi
80104720:	09 f8                	or     %edi,%eax
  asm volatile("cld; rep stosl" :
80104722:	89 d7                	mov    %edx,%edi
80104724:	fc                   	cld    
80104725:	f3 ab                	rep stos %eax,%es:(%edi)
}
80104727:	5b                   	pop    %ebx
80104728:	89 d0                	mov    %edx,%eax
8010472a:	5f                   	pop    %edi
8010472b:	5d                   	pop    %ebp
8010472c:	c3                   	ret    
8010472d:	8d 76 00             	lea    0x0(%esi),%esi

80104730 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
80104730:	55                   	push   %ebp
80104731:	89 e5                	mov    %esp,%ebp
80104733:	57                   	push   %edi
80104734:	56                   	push   %esi
80104735:	53                   	push   %ebx
80104736:	8b 5d 10             	mov    0x10(%ebp),%ebx
80104739:	8b 75 08             	mov    0x8(%ebp),%esi
8010473c:	8b 7d 0c             	mov    0xc(%ebp),%edi
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
8010473f:	85 db                	test   %ebx,%ebx
80104741:	74 29                	je     8010476c <memcmp+0x3c>
    if(*s1 != *s2)
80104743:	0f b6 16             	movzbl (%esi),%edx
80104746:	0f b6 0f             	movzbl (%edi),%ecx
80104749:	38 d1                	cmp    %dl,%cl
8010474b:	75 2b                	jne    80104778 <memcmp+0x48>
8010474d:	b8 01 00 00 00       	mov    $0x1,%eax
80104752:	eb 14                	jmp    80104768 <memcmp+0x38>
80104754:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104758:	0f b6 14 06          	movzbl (%esi,%eax,1),%edx
8010475c:	83 c0 01             	add    $0x1,%eax
8010475f:	0f b6 4c 07 ff       	movzbl -0x1(%edi,%eax,1),%ecx
80104764:	38 ca                	cmp    %cl,%dl
80104766:	75 10                	jne    80104778 <memcmp+0x48>
  while(n-- > 0){
80104768:	39 d8                	cmp    %ebx,%eax
8010476a:	75 ec                	jne    80104758 <memcmp+0x28>
      return *s1 - *s2;
    s1++, s2++;
  }

  return 0;
}
8010476c:	5b                   	pop    %ebx
  return 0;
8010476d:	31 c0                	xor    %eax,%eax
}
8010476f:	5e                   	pop    %esi
80104770:	5f                   	pop    %edi
80104771:	5d                   	pop    %ebp
80104772:	c3                   	ret    
80104773:	90                   	nop
80104774:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      return *s1 - *s2;
80104778:	0f b6 c2             	movzbl %dl,%eax
}
8010477b:	5b                   	pop    %ebx
      return *s1 - *s2;
8010477c:	29 c8                	sub    %ecx,%eax
}
8010477e:	5e                   	pop    %esi
8010477f:	5f                   	pop    %edi
80104780:	5d                   	pop    %ebp
80104781:	c3                   	ret    
80104782:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104789:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104790 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80104790:	55                   	push   %ebp
80104791:	89 e5                	mov    %esp,%ebp
80104793:	56                   	push   %esi
80104794:	53                   	push   %ebx
80104795:	8b 45 08             	mov    0x8(%ebp),%eax
80104798:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010479b:	8b 75 10             	mov    0x10(%ebp),%esi
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
8010479e:	39 c3                	cmp    %eax,%ebx
801047a0:	73 26                	jae    801047c8 <memmove+0x38>
801047a2:	8d 0c 33             	lea    (%ebx,%esi,1),%ecx
801047a5:	39 c8                	cmp    %ecx,%eax
801047a7:	73 1f                	jae    801047c8 <memmove+0x38>
    s += n;
    d += n;
    while(n-- > 0)
801047a9:	85 f6                	test   %esi,%esi
801047ab:	8d 56 ff             	lea    -0x1(%esi),%edx
801047ae:	74 0f                	je     801047bf <memmove+0x2f>
      *--d = *--s;
801047b0:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
801047b4:	88 0c 10             	mov    %cl,(%eax,%edx,1)
    while(n-- > 0)
801047b7:	83 ea 01             	sub    $0x1,%edx
801047ba:	83 fa ff             	cmp    $0xffffffff,%edx
801047bd:	75 f1                	jne    801047b0 <memmove+0x20>
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}
801047bf:	5b                   	pop    %ebx
801047c0:	5e                   	pop    %esi
801047c1:	5d                   	pop    %ebp
801047c2:	c3                   	ret    
801047c3:	90                   	nop
801047c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    while(n-- > 0)
801047c8:	31 d2                	xor    %edx,%edx
801047ca:	85 f6                	test   %esi,%esi
801047cc:	74 f1                	je     801047bf <memmove+0x2f>
801047ce:	66 90                	xchg   %ax,%ax
      *d++ = *s++;
801047d0:	0f b6 0c 13          	movzbl (%ebx,%edx,1),%ecx
801047d4:	88 0c 10             	mov    %cl,(%eax,%edx,1)
801047d7:	83 c2 01             	add    $0x1,%edx
    while(n-- > 0)
801047da:	39 d6                	cmp    %edx,%esi
801047dc:	75 f2                	jne    801047d0 <memmove+0x40>
}
801047de:	5b                   	pop    %ebx
801047df:	5e                   	pop    %esi
801047e0:	5d                   	pop    %ebp
801047e1:	c3                   	ret    
801047e2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801047e9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801047f0 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
801047f0:	55                   	push   %ebp
801047f1:	89 e5                	mov    %esp,%ebp
  return memmove(dst, src, n);
}
801047f3:	5d                   	pop    %ebp
  return memmove(dst, src, n);
801047f4:	eb 9a                	jmp    80104790 <memmove>
801047f6:	8d 76 00             	lea    0x0(%esi),%esi
801047f9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104800 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
80104800:	55                   	push   %ebp
80104801:	89 e5                	mov    %esp,%ebp
80104803:	57                   	push   %edi
80104804:	56                   	push   %esi
80104805:	8b 7d 10             	mov    0x10(%ebp),%edi
80104808:	53                   	push   %ebx
80104809:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010480c:	8b 75 0c             	mov    0xc(%ebp),%esi
  while(n > 0 && *p && *p == *q)
8010480f:	85 ff                	test   %edi,%edi
80104811:	74 2f                	je     80104842 <strncmp+0x42>
80104813:	0f b6 01             	movzbl (%ecx),%eax
80104816:	0f b6 1e             	movzbl (%esi),%ebx
80104819:	84 c0                	test   %al,%al
8010481b:	74 37                	je     80104854 <strncmp+0x54>
8010481d:	38 c3                	cmp    %al,%bl
8010481f:	75 33                	jne    80104854 <strncmp+0x54>
80104821:	01 f7                	add    %esi,%edi
80104823:	eb 13                	jmp    80104838 <strncmp+0x38>
80104825:	8d 76 00             	lea    0x0(%esi),%esi
80104828:	0f b6 01             	movzbl (%ecx),%eax
8010482b:	84 c0                	test   %al,%al
8010482d:	74 21                	je     80104850 <strncmp+0x50>
8010482f:	0f b6 1a             	movzbl (%edx),%ebx
80104832:	89 d6                	mov    %edx,%esi
80104834:	38 d8                	cmp    %bl,%al
80104836:	75 1c                	jne    80104854 <strncmp+0x54>
    n--, p++, q++;
80104838:	8d 56 01             	lea    0x1(%esi),%edx
8010483b:	83 c1 01             	add    $0x1,%ecx
  while(n > 0 && *p && *p == *q)
8010483e:	39 fa                	cmp    %edi,%edx
80104840:	75 e6                	jne    80104828 <strncmp+0x28>
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
}
80104842:	5b                   	pop    %ebx
    return 0;
80104843:	31 c0                	xor    %eax,%eax
}
80104845:	5e                   	pop    %esi
80104846:	5f                   	pop    %edi
80104847:	5d                   	pop    %ebp
80104848:	c3                   	ret    
80104849:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104850:	0f b6 5e 01          	movzbl 0x1(%esi),%ebx
  return (uchar)*p - (uchar)*q;
80104854:	29 d8                	sub    %ebx,%eax
}
80104856:	5b                   	pop    %ebx
80104857:	5e                   	pop    %esi
80104858:	5f                   	pop    %edi
80104859:	5d                   	pop    %ebp
8010485a:	c3                   	ret    
8010485b:	90                   	nop
8010485c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104860 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
80104860:	55                   	push   %ebp
80104861:	89 e5                	mov    %esp,%ebp
80104863:	56                   	push   %esi
80104864:	53                   	push   %ebx
80104865:	8b 45 08             	mov    0x8(%ebp),%eax
80104868:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010486b:	8b 4d 10             	mov    0x10(%ebp),%ecx
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
8010486e:	89 c2                	mov    %eax,%edx
80104870:	eb 19                	jmp    8010488b <strncpy+0x2b>
80104872:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104878:	83 c3 01             	add    $0x1,%ebx
8010487b:	0f b6 4b ff          	movzbl -0x1(%ebx),%ecx
8010487f:	83 c2 01             	add    $0x1,%edx
80104882:	84 c9                	test   %cl,%cl
80104884:	88 4a ff             	mov    %cl,-0x1(%edx)
80104887:	74 09                	je     80104892 <strncpy+0x32>
80104889:	89 f1                	mov    %esi,%ecx
8010488b:	85 c9                	test   %ecx,%ecx
8010488d:	8d 71 ff             	lea    -0x1(%ecx),%esi
80104890:	7f e6                	jg     80104878 <strncpy+0x18>
    ;
  while(n-- > 0)
80104892:	31 c9                	xor    %ecx,%ecx
80104894:	85 f6                	test   %esi,%esi
80104896:	7e 17                	jle    801048af <strncpy+0x4f>
80104898:	90                   	nop
80104899:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    *s++ = 0;
801048a0:	c6 04 0a 00          	movb   $0x0,(%edx,%ecx,1)
801048a4:	89 f3                	mov    %esi,%ebx
801048a6:	83 c1 01             	add    $0x1,%ecx
801048a9:	29 cb                	sub    %ecx,%ebx
  while(n-- > 0)
801048ab:	85 db                	test   %ebx,%ebx
801048ad:	7f f1                	jg     801048a0 <strncpy+0x40>
  return os;
}
801048af:	5b                   	pop    %ebx
801048b0:	5e                   	pop    %esi
801048b1:	5d                   	pop    %ebp
801048b2:	c3                   	ret    
801048b3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801048b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801048c0 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
801048c0:	55                   	push   %ebp
801048c1:	89 e5                	mov    %esp,%ebp
801048c3:	56                   	push   %esi
801048c4:	53                   	push   %ebx
801048c5:	8b 4d 10             	mov    0x10(%ebp),%ecx
801048c8:	8b 45 08             	mov    0x8(%ebp),%eax
801048cb:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *os;

  os = s;
  if(n <= 0)
801048ce:	85 c9                	test   %ecx,%ecx
801048d0:	7e 26                	jle    801048f8 <safestrcpy+0x38>
801048d2:	8d 74 0a ff          	lea    -0x1(%edx,%ecx,1),%esi
801048d6:	89 c1                	mov    %eax,%ecx
801048d8:	eb 17                	jmp    801048f1 <safestrcpy+0x31>
801048da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
801048e0:	83 c2 01             	add    $0x1,%edx
801048e3:	0f b6 5a ff          	movzbl -0x1(%edx),%ebx
801048e7:	83 c1 01             	add    $0x1,%ecx
801048ea:	84 db                	test   %bl,%bl
801048ec:	88 59 ff             	mov    %bl,-0x1(%ecx)
801048ef:	74 04                	je     801048f5 <safestrcpy+0x35>
801048f1:	39 f2                	cmp    %esi,%edx
801048f3:	75 eb                	jne    801048e0 <safestrcpy+0x20>
    ;
  *s = 0;
801048f5:	c6 01 00             	movb   $0x0,(%ecx)
  return os;
}
801048f8:	5b                   	pop    %ebx
801048f9:	5e                   	pop    %esi
801048fa:	5d                   	pop    %ebp
801048fb:	c3                   	ret    
801048fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104900 <strlen>:

int
strlen(const char *s)
{
80104900:	55                   	push   %ebp
  int n;

  for(n = 0; s[n]; n++)
80104901:	31 c0                	xor    %eax,%eax
{
80104903:	89 e5                	mov    %esp,%ebp
80104905:	8b 55 08             	mov    0x8(%ebp),%edx
  for(n = 0; s[n]; n++)
80104908:	80 3a 00             	cmpb   $0x0,(%edx)
8010490b:	74 0c                	je     80104919 <strlen+0x19>
8010490d:	8d 76 00             	lea    0x0(%esi),%esi
80104910:	83 c0 01             	add    $0x1,%eax
80104913:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
80104917:	75 f7                	jne    80104910 <strlen+0x10>
    ;
  return n;
}
80104919:	5d                   	pop    %ebp
8010491a:	c3                   	ret    

8010491b <swtch>:
8010491b:	8b 44 24 04          	mov    0x4(%esp),%eax
8010491f:	8b 54 24 08          	mov    0x8(%esp),%edx
80104923:	55                   	push   %ebp
80104924:	53                   	push   %ebx
80104925:	56                   	push   %esi
80104926:	57                   	push   %edi
80104927:	89 20                	mov    %esp,(%eax)
80104929:	89 d4                	mov    %edx,%esp
8010492b:	5f                   	pop    %edi
8010492c:	5e                   	pop    %esi
8010492d:	5b                   	pop    %ebx
8010492e:	5d                   	pop    %ebp
8010492f:	c3                   	ret    

80104930 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80104930:	55                   	push   %ebp
80104931:	89 e5                	mov    %esp,%ebp
80104933:	53                   	push   %ebx
80104934:	83 ec 04             	sub    $0x4,%esp
80104937:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *curproc = myproc();
8010493a:	e8 31 f1 ff ff       	call   80103a70 <myproc>

  if(addr >= curproc->sz || addr+4 > curproc->sz)
8010493f:	8b 00                	mov    (%eax),%eax
80104941:	39 d8                	cmp    %ebx,%eax
80104943:	76 1b                	jbe    80104960 <fetchint+0x30>
80104945:	8d 53 04             	lea    0x4(%ebx),%edx
80104948:	39 d0                	cmp    %edx,%eax
8010494a:	72 14                	jb     80104960 <fetchint+0x30>
    return -1;
  *ip = *(int*)(addr);
8010494c:	8b 45 0c             	mov    0xc(%ebp),%eax
8010494f:	8b 13                	mov    (%ebx),%edx
80104951:	89 10                	mov    %edx,(%eax)
  return 0;
80104953:	31 c0                	xor    %eax,%eax
}
80104955:	83 c4 04             	add    $0x4,%esp
80104958:	5b                   	pop    %ebx
80104959:	5d                   	pop    %ebp
8010495a:	c3                   	ret    
8010495b:	90                   	nop
8010495c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104960:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104965:	eb ee                	jmp    80104955 <fetchint+0x25>
80104967:	89 f6                	mov    %esi,%esi
80104969:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104970 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
80104970:	55                   	push   %ebp
80104971:	89 e5                	mov    %esp,%ebp
80104973:	53                   	push   %ebx
80104974:	83 ec 04             	sub    $0x4,%esp
80104977:	8b 5d 08             	mov    0x8(%ebp),%ebx
  char *s, *ep;
  struct proc *curproc = myproc();
8010497a:	e8 f1 f0 ff ff       	call   80103a70 <myproc>

  if(addr >= curproc->sz)
8010497f:	39 18                	cmp    %ebx,(%eax)
80104981:	76 29                	jbe    801049ac <fetchstr+0x3c>
    return -1;
  *pp = (char*)addr;
80104983:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80104986:	89 da                	mov    %ebx,%edx
80104988:	89 19                	mov    %ebx,(%ecx)
  ep = (char*)curproc->sz;
8010498a:	8b 00                	mov    (%eax),%eax
  for(s = *pp; s < ep; s++){
8010498c:	39 c3                	cmp    %eax,%ebx
8010498e:	73 1c                	jae    801049ac <fetchstr+0x3c>
    if(*s == 0)
80104990:	80 3b 00             	cmpb   $0x0,(%ebx)
80104993:	75 10                	jne    801049a5 <fetchstr+0x35>
80104995:	eb 39                	jmp    801049d0 <fetchstr+0x60>
80104997:	89 f6                	mov    %esi,%esi
80104999:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801049a0:	80 3a 00             	cmpb   $0x0,(%edx)
801049a3:	74 1b                	je     801049c0 <fetchstr+0x50>
  for(s = *pp; s < ep; s++){
801049a5:	83 c2 01             	add    $0x1,%edx
801049a8:	39 d0                	cmp    %edx,%eax
801049aa:	77 f4                	ja     801049a0 <fetchstr+0x30>
    return -1;
801049ac:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
      return s - *pp;
  }
  return -1;
}
801049b1:	83 c4 04             	add    $0x4,%esp
801049b4:	5b                   	pop    %ebx
801049b5:	5d                   	pop    %ebp
801049b6:	c3                   	ret    
801049b7:	89 f6                	mov    %esi,%esi
801049b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
801049c0:	83 c4 04             	add    $0x4,%esp
801049c3:	89 d0                	mov    %edx,%eax
801049c5:	29 d8                	sub    %ebx,%eax
801049c7:	5b                   	pop    %ebx
801049c8:	5d                   	pop    %ebp
801049c9:	c3                   	ret    
801049ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(*s == 0)
801049d0:	31 c0                	xor    %eax,%eax
      return s - *pp;
801049d2:	eb dd                	jmp    801049b1 <fetchstr+0x41>
801049d4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801049da:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

801049e0 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
801049e0:	55                   	push   %ebp
801049e1:	89 e5                	mov    %esp,%ebp
801049e3:	56                   	push   %esi
801049e4:	53                   	push   %ebx
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
801049e5:	e8 86 f0 ff ff       	call   80103a70 <myproc>
801049ea:	8b 40 18             	mov    0x18(%eax),%eax
801049ed:	8b 55 08             	mov    0x8(%ebp),%edx
801049f0:	8b 40 44             	mov    0x44(%eax),%eax
801049f3:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
801049f6:	e8 75 f0 ff ff       	call   80103a70 <myproc>
  if(addr >= curproc->sz || addr+4 > curproc->sz)
801049fb:	8b 00                	mov    (%eax),%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
801049fd:	8d 73 04             	lea    0x4(%ebx),%esi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
80104a00:	39 c6                	cmp    %eax,%esi
80104a02:	73 1c                	jae    80104a20 <argint+0x40>
80104a04:	8d 53 08             	lea    0x8(%ebx),%edx
80104a07:	39 d0                	cmp    %edx,%eax
80104a09:	72 15                	jb     80104a20 <argint+0x40>
  *ip = *(int*)(addr);
80104a0b:	8b 45 0c             	mov    0xc(%ebp),%eax
80104a0e:	8b 53 04             	mov    0x4(%ebx),%edx
80104a11:	89 10                	mov    %edx,(%eax)
  return 0;
80104a13:	31 c0                	xor    %eax,%eax
}
80104a15:	5b                   	pop    %ebx
80104a16:	5e                   	pop    %esi
80104a17:	5d                   	pop    %ebp
80104a18:	c3                   	ret    
80104a19:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104a20:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104a25:	eb ee                	jmp    80104a15 <argint+0x35>
80104a27:	89 f6                	mov    %esi,%esi
80104a29:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104a30 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80104a30:	55                   	push   %ebp
80104a31:	89 e5                	mov    %esp,%ebp
80104a33:	56                   	push   %esi
80104a34:	53                   	push   %ebx
80104a35:	83 ec 10             	sub    $0x10,%esp
80104a38:	8b 5d 10             	mov    0x10(%ebp),%ebx
  int i;
  struct proc *curproc = myproc();
80104a3b:	e8 30 f0 ff ff       	call   80103a70 <myproc>
80104a40:	89 c6                	mov    %eax,%esi
 
  if(argint(n, &i) < 0)
80104a42:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104a45:	83 ec 08             	sub    $0x8,%esp
80104a48:	50                   	push   %eax
80104a49:	ff 75 08             	pushl  0x8(%ebp)
80104a4c:	e8 8f ff ff ff       	call   801049e0 <argint>
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
80104a51:	83 c4 10             	add    $0x10,%esp
80104a54:	85 c0                	test   %eax,%eax
80104a56:	78 28                	js     80104a80 <argptr+0x50>
80104a58:	85 db                	test   %ebx,%ebx
80104a5a:	78 24                	js     80104a80 <argptr+0x50>
80104a5c:	8b 16                	mov    (%esi),%edx
80104a5e:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104a61:	39 c2                	cmp    %eax,%edx
80104a63:	76 1b                	jbe    80104a80 <argptr+0x50>
80104a65:	01 c3                	add    %eax,%ebx
80104a67:	39 da                	cmp    %ebx,%edx
80104a69:	72 15                	jb     80104a80 <argptr+0x50>
    return -1;
  *pp = (char*)i;
80104a6b:	8b 55 0c             	mov    0xc(%ebp),%edx
80104a6e:	89 02                	mov    %eax,(%edx)
  return 0;
80104a70:	31 c0                	xor    %eax,%eax
}
80104a72:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104a75:	5b                   	pop    %ebx
80104a76:	5e                   	pop    %esi
80104a77:	5d                   	pop    %ebp
80104a78:	c3                   	ret    
80104a79:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104a80:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a85:	eb eb                	jmp    80104a72 <argptr+0x42>
80104a87:	89 f6                	mov    %esi,%esi
80104a89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104a90 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
80104a90:	55                   	push   %ebp
80104a91:	89 e5                	mov    %esp,%ebp
80104a93:	83 ec 20             	sub    $0x20,%esp
  int addr;
  if(argint(n, &addr) < 0)
80104a96:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104a99:	50                   	push   %eax
80104a9a:	ff 75 08             	pushl  0x8(%ebp)
80104a9d:	e8 3e ff ff ff       	call   801049e0 <argint>
80104aa2:	83 c4 10             	add    $0x10,%esp
80104aa5:	85 c0                	test   %eax,%eax
80104aa7:	78 17                	js     80104ac0 <argstr+0x30>
    return -1;
  return fetchstr(addr, pp);
80104aa9:	83 ec 08             	sub    $0x8,%esp
80104aac:	ff 75 0c             	pushl  0xc(%ebp)
80104aaf:	ff 75 f4             	pushl  -0xc(%ebp)
80104ab2:	e8 b9 fe ff ff       	call   80104970 <fetchstr>
80104ab7:	83 c4 10             	add    $0x10,%esp
}
80104aba:	c9                   	leave  
80104abb:	c3                   	ret    
80104abc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104ac0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104ac5:	c9                   	leave  
80104ac6:	c3                   	ret    
80104ac7:	89 f6                	mov    %esi,%esi
80104ac9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104ad0 <syscall>:
[SYS_close]   sys_close,
};

void
syscall(void)
{
80104ad0:	55                   	push   %ebp
80104ad1:	89 e5                	mov    %esp,%ebp
80104ad3:	53                   	push   %ebx
80104ad4:	83 ec 04             	sub    $0x4,%esp
  int num;
  struct proc *curproc = myproc();
80104ad7:	e8 94 ef ff ff       	call   80103a70 <myproc>
80104adc:	89 c3                	mov    %eax,%ebx

  num = curproc->tf->eax;
80104ade:	8b 40 18             	mov    0x18(%eax),%eax
80104ae1:	8b 40 1c             	mov    0x1c(%eax),%eax
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80104ae4:	8d 50 ff             	lea    -0x1(%eax),%edx
80104ae7:	83 fa 14             	cmp    $0x14,%edx
80104aea:	77 1c                	ja     80104b08 <syscall+0x38>
80104aec:	8b 14 85 20 78 10 80 	mov    -0x7fef87e0(,%eax,4),%edx
80104af3:	85 d2                	test   %edx,%edx
80104af5:	74 11                	je     80104b08 <syscall+0x38>
    curproc->tf->eax = syscalls[num]();
80104af7:	ff d2                	call   *%edx
80104af9:	8b 53 18             	mov    0x18(%ebx),%edx
80104afc:	89 42 1c             	mov    %eax,0x1c(%edx)
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
80104aff:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104b02:	c9                   	leave  
80104b03:	c3                   	ret    
80104b04:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("%d %s: unknown sys call %d\n",
80104b08:	50                   	push   %eax
            curproc->pid, curproc->name, num);
80104b09:	8d 43 6c             	lea    0x6c(%ebx),%eax
    cprintf("%d %s: unknown sys call %d\n",
80104b0c:	50                   	push   %eax
80104b0d:	ff 73 10             	pushl  0x10(%ebx)
80104b10:	68 f1 77 10 80       	push   $0x801077f1
80104b15:	e8 56 bc ff ff       	call   80100770 <cprintf>
    curproc->tf->eax = -1;
80104b1a:	8b 43 18             	mov    0x18(%ebx),%eax
80104b1d:	83 c4 10             	add    $0x10,%esp
80104b20:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
}
80104b27:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104b2a:	c9                   	leave  
80104b2b:	c3                   	ret    
80104b2c:	66 90                	xchg   %ax,%ax
80104b2e:	66 90                	xchg   %ax,%ax

80104b30 <create>:
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
80104b30:	55                   	push   %ebp
80104b31:	89 e5                	mov    %esp,%ebp
80104b33:	57                   	push   %edi
80104b34:	56                   	push   %esi
80104b35:	53                   	push   %ebx
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80104b36:	8d 75 da             	lea    -0x26(%ebp),%esi
{
80104b39:	83 ec 34             	sub    $0x34,%esp
80104b3c:	89 4d d0             	mov    %ecx,-0x30(%ebp)
80104b3f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  if((dp = nameiparent(path, name)) == 0)
80104b42:	56                   	push   %esi
80104b43:	50                   	push   %eax
{
80104b44:	89 55 d4             	mov    %edx,-0x2c(%ebp)
80104b47:	89 4d cc             	mov    %ecx,-0x34(%ebp)
  if((dp = nameiparent(path, name)) == 0)
80104b4a:	e8 41 d6 ff ff       	call   80102190 <nameiparent>
80104b4f:	83 c4 10             	add    $0x10,%esp
80104b52:	85 c0                	test   %eax,%eax
80104b54:	0f 84 46 01 00 00    	je     80104ca0 <create+0x170>
    return 0;
  ilock(dp);
80104b5a:	83 ec 0c             	sub    $0xc,%esp
80104b5d:	89 c3                	mov    %eax,%ebx
80104b5f:	50                   	push   %eax
80104b60:	e8 ab cd ff ff       	call   80101910 <ilock>

  if((ip = dirlookup(dp, name, 0)) != 0){
80104b65:	83 c4 0c             	add    $0xc,%esp
80104b68:	6a 00                	push   $0x0
80104b6a:	56                   	push   %esi
80104b6b:	53                   	push   %ebx
80104b6c:	e8 cf d2 ff ff       	call   80101e40 <dirlookup>
80104b71:	83 c4 10             	add    $0x10,%esp
80104b74:	85 c0                	test   %eax,%eax
80104b76:	89 c7                	mov    %eax,%edi
80104b78:	74 36                	je     80104bb0 <create+0x80>
    iunlockput(dp);
80104b7a:	83 ec 0c             	sub    $0xc,%esp
80104b7d:	53                   	push   %ebx
80104b7e:	e8 1d d0 ff ff       	call   80101ba0 <iunlockput>
    ilock(ip);
80104b83:	89 3c 24             	mov    %edi,(%esp)
80104b86:	e8 85 cd ff ff       	call   80101910 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
80104b8b:	83 c4 10             	add    $0x10,%esp
80104b8e:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80104b93:	0f 85 97 00 00 00    	jne    80104c30 <create+0x100>
80104b99:	66 83 7f 50 02       	cmpw   $0x2,0x50(%edi)
80104b9e:	0f 85 8c 00 00 00    	jne    80104c30 <create+0x100>
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
80104ba4:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104ba7:	89 f8                	mov    %edi,%eax
80104ba9:	5b                   	pop    %ebx
80104baa:	5e                   	pop    %esi
80104bab:	5f                   	pop    %edi
80104bac:	5d                   	pop    %ebp
80104bad:	c3                   	ret    
80104bae:	66 90                	xchg   %ax,%ax
  if((ip = ialloc(dp->dev, type)) == 0)
80104bb0:	0f bf 45 d4          	movswl -0x2c(%ebp),%eax
80104bb4:	83 ec 08             	sub    $0x8,%esp
80104bb7:	50                   	push   %eax
80104bb8:	ff 33                	pushl  (%ebx)
80104bba:	e8 e1 cb ff ff       	call   801017a0 <ialloc>
80104bbf:	83 c4 10             	add    $0x10,%esp
80104bc2:	85 c0                	test   %eax,%eax
80104bc4:	89 c7                	mov    %eax,%edi
80104bc6:	0f 84 e8 00 00 00    	je     80104cb4 <create+0x184>
  ilock(ip);
80104bcc:	83 ec 0c             	sub    $0xc,%esp
80104bcf:	50                   	push   %eax
80104bd0:	e8 3b cd ff ff       	call   80101910 <ilock>
  ip->major = major;
80104bd5:	0f b7 45 d0          	movzwl -0x30(%ebp),%eax
80104bd9:	66 89 47 52          	mov    %ax,0x52(%edi)
  ip->minor = minor;
80104bdd:	0f b7 45 cc          	movzwl -0x34(%ebp),%eax
80104be1:	66 89 47 54          	mov    %ax,0x54(%edi)
  ip->nlink = 1;
80104be5:	b8 01 00 00 00       	mov    $0x1,%eax
80104bea:	66 89 47 56          	mov    %ax,0x56(%edi)
  iupdate(ip);
80104bee:	89 3c 24             	mov    %edi,(%esp)
80104bf1:	e8 6a cc ff ff       	call   80101860 <iupdate>
  if(type == T_DIR){  // Create . and .. entries.
80104bf6:	83 c4 10             	add    $0x10,%esp
80104bf9:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
80104bfe:	74 50                	je     80104c50 <create+0x120>
  if(dirlink(dp, name, ip->inum) < 0)
80104c00:	83 ec 04             	sub    $0x4,%esp
80104c03:	ff 77 04             	pushl  0x4(%edi)
80104c06:	56                   	push   %esi
80104c07:	53                   	push   %ebx
80104c08:	e8 a3 d4 ff ff       	call   801020b0 <dirlink>
80104c0d:	83 c4 10             	add    $0x10,%esp
80104c10:	85 c0                	test   %eax,%eax
80104c12:	0f 88 8f 00 00 00    	js     80104ca7 <create+0x177>
  iunlockput(dp);
80104c18:	83 ec 0c             	sub    $0xc,%esp
80104c1b:	53                   	push   %ebx
80104c1c:	e8 7f cf ff ff       	call   80101ba0 <iunlockput>
  return ip;
80104c21:	83 c4 10             	add    $0x10,%esp
}
80104c24:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c27:	89 f8                	mov    %edi,%eax
80104c29:	5b                   	pop    %ebx
80104c2a:	5e                   	pop    %esi
80104c2b:	5f                   	pop    %edi
80104c2c:	5d                   	pop    %ebp
80104c2d:	c3                   	ret    
80104c2e:	66 90                	xchg   %ax,%ax
    iunlockput(ip);
80104c30:	83 ec 0c             	sub    $0xc,%esp
80104c33:	57                   	push   %edi
    return 0;
80104c34:	31 ff                	xor    %edi,%edi
    iunlockput(ip);
80104c36:	e8 65 cf ff ff       	call   80101ba0 <iunlockput>
    return 0;
80104c3b:	83 c4 10             	add    $0x10,%esp
}
80104c3e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104c41:	89 f8                	mov    %edi,%eax
80104c43:	5b                   	pop    %ebx
80104c44:	5e                   	pop    %esi
80104c45:	5f                   	pop    %edi
80104c46:	5d                   	pop    %ebp
80104c47:	c3                   	ret    
80104c48:	90                   	nop
80104c49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    dp->nlink++;  // for ".."
80104c50:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
    iupdate(dp);
80104c55:	83 ec 0c             	sub    $0xc,%esp
80104c58:	53                   	push   %ebx
80104c59:	e8 02 cc ff ff       	call   80101860 <iupdate>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
80104c5e:	83 c4 0c             	add    $0xc,%esp
80104c61:	ff 77 04             	pushl  0x4(%edi)
80104c64:	68 94 78 10 80       	push   $0x80107894
80104c69:	57                   	push   %edi
80104c6a:	e8 41 d4 ff ff       	call   801020b0 <dirlink>
80104c6f:	83 c4 10             	add    $0x10,%esp
80104c72:	85 c0                	test   %eax,%eax
80104c74:	78 1c                	js     80104c92 <create+0x162>
80104c76:	83 ec 04             	sub    $0x4,%esp
80104c79:	ff 73 04             	pushl  0x4(%ebx)
80104c7c:	68 93 78 10 80       	push   $0x80107893
80104c81:	57                   	push   %edi
80104c82:	e8 29 d4 ff ff       	call   801020b0 <dirlink>
80104c87:	83 c4 10             	add    $0x10,%esp
80104c8a:	85 c0                	test   %eax,%eax
80104c8c:	0f 89 6e ff ff ff    	jns    80104c00 <create+0xd0>
      panic("create dots");
80104c92:	83 ec 0c             	sub    $0xc,%esp
80104c95:	68 87 78 10 80       	push   $0x80107887
80104c9a:	e8 f1 b6 ff ff       	call   80100390 <panic>
80104c9f:	90                   	nop
    return 0;
80104ca0:	31 ff                	xor    %edi,%edi
80104ca2:	e9 fd fe ff ff       	jmp    80104ba4 <create+0x74>
    panic("create: dirlink");
80104ca7:	83 ec 0c             	sub    $0xc,%esp
80104caa:	68 96 78 10 80       	push   $0x80107896
80104caf:	e8 dc b6 ff ff       	call   80100390 <panic>
    panic("create: ialloc");
80104cb4:	83 ec 0c             	sub    $0xc,%esp
80104cb7:	68 78 78 10 80       	push   $0x80107878
80104cbc:	e8 cf b6 ff ff       	call   80100390 <panic>
80104cc1:	eb 0d                	jmp    80104cd0 <argfd.constprop.0>
80104cc3:	90                   	nop
80104cc4:	90                   	nop
80104cc5:	90                   	nop
80104cc6:	90                   	nop
80104cc7:	90                   	nop
80104cc8:	90                   	nop
80104cc9:	90                   	nop
80104cca:	90                   	nop
80104ccb:	90                   	nop
80104ccc:	90                   	nop
80104ccd:	90                   	nop
80104cce:	90                   	nop
80104ccf:	90                   	nop

80104cd0 <argfd.constprop.0>:
argfd(int n, int *pfd, struct file **pf)
80104cd0:	55                   	push   %ebp
80104cd1:	89 e5                	mov    %esp,%ebp
80104cd3:	56                   	push   %esi
80104cd4:	53                   	push   %ebx
80104cd5:	89 c3                	mov    %eax,%ebx
  if(argint(n, &fd) < 0)
80104cd7:	8d 45 f4             	lea    -0xc(%ebp),%eax
argfd(int n, int *pfd, struct file **pf)
80104cda:	89 d6                	mov    %edx,%esi
80104cdc:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
80104cdf:	50                   	push   %eax
80104ce0:	6a 00                	push   $0x0
80104ce2:	e8 f9 fc ff ff       	call   801049e0 <argint>
80104ce7:	83 c4 10             	add    $0x10,%esp
80104cea:	85 c0                	test   %eax,%eax
80104cec:	78 2a                	js     80104d18 <argfd.constprop.0+0x48>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80104cee:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104cf2:	77 24                	ja     80104d18 <argfd.constprop.0+0x48>
80104cf4:	e8 77 ed ff ff       	call   80103a70 <myproc>
80104cf9:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104cfc:	8b 44 90 28          	mov    0x28(%eax,%edx,4),%eax
80104d00:	85 c0                	test   %eax,%eax
80104d02:	74 14                	je     80104d18 <argfd.constprop.0+0x48>
  if(pfd)
80104d04:	85 db                	test   %ebx,%ebx
80104d06:	74 02                	je     80104d0a <argfd.constprop.0+0x3a>
    *pfd = fd;
80104d08:	89 13                	mov    %edx,(%ebx)
    *pf = f;
80104d0a:	89 06                	mov    %eax,(%esi)
  return 0;
80104d0c:	31 c0                	xor    %eax,%eax
}
80104d0e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104d11:	5b                   	pop    %ebx
80104d12:	5e                   	pop    %esi
80104d13:	5d                   	pop    %ebp
80104d14:	c3                   	ret    
80104d15:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80104d18:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d1d:	eb ef                	jmp    80104d0e <argfd.constprop.0+0x3e>
80104d1f:	90                   	nop

80104d20 <sys_dup>:
{
80104d20:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0)
80104d21:	31 c0                	xor    %eax,%eax
{
80104d23:	89 e5                	mov    %esp,%ebp
80104d25:	56                   	push   %esi
80104d26:	53                   	push   %ebx
  if(argfd(0, 0, &f) < 0)
80104d27:	8d 55 f4             	lea    -0xc(%ebp),%edx
{
80104d2a:	83 ec 10             	sub    $0x10,%esp
  if(argfd(0, 0, &f) < 0)
80104d2d:	e8 9e ff ff ff       	call   80104cd0 <argfd.constprop.0>
80104d32:	85 c0                	test   %eax,%eax
80104d34:	78 42                	js     80104d78 <sys_dup+0x58>
  if((fd=fdalloc(f)) < 0)
80104d36:	8b 75 f4             	mov    -0xc(%ebp),%esi
  for(fd = 0; fd < NOFILE; fd++){
80104d39:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
80104d3b:	e8 30 ed ff ff       	call   80103a70 <myproc>
80104d40:	eb 0e                	jmp    80104d50 <sys_dup+0x30>
80104d42:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  for(fd = 0; fd < NOFILE; fd++){
80104d48:	83 c3 01             	add    $0x1,%ebx
80104d4b:	83 fb 10             	cmp    $0x10,%ebx
80104d4e:	74 28                	je     80104d78 <sys_dup+0x58>
    if(curproc->ofile[fd] == 0){
80104d50:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80104d54:	85 d2                	test   %edx,%edx
80104d56:	75 f0                	jne    80104d48 <sys_dup+0x28>
      curproc->ofile[fd] = f;
80104d58:	89 74 98 28          	mov    %esi,0x28(%eax,%ebx,4)
  filedup(f);
80104d5c:	83 ec 0c             	sub    $0xc,%esp
80104d5f:	ff 75 f4             	pushl  -0xc(%ebp)
80104d62:	e8 19 c3 ff ff       	call   80101080 <filedup>
  return fd;
80104d67:	83 c4 10             	add    $0x10,%esp
}
80104d6a:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104d6d:	89 d8                	mov    %ebx,%eax
80104d6f:	5b                   	pop    %ebx
80104d70:	5e                   	pop    %esi
80104d71:	5d                   	pop    %ebp
80104d72:	c3                   	ret    
80104d73:	90                   	nop
80104d74:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104d78:	8d 65 f8             	lea    -0x8(%ebp),%esp
    return -1;
80104d7b:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
}
80104d80:	89 d8                	mov    %ebx,%eax
80104d82:	5b                   	pop    %ebx
80104d83:	5e                   	pop    %esi
80104d84:	5d                   	pop    %ebp
80104d85:	c3                   	ret    
80104d86:	8d 76 00             	lea    0x0(%esi),%esi
80104d89:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104d90 <sys_read>:
{
80104d90:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104d91:	31 c0                	xor    %eax,%eax
{
80104d93:	89 e5                	mov    %esp,%ebp
80104d95:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104d98:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104d9b:	e8 30 ff ff ff       	call   80104cd0 <argfd.constprop.0>
80104da0:	85 c0                	test   %eax,%eax
80104da2:	78 4c                	js     80104df0 <sys_read+0x60>
80104da4:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104da7:	83 ec 08             	sub    $0x8,%esp
80104daa:	50                   	push   %eax
80104dab:	6a 02                	push   $0x2
80104dad:	e8 2e fc ff ff       	call   801049e0 <argint>
80104db2:	83 c4 10             	add    $0x10,%esp
80104db5:	85 c0                	test   %eax,%eax
80104db7:	78 37                	js     80104df0 <sys_read+0x60>
80104db9:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104dbc:	83 ec 04             	sub    $0x4,%esp
80104dbf:	ff 75 f0             	pushl  -0x10(%ebp)
80104dc2:	50                   	push   %eax
80104dc3:	6a 01                	push   $0x1
80104dc5:	e8 66 fc ff ff       	call   80104a30 <argptr>
80104dca:	83 c4 10             	add    $0x10,%esp
80104dcd:	85 c0                	test   %eax,%eax
80104dcf:	78 1f                	js     80104df0 <sys_read+0x60>
  return fileread(f, p, n);
80104dd1:	83 ec 04             	sub    $0x4,%esp
80104dd4:	ff 75 f0             	pushl  -0x10(%ebp)
80104dd7:	ff 75 f4             	pushl  -0xc(%ebp)
80104dda:	ff 75 ec             	pushl  -0x14(%ebp)
80104ddd:	e8 0e c4 ff ff       	call   801011f0 <fileread>
80104de2:	83 c4 10             	add    $0x10,%esp
}
80104de5:	c9                   	leave  
80104de6:	c3                   	ret    
80104de7:	89 f6                	mov    %esi,%esi
80104de9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80104df0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104df5:	c9                   	leave  
80104df6:	c3                   	ret    
80104df7:	89 f6                	mov    %esi,%esi
80104df9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104e00 <sys_write>:
{
80104e00:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e01:	31 c0                	xor    %eax,%eax
{
80104e03:	89 e5                	mov    %esp,%ebp
80104e05:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104e08:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104e0b:	e8 c0 fe ff ff       	call   80104cd0 <argfd.constprop.0>
80104e10:	85 c0                	test   %eax,%eax
80104e12:	78 4c                	js     80104e60 <sys_write+0x60>
80104e14:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e17:	83 ec 08             	sub    $0x8,%esp
80104e1a:	50                   	push   %eax
80104e1b:	6a 02                	push   $0x2
80104e1d:	e8 be fb ff ff       	call   801049e0 <argint>
80104e22:	83 c4 10             	add    $0x10,%esp
80104e25:	85 c0                	test   %eax,%eax
80104e27:	78 37                	js     80104e60 <sys_write+0x60>
80104e29:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e2c:	83 ec 04             	sub    $0x4,%esp
80104e2f:	ff 75 f0             	pushl  -0x10(%ebp)
80104e32:	50                   	push   %eax
80104e33:	6a 01                	push   $0x1
80104e35:	e8 f6 fb ff ff       	call   80104a30 <argptr>
80104e3a:	83 c4 10             	add    $0x10,%esp
80104e3d:	85 c0                	test   %eax,%eax
80104e3f:	78 1f                	js     80104e60 <sys_write+0x60>
  return filewrite(f, p, n);
80104e41:	83 ec 04             	sub    $0x4,%esp
80104e44:	ff 75 f0             	pushl  -0x10(%ebp)
80104e47:	ff 75 f4             	pushl  -0xc(%ebp)
80104e4a:	ff 75 ec             	pushl  -0x14(%ebp)
80104e4d:	e8 2e c4 ff ff       	call   80101280 <filewrite>
80104e52:	83 c4 10             	add    $0x10,%esp
}
80104e55:	c9                   	leave  
80104e56:	c3                   	ret    
80104e57:	89 f6                	mov    %esi,%esi
80104e59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80104e60:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104e65:	c9                   	leave  
80104e66:	c3                   	ret    
80104e67:	89 f6                	mov    %esi,%esi
80104e69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104e70 <sys_close>:
{
80104e70:	55                   	push   %ebp
80104e71:	89 e5                	mov    %esp,%ebp
80104e73:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, &fd, &f) < 0)
80104e76:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104e79:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e7c:	e8 4f fe ff ff       	call   80104cd0 <argfd.constprop.0>
80104e81:	85 c0                	test   %eax,%eax
80104e83:	78 2b                	js     80104eb0 <sys_close+0x40>
  myproc()->ofile[fd] = 0;
80104e85:	e8 e6 eb ff ff       	call   80103a70 <myproc>
80104e8a:	8b 55 f0             	mov    -0x10(%ebp),%edx
  fileclose(f);
80104e8d:	83 ec 0c             	sub    $0xc,%esp
  myproc()->ofile[fd] = 0;
80104e90:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
80104e97:	00 
  fileclose(f);
80104e98:	ff 75 f4             	pushl  -0xc(%ebp)
80104e9b:	e8 30 c2 ff ff       	call   801010d0 <fileclose>
  return 0;
80104ea0:	83 c4 10             	add    $0x10,%esp
80104ea3:	31 c0                	xor    %eax,%eax
}
80104ea5:	c9                   	leave  
80104ea6:	c3                   	ret    
80104ea7:	89 f6                	mov    %esi,%esi
80104ea9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    return -1;
80104eb0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104eb5:	c9                   	leave  
80104eb6:	c3                   	ret    
80104eb7:	89 f6                	mov    %esi,%esi
80104eb9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104ec0 <sys_fstat>:
{
80104ec0:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104ec1:	31 c0                	xor    %eax,%eax
{
80104ec3:	89 e5                	mov    %esp,%ebp
80104ec5:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104ec8:	8d 55 f0             	lea    -0x10(%ebp),%edx
80104ecb:	e8 00 fe ff ff       	call   80104cd0 <argfd.constprop.0>
80104ed0:	85 c0                	test   %eax,%eax
80104ed2:	78 2c                	js     80104f00 <sys_fstat+0x40>
80104ed4:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104ed7:	83 ec 04             	sub    $0x4,%esp
80104eda:	6a 14                	push   $0x14
80104edc:	50                   	push   %eax
80104edd:	6a 01                	push   $0x1
80104edf:	e8 4c fb ff ff       	call   80104a30 <argptr>
80104ee4:	83 c4 10             	add    $0x10,%esp
80104ee7:	85 c0                	test   %eax,%eax
80104ee9:	78 15                	js     80104f00 <sys_fstat+0x40>
  return filestat(f, st);
80104eeb:	83 ec 08             	sub    $0x8,%esp
80104eee:	ff 75 f4             	pushl  -0xc(%ebp)
80104ef1:	ff 75 f0             	pushl  -0x10(%ebp)
80104ef4:	e8 a7 c2 ff ff       	call   801011a0 <filestat>
80104ef9:	83 c4 10             	add    $0x10,%esp
}
80104efc:	c9                   	leave  
80104efd:	c3                   	ret    
80104efe:	66 90                	xchg   %ax,%ax
    return -1;
80104f00:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104f05:	c9                   	leave  
80104f06:	c3                   	ret    
80104f07:	89 f6                	mov    %esi,%esi
80104f09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80104f10 <sys_link>:
{
80104f10:	55                   	push   %ebp
80104f11:	89 e5                	mov    %esp,%ebp
80104f13:	57                   	push   %edi
80104f14:	56                   	push   %esi
80104f15:	53                   	push   %ebx
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104f16:	8d 45 d4             	lea    -0x2c(%ebp),%eax
{
80104f19:	83 ec 34             	sub    $0x34,%esp
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104f1c:	50                   	push   %eax
80104f1d:	6a 00                	push   $0x0
80104f1f:	e8 6c fb ff ff       	call   80104a90 <argstr>
80104f24:	83 c4 10             	add    $0x10,%esp
80104f27:	85 c0                	test   %eax,%eax
80104f29:	0f 88 fb 00 00 00    	js     8010502a <sys_link+0x11a>
80104f2f:	8d 45 d0             	lea    -0x30(%ebp),%eax
80104f32:	83 ec 08             	sub    $0x8,%esp
80104f35:	50                   	push   %eax
80104f36:	6a 01                	push   $0x1
80104f38:	e8 53 fb ff ff       	call   80104a90 <argstr>
80104f3d:	83 c4 10             	add    $0x10,%esp
80104f40:	85 c0                	test   %eax,%eax
80104f42:	0f 88 e2 00 00 00    	js     8010502a <sys_link+0x11a>
  begin_op();
80104f48:	e8 e3 de ff ff       	call   80102e30 <begin_op>
  if((ip = namei(old)) == 0){
80104f4d:	83 ec 0c             	sub    $0xc,%esp
80104f50:	ff 75 d4             	pushl  -0x2c(%ebp)
80104f53:	e8 18 d2 ff ff       	call   80102170 <namei>
80104f58:	83 c4 10             	add    $0x10,%esp
80104f5b:	85 c0                	test   %eax,%eax
80104f5d:	89 c3                	mov    %eax,%ebx
80104f5f:	0f 84 ea 00 00 00    	je     8010504f <sys_link+0x13f>
  ilock(ip);
80104f65:	83 ec 0c             	sub    $0xc,%esp
80104f68:	50                   	push   %eax
80104f69:	e8 a2 c9 ff ff       	call   80101910 <ilock>
  if(ip->type == T_DIR){
80104f6e:	83 c4 10             	add    $0x10,%esp
80104f71:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80104f76:	0f 84 bb 00 00 00    	je     80105037 <sys_link+0x127>
  ip->nlink++;
80104f7c:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
  iupdate(ip);
80104f81:	83 ec 0c             	sub    $0xc,%esp
  if((dp = nameiparent(new, name)) == 0)
80104f84:	8d 7d da             	lea    -0x26(%ebp),%edi
  iupdate(ip);
80104f87:	53                   	push   %ebx
80104f88:	e8 d3 c8 ff ff       	call   80101860 <iupdate>
  iunlock(ip);
80104f8d:	89 1c 24             	mov    %ebx,(%esp)
80104f90:	e8 5b ca ff ff       	call   801019f0 <iunlock>
  if((dp = nameiparent(new, name)) == 0)
80104f95:	58                   	pop    %eax
80104f96:	5a                   	pop    %edx
80104f97:	57                   	push   %edi
80104f98:	ff 75 d0             	pushl  -0x30(%ebp)
80104f9b:	e8 f0 d1 ff ff       	call   80102190 <nameiparent>
80104fa0:	83 c4 10             	add    $0x10,%esp
80104fa3:	85 c0                	test   %eax,%eax
80104fa5:	89 c6                	mov    %eax,%esi
80104fa7:	74 5b                	je     80105004 <sys_link+0xf4>
  ilock(dp);
80104fa9:	83 ec 0c             	sub    $0xc,%esp
80104fac:	50                   	push   %eax
80104fad:	e8 5e c9 ff ff       	call   80101910 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80104fb2:	83 c4 10             	add    $0x10,%esp
80104fb5:	8b 03                	mov    (%ebx),%eax
80104fb7:	39 06                	cmp    %eax,(%esi)
80104fb9:	75 3d                	jne    80104ff8 <sys_link+0xe8>
80104fbb:	83 ec 04             	sub    $0x4,%esp
80104fbe:	ff 73 04             	pushl  0x4(%ebx)
80104fc1:	57                   	push   %edi
80104fc2:	56                   	push   %esi
80104fc3:	e8 e8 d0 ff ff       	call   801020b0 <dirlink>
80104fc8:	83 c4 10             	add    $0x10,%esp
80104fcb:	85 c0                	test   %eax,%eax
80104fcd:	78 29                	js     80104ff8 <sys_link+0xe8>
  iunlockput(dp);
80104fcf:	83 ec 0c             	sub    $0xc,%esp
80104fd2:	56                   	push   %esi
80104fd3:	e8 c8 cb ff ff       	call   80101ba0 <iunlockput>
  iput(ip);
80104fd8:	89 1c 24             	mov    %ebx,(%esp)
80104fdb:	e8 60 ca ff ff       	call   80101a40 <iput>
  end_op();
80104fe0:	e8 bb de ff ff       	call   80102ea0 <end_op>
  return 0;
80104fe5:	83 c4 10             	add    $0x10,%esp
80104fe8:	31 c0                	xor    %eax,%eax
}
80104fea:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104fed:	5b                   	pop    %ebx
80104fee:	5e                   	pop    %esi
80104fef:	5f                   	pop    %edi
80104ff0:	5d                   	pop    %ebp
80104ff1:	c3                   	ret    
80104ff2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iunlockput(dp);
80104ff8:	83 ec 0c             	sub    $0xc,%esp
80104ffb:	56                   	push   %esi
80104ffc:	e8 9f cb ff ff       	call   80101ba0 <iunlockput>
    goto bad;
80105001:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80105004:	83 ec 0c             	sub    $0xc,%esp
80105007:	53                   	push   %ebx
80105008:	e8 03 c9 ff ff       	call   80101910 <ilock>
  ip->nlink--;
8010500d:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105012:	89 1c 24             	mov    %ebx,(%esp)
80105015:	e8 46 c8 ff ff       	call   80101860 <iupdate>
  iunlockput(ip);
8010501a:	89 1c 24             	mov    %ebx,(%esp)
8010501d:	e8 7e cb ff ff       	call   80101ba0 <iunlockput>
  end_op();
80105022:	e8 79 de ff ff       	call   80102ea0 <end_op>
  return -1;
80105027:	83 c4 10             	add    $0x10,%esp
}
8010502a:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
8010502d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105032:	5b                   	pop    %ebx
80105033:	5e                   	pop    %esi
80105034:	5f                   	pop    %edi
80105035:	5d                   	pop    %ebp
80105036:	c3                   	ret    
    iunlockput(ip);
80105037:	83 ec 0c             	sub    $0xc,%esp
8010503a:	53                   	push   %ebx
8010503b:	e8 60 cb ff ff       	call   80101ba0 <iunlockput>
    end_op();
80105040:	e8 5b de ff ff       	call   80102ea0 <end_op>
    return -1;
80105045:	83 c4 10             	add    $0x10,%esp
80105048:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010504d:	eb 9b                	jmp    80104fea <sys_link+0xda>
    end_op();
8010504f:	e8 4c de ff ff       	call   80102ea0 <end_op>
    return -1;
80105054:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105059:	eb 8f                	jmp    80104fea <sys_link+0xda>
8010505b:	90                   	nop
8010505c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105060 <sys_unlink>:
{
80105060:	55                   	push   %ebp
80105061:	89 e5                	mov    %esp,%ebp
80105063:	57                   	push   %edi
80105064:	56                   	push   %esi
80105065:	53                   	push   %ebx
  if(argstr(0, &path) < 0)
80105066:	8d 45 c0             	lea    -0x40(%ebp),%eax
{
80105069:	83 ec 44             	sub    $0x44,%esp
  if(argstr(0, &path) < 0)
8010506c:	50                   	push   %eax
8010506d:	6a 00                	push   $0x0
8010506f:	e8 1c fa ff ff       	call   80104a90 <argstr>
80105074:	83 c4 10             	add    $0x10,%esp
80105077:	85 c0                	test   %eax,%eax
80105079:	0f 88 77 01 00 00    	js     801051f6 <sys_unlink+0x196>
  if((dp = nameiparent(path, name)) == 0){
8010507f:	8d 5d ca             	lea    -0x36(%ebp),%ebx
  begin_op();
80105082:	e8 a9 dd ff ff       	call   80102e30 <begin_op>
  if((dp = nameiparent(path, name)) == 0){
80105087:	83 ec 08             	sub    $0x8,%esp
8010508a:	53                   	push   %ebx
8010508b:	ff 75 c0             	pushl  -0x40(%ebp)
8010508e:	e8 fd d0 ff ff       	call   80102190 <nameiparent>
80105093:	83 c4 10             	add    $0x10,%esp
80105096:	85 c0                	test   %eax,%eax
80105098:	89 c6                	mov    %eax,%esi
8010509a:	0f 84 60 01 00 00    	je     80105200 <sys_unlink+0x1a0>
  ilock(dp);
801050a0:	83 ec 0c             	sub    $0xc,%esp
801050a3:	50                   	push   %eax
801050a4:	e8 67 c8 ff ff       	call   80101910 <ilock>
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
801050a9:	58                   	pop    %eax
801050aa:	5a                   	pop    %edx
801050ab:	68 94 78 10 80       	push   $0x80107894
801050b0:	53                   	push   %ebx
801050b1:	e8 6a cd ff ff       	call   80101e20 <namecmp>
801050b6:	83 c4 10             	add    $0x10,%esp
801050b9:	85 c0                	test   %eax,%eax
801050bb:	0f 84 03 01 00 00    	je     801051c4 <sys_unlink+0x164>
801050c1:	83 ec 08             	sub    $0x8,%esp
801050c4:	68 93 78 10 80       	push   $0x80107893
801050c9:	53                   	push   %ebx
801050ca:	e8 51 cd ff ff       	call   80101e20 <namecmp>
801050cf:	83 c4 10             	add    $0x10,%esp
801050d2:	85 c0                	test   %eax,%eax
801050d4:	0f 84 ea 00 00 00    	je     801051c4 <sys_unlink+0x164>
  if((ip = dirlookup(dp, name, &off)) == 0)
801050da:	8d 45 c4             	lea    -0x3c(%ebp),%eax
801050dd:	83 ec 04             	sub    $0x4,%esp
801050e0:	50                   	push   %eax
801050e1:	53                   	push   %ebx
801050e2:	56                   	push   %esi
801050e3:	e8 58 cd ff ff       	call   80101e40 <dirlookup>
801050e8:	83 c4 10             	add    $0x10,%esp
801050eb:	85 c0                	test   %eax,%eax
801050ed:	89 c3                	mov    %eax,%ebx
801050ef:	0f 84 cf 00 00 00    	je     801051c4 <sys_unlink+0x164>
  ilock(ip);
801050f5:	83 ec 0c             	sub    $0xc,%esp
801050f8:	50                   	push   %eax
801050f9:	e8 12 c8 ff ff       	call   80101910 <ilock>
  if(ip->nlink < 1)
801050fe:	83 c4 10             	add    $0x10,%esp
80105101:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80105106:	0f 8e 10 01 00 00    	jle    8010521c <sys_unlink+0x1bc>
  if(ip->type == T_DIR && !isdirempty(ip)){
8010510c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105111:	74 6d                	je     80105180 <sys_unlink+0x120>
  memset(&de, 0, sizeof(de));
80105113:	8d 45 d8             	lea    -0x28(%ebp),%eax
80105116:	83 ec 04             	sub    $0x4,%esp
80105119:	6a 10                	push   $0x10
8010511b:	6a 00                	push   $0x0
8010511d:	50                   	push   %eax
8010511e:	e8 bd f5 ff ff       	call   801046e0 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80105123:	8d 45 d8             	lea    -0x28(%ebp),%eax
80105126:	6a 10                	push   $0x10
80105128:	ff 75 c4             	pushl  -0x3c(%ebp)
8010512b:	50                   	push   %eax
8010512c:	56                   	push   %esi
8010512d:	e8 be cb ff ff       	call   80101cf0 <writei>
80105132:	83 c4 20             	add    $0x20,%esp
80105135:	83 f8 10             	cmp    $0x10,%eax
80105138:	0f 85 eb 00 00 00    	jne    80105229 <sys_unlink+0x1c9>
  if(ip->type == T_DIR){
8010513e:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105143:	0f 84 97 00 00 00    	je     801051e0 <sys_unlink+0x180>
  iunlockput(dp);
80105149:	83 ec 0c             	sub    $0xc,%esp
8010514c:	56                   	push   %esi
8010514d:	e8 4e ca ff ff       	call   80101ba0 <iunlockput>
  ip->nlink--;
80105152:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105157:	89 1c 24             	mov    %ebx,(%esp)
8010515a:	e8 01 c7 ff ff       	call   80101860 <iupdate>
  iunlockput(ip);
8010515f:	89 1c 24             	mov    %ebx,(%esp)
80105162:	e8 39 ca ff ff       	call   80101ba0 <iunlockput>
  end_op();
80105167:	e8 34 dd ff ff       	call   80102ea0 <end_op>
  return 0;
8010516c:	83 c4 10             	add    $0x10,%esp
8010516f:	31 c0                	xor    %eax,%eax
}
80105171:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105174:	5b                   	pop    %ebx
80105175:	5e                   	pop    %esi
80105176:	5f                   	pop    %edi
80105177:	5d                   	pop    %ebp
80105178:	c3                   	ret    
80105179:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
80105180:	83 7b 58 20          	cmpl   $0x20,0x58(%ebx)
80105184:	76 8d                	jbe    80105113 <sys_unlink+0xb3>
80105186:	bf 20 00 00 00       	mov    $0x20,%edi
8010518b:	eb 0f                	jmp    8010519c <sys_unlink+0x13c>
8010518d:	8d 76 00             	lea    0x0(%esi),%esi
80105190:	83 c7 10             	add    $0x10,%edi
80105193:	3b 7b 58             	cmp    0x58(%ebx),%edi
80105196:	0f 83 77 ff ff ff    	jae    80105113 <sys_unlink+0xb3>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
8010519c:	8d 45 d8             	lea    -0x28(%ebp),%eax
8010519f:	6a 10                	push   $0x10
801051a1:	57                   	push   %edi
801051a2:	50                   	push   %eax
801051a3:	53                   	push   %ebx
801051a4:	e8 47 ca ff ff       	call   80101bf0 <readi>
801051a9:	83 c4 10             	add    $0x10,%esp
801051ac:	83 f8 10             	cmp    $0x10,%eax
801051af:	75 5e                	jne    8010520f <sys_unlink+0x1af>
    if(de.inum != 0)
801051b1:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
801051b6:	74 d8                	je     80105190 <sys_unlink+0x130>
    iunlockput(ip);
801051b8:	83 ec 0c             	sub    $0xc,%esp
801051bb:	53                   	push   %ebx
801051bc:	e8 df c9 ff ff       	call   80101ba0 <iunlockput>
    goto bad;
801051c1:	83 c4 10             	add    $0x10,%esp
  iunlockput(dp);
801051c4:	83 ec 0c             	sub    $0xc,%esp
801051c7:	56                   	push   %esi
801051c8:	e8 d3 c9 ff ff       	call   80101ba0 <iunlockput>
  end_op();
801051cd:	e8 ce dc ff ff       	call   80102ea0 <end_op>
  return -1;
801051d2:	83 c4 10             	add    $0x10,%esp
801051d5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801051da:	eb 95                	jmp    80105171 <sys_unlink+0x111>
801051dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    dp->nlink--;
801051e0:	66 83 6e 56 01       	subw   $0x1,0x56(%esi)
    iupdate(dp);
801051e5:	83 ec 0c             	sub    $0xc,%esp
801051e8:	56                   	push   %esi
801051e9:	e8 72 c6 ff ff       	call   80101860 <iupdate>
801051ee:	83 c4 10             	add    $0x10,%esp
801051f1:	e9 53 ff ff ff       	jmp    80105149 <sys_unlink+0xe9>
    return -1;
801051f6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801051fb:	e9 71 ff ff ff       	jmp    80105171 <sys_unlink+0x111>
    end_op();
80105200:	e8 9b dc ff ff       	call   80102ea0 <end_op>
    return -1;
80105205:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010520a:	e9 62 ff ff ff       	jmp    80105171 <sys_unlink+0x111>
      panic("isdirempty: readi");
8010520f:	83 ec 0c             	sub    $0xc,%esp
80105212:	68 b8 78 10 80       	push   $0x801078b8
80105217:	e8 74 b1 ff ff       	call   80100390 <panic>
    panic("unlink: nlink < 1");
8010521c:	83 ec 0c             	sub    $0xc,%esp
8010521f:	68 a6 78 10 80       	push   $0x801078a6
80105224:	e8 67 b1 ff ff       	call   80100390 <panic>
    panic("unlink: writei");
80105229:	83 ec 0c             	sub    $0xc,%esp
8010522c:	68 ca 78 10 80       	push   $0x801078ca
80105231:	e8 5a b1 ff ff       	call   80100390 <panic>
80105236:	8d 76 00             	lea    0x0(%esi),%esi
80105239:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105240 <sys_open>:

int
sys_open(void)
{
80105240:	55                   	push   %ebp
80105241:	89 e5                	mov    %esp,%ebp
80105243:	57                   	push   %edi
80105244:	56                   	push   %esi
80105245:	53                   	push   %ebx
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80105246:	8d 45 e0             	lea    -0x20(%ebp),%eax
{
80105249:	83 ec 24             	sub    $0x24,%esp
  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
8010524c:	50                   	push   %eax
8010524d:	6a 00                	push   $0x0
8010524f:	e8 3c f8 ff ff       	call   80104a90 <argstr>
80105254:	83 c4 10             	add    $0x10,%esp
80105257:	85 c0                	test   %eax,%eax
80105259:	0f 88 1d 01 00 00    	js     8010537c <sys_open+0x13c>
8010525f:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105262:	83 ec 08             	sub    $0x8,%esp
80105265:	50                   	push   %eax
80105266:	6a 01                	push   $0x1
80105268:	e8 73 f7 ff ff       	call   801049e0 <argint>
8010526d:	83 c4 10             	add    $0x10,%esp
80105270:	85 c0                	test   %eax,%eax
80105272:	0f 88 04 01 00 00    	js     8010537c <sys_open+0x13c>
    return -1;

  begin_op();
80105278:	e8 b3 db ff ff       	call   80102e30 <begin_op>

  if(omode & O_CREATE){
8010527d:	f6 45 e5 02          	testb  $0x2,-0x1b(%ebp)
80105281:	0f 85 a9 00 00 00    	jne    80105330 <sys_open+0xf0>
    if(ip == 0){
      end_op();
      return -1;
    }
  } else {
    if((ip = namei(path)) == 0){
80105287:	83 ec 0c             	sub    $0xc,%esp
8010528a:	ff 75 e0             	pushl  -0x20(%ebp)
8010528d:	e8 de ce ff ff       	call   80102170 <namei>
80105292:	83 c4 10             	add    $0x10,%esp
80105295:	85 c0                	test   %eax,%eax
80105297:	89 c6                	mov    %eax,%esi
80105299:	0f 84 b2 00 00 00    	je     80105351 <sys_open+0x111>
      end_op();
      return -1;
    }
    ilock(ip);
8010529f:	83 ec 0c             	sub    $0xc,%esp
801052a2:	50                   	push   %eax
801052a3:	e8 68 c6 ff ff       	call   80101910 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
801052a8:	83 c4 10             	add    $0x10,%esp
801052ab:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
801052b0:	0f 84 aa 00 00 00    	je     80105360 <sys_open+0x120>
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
801052b6:	e8 55 bd ff ff       	call   80101010 <filealloc>
801052bb:	85 c0                	test   %eax,%eax
801052bd:	89 c7                	mov    %eax,%edi
801052bf:	0f 84 a6 00 00 00    	je     8010536b <sys_open+0x12b>
  struct proc *curproc = myproc();
801052c5:	e8 a6 e7 ff ff       	call   80103a70 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
801052ca:	31 db                	xor    %ebx,%ebx
801052cc:	eb 0e                	jmp    801052dc <sys_open+0x9c>
801052ce:	66 90                	xchg   %ax,%ax
801052d0:	83 c3 01             	add    $0x1,%ebx
801052d3:	83 fb 10             	cmp    $0x10,%ebx
801052d6:	0f 84 ac 00 00 00    	je     80105388 <sys_open+0x148>
    if(curproc->ofile[fd] == 0){
801052dc:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
801052e0:	85 d2                	test   %edx,%edx
801052e2:	75 ec                	jne    801052d0 <sys_open+0x90>
      fileclose(f);
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
801052e4:	83 ec 0c             	sub    $0xc,%esp
      curproc->ofile[fd] = f;
801052e7:	89 7c 98 28          	mov    %edi,0x28(%eax,%ebx,4)
  iunlock(ip);
801052eb:	56                   	push   %esi
801052ec:	e8 ff c6 ff ff       	call   801019f0 <iunlock>
  end_op();
801052f1:	e8 aa db ff ff       	call   80102ea0 <end_op>

  f->type = FD_INODE;
801052f6:	c7 07 02 00 00 00    	movl   $0x2,(%edi)
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
801052fc:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801052ff:	83 c4 10             	add    $0x10,%esp
  f->ip = ip;
80105302:	89 77 10             	mov    %esi,0x10(%edi)
  f->off = 0;
80105305:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)
  f->readable = !(omode & O_WRONLY);
8010530c:	89 d0                	mov    %edx,%eax
8010530e:	f7 d0                	not    %eax
80105310:	83 e0 01             	and    $0x1,%eax
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105313:	83 e2 03             	and    $0x3,%edx
  f->readable = !(omode & O_WRONLY);
80105316:	88 47 08             	mov    %al,0x8(%edi)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105319:	0f 95 47 09          	setne  0x9(%edi)
  return fd;
}
8010531d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105320:	89 d8                	mov    %ebx,%eax
80105322:	5b                   	pop    %ebx
80105323:	5e                   	pop    %esi
80105324:	5f                   	pop    %edi
80105325:	5d                   	pop    %ebp
80105326:	c3                   	ret    
80105327:	89 f6                	mov    %esi,%esi
80105329:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    ip = create(path, T_FILE, 0, 0);
80105330:	83 ec 0c             	sub    $0xc,%esp
80105333:	8b 45 e0             	mov    -0x20(%ebp),%eax
80105336:	31 c9                	xor    %ecx,%ecx
80105338:	6a 00                	push   $0x0
8010533a:	ba 02 00 00 00       	mov    $0x2,%edx
8010533f:	e8 ec f7 ff ff       	call   80104b30 <create>
    if(ip == 0){
80105344:	83 c4 10             	add    $0x10,%esp
80105347:	85 c0                	test   %eax,%eax
    ip = create(path, T_FILE, 0, 0);
80105349:	89 c6                	mov    %eax,%esi
    if(ip == 0){
8010534b:	0f 85 65 ff ff ff    	jne    801052b6 <sys_open+0x76>
      end_op();
80105351:	e8 4a db ff ff       	call   80102ea0 <end_op>
      return -1;
80105356:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
8010535b:	eb c0                	jmp    8010531d <sys_open+0xdd>
8010535d:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->type == T_DIR && omode != O_RDONLY){
80105360:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80105363:	85 c9                	test   %ecx,%ecx
80105365:	0f 84 4b ff ff ff    	je     801052b6 <sys_open+0x76>
    iunlockput(ip);
8010536b:	83 ec 0c             	sub    $0xc,%esp
8010536e:	56                   	push   %esi
8010536f:	e8 2c c8 ff ff       	call   80101ba0 <iunlockput>
    end_op();
80105374:	e8 27 db ff ff       	call   80102ea0 <end_op>
    return -1;
80105379:	83 c4 10             	add    $0x10,%esp
8010537c:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105381:	eb 9a                	jmp    8010531d <sys_open+0xdd>
80105383:	90                   	nop
80105384:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      fileclose(f);
80105388:	83 ec 0c             	sub    $0xc,%esp
8010538b:	57                   	push   %edi
8010538c:	e8 3f bd ff ff       	call   801010d0 <fileclose>
80105391:	83 c4 10             	add    $0x10,%esp
80105394:	eb d5                	jmp    8010536b <sys_open+0x12b>
80105396:	8d 76 00             	lea    0x0(%esi),%esi
80105399:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801053a0 <sys_mkdir>:

int
sys_mkdir(void)
{
801053a0:	55                   	push   %ebp
801053a1:	89 e5                	mov    %esp,%ebp
801053a3:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
801053a6:	e8 85 da ff ff       	call   80102e30 <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
801053ab:	8d 45 f4             	lea    -0xc(%ebp),%eax
801053ae:	83 ec 08             	sub    $0x8,%esp
801053b1:	50                   	push   %eax
801053b2:	6a 00                	push   $0x0
801053b4:	e8 d7 f6 ff ff       	call   80104a90 <argstr>
801053b9:	83 c4 10             	add    $0x10,%esp
801053bc:	85 c0                	test   %eax,%eax
801053be:	78 30                	js     801053f0 <sys_mkdir+0x50>
801053c0:	83 ec 0c             	sub    $0xc,%esp
801053c3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801053c6:	31 c9                	xor    %ecx,%ecx
801053c8:	6a 00                	push   $0x0
801053ca:	ba 01 00 00 00       	mov    $0x1,%edx
801053cf:	e8 5c f7 ff ff       	call   80104b30 <create>
801053d4:	83 c4 10             	add    $0x10,%esp
801053d7:	85 c0                	test   %eax,%eax
801053d9:	74 15                	je     801053f0 <sys_mkdir+0x50>
    end_op();
    return -1;
  }
  iunlockput(ip);
801053db:	83 ec 0c             	sub    $0xc,%esp
801053de:	50                   	push   %eax
801053df:	e8 bc c7 ff ff       	call   80101ba0 <iunlockput>
  end_op();
801053e4:	e8 b7 da ff ff       	call   80102ea0 <end_op>
  return 0;
801053e9:	83 c4 10             	add    $0x10,%esp
801053ec:	31 c0                	xor    %eax,%eax
}
801053ee:	c9                   	leave  
801053ef:	c3                   	ret    
    end_op();
801053f0:	e8 ab da ff ff       	call   80102ea0 <end_op>
    return -1;
801053f5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801053fa:	c9                   	leave  
801053fb:	c3                   	ret    
801053fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105400 <sys_mknod>:

int
sys_mknod(void)
{
80105400:	55                   	push   %ebp
80105401:	89 e5                	mov    %esp,%ebp
80105403:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80105406:	e8 25 da ff ff       	call   80102e30 <begin_op>
  if((argstr(0, &path)) < 0 ||
8010540b:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010540e:	83 ec 08             	sub    $0x8,%esp
80105411:	50                   	push   %eax
80105412:	6a 00                	push   $0x0
80105414:	e8 77 f6 ff ff       	call   80104a90 <argstr>
80105419:	83 c4 10             	add    $0x10,%esp
8010541c:	85 c0                	test   %eax,%eax
8010541e:	78 60                	js     80105480 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
80105420:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105423:	83 ec 08             	sub    $0x8,%esp
80105426:	50                   	push   %eax
80105427:	6a 01                	push   $0x1
80105429:	e8 b2 f5 ff ff       	call   801049e0 <argint>
  if((argstr(0, &path)) < 0 ||
8010542e:	83 c4 10             	add    $0x10,%esp
80105431:	85 c0                	test   %eax,%eax
80105433:	78 4b                	js     80105480 <sys_mknod+0x80>
     argint(2, &minor) < 0 ||
80105435:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105438:	83 ec 08             	sub    $0x8,%esp
8010543b:	50                   	push   %eax
8010543c:	6a 02                	push   $0x2
8010543e:	e8 9d f5 ff ff       	call   801049e0 <argint>
     argint(1, &major) < 0 ||
80105443:	83 c4 10             	add    $0x10,%esp
80105446:	85 c0                	test   %eax,%eax
80105448:	78 36                	js     80105480 <sys_mknod+0x80>
     (ip = create(path, T_DEV, major, minor)) == 0){
8010544a:	0f bf 45 f4          	movswl -0xc(%ebp),%eax
     argint(2, &minor) < 0 ||
8010544e:	83 ec 0c             	sub    $0xc,%esp
     (ip = create(path, T_DEV, major, minor)) == 0){
80105451:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
     argint(2, &minor) < 0 ||
80105455:	ba 03 00 00 00       	mov    $0x3,%edx
8010545a:	50                   	push   %eax
8010545b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010545e:	e8 cd f6 ff ff       	call   80104b30 <create>
80105463:	83 c4 10             	add    $0x10,%esp
80105466:	85 c0                	test   %eax,%eax
80105468:	74 16                	je     80105480 <sys_mknod+0x80>
    end_op();
    return -1;
  }
  iunlockput(ip);
8010546a:	83 ec 0c             	sub    $0xc,%esp
8010546d:	50                   	push   %eax
8010546e:	e8 2d c7 ff ff       	call   80101ba0 <iunlockput>
  end_op();
80105473:	e8 28 da ff ff       	call   80102ea0 <end_op>
  return 0;
80105478:	83 c4 10             	add    $0x10,%esp
8010547b:	31 c0                	xor    %eax,%eax
}
8010547d:	c9                   	leave  
8010547e:	c3                   	ret    
8010547f:	90                   	nop
    end_op();
80105480:	e8 1b da ff ff       	call   80102ea0 <end_op>
    return -1;
80105485:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010548a:	c9                   	leave  
8010548b:	c3                   	ret    
8010548c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105490 <sys_chdir>:

int
sys_chdir(void)
{
80105490:	55                   	push   %ebp
80105491:	89 e5                	mov    %esp,%ebp
80105493:	56                   	push   %esi
80105494:	53                   	push   %ebx
80105495:	83 ec 10             	sub    $0x10,%esp
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
80105498:	e8 d3 e5 ff ff       	call   80103a70 <myproc>
8010549d:	89 c6                	mov    %eax,%esi
  
  begin_op();
8010549f:	e8 8c d9 ff ff       	call   80102e30 <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
801054a4:	8d 45 f4             	lea    -0xc(%ebp),%eax
801054a7:	83 ec 08             	sub    $0x8,%esp
801054aa:	50                   	push   %eax
801054ab:	6a 00                	push   $0x0
801054ad:	e8 de f5 ff ff       	call   80104a90 <argstr>
801054b2:	83 c4 10             	add    $0x10,%esp
801054b5:	85 c0                	test   %eax,%eax
801054b7:	78 77                	js     80105530 <sys_chdir+0xa0>
801054b9:	83 ec 0c             	sub    $0xc,%esp
801054bc:	ff 75 f4             	pushl  -0xc(%ebp)
801054bf:	e8 ac cc ff ff       	call   80102170 <namei>
801054c4:	83 c4 10             	add    $0x10,%esp
801054c7:	85 c0                	test   %eax,%eax
801054c9:	89 c3                	mov    %eax,%ebx
801054cb:	74 63                	je     80105530 <sys_chdir+0xa0>
    end_op();
    return -1;
  }
  ilock(ip);
801054cd:	83 ec 0c             	sub    $0xc,%esp
801054d0:	50                   	push   %eax
801054d1:	e8 3a c4 ff ff       	call   80101910 <ilock>
  if(ip->type != T_DIR){
801054d6:	83 c4 10             	add    $0x10,%esp
801054d9:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801054de:	75 30                	jne    80105510 <sys_chdir+0x80>
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
801054e0:	83 ec 0c             	sub    $0xc,%esp
801054e3:	53                   	push   %ebx
801054e4:	e8 07 c5 ff ff       	call   801019f0 <iunlock>
  iput(curproc->cwd);
801054e9:	58                   	pop    %eax
801054ea:	ff 76 68             	pushl  0x68(%esi)
801054ed:	e8 4e c5 ff ff       	call   80101a40 <iput>
  end_op();
801054f2:	e8 a9 d9 ff ff       	call   80102ea0 <end_op>
  curproc->cwd = ip;
801054f7:	89 5e 68             	mov    %ebx,0x68(%esi)
  return 0;
801054fa:	83 c4 10             	add    $0x10,%esp
801054fd:	31 c0                	xor    %eax,%eax
}
801054ff:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105502:	5b                   	pop    %ebx
80105503:	5e                   	pop    %esi
80105504:	5d                   	pop    %ebp
80105505:	c3                   	ret    
80105506:	8d 76 00             	lea    0x0(%esi),%esi
80105509:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    iunlockput(ip);
80105510:	83 ec 0c             	sub    $0xc,%esp
80105513:	53                   	push   %ebx
80105514:	e8 87 c6 ff ff       	call   80101ba0 <iunlockput>
    end_op();
80105519:	e8 82 d9 ff ff       	call   80102ea0 <end_op>
    return -1;
8010551e:	83 c4 10             	add    $0x10,%esp
80105521:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105526:	eb d7                	jmp    801054ff <sys_chdir+0x6f>
80105528:	90                   	nop
80105529:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    end_op();
80105530:	e8 6b d9 ff ff       	call   80102ea0 <end_op>
    return -1;
80105535:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010553a:	eb c3                	jmp    801054ff <sys_chdir+0x6f>
8010553c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105540 <sys_exec>:

int
sys_exec(void)
{
80105540:	55                   	push   %ebp
80105541:	89 e5                	mov    %esp,%ebp
80105543:	57                   	push   %edi
80105544:	56                   	push   %esi
80105545:	53                   	push   %ebx
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105546:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
{
8010554c:	81 ec a4 00 00 00    	sub    $0xa4,%esp
  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105552:	50                   	push   %eax
80105553:	6a 00                	push   $0x0
80105555:	e8 36 f5 ff ff       	call   80104a90 <argstr>
8010555a:	83 c4 10             	add    $0x10,%esp
8010555d:	85 c0                	test   %eax,%eax
8010555f:	0f 88 87 00 00 00    	js     801055ec <sys_exec+0xac>
80105565:	8d 85 60 ff ff ff    	lea    -0xa0(%ebp),%eax
8010556b:	83 ec 08             	sub    $0x8,%esp
8010556e:	50                   	push   %eax
8010556f:	6a 01                	push   $0x1
80105571:	e8 6a f4 ff ff       	call   801049e0 <argint>
80105576:	83 c4 10             	add    $0x10,%esp
80105579:	85 c0                	test   %eax,%eax
8010557b:	78 6f                	js     801055ec <sys_exec+0xac>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
8010557d:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80105583:	83 ec 04             	sub    $0x4,%esp
  for(i=0;; i++){
80105586:	31 db                	xor    %ebx,%ebx
  memset(argv, 0, sizeof(argv));
80105588:	68 80 00 00 00       	push   $0x80
8010558d:	6a 00                	push   $0x0
8010558f:	8d bd 64 ff ff ff    	lea    -0x9c(%ebp),%edi
80105595:	50                   	push   %eax
80105596:	e8 45 f1 ff ff       	call   801046e0 <memset>
8010559b:	83 c4 10             	add    $0x10,%esp
8010559e:	eb 2c                	jmp    801055cc <sys_exec+0x8c>
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
      return -1;
    if(uarg == 0){
801055a0:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
801055a6:	85 c0                	test   %eax,%eax
801055a8:	74 56                	je     80105600 <sys_exec+0xc0>
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
801055aa:	8d 8d 68 ff ff ff    	lea    -0x98(%ebp),%ecx
801055b0:	83 ec 08             	sub    $0x8,%esp
801055b3:	8d 14 31             	lea    (%ecx,%esi,1),%edx
801055b6:	52                   	push   %edx
801055b7:	50                   	push   %eax
801055b8:	e8 b3 f3 ff ff       	call   80104970 <fetchstr>
801055bd:	83 c4 10             	add    $0x10,%esp
801055c0:	85 c0                	test   %eax,%eax
801055c2:	78 28                	js     801055ec <sys_exec+0xac>
  for(i=0;; i++){
801055c4:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
801055c7:	83 fb 20             	cmp    $0x20,%ebx
801055ca:	74 20                	je     801055ec <sys_exec+0xac>
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
801055cc:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
801055d2:	8d 34 9d 00 00 00 00 	lea    0x0(,%ebx,4),%esi
801055d9:	83 ec 08             	sub    $0x8,%esp
801055dc:	57                   	push   %edi
801055dd:	01 f0                	add    %esi,%eax
801055df:	50                   	push   %eax
801055e0:	e8 4b f3 ff ff       	call   80104930 <fetchint>
801055e5:	83 c4 10             	add    $0x10,%esp
801055e8:	85 c0                	test   %eax,%eax
801055ea:	79 b4                	jns    801055a0 <sys_exec+0x60>
      return -1;
  }
  return exec(path, argv);
}
801055ec:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
801055ef:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801055f4:	5b                   	pop    %ebx
801055f5:	5e                   	pop    %esi
801055f6:	5f                   	pop    %edi
801055f7:	5d                   	pop    %ebp
801055f8:	c3                   	ret    
801055f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return exec(path, argv);
80105600:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
80105606:	83 ec 08             	sub    $0x8,%esp
      argv[i] = 0;
80105609:	c7 84 9d 68 ff ff ff 	movl   $0x0,-0x98(%ebp,%ebx,4)
80105610:	00 00 00 00 
  return exec(path, argv);
80105614:	50                   	push   %eax
80105615:	ff b5 5c ff ff ff    	pushl  -0xa4(%ebp)
8010561b:	e8 80 b6 ff ff       	call   80100ca0 <exec>
80105620:	83 c4 10             	add    $0x10,%esp
}
80105623:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105626:	5b                   	pop    %ebx
80105627:	5e                   	pop    %esi
80105628:	5f                   	pop    %edi
80105629:	5d                   	pop    %ebp
8010562a:	c3                   	ret    
8010562b:	90                   	nop
8010562c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105630 <sys_pipe>:

int
sys_pipe(void)
{
80105630:	55                   	push   %ebp
80105631:	89 e5                	mov    %esp,%ebp
80105633:	57                   	push   %edi
80105634:	56                   	push   %esi
80105635:	53                   	push   %ebx
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80105636:	8d 45 dc             	lea    -0x24(%ebp),%eax
{
80105639:	83 ec 20             	sub    $0x20,%esp
  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
8010563c:	6a 08                	push   $0x8
8010563e:	50                   	push   %eax
8010563f:	6a 00                	push   $0x0
80105641:	e8 ea f3 ff ff       	call   80104a30 <argptr>
80105646:	83 c4 10             	add    $0x10,%esp
80105649:	85 c0                	test   %eax,%eax
8010564b:	0f 88 ae 00 00 00    	js     801056ff <sys_pipe+0xcf>
    return -1;
  if(pipealloc(&rf, &wf) < 0)
80105651:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105654:	83 ec 08             	sub    $0x8,%esp
80105657:	50                   	push   %eax
80105658:	8d 45 e0             	lea    -0x20(%ebp),%eax
8010565b:	50                   	push   %eax
8010565c:	e8 6f de ff ff       	call   801034d0 <pipealloc>
80105661:	83 c4 10             	add    $0x10,%esp
80105664:	85 c0                	test   %eax,%eax
80105666:	0f 88 93 00 00 00    	js     801056ff <sys_pipe+0xcf>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
8010566c:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(fd = 0; fd < NOFILE; fd++){
8010566f:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
80105671:	e8 fa e3 ff ff       	call   80103a70 <myproc>
80105676:	eb 10                	jmp    80105688 <sys_pipe+0x58>
80105678:	90                   	nop
80105679:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(fd = 0; fd < NOFILE; fd++){
80105680:	83 c3 01             	add    $0x1,%ebx
80105683:	83 fb 10             	cmp    $0x10,%ebx
80105686:	74 60                	je     801056e8 <sys_pipe+0xb8>
    if(curproc->ofile[fd] == 0){
80105688:	8b 74 98 28          	mov    0x28(%eax,%ebx,4),%esi
8010568c:	85 f6                	test   %esi,%esi
8010568e:	75 f0                	jne    80105680 <sys_pipe+0x50>
      curproc->ofile[fd] = f;
80105690:	8d 73 08             	lea    0x8(%ebx),%esi
80105693:	89 7c b0 08          	mov    %edi,0x8(%eax,%esi,4)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80105697:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  struct proc *curproc = myproc();
8010569a:	e8 d1 e3 ff ff       	call   80103a70 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
8010569f:	31 d2                	xor    %edx,%edx
801056a1:	eb 0d                	jmp    801056b0 <sys_pipe+0x80>
801056a3:	90                   	nop
801056a4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801056a8:	83 c2 01             	add    $0x1,%edx
801056ab:	83 fa 10             	cmp    $0x10,%edx
801056ae:	74 28                	je     801056d8 <sys_pipe+0xa8>
    if(curproc->ofile[fd] == 0){
801056b0:	8b 4c 90 28          	mov    0x28(%eax,%edx,4),%ecx
801056b4:	85 c9                	test   %ecx,%ecx
801056b6:	75 f0                	jne    801056a8 <sys_pipe+0x78>
      curproc->ofile[fd] = f;
801056b8:	89 7c 90 28          	mov    %edi,0x28(%eax,%edx,4)
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  fd[0] = fd0;
801056bc:	8b 45 dc             	mov    -0x24(%ebp),%eax
801056bf:	89 18                	mov    %ebx,(%eax)
  fd[1] = fd1;
801056c1:	8b 45 dc             	mov    -0x24(%ebp),%eax
801056c4:	89 50 04             	mov    %edx,0x4(%eax)
  return 0;
801056c7:	31 c0                	xor    %eax,%eax
}
801056c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
801056cc:	5b                   	pop    %ebx
801056cd:	5e                   	pop    %esi
801056ce:	5f                   	pop    %edi
801056cf:	5d                   	pop    %ebp
801056d0:	c3                   	ret    
801056d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      myproc()->ofile[fd0] = 0;
801056d8:	e8 93 e3 ff ff       	call   80103a70 <myproc>
801056dd:	c7 44 b0 08 00 00 00 	movl   $0x0,0x8(%eax,%esi,4)
801056e4:	00 
801056e5:	8d 76 00             	lea    0x0(%esi),%esi
    fileclose(rf);
801056e8:	83 ec 0c             	sub    $0xc,%esp
801056eb:	ff 75 e0             	pushl  -0x20(%ebp)
801056ee:	e8 dd b9 ff ff       	call   801010d0 <fileclose>
    fileclose(wf);
801056f3:	58                   	pop    %eax
801056f4:	ff 75 e4             	pushl  -0x1c(%ebp)
801056f7:	e8 d4 b9 ff ff       	call   801010d0 <fileclose>
    return -1;
801056fc:	83 c4 10             	add    $0x10,%esp
801056ff:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105704:	eb c3                	jmp    801056c9 <sys_pipe+0x99>
80105706:	66 90                	xchg   %ax,%ax
80105708:	66 90                	xchg   %ax,%ax
8010570a:	66 90                	xchg   %ax,%ax
8010570c:	66 90                	xchg   %ax,%ax
8010570e:	66 90                	xchg   %ax,%ax

80105710 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
80105710:	55                   	push   %ebp
80105711:	89 e5                	mov    %esp,%ebp
  return fork();
}
80105713:	5d                   	pop    %ebp
  return fork();
80105714:	e9 f7 e4 ff ff       	jmp    80103c10 <fork>
80105719:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105720 <sys_exit>:

int
sys_exit(void)
{
80105720:	55                   	push   %ebp
80105721:	89 e5                	mov    %esp,%ebp
80105723:	83 ec 08             	sub    $0x8,%esp
  exit();
80105726:	e8 65 e7 ff ff       	call   80103e90 <exit>
  return 0;  // not reached
}
8010572b:	31 c0                	xor    %eax,%eax
8010572d:	c9                   	leave  
8010572e:	c3                   	ret    
8010572f:	90                   	nop

80105730 <sys_wait>:

int
sys_wait(void)
{
80105730:	55                   	push   %ebp
80105731:	89 e5                	mov    %esp,%ebp
  return wait();
}
80105733:	5d                   	pop    %ebp
  return wait();
80105734:	e9 97 e9 ff ff       	jmp    801040d0 <wait>
80105739:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105740 <sys_kill>:

int
sys_kill(void)
{
80105740:	55                   	push   %ebp
80105741:	89 e5                	mov    %esp,%ebp
80105743:	83 ec 20             	sub    $0x20,%esp
  int pid;

  if(argint(0, &pid) < 0)
80105746:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105749:	50                   	push   %eax
8010574a:	6a 00                	push   $0x0
8010574c:	e8 8f f2 ff ff       	call   801049e0 <argint>
80105751:	83 c4 10             	add    $0x10,%esp
80105754:	85 c0                	test   %eax,%eax
80105756:	78 18                	js     80105770 <sys_kill+0x30>
    return -1;
  return kill(pid);
80105758:	83 ec 0c             	sub    $0xc,%esp
8010575b:	ff 75 f4             	pushl  -0xc(%ebp)
8010575e:	e8 bd ea ff ff       	call   80104220 <kill>
80105763:	83 c4 10             	add    $0x10,%esp
}
80105766:	c9                   	leave  
80105767:	c3                   	ret    
80105768:	90                   	nop
80105769:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80105770:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105775:	c9                   	leave  
80105776:	c3                   	ret    
80105777:	89 f6                	mov    %esi,%esi
80105779:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105780 <sys_getpid>:

int
sys_getpid(void)
{
80105780:	55                   	push   %ebp
80105781:	89 e5                	mov    %esp,%ebp
80105783:	83 ec 08             	sub    $0x8,%esp
  return myproc()->pid;
80105786:	e8 e5 e2 ff ff       	call   80103a70 <myproc>
8010578b:	8b 40 10             	mov    0x10(%eax),%eax
}
8010578e:	c9                   	leave  
8010578f:	c3                   	ret    

80105790 <sys_sbrk>:

int
sys_sbrk(void)
{
80105790:	55                   	push   %ebp
80105791:	89 e5                	mov    %esp,%ebp
80105793:	53                   	push   %ebx
  int addr;
  int n;

  if(argint(0, &n) < 0)
80105794:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
80105797:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
8010579a:	50                   	push   %eax
8010579b:	6a 00                	push   $0x0
8010579d:	e8 3e f2 ff ff       	call   801049e0 <argint>
801057a2:	83 c4 10             	add    $0x10,%esp
801057a5:	85 c0                	test   %eax,%eax
801057a7:	78 27                	js     801057d0 <sys_sbrk+0x40>
    return -1;
  addr = myproc()->sz;
801057a9:	e8 c2 e2 ff ff       	call   80103a70 <myproc>
  if(growproc(n) < 0)
801057ae:	83 ec 0c             	sub    $0xc,%esp
  addr = myproc()->sz;
801057b1:	8b 18                	mov    (%eax),%ebx
  if(growproc(n) < 0)
801057b3:	ff 75 f4             	pushl  -0xc(%ebp)
801057b6:	e8 d5 e3 ff ff       	call   80103b90 <growproc>
801057bb:	83 c4 10             	add    $0x10,%esp
801057be:	85 c0                	test   %eax,%eax
801057c0:	78 0e                	js     801057d0 <sys_sbrk+0x40>
    return -1;
  return addr;
}
801057c2:	89 d8                	mov    %ebx,%eax
801057c4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801057c7:	c9                   	leave  
801057c8:	c3                   	ret    
801057c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
801057d0:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801057d5:	eb eb                	jmp    801057c2 <sys_sbrk+0x32>
801057d7:	89 f6                	mov    %esi,%esi
801057d9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

801057e0 <sys_sleep>:

int
sys_sleep(void)
{
801057e0:	55                   	push   %ebp
801057e1:	89 e5                	mov    %esp,%ebp
801057e3:	53                   	push   %ebx
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
801057e4:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
801057e7:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
801057ea:	50                   	push   %eax
801057eb:	6a 00                	push   $0x0
801057ed:	e8 ee f1 ff ff       	call   801049e0 <argint>
801057f2:	83 c4 10             	add    $0x10,%esp
801057f5:	85 c0                	test   %eax,%eax
801057f7:	0f 88 8a 00 00 00    	js     80105887 <sys_sleep+0xa7>
    return -1;
  acquire(&tickslock);
801057fd:	83 ec 0c             	sub    $0xc,%esp
80105800:	68 60 4c 11 80       	push   $0x80114c60
80105805:	e8 c6 ed ff ff       	call   801045d0 <acquire>
  ticks0 = ticks;
  while(ticks - ticks0 < n){
8010580a:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010580d:	83 c4 10             	add    $0x10,%esp
  ticks0 = ticks;
80105810:	8b 1d a0 54 11 80    	mov    0x801154a0,%ebx
  while(ticks - ticks0 < n){
80105816:	85 d2                	test   %edx,%edx
80105818:	75 27                	jne    80105841 <sys_sleep+0x61>
8010581a:	eb 54                	jmp    80105870 <sys_sleep+0x90>
8010581c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
80105820:	83 ec 08             	sub    $0x8,%esp
80105823:	68 60 4c 11 80       	push   $0x80114c60
80105828:	68 a0 54 11 80       	push   $0x801154a0
8010582d:	e8 de e7 ff ff       	call   80104010 <sleep>
  while(ticks - ticks0 < n){
80105832:	a1 a0 54 11 80       	mov    0x801154a0,%eax
80105837:	83 c4 10             	add    $0x10,%esp
8010583a:	29 d8                	sub    %ebx,%eax
8010583c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010583f:	73 2f                	jae    80105870 <sys_sleep+0x90>
    if(myproc()->killed){
80105841:	e8 2a e2 ff ff       	call   80103a70 <myproc>
80105846:	8b 40 24             	mov    0x24(%eax),%eax
80105849:	85 c0                	test   %eax,%eax
8010584b:	74 d3                	je     80105820 <sys_sleep+0x40>
      release(&tickslock);
8010584d:	83 ec 0c             	sub    $0xc,%esp
80105850:	68 60 4c 11 80       	push   $0x80114c60
80105855:	e8 36 ee ff ff       	call   80104690 <release>
      return -1;
8010585a:	83 c4 10             	add    $0x10,%esp
8010585d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  release(&tickslock);
  return 0;
}
80105862:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105865:	c9                   	leave  
80105866:	c3                   	ret    
80105867:	89 f6                	mov    %esi,%esi
80105869:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
  release(&tickslock);
80105870:	83 ec 0c             	sub    $0xc,%esp
80105873:	68 60 4c 11 80       	push   $0x80114c60
80105878:	e8 13 ee ff ff       	call   80104690 <release>
  return 0;
8010587d:	83 c4 10             	add    $0x10,%esp
80105880:	31 c0                	xor    %eax,%eax
}
80105882:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105885:	c9                   	leave  
80105886:	c3                   	ret    
    return -1;
80105887:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010588c:	eb f4                	jmp    80105882 <sys_sleep+0xa2>
8010588e:	66 90                	xchg   %ax,%ax

80105890 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80105890:	55                   	push   %ebp
80105891:	89 e5                	mov    %esp,%ebp
80105893:	53                   	push   %ebx
80105894:	83 ec 10             	sub    $0x10,%esp
  uint xticks;

  acquire(&tickslock);
80105897:	68 60 4c 11 80       	push   $0x80114c60
8010589c:	e8 2f ed ff ff       	call   801045d0 <acquire>
  xticks = ticks;
801058a1:	8b 1d a0 54 11 80    	mov    0x801154a0,%ebx
  release(&tickslock);
801058a7:	c7 04 24 60 4c 11 80 	movl   $0x80114c60,(%esp)
801058ae:	e8 dd ed ff ff       	call   80104690 <release>
  return xticks;
}
801058b3:	89 d8                	mov    %ebx,%eax
801058b5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801058b8:	c9                   	leave  
801058b9:	c3                   	ret    

801058ba <alltraps>:
801058ba:	1e                   	push   %ds
801058bb:	06                   	push   %es
801058bc:	0f a0                	push   %fs
801058be:	0f a8                	push   %gs
801058c0:	60                   	pusha  
801058c1:	66 b8 10 00          	mov    $0x10,%ax
801058c5:	8e d8                	mov    %eax,%ds
801058c7:	8e c0                	mov    %eax,%es
801058c9:	54                   	push   %esp
801058ca:	e8 c1 00 00 00       	call   80105990 <trap>
801058cf:	83 c4 04             	add    $0x4,%esp

801058d2 <trapret>:
801058d2:	61                   	popa   
801058d3:	0f a9                	pop    %gs
801058d5:	0f a1                	pop    %fs
801058d7:	07                   	pop    %es
801058d8:	1f                   	pop    %ds
801058d9:	83 c4 08             	add    $0x8,%esp
801058dc:	cf                   	iret   
801058dd:	66 90                	xchg   %ax,%ax
801058df:	90                   	nop

801058e0 <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
801058e0:	55                   	push   %ebp
  int i;

  for(i = 0; i < 256; i++)
801058e1:	31 c0                	xor    %eax,%eax
{
801058e3:	89 e5                	mov    %esp,%ebp
801058e5:	83 ec 08             	sub    $0x8,%esp
801058e8:	90                   	nop
801058e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
801058f0:	8b 14 85 08 a0 10 80 	mov    -0x7fef5ff8(,%eax,4),%edx
801058f7:	c7 04 c5 a2 4c 11 80 	movl   $0x8e000008,-0x7feeb35e(,%eax,8)
801058fe:	08 00 00 8e 
80105902:	66 89 14 c5 a0 4c 11 	mov    %dx,-0x7feeb360(,%eax,8)
80105909:	80 
8010590a:	c1 ea 10             	shr    $0x10,%edx
8010590d:	66 89 14 c5 a6 4c 11 	mov    %dx,-0x7feeb35a(,%eax,8)
80105914:	80 
  for(i = 0; i < 256; i++)
80105915:	83 c0 01             	add    $0x1,%eax
80105918:	3d 00 01 00 00       	cmp    $0x100,%eax
8010591d:	75 d1                	jne    801058f0 <tvinit+0x10>
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
8010591f:	a1 08 a1 10 80       	mov    0x8010a108,%eax

  initlock(&tickslock, "time");
80105924:	83 ec 08             	sub    $0x8,%esp
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80105927:	c7 05 a2 4e 11 80 08 	movl   $0xef000008,0x80114ea2
8010592e:	00 00 ef 
  initlock(&tickslock, "time");
80105931:	68 d9 78 10 80       	push   $0x801078d9
80105936:	68 60 4c 11 80       	push   $0x80114c60
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
8010593b:	66 a3 a0 4e 11 80    	mov    %ax,0x80114ea0
80105941:	c1 e8 10             	shr    $0x10,%eax
80105944:	66 a3 a6 4e 11 80    	mov    %ax,0x80114ea6
  initlock(&tickslock, "time");
8010594a:	e8 41 eb ff ff       	call   80104490 <initlock>
}
8010594f:	83 c4 10             	add    $0x10,%esp
80105952:	c9                   	leave  
80105953:	c3                   	ret    
80105954:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
8010595a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80105960 <idtinit>:

void
idtinit(void)
{
80105960:	55                   	push   %ebp
  pd[0] = size-1;
80105961:	b8 ff 07 00 00       	mov    $0x7ff,%eax
80105966:	89 e5                	mov    %esp,%ebp
80105968:	83 ec 10             	sub    $0x10,%esp
8010596b:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
8010596f:	b8 a0 4c 11 80       	mov    $0x80114ca0,%eax
80105974:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
80105978:	c1 e8 10             	shr    $0x10,%eax
8010597b:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
  asm volatile("lidt (%0)" : : "r" (pd));
8010597f:	8d 45 fa             	lea    -0x6(%ebp),%eax
80105982:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
80105985:	c9                   	leave  
80105986:	c3                   	ret    
80105987:	89 f6                	mov    %esi,%esi
80105989:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105990 <trap>:

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
80105990:	55                   	push   %ebp
80105991:	89 e5                	mov    %esp,%ebp
80105993:	57                   	push   %edi
80105994:	56                   	push   %esi
80105995:	53                   	push   %ebx
80105996:	83 ec 1c             	sub    $0x1c,%esp
80105999:	8b 7d 08             	mov    0x8(%ebp),%edi
  if(tf->trapno == T_SYSCALL){
8010599c:	8b 47 30             	mov    0x30(%edi),%eax
8010599f:	83 f8 40             	cmp    $0x40,%eax
801059a2:	0f 84 f0 00 00 00    	je     80105a98 <trap+0x108>
    if(myproc()->killed)
      exit();
    return;
  }

  switch(tf->trapno){
801059a8:	83 e8 20             	sub    $0x20,%eax
801059ab:	83 f8 1f             	cmp    $0x1f,%eax
801059ae:	77 10                	ja     801059c0 <trap+0x30>
801059b0:	ff 24 85 80 79 10 80 	jmp    *-0x7fef8680(,%eax,4)
801059b7:	89 f6                	mov    %esi,%esi
801059b9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    lapiceoi();
    break;

  //PAGEBREAK: 13
  default:
    if(myproc() == 0 || (tf->cs&3) == 0){
801059c0:	e8 ab e0 ff ff       	call   80103a70 <myproc>
801059c5:	85 c0                	test   %eax,%eax
801059c7:	8b 5f 38             	mov    0x38(%edi),%ebx
801059ca:	0f 84 14 02 00 00    	je     80105be4 <trap+0x254>
801059d0:	f6 47 3c 03          	testb  $0x3,0x3c(%edi)
801059d4:	0f 84 0a 02 00 00    	je     80105be4 <trap+0x254>

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
801059da:	0f 20 d1             	mov    %cr2,%ecx
801059dd:	89 4d d8             	mov    %ecx,-0x28(%ebp)
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
              tf->trapno, cpuid(), tf->eip, rcr2());
      panic("trap");
    }
    // In user space, assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
801059e0:	e8 6b e0 ff ff       	call   80103a50 <cpuid>
801059e5:	89 45 dc             	mov    %eax,-0x24(%ebp)
801059e8:	8b 47 34             	mov    0x34(%edi),%eax
801059eb:	8b 77 30             	mov    0x30(%edi),%esi
801059ee:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            "eip 0x%x addr 0x%x--kill proc\n",
            myproc()->pid, myproc()->name, tf->trapno,
801059f1:	e8 7a e0 ff ff       	call   80103a70 <myproc>
801059f6:	89 45 e0             	mov    %eax,-0x20(%ebp)
801059f9:	e8 72 e0 ff ff       	call   80103a70 <myproc>
    cprintf("pid %d %s: trap %d err %d on cpu %d "
801059fe:	8b 4d d8             	mov    -0x28(%ebp),%ecx
80105a01:	8b 55 dc             	mov    -0x24(%ebp),%edx
80105a04:	51                   	push   %ecx
80105a05:	53                   	push   %ebx
80105a06:	52                   	push   %edx
            myproc()->pid, myproc()->name, tf->trapno,
80105a07:	8b 55 e0             	mov    -0x20(%ebp),%edx
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105a0a:	ff 75 e4             	pushl  -0x1c(%ebp)
80105a0d:	56                   	push   %esi
            myproc()->pid, myproc()->name, tf->trapno,
80105a0e:	83 c2 6c             	add    $0x6c,%edx
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105a11:	52                   	push   %edx
80105a12:	ff 70 10             	pushl  0x10(%eax)
80105a15:	68 3c 79 10 80       	push   $0x8010793c
80105a1a:	e8 51 ad ff ff       	call   80100770 <cprintf>
            tf->err, cpuid(), tf->eip, rcr2());
    myproc()->killed = 1;
80105a1f:	83 c4 20             	add    $0x20,%esp
80105a22:	e8 49 e0 ff ff       	call   80103a70 <myproc>
80105a27:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105a2e:	e8 3d e0 ff ff       	call   80103a70 <myproc>
80105a33:	85 c0                	test   %eax,%eax
80105a35:	74 1d                	je     80105a54 <trap+0xc4>
80105a37:	e8 34 e0 ff ff       	call   80103a70 <myproc>
80105a3c:	8b 50 24             	mov    0x24(%eax),%edx
80105a3f:	85 d2                	test   %edx,%edx
80105a41:	74 11                	je     80105a54 <trap+0xc4>
80105a43:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105a47:	83 e0 03             	and    $0x3,%eax
80105a4a:	66 83 f8 03          	cmp    $0x3,%ax
80105a4e:	0f 84 4c 01 00 00    	je     80105ba0 <trap+0x210>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(myproc() && myproc()->state == RUNNING &&
80105a54:	e8 17 e0 ff ff       	call   80103a70 <myproc>
80105a59:	85 c0                	test   %eax,%eax
80105a5b:	74 0b                	je     80105a68 <trap+0xd8>
80105a5d:	e8 0e e0 ff ff       	call   80103a70 <myproc>
80105a62:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
80105a66:	74 68                	je     80105ad0 <trap+0x140>
     tf->trapno == T_IRQ0+IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105a68:	e8 03 e0 ff ff       	call   80103a70 <myproc>
80105a6d:	85 c0                	test   %eax,%eax
80105a6f:	74 19                	je     80105a8a <trap+0xfa>
80105a71:	e8 fa df ff ff       	call   80103a70 <myproc>
80105a76:	8b 40 24             	mov    0x24(%eax),%eax
80105a79:	85 c0                	test   %eax,%eax
80105a7b:	74 0d                	je     80105a8a <trap+0xfa>
80105a7d:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105a81:	83 e0 03             	and    $0x3,%eax
80105a84:	66 83 f8 03          	cmp    $0x3,%ax
80105a88:	74 37                	je     80105ac1 <trap+0x131>
    exit();
}
80105a8a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105a8d:	5b                   	pop    %ebx
80105a8e:	5e                   	pop    %esi
80105a8f:	5f                   	pop    %edi
80105a90:	5d                   	pop    %ebp
80105a91:	c3                   	ret    
80105a92:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(myproc()->killed)
80105a98:	e8 d3 df ff ff       	call   80103a70 <myproc>
80105a9d:	8b 58 24             	mov    0x24(%eax),%ebx
80105aa0:	85 db                	test   %ebx,%ebx
80105aa2:	0f 85 e8 00 00 00    	jne    80105b90 <trap+0x200>
    myproc()->tf = tf;
80105aa8:	e8 c3 df ff ff       	call   80103a70 <myproc>
80105aad:	89 78 18             	mov    %edi,0x18(%eax)
    syscall();
80105ab0:	e8 1b f0 ff ff       	call   80104ad0 <syscall>
    if(myproc()->killed)
80105ab5:	e8 b6 df ff ff       	call   80103a70 <myproc>
80105aba:	8b 48 24             	mov    0x24(%eax),%ecx
80105abd:	85 c9                	test   %ecx,%ecx
80105abf:	74 c9                	je     80105a8a <trap+0xfa>
}
80105ac1:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105ac4:	5b                   	pop    %ebx
80105ac5:	5e                   	pop    %esi
80105ac6:	5f                   	pop    %edi
80105ac7:	5d                   	pop    %ebp
      exit();
80105ac8:	e9 c3 e3 ff ff       	jmp    80103e90 <exit>
80105acd:	8d 76 00             	lea    0x0(%esi),%esi
  if(myproc() && myproc()->state == RUNNING &&
80105ad0:	83 7f 30 20          	cmpl   $0x20,0x30(%edi)
80105ad4:	75 92                	jne    80105a68 <trap+0xd8>
    yield();
80105ad6:	e8 e5 e4 ff ff       	call   80103fc0 <yield>
80105adb:	eb 8b                	jmp    80105a68 <trap+0xd8>
80105add:	8d 76 00             	lea    0x0(%esi),%esi
    if(cpuid() == 0){
80105ae0:	e8 6b df ff ff       	call   80103a50 <cpuid>
80105ae5:	85 c0                	test   %eax,%eax
80105ae7:	0f 84 c3 00 00 00    	je     80105bb0 <trap+0x220>
    lapiceoi();
80105aed:	e8 ee ce ff ff       	call   801029e0 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105af2:	e8 79 df ff ff       	call   80103a70 <myproc>
80105af7:	85 c0                	test   %eax,%eax
80105af9:	0f 85 38 ff ff ff    	jne    80105a37 <trap+0xa7>
80105aff:	e9 50 ff ff ff       	jmp    80105a54 <trap+0xc4>
80105b04:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    kbdintr();
80105b08:	e8 93 cd ff ff       	call   801028a0 <kbdintr>
    lapiceoi();
80105b0d:	e8 ce ce ff ff       	call   801029e0 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b12:	e8 59 df ff ff       	call   80103a70 <myproc>
80105b17:	85 c0                	test   %eax,%eax
80105b19:	0f 85 18 ff ff ff    	jne    80105a37 <trap+0xa7>
80105b1f:	e9 30 ff ff ff       	jmp    80105a54 <trap+0xc4>
80105b24:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    uartintr();
80105b28:	e8 53 02 00 00       	call   80105d80 <uartintr>
    lapiceoi();
80105b2d:	e8 ae ce ff ff       	call   801029e0 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b32:	e8 39 df ff ff       	call   80103a70 <myproc>
80105b37:	85 c0                	test   %eax,%eax
80105b39:	0f 85 f8 fe ff ff    	jne    80105a37 <trap+0xa7>
80105b3f:	e9 10 ff ff ff       	jmp    80105a54 <trap+0xc4>
80105b44:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80105b48:	0f b7 5f 3c          	movzwl 0x3c(%edi),%ebx
80105b4c:	8b 77 38             	mov    0x38(%edi),%esi
80105b4f:	e8 fc de ff ff       	call   80103a50 <cpuid>
80105b54:	56                   	push   %esi
80105b55:	53                   	push   %ebx
80105b56:	50                   	push   %eax
80105b57:	68 e4 78 10 80       	push   $0x801078e4
80105b5c:	e8 0f ac ff ff       	call   80100770 <cprintf>
    lapiceoi();
80105b61:	e8 7a ce ff ff       	call   801029e0 <lapiceoi>
    break;
80105b66:	83 c4 10             	add    $0x10,%esp
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b69:	e8 02 df ff ff       	call   80103a70 <myproc>
80105b6e:	85 c0                	test   %eax,%eax
80105b70:	0f 85 c1 fe ff ff    	jne    80105a37 <trap+0xa7>
80105b76:	e9 d9 fe ff ff       	jmp    80105a54 <trap+0xc4>
80105b7b:	90                   	nop
80105b7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    ideintr();
80105b80:	e8 8b c7 ff ff       	call   80102310 <ideintr>
80105b85:	e9 63 ff ff ff       	jmp    80105aed <trap+0x15d>
80105b8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      exit();
80105b90:	e8 fb e2 ff ff       	call   80103e90 <exit>
80105b95:	e9 0e ff ff ff       	jmp    80105aa8 <trap+0x118>
80105b9a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    exit();
80105ba0:	e8 eb e2 ff ff       	call   80103e90 <exit>
80105ba5:	e9 aa fe ff ff       	jmp    80105a54 <trap+0xc4>
80105baa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      acquire(&tickslock);
80105bb0:	83 ec 0c             	sub    $0xc,%esp
80105bb3:	68 60 4c 11 80       	push   $0x80114c60
80105bb8:	e8 13 ea ff ff       	call   801045d0 <acquire>
      wakeup(&ticks);
80105bbd:	c7 04 24 a0 54 11 80 	movl   $0x801154a0,(%esp)
      ticks++;
80105bc4:	83 05 a0 54 11 80 01 	addl   $0x1,0x801154a0
      wakeup(&ticks);
80105bcb:	e8 f0 e5 ff ff       	call   801041c0 <wakeup>
      release(&tickslock);
80105bd0:	c7 04 24 60 4c 11 80 	movl   $0x80114c60,(%esp)
80105bd7:	e8 b4 ea ff ff       	call   80104690 <release>
80105bdc:	83 c4 10             	add    $0x10,%esp
80105bdf:	e9 09 ff ff ff       	jmp    80105aed <trap+0x15d>
80105be4:	0f 20 d6             	mov    %cr2,%esi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80105be7:	e8 64 de ff ff       	call   80103a50 <cpuid>
80105bec:	83 ec 0c             	sub    $0xc,%esp
80105bef:	56                   	push   %esi
80105bf0:	53                   	push   %ebx
80105bf1:	50                   	push   %eax
80105bf2:	ff 77 30             	pushl  0x30(%edi)
80105bf5:	68 08 79 10 80       	push   $0x80107908
80105bfa:	e8 71 ab ff ff       	call   80100770 <cprintf>
      panic("trap");
80105bff:	83 c4 14             	add    $0x14,%esp
80105c02:	68 de 78 10 80       	push   $0x801078de
80105c07:	e8 84 a7 ff ff       	call   80100390 <panic>
80105c0c:	66 90                	xchg   %ax,%ax
80105c0e:	66 90                	xchg   %ax,%ax

80105c10 <uartgetc>:
}

static int
uartgetc(void)
{
  if(!uart)
80105c10:	a1 bc a5 10 80       	mov    0x8010a5bc,%eax
{
80105c15:	55                   	push   %ebp
80105c16:	89 e5                	mov    %esp,%ebp
  if(!uart)
80105c18:	85 c0                	test   %eax,%eax
80105c1a:	74 1c                	je     80105c38 <uartgetc+0x28>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105c1c:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105c21:	ec                   	in     (%dx),%al
    return -1;
  if(!(inb(COM1+5) & 0x01))
80105c22:	a8 01                	test   $0x1,%al
80105c24:	74 12                	je     80105c38 <uartgetc+0x28>
80105c26:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105c2b:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
80105c2c:	0f b6 c0             	movzbl %al,%eax
}
80105c2f:	5d                   	pop    %ebp
80105c30:	c3                   	ret    
80105c31:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80105c38:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105c3d:	5d                   	pop    %ebp
80105c3e:	c3                   	ret    
80105c3f:	90                   	nop

80105c40 <uartputc.part.0>:
uartputc(int c)
80105c40:	55                   	push   %ebp
80105c41:	89 e5                	mov    %esp,%ebp
80105c43:	57                   	push   %edi
80105c44:	56                   	push   %esi
80105c45:	53                   	push   %ebx
80105c46:	89 c7                	mov    %eax,%edi
80105c48:	bb 80 00 00 00       	mov    $0x80,%ebx
80105c4d:	be fd 03 00 00       	mov    $0x3fd,%esi
80105c52:	83 ec 0c             	sub    $0xc,%esp
80105c55:	eb 1b                	jmp    80105c72 <uartputc.part.0+0x32>
80105c57:	89 f6                	mov    %esi,%esi
80105c59:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
    microdelay(10);
80105c60:	83 ec 0c             	sub    $0xc,%esp
80105c63:	6a 0a                	push   $0xa
80105c65:	e8 96 cd ff ff       	call   80102a00 <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
80105c6a:	83 c4 10             	add    $0x10,%esp
80105c6d:	83 eb 01             	sub    $0x1,%ebx
80105c70:	74 07                	je     80105c79 <uartputc.part.0+0x39>
80105c72:	89 f2                	mov    %esi,%edx
80105c74:	ec                   	in     (%dx),%al
80105c75:	a8 20                	test   $0x20,%al
80105c77:	74 e7                	je     80105c60 <uartputc.part.0+0x20>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80105c79:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105c7e:	89 f8                	mov    %edi,%eax
80105c80:	ee                   	out    %al,(%dx)
}
80105c81:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105c84:	5b                   	pop    %ebx
80105c85:	5e                   	pop    %esi
80105c86:	5f                   	pop    %edi
80105c87:	5d                   	pop    %ebp
80105c88:	c3                   	ret    
80105c89:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105c90 <uartinit>:
{
80105c90:	55                   	push   %ebp
80105c91:	31 c9                	xor    %ecx,%ecx
80105c93:	89 c8                	mov    %ecx,%eax
80105c95:	89 e5                	mov    %esp,%ebp
80105c97:	57                   	push   %edi
80105c98:	56                   	push   %esi
80105c99:	53                   	push   %ebx
80105c9a:	bb fa 03 00 00       	mov    $0x3fa,%ebx
80105c9f:	89 da                	mov    %ebx,%edx
80105ca1:	83 ec 0c             	sub    $0xc,%esp
80105ca4:	ee                   	out    %al,(%dx)
80105ca5:	bf fb 03 00 00       	mov    $0x3fb,%edi
80105caa:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80105caf:	89 fa                	mov    %edi,%edx
80105cb1:	ee                   	out    %al,(%dx)
80105cb2:	b8 0c 00 00 00       	mov    $0xc,%eax
80105cb7:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105cbc:	ee                   	out    %al,(%dx)
80105cbd:	be f9 03 00 00       	mov    $0x3f9,%esi
80105cc2:	89 c8                	mov    %ecx,%eax
80105cc4:	89 f2                	mov    %esi,%edx
80105cc6:	ee                   	out    %al,(%dx)
80105cc7:	b8 03 00 00 00       	mov    $0x3,%eax
80105ccc:	89 fa                	mov    %edi,%edx
80105cce:	ee                   	out    %al,(%dx)
80105ccf:	ba fc 03 00 00       	mov    $0x3fc,%edx
80105cd4:	89 c8                	mov    %ecx,%eax
80105cd6:	ee                   	out    %al,(%dx)
80105cd7:	b8 01 00 00 00       	mov    $0x1,%eax
80105cdc:	89 f2                	mov    %esi,%edx
80105cde:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105cdf:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105ce4:	ec                   	in     (%dx),%al
  if(inb(COM1+5) == 0xFF)
80105ce5:	3c ff                	cmp    $0xff,%al
80105ce7:	74 5a                	je     80105d43 <uartinit+0xb3>
  uart = 1;
80105ce9:	c7 05 bc a5 10 80 01 	movl   $0x1,0x8010a5bc
80105cf0:	00 00 00 
80105cf3:	89 da                	mov    %ebx,%edx
80105cf5:	ec                   	in     (%dx),%al
80105cf6:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105cfb:	ec                   	in     (%dx),%al
  ioapicenable(IRQ_COM1, 0);
80105cfc:	83 ec 08             	sub    $0x8,%esp
  for(p="xv6...\n"; *p; p++)
80105cff:	bb 00 7a 10 80       	mov    $0x80107a00,%ebx
  ioapicenable(IRQ_COM1, 0);
80105d04:	6a 00                	push   $0x0
80105d06:	6a 04                	push   $0x4
80105d08:	e8 53 c8 ff ff       	call   80102560 <ioapicenable>
80105d0d:	83 c4 10             	add    $0x10,%esp
  for(p="xv6...\n"; *p; p++)
80105d10:	b8 78 00 00 00       	mov    $0x78,%eax
80105d15:	eb 13                	jmp    80105d2a <uartinit+0x9a>
80105d17:	89 f6                	mov    %esi,%esi
80105d19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105d20:	83 c3 01             	add    $0x1,%ebx
80105d23:	0f be 03             	movsbl (%ebx),%eax
80105d26:	84 c0                	test   %al,%al
80105d28:	74 19                	je     80105d43 <uartinit+0xb3>
  if(!uart)
80105d2a:	8b 15 bc a5 10 80    	mov    0x8010a5bc,%edx
80105d30:	85 d2                	test   %edx,%edx
80105d32:	74 ec                	je     80105d20 <uartinit+0x90>
  for(p="xv6...\n"; *p; p++)
80105d34:	83 c3 01             	add    $0x1,%ebx
80105d37:	e8 04 ff ff ff       	call   80105c40 <uartputc.part.0>
80105d3c:	0f be 03             	movsbl (%ebx),%eax
80105d3f:	84 c0                	test   %al,%al
80105d41:	75 e7                	jne    80105d2a <uartinit+0x9a>
}
80105d43:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105d46:	5b                   	pop    %ebx
80105d47:	5e                   	pop    %esi
80105d48:	5f                   	pop    %edi
80105d49:	5d                   	pop    %ebp
80105d4a:	c3                   	ret    
80105d4b:	90                   	nop
80105d4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105d50 <uartputc>:
  if(!uart)
80105d50:	8b 15 bc a5 10 80    	mov    0x8010a5bc,%edx
{
80105d56:	55                   	push   %ebp
80105d57:	89 e5                	mov    %esp,%ebp
  if(!uart)
80105d59:	85 d2                	test   %edx,%edx
{
80105d5b:	8b 45 08             	mov    0x8(%ebp),%eax
  if(!uart)
80105d5e:	74 10                	je     80105d70 <uartputc+0x20>
}
80105d60:	5d                   	pop    %ebp
80105d61:	e9 da fe ff ff       	jmp    80105c40 <uartputc.part.0>
80105d66:	8d 76 00             	lea    0x0(%esi),%esi
80105d69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80105d70:	5d                   	pop    %ebp
80105d71:	c3                   	ret    
80105d72:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105d79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80105d80 <uartintr>:

void
uartintr(void)
{
80105d80:	55                   	push   %ebp
80105d81:	89 e5                	mov    %esp,%ebp
80105d83:	83 ec 14             	sub    $0x14,%esp
  consoleintr(uartgetc);
80105d86:	68 10 5c 10 80       	push   $0x80105c10
80105d8b:	e8 90 ab ff ff       	call   80100920 <consoleintr>
}
80105d90:	83 c4 10             	add    $0x10,%esp
80105d93:	c9                   	leave  
80105d94:	c3                   	ret    

80105d95 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
80105d95:	6a 00                	push   $0x0
  pushl $0
80105d97:	6a 00                	push   $0x0
  jmp alltraps
80105d99:	e9 1c fb ff ff       	jmp    801058ba <alltraps>

80105d9e <vector1>:
.globl vector1
vector1:
  pushl $0
80105d9e:	6a 00                	push   $0x0
  pushl $1
80105da0:	6a 01                	push   $0x1
  jmp alltraps
80105da2:	e9 13 fb ff ff       	jmp    801058ba <alltraps>

80105da7 <vector2>:
.globl vector2
vector2:
  pushl $0
80105da7:	6a 00                	push   $0x0
  pushl $2
80105da9:	6a 02                	push   $0x2
  jmp alltraps
80105dab:	e9 0a fb ff ff       	jmp    801058ba <alltraps>

80105db0 <vector3>:
.globl vector3
vector3:
  pushl $0
80105db0:	6a 00                	push   $0x0
  pushl $3
80105db2:	6a 03                	push   $0x3
  jmp alltraps
80105db4:	e9 01 fb ff ff       	jmp    801058ba <alltraps>

80105db9 <vector4>:
.globl vector4
vector4:
  pushl $0
80105db9:	6a 00                	push   $0x0
  pushl $4
80105dbb:	6a 04                	push   $0x4
  jmp alltraps
80105dbd:	e9 f8 fa ff ff       	jmp    801058ba <alltraps>

80105dc2 <vector5>:
.globl vector5
vector5:
  pushl $0
80105dc2:	6a 00                	push   $0x0
  pushl $5
80105dc4:	6a 05                	push   $0x5
  jmp alltraps
80105dc6:	e9 ef fa ff ff       	jmp    801058ba <alltraps>

80105dcb <vector6>:
.globl vector6
vector6:
  pushl $0
80105dcb:	6a 00                	push   $0x0
  pushl $6
80105dcd:	6a 06                	push   $0x6
  jmp alltraps
80105dcf:	e9 e6 fa ff ff       	jmp    801058ba <alltraps>

80105dd4 <vector7>:
.globl vector7
vector7:
  pushl $0
80105dd4:	6a 00                	push   $0x0
  pushl $7
80105dd6:	6a 07                	push   $0x7
  jmp alltraps
80105dd8:	e9 dd fa ff ff       	jmp    801058ba <alltraps>

80105ddd <vector8>:
.globl vector8
vector8:
  pushl $8
80105ddd:	6a 08                	push   $0x8
  jmp alltraps
80105ddf:	e9 d6 fa ff ff       	jmp    801058ba <alltraps>

80105de4 <vector9>:
.globl vector9
vector9:
  pushl $0
80105de4:	6a 00                	push   $0x0
  pushl $9
80105de6:	6a 09                	push   $0x9
  jmp alltraps
80105de8:	e9 cd fa ff ff       	jmp    801058ba <alltraps>

80105ded <vector10>:
.globl vector10
vector10:
  pushl $10
80105ded:	6a 0a                	push   $0xa
  jmp alltraps
80105def:	e9 c6 fa ff ff       	jmp    801058ba <alltraps>

80105df4 <vector11>:
.globl vector11
vector11:
  pushl $11
80105df4:	6a 0b                	push   $0xb
  jmp alltraps
80105df6:	e9 bf fa ff ff       	jmp    801058ba <alltraps>

80105dfb <vector12>:
.globl vector12
vector12:
  pushl $12
80105dfb:	6a 0c                	push   $0xc
  jmp alltraps
80105dfd:	e9 b8 fa ff ff       	jmp    801058ba <alltraps>

80105e02 <vector13>:
.globl vector13
vector13:
  pushl $13
80105e02:	6a 0d                	push   $0xd
  jmp alltraps
80105e04:	e9 b1 fa ff ff       	jmp    801058ba <alltraps>

80105e09 <vector14>:
.globl vector14
vector14:
  pushl $14
80105e09:	6a 0e                	push   $0xe
  jmp alltraps
80105e0b:	e9 aa fa ff ff       	jmp    801058ba <alltraps>

80105e10 <vector15>:
.globl vector15
vector15:
  pushl $0
80105e10:	6a 00                	push   $0x0
  pushl $15
80105e12:	6a 0f                	push   $0xf
  jmp alltraps
80105e14:	e9 a1 fa ff ff       	jmp    801058ba <alltraps>

80105e19 <vector16>:
.globl vector16
vector16:
  pushl $0
80105e19:	6a 00                	push   $0x0
  pushl $16
80105e1b:	6a 10                	push   $0x10
  jmp alltraps
80105e1d:	e9 98 fa ff ff       	jmp    801058ba <alltraps>

80105e22 <vector17>:
.globl vector17
vector17:
  pushl $17
80105e22:	6a 11                	push   $0x11
  jmp alltraps
80105e24:	e9 91 fa ff ff       	jmp    801058ba <alltraps>

80105e29 <vector18>:
.globl vector18
vector18:
  pushl $0
80105e29:	6a 00                	push   $0x0
  pushl $18
80105e2b:	6a 12                	push   $0x12
  jmp alltraps
80105e2d:	e9 88 fa ff ff       	jmp    801058ba <alltraps>

80105e32 <vector19>:
.globl vector19
vector19:
  pushl $0
80105e32:	6a 00                	push   $0x0
  pushl $19
80105e34:	6a 13                	push   $0x13
  jmp alltraps
80105e36:	e9 7f fa ff ff       	jmp    801058ba <alltraps>

80105e3b <vector20>:
.globl vector20
vector20:
  pushl $0
80105e3b:	6a 00                	push   $0x0
  pushl $20
80105e3d:	6a 14                	push   $0x14
  jmp alltraps
80105e3f:	e9 76 fa ff ff       	jmp    801058ba <alltraps>

80105e44 <vector21>:
.globl vector21
vector21:
  pushl $0
80105e44:	6a 00                	push   $0x0
  pushl $21
80105e46:	6a 15                	push   $0x15
  jmp alltraps
80105e48:	e9 6d fa ff ff       	jmp    801058ba <alltraps>

80105e4d <vector22>:
.globl vector22
vector22:
  pushl $0
80105e4d:	6a 00                	push   $0x0
  pushl $22
80105e4f:	6a 16                	push   $0x16
  jmp alltraps
80105e51:	e9 64 fa ff ff       	jmp    801058ba <alltraps>

80105e56 <vector23>:
.globl vector23
vector23:
  pushl $0
80105e56:	6a 00                	push   $0x0
  pushl $23
80105e58:	6a 17                	push   $0x17
  jmp alltraps
80105e5a:	e9 5b fa ff ff       	jmp    801058ba <alltraps>

80105e5f <vector24>:
.globl vector24
vector24:
  pushl $0
80105e5f:	6a 00                	push   $0x0
  pushl $24
80105e61:	6a 18                	push   $0x18
  jmp alltraps
80105e63:	e9 52 fa ff ff       	jmp    801058ba <alltraps>

80105e68 <vector25>:
.globl vector25
vector25:
  pushl $0
80105e68:	6a 00                	push   $0x0
  pushl $25
80105e6a:	6a 19                	push   $0x19
  jmp alltraps
80105e6c:	e9 49 fa ff ff       	jmp    801058ba <alltraps>

80105e71 <vector26>:
.globl vector26
vector26:
  pushl $0
80105e71:	6a 00                	push   $0x0
  pushl $26
80105e73:	6a 1a                	push   $0x1a
  jmp alltraps
80105e75:	e9 40 fa ff ff       	jmp    801058ba <alltraps>

80105e7a <vector27>:
.globl vector27
vector27:
  pushl $0
80105e7a:	6a 00                	push   $0x0
  pushl $27
80105e7c:	6a 1b                	push   $0x1b
  jmp alltraps
80105e7e:	e9 37 fa ff ff       	jmp    801058ba <alltraps>

80105e83 <vector28>:
.globl vector28
vector28:
  pushl $0
80105e83:	6a 00                	push   $0x0
  pushl $28
80105e85:	6a 1c                	push   $0x1c
  jmp alltraps
80105e87:	e9 2e fa ff ff       	jmp    801058ba <alltraps>

80105e8c <vector29>:
.globl vector29
vector29:
  pushl $0
80105e8c:	6a 00                	push   $0x0
  pushl $29
80105e8e:	6a 1d                	push   $0x1d
  jmp alltraps
80105e90:	e9 25 fa ff ff       	jmp    801058ba <alltraps>

80105e95 <vector30>:
.globl vector30
vector30:
  pushl $0
80105e95:	6a 00                	push   $0x0
  pushl $30
80105e97:	6a 1e                	push   $0x1e
  jmp alltraps
80105e99:	e9 1c fa ff ff       	jmp    801058ba <alltraps>

80105e9e <vector31>:
.globl vector31
vector31:
  pushl $0
80105e9e:	6a 00                	push   $0x0
  pushl $31
80105ea0:	6a 1f                	push   $0x1f
  jmp alltraps
80105ea2:	e9 13 fa ff ff       	jmp    801058ba <alltraps>

80105ea7 <vector32>:
.globl vector32
vector32:
  pushl $0
80105ea7:	6a 00                	push   $0x0
  pushl $32
80105ea9:	6a 20                	push   $0x20
  jmp alltraps
80105eab:	e9 0a fa ff ff       	jmp    801058ba <alltraps>

80105eb0 <vector33>:
.globl vector33
vector33:
  pushl $0
80105eb0:	6a 00                	push   $0x0
  pushl $33
80105eb2:	6a 21                	push   $0x21
  jmp alltraps
80105eb4:	e9 01 fa ff ff       	jmp    801058ba <alltraps>

80105eb9 <vector34>:
.globl vector34
vector34:
  pushl $0
80105eb9:	6a 00                	push   $0x0
  pushl $34
80105ebb:	6a 22                	push   $0x22
  jmp alltraps
80105ebd:	e9 f8 f9 ff ff       	jmp    801058ba <alltraps>

80105ec2 <vector35>:
.globl vector35
vector35:
  pushl $0
80105ec2:	6a 00                	push   $0x0
  pushl $35
80105ec4:	6a 23                	push   $0x23
  jmp alltraps
80105ec6:	e9 ef f9 ff ff       	jmp    801058ba <alltraps>

80105ecb <vector36>:
.globl vector36
vector36:
  pushl $0
80105ecb:	6a 00                	push   $0x0
  pushl $36
80105ecd:	6a 24                	push   $0x24
  jmp alltraps
80105ecf:	e9 e6 f9 ff ff       	jmp    801058ba <alltraps>

80105ed4 <vector37>:
.globl vector37
vector37:
  pushl $0
80105ed4:	6a 00                	push   $0x0
  pushl $37
80105ed6:	6a 25                	push   $0x25
  jmp alltraps
80105ed8:	e9 dd f9 ff ff       	jmp    801058ba <alltraps>

80105edd <vector38>:
.globl vector38
vector38:
  pushl $0
80105edd:	6a 00                	push   $0x0
  pushl $38
80105edf:	6a 26                	push   $0x26
  jmp alltraps
80105ee1:	e9 d4 f9 ff ff       	jmp    801058ba <alltraps>

80105ee6 <vector39>:
.globl vector39
vector39:
  pushl $0
80105ee6:	6a 00                	push   $0x0
  pushl $39
80105ee8:	6a 27                	push   $0x27
  jmp alltraps
80105eea:	e9 cb f9 ff ff       	jmp    801058ba <alltraps>

80105eef <vector40>:
.globl vector40
vector40:
  pushl $0
80105eef:	6a 00                	push   $0x0
  pushl $40
80105ef1:	6a 28                	push   $0x28
  jmp alltraps
80105ef3:	e9 c2 f9 ff ff       	jmp    801058ba <alltraps>

80105ef8 <vector41>:
.globl vector41
vector41:
  pushl $0
80105ef8:	6a 00                	push   $0x0
  pushl $41
80105efa:	6a 29                	push   $0x29
  jmp alltraps
80105efc:	e9 b9 f9 ff ff       	jmp    801058ba <alltraps>

80105f01 <vector42>:
.globl vector42
vector42:
  pushl $0
80105f01:	6a 00                	push   $0x0
  pushl $42
80105f03:	6a 2a                	push   $0x2a
  jmp alltraps
80105f05:	e9 b0 f9 ff ff       	jmp    801058ba <alltraps>

80105f0a <vector43>:
.globl vector43
vector43:
  pushl $0
80105f0a:	6a 00                	push   $0x0
  pushl $43
80105f0c:	6a 2b                	push   $0x2b
  jmp alltraps
80105f0e:	e9 a7 f9 ff ff       	jmp    801058ba <alltraps>

80105f13 <vector44>:
.globl vector44
vector44:
  pushl $0
80105f13:	6a 00                	push   $0x0
  pushl $44
80105f15:	6a 2c                	push   $0x2c
  jmp alltraps
80105f17:	e9 9e f9 ff ff       	jmp    801058ba <alltraps>

80105f1c <vector45>:
.globl vector45
vector45:
  pushl $0
80105f1c:	6a 00                	push   $0x0
  pushl $45
80105f1e:	6a 2d                	push   $0x2d
  jmp alltraps
80105f20:	e9 95 f9 ff ff       	jmp    801058ba <alltraps>

80105f25 <vector46>:
.globl vector46
vector46:
  pushl $0
80105f25:	6a 00                	push   $0x0
  pushl $46
80105f27:	6a 2e                	push   $0x2e
  jmp alltraps
80105f29:	e9 8c f9 ff ff       	jmp    801058ba <alltraps>

80105f2e <vector47>:
.globl vector47
vector47:
  pushl $0
80105f2e:	6a 00                	push   $0x0
  pushl $47
80105f30:	6a 2f                	push   $0x2f
  jmp alltraps
80105f32:	e9 83 f9 ff ff       	jmp    801058ba <alltraps>

80105f37 <vector48>:
.globl vector48
vector48:
  pushl $0
80105f37:	6a 00                	push   $0x0
  pushl $48
80105f39:	6a 30                	push   $0x30
  jmp alltraps
80105f3b:	e9 7a f9 ff ff       	jmp    801058ba <alltraps>

80105f40 <vector49>:
.globl vector49
vector49:
  pushl $0
80105f40:	6a 00                	push   $0x0
  pushl $49
80105f42:	6a 31                	push   $0x31
  jmp alltraps
80105f44:	e9 71 f9 ff ff       	jmp    801058ba <alltraps>

80105f49 <vector50>:
.globl vector50
vector50:
  pushl $0
80105f49:	6a 00                	push   $0x0
  pushl $50
80105f4b:	6a 32                	push   $0x32
  jmp alltraps
80105f4d:	e9 68 f9 ff ff       	jmp    801058ba <alltraps>

80105f52 <vector51>:
.globl vector51
vector51:
  pushl $0
80105f52:	6a 00                	push   $0x0
  pushl $51
80105f54:	6a 33                	push   $0x33
  jmp alltraps
80105f56:	e9 5f f9 ff ff       	jmp    801058ba <alltraps>

80105f5b <vector52>:
.globl vector52
vector52:
  pushl $0
80105f5b:	6a 00                	push   $0x0
  pushl $52
80105f5d:	6a 34                	push   $0x34
  jmp alltraps
80105f5f:	e9 56 f9 ff ff       	jmp    801058ba <alltraps>

80105f64 <vector53>:
.globl vector53
vector53:
  pushl $0
80105f64:	6a 00                	push   $0x0
  pushl $53
80105f66:	6a 35                	push   $0x35
  jmp alltraps
80105f68:	e9 4d f9 ff ff       	jmp    801058ba <alltraps>

80105f6d <vector54>:
.globl vector54
vector54:
  pushl $0
80105f6d:	6a 00                	push   $0x0
  pushl $54
80105f6f:	6a 36                	push   $0x36
  jmp alltraps
80105f71:	e9 44 f9 ff ff       	jmp    801058ba <alltraps>

80105f76 <vector55>:
.globl vector55
vector55:
  pushl $0
80105f76:	6a 00                	push   $0x0
  pushl $55
80105f78:	6a 37                	push   $0x37
  jmp alltraps
80105f7a:	e9 3b f9 ff ff       	jmp    801058ba <alltraps>

80105f7f <vector56>:
.globl vector56
vector56:
  pushl $0
80105f7f:	6a 00                	push   $0x0
  pushl $56
80105f81:	6a 38                	push   $0x38
  jmp alltraps
80105f83:	e9 32 f9 ff ff       	jmp    801058ba <alltraps>

80105f88 <vector57>:
.globl vector57
vector57:
  pushl $0
80105f88:	6a 00                	push   $0x0
  pushl $57
80105f8a:	6a 39                	push   $0x39
  jmp alltraps
80105f8c:	e9 29 f9 ff ff       	jmp    801058ba <alltraps>

80105f91 <vector58>:
.globl vector58
vector58:
  pushl $0
80105f91:	6a 00                	push   $0x0
  pushl $58
80105f93:	6a 3a                	push   $0x3a
  jmp alltraps
80105f95:	e9 20 f9 ff ff       	jmp    801058ba <alltraps>

80105f9a <vector59>:
.globl vector59
vector59:
  pushl $0
80105f9a:	6a 00                	push   $0x0
  pushl $59
80105f9c:	6a 3b                	push   $0x3b
  jmp alltraps
80105f9e:	e9 17 f9 ff ff       	jmp    801058ba <alltraps>

80105fa3 <vector60>:
.globl vector60
vector60:
  pushl $0
80105fa3:	6a 00                	push   $0x0
  pushl $60
80105fa5:	6a 3c                	push   $0x3c
  jmp alltraps
80105fa7:	e9 0e f9 ff ff       	jmp    801058ba <alltraps>

80105fac <vector61>:
.globl vector61
vector61:
  pushl $0
80105fac:	6a 00                	push   $0x0
  pushl $61
80105fae:	6a 3d                	push   $0x3d
  jmp alltraps
80105fb0:	e9 05 f9 ff ff       	jmp    801058ba <alltraps>

80105fb5 <vector62>:
.globl vector62
vector62:
  pushl $0
80105fb5:	6a 00                	push   $0x0
  pushl $62
80105fb7:	6a 3e                	push   $0x3e
  jmp alltraps
80105fb9:	e9 fc f8 ff ff       	jmp    801058ba <alltraps>

80105fbe <vector63>:
.globl vector63
vector63:
  pushl $0
80105fbe:	6a 00                	push   $0x0
  pushl $63
80105fc0:	6a 3f                	push   $0x3f
  jmp alltraps
80105fc2:	e9 f3 f8 ff ff       	jmp    801058ba <alltraps>

80105fc7 <vector64>:
.globl vector64
vector64:
  pushl $0
80105fc7:	6a 00                	push   $0x0
  pushl $64
80105fc9:	6a 40                	push   $0x40
  jmp alltraps
80105fcb:	e9 ea f8 ff ff       	jmp    801058ba <alltraps>

80105fd0 <vector65>:
.globl vector65
vector65:
  pushl $0
80105fd0:	6a 00                	push   $0x0
  pushl $65
80105fd2:	6a 41                	push   $0x41
  jmp alltraps
80105fd4:	e9 e1 f8 ff ff       	jmp    801058ba <alltraps>

80105fd9 <vector66>:
.globl vector66
vector66:
  pushl $0
80105fd9:	6a 00                	push   $0x0
  pushl $66
80105fdb:	6a 42                	push   $0x42
  jmp alltraps
80105fdd:	e9 d8 f8 ff ff       	jmp    801058ba <alltraps>

80105fe2 <vector67>:
.globl vector67
vector67:
  pushl $0
80105fe2:	6a 00                	push   $0x0
  pushl $67
80105fe4:	6a 43                	push   $0x43
  jmp alltraps
80105fe6:	e9 cf f8 ff ff       	jmp    801058ba <alltraps>

80105feb <vector68>:
.globl vector68
vector68:
  pushl $0
80105feb:	6a 00                	push   $0x0
  pushl $68
80105fed:	6a 44                	push   $0x44
  jmp alltraps
80105fef:	e9 c6 f8 ff ff       	jmp    801058ba <alltraps>

80105ff4 <vector69>:
.globl vector69
vector69:
  pushl $0
80105ff4:	6a 00                	push   $0x0
  pushl $69
80105ff6:	6a 45                	push   $0x45
  jmp alltraps
80105ff8:	e9 bd f8 ff ff       	jmp    801058ba <alltraps>

80105ffd <vector70>:
.globl vector70
vector70:
  pushl $0
80105ffd:	6a 00                	push   $0x0
  pushl $70
80105fff:	6a 46                	push   $0x46
  jmp alltraps
80106001:	e9 b4 f8 ff ff       	jmp    801058ba <alltraps>

80106006 <vector71>:
.globl vector71
vector71:
  pushl $0
80106006:	6a 00                	push   $0x0
  pushl $71
80106008:	6a 47                	push   $0x47
  jmp alltraps
8010600a:	e9 ab f8 ff ff       	jmp    801058ba <alltraps>

8010600f <vector72>:
.globl vector72
vector72:
  pushl $0
8010600f:	6a 00                	push   $0x0
  pushl $72
80106011:	6a 48                	push   $0x48
  jmp alltraps
80106013:	e9 a2 f8 ff ff       	jmp    801058ba <alltraps>

80106018 <vector73>:
.globl vector73
vector73:
  pushl $0
80106018:	6a 00                	push   $0x0
  pushl $73
8010601a:	6a 49                	push   $0x49
  jmp alltraps
8010601c:	e9 99 f8 ff ff       	jmp    801058ba <alltraps>

80106021 <vector74>:
.globl vector74
vector74:
  pushl $0
80106021:	6a 00                	push   $0x0
  pushl $74
80106023:	6a 4a                	push   $0x4a
  jmp alltraps
80106025:	e9 90 f8 ff ff       	jmp    801058ba <alltraps>

8010602a <vector75>:
.globl vector75
vector75:
  pushl $0
8010602a:	6a 00                	push   $0x0
  pushl $75
8010602c:	6a 4b                	push   $0x4b
  jmp alltraps
8010602e:	e9 87 f8 ff ff       	jmp    801058ba <alltraps>

80106033 <vector76>:
.globl vector76
vector76:
  pushl $0
80106033:	6a 00                	push   $0x0
  pushl $76
80106035:	6a 4c                	push   $0x4c
  jmp alltraps
80106037:	e9 7e f8 ff ff       	jmp    801058ba <alltraps>

8010603c <vector77>:
.globl vector77
vector77:
  pushl $0
8010603c:	6a 00                	push   $0x0
  pushl $77
8010603e:	6a 4d                	push   $0x4d
  jmp alltraps
80106040:	e9 75 f8 ff ff       	jmp    801058ba <alltraps>

80106045 <vector78>:
.globl vector78
vector78:
  pushl $0
80106045:	6a 00                	push   $0x0
  pushl $78
80106047:	6a 4e                	push   $0x4e
  jmp alltraps
80106049:	e9 6c f8 ff ff       	jmp    801058ba <alltraps>

8010604e <vector79>:
.globl vector79
vector79:
  pushl $0
8010604e:	6a 00                	push   $0x0
  pushl $79
80106050:	6a 4f                	push   $0x4f
  jmp alltraps
80106052:	e9 63 f8 ff ff       	jmp    801058ba <alltraps>

80106057 <vector80>:
.globl vector80
vector80:
  pushl $0
80106057:	6a 00                	push   $0x0
  pushl $80
80106059:	6a 50                	push   $0x50
  jmp alltraps
8010605b:	e9 5a f8 ff ff       	jmp    801058ba <alltraps>

80106060 <vector81>:
.globl vector81
vector81:
  pushl $0
80106060:	6a 00                	push   $0x0
  pushl $81
80106062:	6a 51                	push   $0x51
  jmp alltraps
80106064:	e9 51 f8 ff ff       	jmp    801058ba <alltraps>

80106069 <vector82>:
.globl vector82
vector82:
  pushl $0
80106069:	6a 00                	push   $0x0
  pushl $82
8010606b:	6a 52                	push   $0x52
  jmp alltraps
8010606d:	e9 48 f8 ff ff       	jmp    801058ba <alltraps>

80106072 <vector83>:
.globl vector83
vector83:
  pushl $0
80106072:	6a 00                	push   $0x0
  pushl $83
80106074:	6a 53                	push   $0x53
  jmp alltraps
80106076:	e9 3f f8 ff ff       	jmp    801058ba <alltraps>

8010607b <vector84>:
.globl vector84
vector84:
  pushl $0
8010607b:	6a 00                	push   $0x0
  pushl $84
8010607d:	6a 54                	push   $0x54
  jmp alltraps
8010607f:	e9 36 f8 ff ff       	jmp    801058ba <alltraps>

80106084 <vector85>:
.globl vector85
vector85:
  pushl $0
80106084:	6a 00                	push   $0x0
  pushl $85
80106086:	6a 55                	push   $0x55
  jmp alltraps
80106088:	e9 2d f8 ff ff       	jmp    801058ba <alltraps>

8010608d <vector86>:
.globl vector86
vector86:
  pushl $0
8010608d:	6a 00                	push   $0x0
  pushl $86
8010608f:	6a 56                	push   $0x56
  jmp alltraps
80106091:	e9 24 f8 ff ff       	jmp    801058ba <alltraps>

80106096 <vector87>:
.globl vector87
vector87:
  pushl $0
80106096:	6a 00                	push   $0x0
  pushl $87
80106098:	6a 57                	push   $0x57
  jmp alltraps
8010609a:	e9 1b f8 ff ff       	jmp    801058ba <alltraps>

8010609f <vector88>:
.globl vector88
vector88:
  pushl $0
8010609f:	6a 00                	push   $0x0
  pushl $88
801060a1:	6a 58                	push   $0x58
  jmp alltraps
801060a3:	e9 12 f8 ff ff       	jmp    801058ba <alltraps>

801060a8 <vector89>:
.globl vector89
vector89:
  pushl $0
801060a8:	6a 00                	push   $0x0
  pushl $89
801060aa:	6a 59                	push   $0x59
  jmp alltraps
801060ac:	e9 09 f8 ff ff       	jmp    801058ba <alltraps>

801060b1 <vector90>:
.globl vector90
vector90:
  pushl $0
801060b1:	6a 00                	push   $0x0
  pushl $90
801060b3:	6a 5a                	push   $0x5a
  jmp alltraps
801060b5:	e9 00 f8 ff ff       	jmp    801058ba <alltraps>

801060ba <vector91>:
.globl vector91
vector91:
  pushl $0
801060ba:	6a 00                	push   $0x0
  pushl $91
801060bc:	6a 5b                	push   $0x5b
  jmp alltraps
801060be:	e9 f7 f7 ff ff       	jmp    801058ba <alltraps>

801060c3 <vector92>:
.globl vector92
vector92:
  pushl $0
801060c3:	6a 00                	push   $0x0
  pushl $92
801060c5:	6a 5c                	push   $0x5c
  jmp alltraps
801060c7:	e9 ee f7 ff ff       	jmp    801058ba <alltraps>

801060cc <vector93>:
.globl vector93
vector93:
  pushl $0
801060cc:	6a 00                	push   $0x0
  pushl $93
801060ce:	6a 5d                	push   $0x5d
  jmp alltraps
801060d0:	e9 e5 f7 ff ff       	jmp    801058ba <alltraps>

801060d5 <vector94>:
.globl vector94
vector94:
  pushl $0
801060d5:	6a 00                	push   $0x0
  pushl $94
801060d7:	6a 5e                	push   $0x5e
  jmp alltraps
801060d9:	e9 dc f7 ff ff       	jmp    801058ba <alltraps>

801060de <vector95>:
.globl vector95
vector95:
  pushl $0
801060de:	6a 00                	push   $0x0
  pushl $95
801060e0:	6a 5f                	push   $0x5f
  jmp alltraps
801060e2:	e9 d3 f7 ff ff       	jmp    801058ba <alltraps>

801060e7 <vector96>:
.globl vector96
vector96:
  pushl $0
801060e7:	6a 00                	push   $0x0
  pushl $96
801060e9:	6a 60                	push   $0x60
  jmp alltraps
801060eb:	e9 ca f7 ff ff       	jmp    801058ba <alltraps>

801060f0 <vector97>:
.globl vector97
vector97:
  pushl $0
801060f0:	6a 00                	push   $0x0
  pushl $97
801060f2:	6a 61                	push   $0x61
  jmp alltraps
801060f4:	e9 c1 f7 ff ff       	jmp    801058ba <alltraps>

801060f9 <vector98>:
.globl vector98
vector98:
  pushl $0
801060f9:	6a 00                	push   $0x0
  pushl $98
801060fb:	6a 62                	push   $0x62
  jmp alltraps
801060fd:	e9 b8 f7 ff ff       	jmp    801058ba <alltraps>

80106102 <vector99>:
.globl vector99
vector99:
  pushl $0
80106102:	6a 00                	push   $0x0
  pushl $99
80106104:	6a 63                	push   $0x63
  jmp alltraps
80106106:	e9 af f7 ff ff       	jmp    801058ba <alltraps>

8010610b <vector100>:
.globl vector100
vector100:
  pushl $0
8010610b:	6a 00                	push   $0x0
  pushl $100
8010610d:	6a 64                	push   $0x64
  jmp alltraps
8010610f:	e9 a6 f7 ff ff       	jmp    801058ba <alltraps>

80106114 <vector101>:
.globl vector101
vector101:
  pushl $0
80106114:	6a 00                	push   $0x0
  pushl $101
80106116:	6a 65                	push   $0x65
  jmp alltraps
80106118:	e9 9d f7 ff ff       	jmp    801058ba <alltraps>

8010611d <vector102>:
.globl vector102
vector102:
  pushl $0
8010611d:	6a 00                	push   $0x0
  pushl $102
8010611f:	6a 66                	push   $0x66
  jmp alltraps
80106121:	e9 94 f7 ff ff       	jmp    801058ba <alltraps>

80106126 <vector103>:
.globl vector103
vector103:
  pushl $0
80106126:	6a 00                	push   $0x0
  pushl $103
80106128:	6a 67                	push   $0x67
  jmp alltraps
8010612a:	e9 8b f7 ff ff       	jmp    801058ba <alltraps>

8010612f <vector104>:
.globl vector104
vector104:
  pushl $0
8010612f:	6a 00                	push   $0x0
  pushl $104
80106131:	6a 68                	push   $0x68
  jmp alltraps
80106133:	e9 82 f7 ff ff       	jmp    801058ba <alltraps>

80106138 <vector105>:
.globl vector105
vector105:
  pushl $0
80106138:	6a 00                	push   $0x0
  pushl $105
8010613a:	6a 69                	push   $0x69
  jmp alltraps
8010613c:	e9 79 f7 ff ff       	jmp    801058ba <alltraps>

80106141 <vector106>:
.globl vector106
vector106:
  pushl $0
80106141:	6a 00                	push   $0x0
  pushl $106
80106143:	6a 6a                	push   $0x6a
  jmp alltraps
80106145:	e9 70 f7 ff ff       	jmp    801058ba <alltraps>

8010614a <vector107>:
.globl vector107
vector107:
  pushl $0
8010614a:	6a 00                	push   $0x0
  pushl $107
8010614c:	6a 6b                	push   $0x6b
  jmp alltraps
8010614e:	e9 67 f7 ff ff       	jmp    801058ba <alltraps>

80106153 <vector108>:
.globl vector108
vector108:
  pushl $0
80106153:	6a 00                	push   $0x0
  pushl $108
80106155:	6a 6c                	push   $0x6c
  jmp alltraps
80106157:	e9 5e f7 ff ff       	jmp    801058ba <alltraps>

8010615c <vector109>:
.globl vector109
vector109:
  pushl $0
8010615c:	6a 00                	push   $0x0
  pushl $109
8010615e:	6a 6d                	push   $0x6d
  jmp alltraps
80106160:	e9 55 f7 ff ff       	jmp    801058ba <alltraps>

80106165 <vector110>:
.globl vector110
vector110:
  pushl $0
80106165:	6a 00                	push   $0x0
  pushl $110
80106167:	6a 6e                	push   $0x6e
  jmp alltraps
80106169:	e9 4c f7 ff ff       	jmp    801058ba <alltraps>

8010616e <vector111>:
.globl vector111
vector111:
  pushl $0
8010616e:	6a 00                	push   $0x0
  pushl $111
80106170:	6a 6f                	push   $0x6f
  jmp alltraps
80106172:	e9 43 f7 ff ff       	jmp    801058ba <alltraps>

80106177 <vector112>:
.globl vector112
vector112:
  pushl $0
80106177:	6a 00                	push   $0x0
  pushl $112
80106179:	6a 70                	push   $0x70
  jmp alltraps
8010617b:	e9 3a f7 ff ff       	jmp    801058ba <alltraps>

80106180 <vector113>:
.globl vector113
vector113:
  pushl $0
80106180:	6a 00                	push   $0x0
  pushl $113
80106182:	6a 71                	push   $0x71
  jmp alltraps
80106184:	e9 31 f7 ff ff       	jmp    801058ba <alltraps>

80106189 <vector114>:
.globl vector114
vector114:
  pushl $0
80106189:	6a 00                	push   $0x0
  pushl $114
8010618b:	6a 72                	push   $0x72
  jmp alltraps
8010618d:	e9 28 f7 ff ff       	jmp    801058ba <alltraps>

80106192 <vector115>:
.globl vector115
vector115:
  pushl $0
80106192:	6a 00                	push   $0x0
  pushl $115
80106194:	6a 73                	push   $0x73
  jmp alltraps
80106196:	e9 1f f7 ff ff       	jmp    801058ba <alltraps>

8010619b <vector116>:
.globl vector116
vector116:
  pushl $0
8010619b:	6a 00                	push   $0x0
  pushl $116
8010619d:	6a 74                	push   $0x74
  jmp alltraps
8010619f:	e9 16 f7 ff ff       	jmp    801058ba <alltraps>

801061a4 <vector117>:
.globl vector117
vector117:
  pushl $0
801061a4:	6a 00                	push   $0x0
  pushl $117
801061a6:	6a 75                	push   $0x75
  jmp alltraps
801061a8:	e9 0d f7 ff ff       	jmp    801058ba <alltraps>

801061ad <vector118>:
.globl vector118
vector118:
  pushl $0
801061ad:	6a 00                	push   $0x0
  pushl $118
801061af:	6a 76                	push   $0x76
  jmp alltraps
801061b1:	e9 04 f7 ff ff       	jmp    801058ba <alltraps>

801061b6 <vector119>:
.globl vector119
vector119:
  pushl $0
801061b6:	6a 00                	push   $0x0
  pushl $119
801061b8:	6a 77                	push   $0x77
  jmp alltraps
801061ba:	e9 fb f6 ff ff       	jmp    801058ba <alltraps>

801061bf <vector120>:
.globl vector120
vector120:
  pushl $0
801061bf:	6a 00                	push   $0x0
  pushl $120
801061c1:	6a 78                	push   $0x78
  jmp alltraps
801061c3:	e9 f2 f6 ff ff       	jmp    801058ba <alltraps>

801061c8 <vector121>:
.globl vector121
vector121:
  pushl $0
801061c8:	6a 00                	push   $0x0
  pushl $121
801061ca:	6a 79                	push   $0x79
  jmp alltraps
801061cc:	e9 e9 f6 ff ff       	jmp    801058ba <alltraps>

801061d1 <vector122>:
.globl vector122
vector122:
  pushl $0
801061d1:	6a 00                	push   $0x0
  pushl $122
801061d3:	6a 7a                	push   $0x7a
  jmp alltraps
801061d5:	e9 e0 f6 ff ff       	jmp    801058ba <alltraps>

801061da <vector123>:
.globl vector123
vector123:
  pushl $0
801061da:	6a 00                	push   $0x0
  pushl $123
801061dc:	6a 7b                	push   $0x7b
  jmp alltraps
801061de:	e9 d7 f6 ff ff       	jmp    801058ba <alltraps>

801061e3 <vector124>:
.globl vector124
vector124:
  pushl $0
801061e3:	6a 00                	push   $0x0
  pushl $124
801061e5:	6a 7c                	push   $0x7c
  jmp alltraps
801061e7:	e9 ce f6 ff ff       	jmp    801058ba <alltraps>

801061ec <vector125>:
.globl vector125
vector125:
  pushl $0
801061ec:	6a 00                	push   $0x0
  pushl $125
801061ee:	6a 7d                	push   $0x7d
  jmp alltraps
801061f0:	e9 c5 f6 ff ff       	jmp    801058ba <alltraps>

801061f5 <vector126>:
.globl vector126
vector126:
  pushl $0
801061f5:	6a 00                	push   $0x0
  pushl $126
801061f7:	6a 7e                	push   $0x7e
  jmp alltraps
801061f9:	e9 bc f6 ff ff       	jmp    801058ba <alltraps>

801061fe <vector127>:
.globl vector127
vector127:
  pushl $0
801061fe:	6a 00                	push   $0x0
  pushl $127
80106200:	6a 7f                	push   $0x7f
  jmp alltraps
80106202:	e9 b3 f6 ff ff       	jmp    801058ba <alltraps>

80106207 <vector128>:
.globl vector128
vector128:
  pushl $0
80106207:	6a 00                	push   $0x0
  pushl $128
80106209:	68 80 00 00 00       	push   $0x80
  jmp alltraps
8010620e:	e9 a7 f6 ff ff       	jmp    801058ba <alltraps>

80106213 <vector129>:
.globl vector129
vector129:
  pushl $0
80106213:	6a 00                	push   $0x0
  pushl $129
80106215:	68 81 00 00 00       	push   $0x81
  jmp alltraps
8010621a:	e9 9b f6 ff ff       	jmp    801058ba <alltraps>

8010621f <vector130>:
.globl vector130
vector130:
  pushl $0
8010621f:	6a 00                	push   $0x0
  pushl $130
80106221:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80106226:	e9 8f f6 ff ff       	jmp    801058ba <alltraps>

8010622b <vector131>:
.globl vector131
vector131:
  pushl $0
8010622b:	6a 00                	push   $0x0
  pushl $131
8010622d:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80106232:	e9 83 f6 ff ff       	jmp    801058ba <alltraps>

80106237 <vector132>:
.globl vector132
vector132:
  pushl $0
80106237:	6a 00                	push   $0x0
  pushl $132
80106239:	68 84 00 00 00       	push   $0x84
  jmp alltraps
8010623e:	e9 77 f6 ff ff       	jmp    801058ba <alltraps>

80106243 <vector133>:
.globl vector133
vector133:
  pushl $0
80106243:	6a 00                	push   $0x0
  pushl $133
80106245:	68 85 00 00 00       	push   $0x85
  jmp alltraps
8010624a:	e9 6b f6 ff ff       	jmp    801058ba <alltraps>

8010624f <vector134>:
.globl vector134
vector134:
  pushl $0
8010624f:	6a 00                	push   $0x0
  pushl $134
80106251:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80106256:	e9 5f f6 ff ff       	jmp    801058ba <alltraps>

8010625b <vector135>:
.globl vector135
vector135:
  pushl $0
8010625b:	6a 00                	push   $0x0
  pushl $135
8010625d:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80106262:	e9 53 f6 ff ff       	jmp    801058ba <alltraps>

80106267 <vector136>:
.globl vector136
vector136:
  pushl $0
80106267:	6a 00                	push   $0x0
  pushl $136
80106269:	68 88 00 00 00       	push   $0x88
  jmp alltraps
8010626e:	e9 47 f6 ff ff       	jmp    801058ba <alltraps>

80106273 <vector137>:
.globl vector137
vector137:
  pushl $0
80106273:	6a 00                	push   $0x0
  pushl $137
80106275:	68 89 00 00 00       	push   $0x89
  jmp alltraps
8010627a:	e9 3b f6 ff ff       	jmp    801058ba <alltraps>

8010627f <vector138>:
.globl vector138
vector138:
  pushl $0
8010627f:	6a 00                	push   $0x0
  pushl $138
80106281:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80106286:	e9 2f f6 ff ff       	jmp    801058ba <alltraps>

8010628b <vector139>:
.globl vector139
vector139:
  pushl $0
8010628b:	6a 00                	push   $0x0
  pushl $139
8010628d:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80106292:	e9 23 f6 ff ff       	jmp    801058ba <alltraps>

80106297 <vector140>:
.globl vector140
vector140:
  pushl $0
80106297:	6a 00                	push   $0x0
  pushl $140
80106299:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
8010629e:	e9 17 f6 ff ff       	jmp    801058ba <alltraps>

801062a3 <vector141>:
.globl vector141
vector141:
  pushl $0
801062a3:	6a 00                	push   $0x0
  pushl $141
801062a5:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
801062aa:	e9 0b f6 ff ff       	jmp    801058ba <alltraps>

801062af <vector142>:
.globl vector142
vector142:
  pushl $0
801062af:	6a 00                	push   $0x0
  pushl $142
801062b1:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
801062b6:	e9 ff f5 ff ff       	jmp    801058ba <alltraps>

801062bb <vector143>:
.globl vector143
vector143:
  pushl $0
801062bb:	6a 00                	push   $0x0
  pushl $143
801062bd:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
801062c2:	e9 f3 f5 ff ff       	jmp    801058ba <alltraps>

801062c7 <vector144>:
.globl vector144
vector144:
  pushl $0
801062c7:	6a 00                	push   $0x0
  pushl $144
801062c9:	68 90 00 00 00       	push   $0x90
  jmp alltraps
801062ce:	e9 e7 f5 ff ff       	jmp    801058ba <alltraps>

801062d3 <vector145>:
.globl vector145
vector145:
  pushl $0
801062d3:	6a 00                	push   $0x0
  pushl $145
801062d5:	68 91 00 00 00       	push   $0x91
  jmp alltraps
801062da:	e9 db f5 ff ff       	jmp    801058ba <alltraps>

801062df <vector146>:
.globl vector146
vector146:
  pushl $0
801062df:	6a 00                	push   $0x0
  pushl $146
801062e1:	68 92 00 00 00       	push   $0x92
  jmp alltraps
801062e6:	e9 cf f5 ff ff       	jmp    801058ba <alltraps>

801062eb <vector147>:
.globl vector147
vector147:
  pushl $0
801062eb:	6a 00                	push   $0x0
  pushl $147
801062ed:	68 93 00 00 00       	push   $0x93
  jmp alltraps
801062f2:	e9 c3 f5 ff ff       	jmp    801058ba <alltraps>

801062f7 <vector148>:
.globl vector148
vector148:
  pushl $0
801062f7:	6a 00                	push   $0x0
  pushl $148
801062f9:	68 94 00 00 00       	push   $0x94
  jmp alltraps
801062fe:	e9 b7 f5 ff ff       	jmp    801058ba <alltraps>

80106303 <vector149>:
.globl vector149
vector149:
  pushl $0
80106303:	6a 00                	push   $0x0
  pushl $149
80106305:	68 95 00 00 00       	push   $0x95
  jmp alltraps
8010630a:	e9 ab f5 ff ff       	jmp    801058ba <alltraps>

8010630f <vector150>:
.globl vector150
vector150:
  pushl $0
8010630f:	6a 00                	push   $0x0
  pushl $150
80106311:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80106316:	e9 9f f5 ff ff       	jmp    801058ba <alltraps>

8010631b <vector151>:
.globl vector151
vector151:
  pushl $0
8010631b:	6a 00                	push   $0x0
  pushl $151
8010631d:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80106322:	e9 93 f5 ff ff       	jmp    801058ba <alltraps>

80106327 <vector152>:
.globl vector152
vector152:
  pushl $0
80106327:	6a 00                	push   $0x0
  pushl $152
80106329:	68 98 00 00 00       	push   $0x98
  jmp alltraps
8010632e:	e9 87 f5 ff ff       	jmp    801058ba <alltraps>

80106333 <vector153>:
.globl vector153
vector153:
  pushl $0
80106333:	6a 00                	push   $0x0
  pushl $153
80106335:	68 99 00 00 00       	push   $0x99
  jmp alltraps
8010633a:	e9 7b f5 ff ff       	jmp    801058ba <alltraps>

8010633f <vector154>:
.globl vector154
vector154:
  pushl $0
8010633f:	6a 00                	push   $0x0
  pushl $154
80106341:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80106346:	e9 6f f5 ff ff       	jmp    801058ba <alltraps>

8010634b <vector155>:
.globl vector155
vector155:
  pushl $0
8010634b:	6a 00                	push   $0x0
  pushl $155
8010634d:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80106352:	e9 63 f5 ff ff       	jmp    801058ba <alltraps>

80106357 <vector156>:
.globl vector156
vector156:
  pushl $0
80106357:	6a 00                	push   $0x0
  pushl $156
80106359:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
8010635e:	e9 57 f5 ff ff       	jmp    801058ba <alltraps>

80106363 <vector157>:
.globl vector157
vector157:
  pushl $0
80106363:	6a 00                	push   $0x0
  pushl $157
80106365:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
8010636a:	e9 4b f5 ff ff       	jmp    801058ba <alltraps>

8010636f <vector158>:
.globl vector158
vector158:
  pushl $0
8010636f:	6a 00                	push   $0x0
  pushl $158
80106371:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80106376:	e9 3f f5 ff ff       	jmp    801058ba <alltraps>

8010637b <vector159>:
.globl vector159
vector159:
  pushl $0
8010637b:	6a 00                	push   $0x0
  pushl $159
8010637d:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80106382:	e9 33 f5 ff ff       	jmp    801058ba <alltraps>

80106387 <vector160>:
.globl vector160
vector160:
  pushl $0
80106387:	6a 00                	push   $0x0
  pushl $160
80106389:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
8010638e:	e9 27 f5 ff ff       	jmp    801058ba <alltraps>

80106393 <vector161>:
.globl vector161
vector161:
  pushl $0
80106393:	6a 00                	push   $0x0
  pushl $161
80106395:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
8010639a:	e9 1b f5 ff ff       	jmp    801058ba <alltraps>

8010639f <vector162>:
.globl vector162
vector162:
  pushl $0
8010639f:	6a 00                	push   $0x0
  pushl $162
801063a1:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
801063a6:	e9 0f f5 ff ff       	jmp    801058ba <alltraps>

801063ab <vector163>:
.globl vector163
vector163:
  pushl $0
801063ab:	6a 00                	push   $0x0
  pushl $163
801063ad:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
801063b2:	e9 03 f5 ff ff       	jmp    801058ba <alltraps>

801063b7 <vector164>:
.globl vector164
vector164:
  pushl $0
801063b7:	6a 00                	push   $0x0
  pushl $164
801063b9:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
801063be:	e9 f7 f4 ff ff       	jmp    801058ba <alltraps>

801063c3 <vector165>:
.globl vector165
vector165:
  pushl $0
801063c3:	6a 00                	push   $0x0
  pushl $165
801063c5:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
801063ca:	e9 eb f4 ff ff       	jmp    801058ba <alltraps>

801063cf <vector166>:
.globl vector166
vector166:
  pushl $0
801063cf:	6a 00                	push   $0x0
  pushl $166
801063d1:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
801063d6:	e9 df f4 ff ff       	jmp    801058ba <alltraps>

801063db <vector167>:
.globl vector167
vector167:
  pushl $0
801063db:	6a 00                	push   $0x0
  pushl $167
801063dd:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
801063e2:	e9 d3 f4 ff ff       	jmp    801058ba <alltraps>

801063e7 <vector168>:
.globl vector168
vector168:
  pushl $0
801063e7:	6a 00                	push   $0x0
  pushl $168
801063e9:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
801063ee:	e9 c7 f4 ff ff       	jmp    801058ba <alltraps>

801063f3 <vector169>:
.globl vector169
vector169:
  pushl $0
801063f3:	6a 00                	push   $0x0
  pushl $169
801063f5:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
801063fa:	e9 bb f4 ff ff       	jmp    801058ba <alltraps>

801063ff <vector170>:
.globl vector170
vector170:
  pushl $0
801063ff:	6a 00                	push   $0x0
  pushl $170
80106401:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80106406:	e9 af f4 ff ff       	jmp    801058ba <alltraps>

8010640b <vector171>:
.globl vector171
vector171:
  pushl $0
8010640b:	6a 00                	push   $0x0
  pushl $171
8010640d:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80106412:	e9 a3 f4 ff ff       	jmp    801058ba <alltraps>

80106417 <vector172>:
.globl vector172
vector172:
  pushl $0
80106417:	6a 00                	push   $0x0
  pushl $172
80106419:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
8010641e:	e9 97 f4 ff ff       	jmp    801058ba <alltraps>

80106423 <vector173>:
.globl vector173
vector173:
  pushl $0
80106423:	6a 00                	push   $0x0
  pushl $173
80106425:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
8010642a:	e9 8b f4 ff ff       	jmp    801058ba <alltraps>

8010642f <vector174>:
.globl vector174
vector174:
  pushl $0
8010642f:	6a 00                	push   $0x0
  pushl $174
80106431:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80106436:	e9 7f f4 ff ff       	jmp    801058ba <alltraps>

8010643b <vector175>:
.globl vector175
vector175:
  pushl $0
8010643b:	6a 00                	push   $0x0
  pushl $175
8010643d:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80106442:	e9 73 f4 ff ff       	jmp    801058ba <alltraps>

80106447 <vector176>:
.globl vector176
vector176:
  pushl $0
80106447:	6a 00                	push   $0x0
  pushl $176
80106449:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
8010644e:	e9 67 f4 ff ff       	jmp    801058ba <alltraps>

80106453 <vector177>:
.globl vector177
vector177:
  pushl $0
80106453:	6a 00                	push   $0x0
  pushl $177
80106455:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
8010645a:	e9 5b f4 ff ff       	jmp    801058ba <alltraps>

8010645f <vector178>:
.globl vector178
vector178:
  pushl $0
8010645f:	6a 00                	push   $0x0
  pushl $178
80106461:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
80106466:	e9 4f f4 ff ff       	jmp    801058ba <alltraps>

8010646b <vector179>:
.globl vector179
vector179:
  pushl $0
8010646b:	6a 00                	push   $0x0
  pushl $179
8010646d:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80106472:	e9 43 f4 ff ff       	jmp    801058ba <alltraps>

80106477 <vector180>:
.globl vector180
vector180:
  pushl $0
80106477:	6a 00                	push   $0x0
  pushl $180
80106479:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
8010647e:	e9 37 f4 ff ff       	jmp    801058ba <alltraps>

80106483 <vector181>:
.globl vector181
vector181:
  pushl $0
80106483:	6a 00                	push   $0x0
  pushl $181
80106485:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
8010648a:	e9 2b f4 ff ff       	jmp    801058ba <alltraps>

8010648f <vector182>:
.globl vector182
vector182:
  pushl $0
8010648f:	6a 00                	push   $0x0
  pushl $182
80106491:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
80106496:	e9 1f f4 ff ff       	jmp    801058ba <alltraps>

8010649b <vector183>:
.globl vector183
vector183:
  pushl $0
8010649b:	6a 00                	push   $0x0
  pushl $183
8010649d:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
801064a2:	e9 13 f4 ff ff       	jmp    801058ba <alltraps>

801064a7 <vector184>:
.globl vector184
vector184:
  pushl $0
801064a7:	6a 00                	push   $0x0
  pushl $184
801064a9:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
801064ae:	e9 07 f4 ff ff       	jmp    801058ba <alltraps>

801064b3 <vector185>:
.globl vector185
vector185:
  pushl $0
801064b3:	6a 00                	push   $0x0
  pushl $185
801064b5:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
801064ba:	e9 fb f3 ff ff       	jmp    801058ba <alltraps>

801064bf <vector186>:
.globl vector186
vector186:
  pushl $0
801064bf:	6a 00                	push   $0x0
  pushl $186
801064c1:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
801064c6:	e9 ef f3 ff ff       	jmp    801058ba <alltraps>

801064cb <vector187>:
.globl vector187
vector187:
  pushl $0
801064cb:	6a 00                	push   $0x0
  pushl $187
801064cd:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
801064d2:	e9 e3 f3 ff ff       	jmp    801058ba <alltraps>

801064d7 <vector188>:
.globl vector188
vector188:
  pushl $0
801064d7:	6a 00                	push   $0x0
  pushl $188
801064d9:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
801064de:	e9 d7 f3 ff ff       	jmp    801058ba <alltraps>

801064e3 <vector189>:
.globl vector189
vector189:
  pushl $0
801064e3:	6a 00                	push   $0x0
  pushl $189
801064e5:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
801064ea:	e9 cb f3 ff ff       	jmp    801058ba <alltraps>

801064ef <vector190>:
.globl vector190
vector190:
  pushl $0
801064ef:	6a 00                	push   $0x0
  pushl $190
801064f1:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
801064f6:	e9 bf f3 ff ff       	jmp    801058ba <alltraps>

801064fb <vector191>:
.globl vector191
vector191:
  pushl $0
801064fb:	6a 00                	push   $0x0
  pushl $191
801064fd:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80106502:	e9 b3 f3 ff ff       	jmp    801058ba <alltraps>

80106507 <vector192>:
.globl vector192
vector192:
  pushl $0
80106507:	6a 00                	push   $0x0
  pushl $192
80106509:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
8010650e:	e9 a7 f3 ff ff       	jmp    801058ba <alltraps>

80106513 <vector193>:
.globl vector193
vector193:
  pushl $0
80106513:	6a 00                	push   $0x0
  pushl $193
80106515:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
8010651a:	e9 9b f3 ff ff       	jmp    801058ba <alltraps>

8010651f <vector194>:
.globl vector194
vector194:
  pushl $0
8010651f:	6a 00                	push   $0x0
  pushl $194
80106521:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80106526:	e9 8f f3 ff ff       	jmp    801058ba <alltraps>

8010652b <vector195>:
.globl vector195
vector195:
  pushl $0
8010652b:	6a 00                	push   $0x0
  pushl $195
8010652d:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80106532:	e9 83 f3 ff ff       	jmp    801058ba <alltraps>

80106537 <vector196>:
.globl vector196
vector196:
  pushl $0
80106537:	6a 00                	push   $0x0
  pushl $196
80106539:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
8010653e:	e9 77 f3 ff ff       	jmp    801058ba <alltraps>

80106543 <vector197>:
.globl vector197
vector197:
  pushl $0
80106543:	6a 00                	push   $0x0
  pushl $197
80106545:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
8010654a:	e9 6b f3 ff ff       	jmp    801058ba <alltraps>

8010654f <vector198>:
.globl vector198
vector198:
  pushl $0
8010654f:	6a 00                	push   $0x0
  pushl $198
80106551:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
80106556:	e9 5f f3 ff ff       	jmp    801058ba <alltraps>

8010655b <vector199>:
.globl vector199
vector199:
  pushl $0
8010655b:	6a 00                	push   $0x0
  pushl $199
8010655d:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80106562:	e9 53 f3 ff ff       	jmp    801058ba <alltraps>

80106567 <vector200>:
.globl vector200
vector200:
  pushl $0
80106567:	6a 00                	push   $0x0
  pushl $200
80106569:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
8010656e:	e9 47 f3 ff ff       	jmp    801058ba <alltraps>

80106573 <vector201>:
.globl vector201
vector201:
  pushl $0
80106573:	6a 00                	push   $0x0
  pushl $201
80106575:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
8010657a:	e9 3b f3 ff ff       	jmp    801058ba <alltraps>

8010657f <vector202>:
.globl vector202
vector202:
  pushl $0
8010657f:	6a 00                	push   $0x0
  pushl $202
80106581:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
80106586:	e9 2f f3 ff ff       	jmp    801058ba <alltraps>

8010658b <vector203>:
.globl vector203
vector203:
  pushl $0
8010658b:	6a 00                	push   $0x0
  pushl $203
8010658d:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80106592:	e9 23 f3 ff ff       	jmp    801058ba <alltraps>

80106597 <vector204>:
.globl vector204
vector204:
  pushl $0
80106597:	6a 00                	push   $0x0
  pushl $204
80106599:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
8010659e:	e9 17 f3 ff ff       	jmp    801058ba <alltraps>

801065a3 <vector205>:
.globl vector205
vector205:
  pushl $0
801065a3:	6a 00                	push   $0x0
  pushl $205
801065a5:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
801065aa:	e9 0b f3 ff ff       	jmp    801058ba <alltraps>

801065af <vector206>:
.globl vector206
vector206:
  pushl $0
801065af:	6a 00                	push   $0x0
  pushl $206
801065b1:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
801065b6:	e9 ff f2 ff ff       	jmp    801058ba <alltraps>

801065bb <vector207>:
.globl vector207
vector207:
  pushl $0
801065bb:	6a 00                	push   $0x0
  pushl $207
801065bd:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
801065c2:	e9 f3 f2 ff ff       	jmp    801058ba <alltraps>

801065c7 <vector208>:
.globl vector208
vector208:
  pushl $0
801065c7:	6a 00                	push   $0x0
  pushl $208
801065c9:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
801065ce:	e9 e7 f2 ff ff       	jmp    801058ba <alltraps>

801065d3 <vector209>:
.globl vector209
vector209:
  pushl $0
801065d3:	6a 00                	push   $0x0
  pushl $209
801065d5:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
801065da:	e9 db f2 ff ff       	jmp    801058ba <alltraps>

801065df <vector210>:
.globl vector210
vector210:
  pushl $0
801065df:	6a 00                	push   $0x0
  pushl $210
801065e1:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
801065e6:	e9 cf f2 ff ff       	jmp    801058ba <alltraps>

801065eb <vector211>:
.globl vector211
vector211:
  pushl $0
801065eb:	6a 00                	push   $0x0
  pushl $211
801065ed:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
801065f2:	e9 c3 f2 ff ff       	jmp    801058ba <alltraps>

801065f7 <vector212>:
.globl vector212
vector212:
  pushl $0
801065f7:	6a 00                	push   $0x0
  pushl $212
801065f9:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
801065fe:	e9 b7 f2 ff ff       	jmp    801058ba <alltraps>

80106603 <vector213>:
.globl vector213
vector213:
  pushl $0
80106603:	6a 00                	push   $0x0
  pushl $213
80106605:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
8010660a:	e9 ab f2 ff ff       	jmp    801058ba <alltraps>

8010660f <vector214>:
.globl vector214
vector214:
  pushl $0
8010660f:	6a 00                	push   $0x0
  pushl $214
80106611:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80106616:	e9 9f f2 ff ff       	jmp    801058ba <alltraps>

8010661b <vector215>:
.globl vector215
vector215:
  pushl $0
8010661b:	6a 00                	push   $0x0
  pushl $215
8010661d:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80106622:	e9 93 f2 ff ff       	jmp    801058ba <alltraps>

80106627 <vector216>:
.globl vector216
vector216:
  pushl $0
80106627:	6a 00                	push   $0x0
  pushl $216
80106629:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
8010662e:	e9 87 f2 ff ff       	jmp    801058ba <alltraps>

80106633 <vector217>:
.globl vector217
vector217:
  pushl $0
80106633:	6a 00                	push   $0x0
  pushl $217
80106635:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
8010663a:	e9 7b f2 ff ff       	jmp    801058ba <alltraps>

8010663f <vector218>:
.globl vector218
vector218:
  pushl $0
8010663f:	6a 00                	push   $0x0
  pushl $218
80106641:	68 da 00 00 00       	push   $0xda
  jmp alltraps
80106646:	e9 6f f2 ff ff       	jmp    801058ba <alltraps>

8010664b <vector219>:
.globl vector219
vector219:
  pushl $0
8010664b:	6a 00                	push   $0x0
  pushl $219
8010664d:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
80106652:	e9 63 f2 ff ff       	jmp    801058ba <alltraps>

80106657 <vector220>:
.globl vector220
vector220:
  pushl $0
80106657:	6a 00                	push   $0x0
  pushl $220
80106659:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
8010665e:	e9 57 f2 ff ff       	jmp    801058ba <alltraps>

80106663 <vector221>:
.globl vector221
vector221:
  pushl $0
80106663:	6a 00                	push   $0x0
  pushl $221
80106665:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
8010666a:	e9 4b f2 ff ff       	jmp    801058ba <alltraps>

8010666f <vector222>:
.globl vector222
vector222:
  pushl $0
8010666f:	6a 00                	push   $0x0
  pushl $222
80106671:	68 de 00 00 00       	push   $0xde
  jmp alltraps
80106676:	e9 3f f2 ff ff       	jmp    801058ba <alltraps>

8010667b <vector223>:
.globl vector223
vector223:
  pushl $0
8010667b:	6a 00                	push   $0x0
  pushl $223
8010667d:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80106682:	e9 33 f2 ff ff       	jmp    801058ba <alltraps>

80106687 <vector224>:
.globl vector224
vector224:
  pushl $0
80106687:	6a 00                	push   $0x0
  pushl $224
80106689:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
8010668e:	e9 27 f2 ff ff       	jmp    801058ba <alltraps>

80106693 <vector225>:
.globl vector225
vector225:
  pushl $0
80106693:	6a 00                	push   $0x0
  pushl $225
80106695:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
8010669a:	e9 1b f2 ff ff       	jmp    801058ba <alltraps>

8010669f <vector226>:
.globl vector226
vector226:
  pushl $0
8010669f:	6a 00                	push   $0x0
  pushl $226
801066a1:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
801066a6:	e9 0f f2 ff ff       	jmp    801058ba <alltraps>

801066ab <vector227>:
.globl vector227
vector227:
  pushl $0
801066ab:	6a 00                	push   $0x0
  pushl $227
801066ad:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
801066b2:	e9 03 f2 ff ff       	jmp    801058ba <alltraps>

801066b7 <vector228>:
.globl vector228
vector228:
  pushl $0
801066b7:	6a 00                	push   $0x0
  pushl $228
801066b9:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
801066be:	e9 f7 f1 ff ff       	jmp    801058ba <alltraps>

801066c3 <vector229>:
.globl vector229
vector229:
  pushl $0
801066c3:	6a 00                	push   $0x0
  pushl $229
801066c5:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
801066ca:	e9 eb f1 ff ff       	jmp    801058ba <alltraps>

801066cf <vector230>:
.globl vector230
vector230:
  pushl $0
801066cf:	6a 00                	push   $0x0
  pushl $230
801066d1:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
801066d6:	e9 df f1 ff ff       	jmp    801058ba <alltraps>

801066db <vector231>:
.globl vector231
vector231:
  pushl $0
801066db:	6a 00                	push   $0x0
  pushl $231
801066dd:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
801066e2:	e9 d3 f1 ff ff       	jmp    801058ba <alltraps>

801066e7 <vector232>:
.globl vector232
vector232:
  pushl $0
801066e7:	6a 00                	push   $0x0
  pushl $232
801066e9:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
801066ee:	e9 c7 f1 ff ff       	jmp    801058ba <alltraps>

801066f3 <vector233>:
.globl vector233
vector233:
  pushl $0
801066f3:	6a 00                	push   $0x0
  pushl $233
801066f5:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
801066fa:	e9 bb f1 ff ff       	jmp    801058ba <alltraps>

801066ff <vector234>:
.globl vector234
vector234:
  pushl $0
801066ff:	6a 00                	push   $0x0
  pushl $234
80106701:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
80106706:	e9 af f1 ff ff       	jmp    801058ba <alltraps>

8010670b <vector235>:
.globl vector235
vector235:
  pushl $0
8010670b:	6a 00                	push   $0x0
  pushl $235
8010670d:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80106712:	e9 a3 f1 ff ff       	jmp    801058ba <alltraps>

80106717 <vector236>:
.globl vector236
vector236:
  pushl $0
80106717:	6a 00                	push   $0x0
  pushl $236
80106719:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
8010671e:	e9 97 f1 ff ff       	jmp    801058ba <alltraps>

80106723 <vector237>:
.globl vector237
vector237:
  pushl $0
80106723:	6a 00                	push   $0x0
  pushl $237
80106725:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
8010672a:	e9 8b f1 ff ff       	jmp    801058ba <alltraps>

8010672f <vector238>:
.globl vector238
vector238:
  pushl $0
8010672f:	6a 00                	push   $0x0
  pushl $238
80106731:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
80106736:	e9 7f f1 ff ff       	jmp    801058ba <alltraps>

8010673b <vector239>:
.globl vector239
vector239:
  pushl $0
8010673b:	6a 00                	push   $0x0
  pushl $239
8010673d:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
80106742:	e9 73 f1 ff ff       	jmp    801058ba <alltraps>

80106747 <vector240>:
.globl vector240
vector240:
  pushl $0
80106747:	6a 00                	push   $0x0
  pushl $240
80106749:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
8010674e:	e9 67 f1 ff ff       	jmp    801058ba <alltraps>

80106753 <vector241>:
.globl vector241
vector241:
  pushl $0
80106753:	6a 00                	push   $0x0
  pushl $241
80106755:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
8010675a:	e9 5b f1 ff ff       	jmp    801058ba <alltraps>

8010675f <vector242>:
.globl vector242
vector242:
  pushl $0
8010675f:	6a 00                	push   $0x0
  pushl $242
80106761:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
80106766:	e9 4f f1 ff ff       	jmp    801058ba <alltraps>

8010676b <vector243>:
.globl vector243
vector243:
  pushl $0
8010676b:	6a 00                	push   $0x0
  pushl $243
8010676d:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80106772:	e9 43 f1 ff ff       	jmp    801058ba <alltraps>

80106777 <vector244>:
.globl vector244
vector244:
  pushl $0
80106777:	6a 00                	push   $0x0
  pushl $244
80106779:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
8010677e:	e9 37 f1 ff ff       	jmp    801058ba <alltraps>

80106783 <vector245>:
.globl vector245
vector245:
  pushl $0
80106783:	6a 00                	push   $0x0
  pushl $245
80106785:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
8010678a:	e9 2b f1 ff ff       	jmp    801058ba <alltraps>

8010678f <vector246>:
.globl vector246
vector246:
  pushl $0
8010678f:	6a 00                	push   $0x0
  pushl $246
80106791:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
80106796:	e9 1f f1 ff ff       	jmp    801058ba <alltraps>

8010679b <vector247>:
.globl vector247
vector247:
  pushl $0
8010679b:	6a 00                	push   $0x0
  pushl $247
8010679d:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
801067a2:	e9 13 f1 ff ff       	jmp    801058ba <alltraps>

801067a7 <vector248>:
.globl vector248
vector248:
  pushl $0
801067a7:	6a 00                	push   $0x0
  pushl $248
801067a9:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
801067ae:	e9 07 f1 ff ff       	jmp    801058ba <alltraps>

801067b3 <vector249>:
.globl vector249
vector249:
  pushl $0
801067b3:	6a 00                	push   $0x0
  pushl $249
801067b5:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
801067ba:	e9 fb f0 ff ff       	jmp    801058ba <alltraps>

801067bf <vector250>:
.globl vector250
vector250:
  pushl $0
801067bf:	6a 00                	push   $0x0
  pushl $250
801067c1:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
801067c6:	e9 ef f0 ff ff       	jmp    801058ba <alltraps>

801067cb <vector251>:
.globl vector251
vector251:
  pushl $0
801067cb:	6a 00                	push   $0x0
  pushl $251
801067cd:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
801067d2:	e9 e3 f0 ff ff       	jmp    801058ba <alltraps>

801067d7 <vector252>:
.globl vector252
vector252:
  pushl $0
801067d7:	6a 00                	push   $0x0
  pushl $252
801067d9:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
801067de:	e9 d7 f0 ff ff       	jmp    801058ba <alltraps>

801067e3 <vector253>:
.globl vector253
vector253:
  pushl $0
801067e3:	6a 00                	push   $0x0
  pushl $253
801067e5:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
801067ea:	e9 cb f0 ff ff       	jmp    801058ba <alltraps>

801067ef <vector254>:
.globl vector254
vector254:
  pushl $0
801067ef:	6a 00                	push   $0x0
  pushl $254
801067f1:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
801067f6:	e9 bf f0 ff ff       	jmp    801058ba <alltraps>

801067fb <vector255>:
.globl vector255
vector255:
  pushl $0
801067fb:	6a 00                	push   $0x0
  pushl $255
801067fd:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80106802:	e9 b3 f0 ff ff       	jmp    801058ba <alltraps>
80106807:	66 90                	xchg   %ax,%ax
80106809:	66 90                	xchg   %ax,%ax
8010680b:	66 90                	xchg   %ax,%ax
8010680d:	66 90                	xchg   %ax,%ax
8010680f:	90                   	nop

80106810 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
80106810:	55                   	push   %ebp
80106811:	89 e5                	mov    %esp,%ebp
80106813:	57                   	push   %edi
80106814:	56                   	push   %esi
80106815:	53                   	push   %ebx
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
80106816:	89 d3                	mov    %edx,%ebx
{
80106818:	89 d7                	mov    %edx,%edi
  pde = &pgdir[PDX(va)];
8010681a:	c1 eb 16             	shr    $0x16,%ebx
8010681d:	8d 34 98             	lea    (%eax,%ebx,4),%esi
{
80106820:	83 ec 0c             	sub    $0xc,%esp
  if(*pde & PTE_P){
80106823:	8b 06                	mov    (%esi),%eax
80106825:	a8 01                	test   $0x1,%al
80106827:	74 27                	je     80106850 <walkpgdir+0x40>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80106829:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010682e:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
80106834:	c1 ef 0a             	shr    $0xa,%edi
}
80106837:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return &pgtab[PTX(va)];
8010683a:	89 fa                	mov    %edi,%edx
8010683c:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
80106842:	8d 04 13             	lea    (%ebx,%edx,1),%eax
}
80106845:	5b                   	pop    %ebx
80106846:	5e                   	pop    %esi
80106847:	5f                   	pop    %edi
80106848:	5d                   	pop    %ebp
80106849:	c3                   	ret    
8010684a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
80106850:	85 c9                	test   %ecx,%ecx
80106852:	74 2c                	je     80106880 <walkpgdir+0x70>
80106854:	e8 f7 be ff ff       	call   80102750 <kalloc>
80106859:	85 c0                	test   %eax,%eax
8010685b:	89 c3                	mov    %eax,%ebx
8010685d:	74 21                	je     80106880 <walkpgdir+0x70>
    memset(pgtab, 0, PGSIZE);
8010685f:	83 ec 04             	sub    $0x4,%esp
80106862:	68 00 10 00 00       	push   $0x1000
80106867:	6a 00                	push   $0x0
80106869:	50                   	push   %eax
8010686a:	e8 71 de ff ff       	call   801046e0 <memset>
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
8010686f:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106875:	83 c4 10             	add    $0x10,%esp
80106878:	83 c8 07             	or     $0x7,%eax
8010687b:	89 06                	mov    %eax,(%esi)
8010687d:	eb b5                	jmp    80106834 <walkpgdir+0x24>
8010687f:	90                   	nop
}
80106880:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return 0;
80106883:	31 c0                	xor    %eax,%eax
}
80106885:	5b                   	pop    %ebx
80106886:	5e                   	pop    %esi
80106887:	5f                   	pop    %edi
80106888:	5d                   	pop    %ebp
80106889:	c3                   	ret    
8010688a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106890 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80106890:	55                   	push   %ebp
80106891:	89 e5                	mov    %esp,%ebp
80106893:	57                   	push   %edi
80106894:	56                   	push   %esi
80106895:	53                   	push   %ebx
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
80106896:	89 d3                	mov    %edx,%ebx
80106898:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
{
8010689e:	83 ec 1c             	sub    $0x1c,%esp
801068a1:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
801068a4:	8d 44 0a ff          	lea    -0x1(%edx,%ecx,1),%eax
801068a8:	8b 7d 08             	mov    0x8(%ebp),%edi
801068ab:	25 00 f0 ff ff       	and    $0xfffff000,%eax
801068b0:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
801068b3:	8b 45 0c             	mov    0xc(%ebp),%eax
801068b6:	29 df                	sub    %ebx,%edi
801068b8:	83 c8 01             	or     $0x1,%eax
801068bb:	89 45 dc             	mov    %eax,-0x24(%ebp)
801068be:	eb 15                	jmp    801068d5 <mappages+0x45>
    if(*pte & PTE_P)
801068c0:	f6 00 01             	testb  $0x1,(%eax)
801068c3:	75 45                	jne    8010690a <mappages+0x7a>
    *pte = pa | perm | PTE_P;
801068c5:	0b 75 dc             	or     -0x24(%ebp),%esi
    if(a == last)
801068c8:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
    *pte = pa | perm | PTE_P;
801068cb:	89 30                	mov    %esi,(%eax)
    if(a == last)
801068cd:	74 31                	je     80106900 <mappages+0x70>
      break;
    a += PGSIZE;
801068cf:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
801068d5:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801068d8:	b9 01 00 00 00       	mov    $0x1,%ecx
801068dd:	89 da                	mov    %ebx,%edx
801068df:	8d 34 3b             	lea    (%ebx,%edi,1),%esi
801068e2:	e8 29 ff ff ff       	call   80106810 <walkpgdir>
801068e7:	85 c0                	test   %eax,%eax
801068e9:	75 d5                	jne    801068c0 <mappages+0x30>
    pa += PGSIZE;
  }
  return 0;
}
801068eb:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
801068ee:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801068f3:	5b                   	pop    %ebx
801068f4:	5e                   	pop    %esi
801068f5:	5f                   	pop    %edi
801068f6:	5d                   	pop    %ebp
801068f7:	c3                   	ret    
801068f8:	90                   	nop
801068f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106900:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80106903:	31 c0                	xor    %eax,%eax
}
80106905:	5b                   	pop    %ebx
80106906:	5e                   	pop    %esi
80106907:	5f                   	pop    %edi
80106908:	5d                   	pop    %ebp
80106909:	c3                   	ret    
      panic("remap");
8010690a:	83 ec 0c             	sub    $0xc,%esp
8010690d:	68 08 7a 10 80       	push   $0x80107a08
80106912:	e8 79 9a ff ff       	call   80100390 <panic>
80106917:	89 f6                	mov    %esi,%esi
80106919:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106920 <deallocuvm.part.0>:
// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
80106920:	55                   	push   %ebp
80106921:	89 e5                	mov    %esp,%ebp
80106923:	57                   	push   %edi
80106924:	56                   	push   %esi
80106925:	53                   	push   %ebx
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
80106926:	8d 99 ff 0f 00 00    	lea    0xfff(%ecx),%ebx
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
8010692c:	89 c7                	mov    %eax,%edi
  a = PGROUNDUP(newsz);
8010692e:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
80106934:	83 ec 1c             	sub    $0x1c,%esp
80106937:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  for(; a  < oldsz; a += PGSIZE){
8010693a:	39 d3                	cmp    %edx,%ebx
8010693c:	73 66                	jae    801069a4 <deallocuvm.part.0+0x84>
8010693e:	89 d6                	mov    %edx,%esi
80106940:	eb 3d                	jmp    8010697f <deallocuvm.part.0+0x5f>
80106942:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
80106948:	8b 10                	mov    (%eax),%edx
8010694a:	f6 c2 01             	test   $0x1,%dl
8010694d:	74 26                	je     80106975 <deallocuvm.part.0+0x55>
      pa = PTE_ADDR(*pte);
      if(pa == 0)
8010694f:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
80106955:	74 58                	je     801069af <deallocuvm.part.0+0x8f>
        panic("kfree");
      char *v = P2V(pa);
      kfree(v);
80106957:	83 ec 0c             	sub    $0xc,%esp
      char *v = P2V(pa);
8010695a:	81 c2 00 00 00 80    	add    $0x80000000,%edx
80106960:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      kfree(v);
80106963:	52                   	push   %edx
80106964:	e8 37 bc ff ff       	call   801025a0 <kfree>
      *pte = 0;
80106969:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010696c:	83 c4 10             	add    $0x10,%esp
8010696f:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  for(; a  < oldsz; a += PGSIZE){
80106975:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010697b:	39 f3                	cmp    %esi,%ebx
8010697d:	73 25                	jae    801069a4 <deallocuvm.part.0+0x84>
    pte = walkpgdir(pgdir, (char*)a, 0);
8010697f:	31 c9                	xor    %ecx,%ecx
80106981:	89 da                	mov    %ebx,%edx
80106983:	89 f8                	mov    %edi,%eax
80106985:	e8 86 fe ff ff       	call   80106810 <walkpgdir>
    if(!pte)
8010698a:	85 c0                	test   %eax,%eax
8010698c:	75 ba                	jne    80106948 <deallocuvm.part.0+0x28>
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
8010698e:	81 e3 00 00 c0 ff    	and    $0xffc00000,%ebx
80106994:	81 c3 00 f0 3f 00    	add    $0x3ff000,%ebx
  for(; a  < oldsz; a += PGSIZE){
8010699a:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801069a0:	39 f3                	cmp    %esi,%ebx
801069a2:	72 db                	jb     8010697f <deallocuvm.part.0+0x5f>
    }
  }
  return newsz;
}
801069a4:	8b 45 e0             	mov    -0x20(%ebp),%eax
801069a7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801069aa:	5b                   	pop    %ebx
801069ab:	5e                   	pop    %esi
801069ac:	5f                   	pop    %edi
801069ad:	5d                   	pop    %ebp
801069ae:	c3                   	ret    
        panic("kfree");
801069af:	83 ec 0c             	sub    $0xc,%esp
801069b2:	68 a6 73 10 80       	push   $0x801073a6
801069b7:	e8 d4 99 ff ff       	call   80100390 <panic>
801069bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801069c0 <seginit>:
{
801069c0:	55                   	push   %ebp
801069c1:	89 e5                	mov    %esp,%ebp
801069c3:	83 ec 18             	sub    $0x18,%esp
  c = &cpus[cpuid()];
801069c6:	e8 85 d0 ff ff       	call   80103a50 <cpuid>
801069cb:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
  pd[0] = size-1;
801069d1:	ba 2f 00 00 00       	mov    $0x2f,%edx
801069d6:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
801069da:	c7 80 f8 27 11 80 ff 	movl   $0xffff,-0x7feed808(%eax)
801069e1:	ff 00 00 
801069e4:	c7 80 fc 27 11 80 00 	movl   $0xcf9a00,-0x7feed804(%eax)
801069eb:	9a cf 00 
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
801069ee:	c7 80 00 28 11 80 ff 	movl   $0xffff,-0x7feed800(%eax)
801069f5:	ff 00 00 
801069f8:	c7 80 04 28 11 80 00 	movl   $0xcf9200,-0x7feed7fc(%eax)
801069ff:	92 cf 00 
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
80106a02:	c7 80 08 28 11 80 ff 	movl   $0xffff,-0x7feed7f8(%eax)
80106a09:	ff 00 00 
80106a0c:	c7 80 0c 28 11 80 00 	movl   $0xcffa00,-0x7feed7f4(%eax)
80106a13:	fa cf 00 
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80106a16:	c7 80 10 28 11 80 ff 	movl   $0xffff,-0x7feed7f0(%eax)
80106a1d:	ff 00 00 
80106a20:	c7 80 14 28 11 80 00 	movl   $0xcff200,-0x7feed7ec(%eax)
80106a27:	f2 cf 00 
  lgdt(c->gdt, sizeof(c->gdt));
80106a2a:	05 f0 27 11 80       	add    $0x801127f0,%eax
  pd[1] = (uint)p;
80106a2f:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
80106a33:	c1 e8 10             	shr    $0x10,%eax
80106a36:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
  asm volatile("lgdt (%0)" : : "r" (pd));
80106a3a:	8d 45 f2             	lea    -0xe(%ebp),%eax
80106a3d:	0f 01 10             	lgdtl  (%eax)
}
80106a40:	c9                   	leave  
80106a41:	c3                   	ret    
80106a42:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a49:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106a50 <switchkvm>:
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106a50:	a1 a4 54 11 80       	mov    0x801154a4,%eax
{
80106a55:	55                   	push   %ebp
80106a56:	89 e5                	mov    %esp,%ebp
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106a58:	05 00 00 00 80       	add    $0x80000000,%eax
}

static inline void
lcr3(uint val)
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106a5d:	0f 22 d8             	mov    %eax,%cr3
}
80106a60:	5d                   	pop    %ebp
80106a61:	c3                   	ret    
80106a62:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a69:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106a70 <switchuvm>:
{
80106a70:	55                   	push   %ebp
80106a71:	89 e5                	mov    %esp,%ebp
80106a73:	57                   	push   %edi
80106a74:	56                   	push   %esi
80106a75:	53                   	push   %ebx
80106a76:	83 ec 1c             	sub    $0x1c,%esp
80106a79:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(p == 0)
80106a7c:	85 db                	test   %ebx,%ebx
80106a7e:	0f 84 cb 00 00 00    	je     80106b4f <switchuvm+0xdf>
  if(p->kstack == 0)
80106a84:	8b 43 08             	mov    0x8(%ebx),%eax
80106a87:	85 c0                	test   %eax,%eax
80106a89:	0f 84 da 00 00 00    	je     80106b69 <switchuvm+0xf9>
  if(p->pgdir == 0)
80106a8f:	8b 43 04             	mov    0x4(%ebx),%eax
80106a92:	85 c0                	test   %eax,%eax
80106a94:	0f 84 c2 00 00 00    	je     80106b5c <switchuvm+0xec>
  pushcli();
80106a9a:	e8 61 da ff ff       	call   80104500 <pushcli>
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106a9f:	e8 2c cf ff ff       	call   801039d0 <mycpu>
80106aa4:	89 c6                	mov    %eax,%esi
80106aa6:	e8 25 cf ff ff       	call   801039d0 <mycpu>
80106aab:	89 c7                	mov    %eax,%edi
80106aad:	e8 1e cf ff ff       	call   801039d0 <mycpu>
80106ab2:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106ab5:	83 c7 08             	add    $0x8,%edi
80106ab8:	e8 13 cf ff ff       	call   801039d0 <mycpu>
80106abd:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80106ac0:	83 c0 08             	add    $0x8,%eax
80106ac3:	ba 67 00 00 00       	mov    $0x67,%edx
80106ac8:	c1 e8 18             	shr    $0x18,%eax
80106acb:	66 89 96 98 00 00 00 	mov    %dx,0x98(%esi)
80106ad2:	66 89 be 9a 00 00 00 	mov    %di,0x9a(%esi)
80106ad9:	88 86 9f 00 00 00    	mov    %al,0x9f(%esi)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106adf:	bf ff ff ff ff       	mov    $0xffffffff,%edi
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106ae4:	83 c1 08             	add    $0x8,%ecx
80106ae7:	c1 e9 10             	shr    $0x10,%ecx
80106aea:	88 8e 9c 00 00 00    	mov    %cl,0x9c(%esi)
80106af0:	b9 99 40 00 00       	mov    $0x4099,%ecx
80106af5:	66 89 8e 9d 00 00 00 	mov    %cx,0x9d(%esi)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106afc:	be 10 00 00 00       	mov    $0x10,%esi
  mycpu()->gdt[SEG_TSS].s = 0;
80106b01:	e8 ca ce ff ff       	call   801039d0 <mycpu>
80106b06:	80 a0 9d 00 00 00 ef 	andb   $0xef,0x9d(%eax)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106b0d:	e8 be ce ff ff       	call   801039d0 <mycpu>
80106b12:	66 89 70 10          	mov    %si,0x10(%eax)
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
80106b16:	8b 73 08             	mov    0x8(%ebx),%esi
80106b19:	e8 b2 ce ff ff       	call   801039d0 <mycpu>
80106b1e:	81 c6 00 10 00 00    	add    $0x1000,%esi
80106b24:	89 70 0c             	mov    %esi,0xc(%eax)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106b27:	e8 a4 ce ff ff       	call   801039d0 <mycpu>
80106b2c:	66 89 78 6e          	mov    %di,0x6e(%eax)
  asm volatile("ltr %0" : : "r" (sel));
80106b30:	b8 28 00 00 00       	mov    $0x28,%eax
80106b35:	0f 00 d8             	ltr    %ax
  lcr3(V2P(p->pgdir));  // switch to process's address space
80106b38:	8b 43 04             	mov    0x4(%ebx),%eax
80106b3b:	05 00 00 00 80       	add    $0x80000000,%eax
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106b40:	0f 22 d8             	mov    %eax,%cr3
}
80106b43:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106b46:	5b                   	pop    %ebx
80106b47:	5e                   	pop    %esi
80106b48:	5f                   	pop    %edi
80106b49:	5d                   	pop    %ebp
  popcli();
80106b4a:	e9 f1 d9 ff ff       	jmp    80104540 <popcli>
    panic("switchuvm: no process");
80106b4f:	83 ec 0c             	sub    $0xc,%esp
80106b52:	68 0e 7a 10 80       	push   $0x80107a0e
80106b57:	e8 34 98 ff ff       	call   80100390 <panic>
    panic("switchuvm: no pgdir");
80106b5c:	83 ec 0c             	sub    $0xc,%esp
80106b5f:	68 39 7a 10 80       	push   $0x80107a39
80106b64:	e8 27 98 ff ff       	call   80100390 <panic>
    panic("switchuvm: no kstack");
80106b69:	83 ec 0c             	sub    $0xc,%esp
80106b6c:	68 24 7a 10 80       	push   $0x80107a24
80106b71:	e8 1a 98 ff ff       	call   80100390 <panic>
80106b76:	8d 76 00             	lea    0x0(%esi),%esi
80106b79:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106b80 <inituvm>:
{
80106b80:	55                   	push   %ebp
80106b81:	89 e5                	mov    %esp,%ebp
80106b83:	57                   	push   %edi
80106b84:	56                   	push   %esi
80106b85:	53                   	push   %ebx
80106b86:	83 ec 1c             	sub    $0x1c,%esp
80106b89:	8b 75 10             	mov    0x10(%ebp),%esi
80106b8c:	8b 45 08             	mov    0x8(%ebp),%eax
80106b8f:	8b 7d 0c             	mov    0xc(%ebp),%edi
  if(sz >= PGSIZE)
80106b92:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
{
80106b98:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(sz >= PGSIZE)
80106b9b:	77 49                	ja     80106be6 <inituvm+0x66>
  mem = kalloc();
80106b9d:	e8 ae bb ff ff       	call   80102750 <kalloc>
  memset(mem, 0, PGSIZE);
80106ba2:	83 ec 04             	sub    $0x4,%esp
  mem = kalloc();
80106ba5:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
80106ba7:	68 00 10 00 00       	push   $0x1000
80106bac:	6a 00                	push   $0x0
80106bae:	50                   	push   %eax
80106baf:	e8 2c db ff ff       	call   801046e0 <memset>
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
80106bb4:	58                   	pop    %eax
80106bb5:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106bbb:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106bc0:	5a                   	pop    %edx
80106bc1:	6a 06                	push   $0x6
80106bc3:	50                   	push   %eax
80106bc4:	31 d2                	xor    %edx,%edx
80106bc6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106bc9:	e8 c2 fc ff ff       	call   80106890 <mappages>
  memmove(mem, init, sz);
80106bce:	89 75 10             	mov    %esi,0x10(%ebp)
80106bd1:	89 7d 0c             	mov    %edi,0xc(%ebp)
80106bd4:	83 c4 10             	add    $0x10,%esp
80106bd7:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
80106bda:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106bdd:	5b                   	pop    %ebx
80106bde:	5e                   	pop    %esi
80106bdf:	5f                   	pop    %edi
80106be0:	5d                   	pop    %ebp
  memmove(mem, init, sz);
80106be1:	e9 aa db ff ff       	jmp    80104790 <memmove>
    panic("inituvm: more than a page");
80106be6:	83 ec 0c             	sub    $0xc,%esp
80106be9:	68 4d 7a 10 80       	push   $0x80107a4d
80106bee:	e8 9d 97 ff ff       	call   80100390 <panic>
80106bf3:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106bf9:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106c00 <loaduvm>:
{
80106c00:	55                   	push   %ebp
80106c01:	89 e5                	mov    %esp,%ebp
80106c03:	57                   	push   %edi
80106c04:	56                   	push   %esi
80106c05:	53                   	push   %ebx
80106c06:	83 ec 0c             	sub    $0xc,%esp
  if((uint) addr % PGSIZE != 0)
80106c09:	f7 45 0c ff 0f 00 00 	testl  $0xfff,0xc(%ebp)
80106c10:	0f 85 91 00 00 00    	jne    80106ca7 <loaduvm+0xa7>
  for(i = 0; i < sz; i += PGSIZE){
80106c16:	8b 75 18             	mov    0x18(%ebp),%esi
80106c19:	31 db                	xor    %ebx,%ebx
80106c1b:	85 f6                	test   %esi,%esi
80106c1d:	75 1a                	jne    80106c39 <loaduvm+0x39>
80106c1f:	eb 6f                	jmp    80106c90 <loaduvm+0x90>
80106c21:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106c28:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106c2e:	81 ee 00 10 00 00    	sub    $0x1000,%esi
80106c34:	39 5d 18             	cmp    %ebx,0x18(%ebp)
80106c37:	76 57                	jbe    80106c90 <loaduvm+0x90>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80106c39:	8b 55 0c             	mov    0xc(%ebp),%edx
80106c3c:	8b 45 08             	mov    0x8(%ebp),%eax
80106c3f:	31 c9                	xor    %ecx,%ecx
80106c41:	01 da                	add    %ebx,%edx
80106c43:	e8 c8 fb ff ff       	call   80106810 <walkpgdir>
80106c48:	85 c0                	test   %eax,%eax
80106c4a:	74 4e                	je     80106c9a <loaduvm+0x9a>
    pa = PTE_ADDR(*pte);
80106c4c:	8b 00                	mov    (%eax),%eax
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106c4e:	8b 4d 14             	mov    0x14(%ebp),%ecx
    if(sz - i < PGSIZE)
80106c51:	bf 00 10 00 00       	mov    $0x1000,%edi
    pa = PTE_ADDR(*pte);
80106c56:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if(sz - i < PGSIZE)
80106c5b:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106c61:	0f 46 fe             	cmovbe %esi,%edi
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106c64:	01 d9                	add    %ebx,%ecx
80106c66:	05 00 00 00 80       	add    $0x80000000,%eax
80106c6b:	57                   	push   %edi
80106c6c:	51                   	push   %ecx
80106c6d:	50                   	push   %eax
80106c6e:	ff 75 10             	pushl  0x10(%ebp)
80106c71:	e8 7a af ff ff       	call   80101bf0 <readi>
80106c76:	83 c4 10             	add    $0x10,%esp
80106c79:	39 f8                	cmp    %edi,%eax
80106c7b:	74 ab                	je     80106c28 <loaduvm+0x28>
}
80106c7d:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80106c80:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106c85:	5b                   	pop    %ebx
80106c86:	5e                   	pop    %esi
80106c87:	5f                   	pop    %edi
80106c88:	5d                   	pop    %ebp
80106c89:	c3                   	ret    
80106c8a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106c90:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80106c93:	31 c0                	xor    %eax,%eax
}
80106c95:	5b                   	pop    %ebx
80106c96:	5e                   	pop    %esi
80106c97:	5f                   	pop    %edi
80106c98:	5d                   	pop    %ebp
80106c99:	c3                   	ret    
      panic("loaduvm: address should exist");
80106c9a:	83 ec 0c             	sub    $0xc,%esp
80106c9d:	68 67 7a 10 80       	push   $0x80107a67
80106ca2:	e8 e9 96 ff ff       	call   80100390 <panic>
    panic("loaduvm: addr must be page aligned");
80106ca7:	83 ec 0c             	sub    $0xc,%esp
80106caa:	68 08 7b 10 80       	push   $0x80107b08
80106caf:	e8 dc 96 ff ff       	call   80100390 <panic>
80106cb4:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106cba:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80106cc0 <allocuvm>:
{
80106cc0:	55                   	push   %ebp
80106cc1:	89 e5                	mov    %esp,%ebp
80106cc3:	57                   	push   %edi
80106cc4:	56                   	push   %esi
80106cc5:	53                   	push   %ebx
80106cc6:	83 ec 1c             	sub    $0x1c,%esp
  if(newsz >= KERNBASE)
80106cc9:	8b 7d 10             	mov    0x10(%ebp),%edi
80106ccc:	85 ff                	test   %edi,%edi
80106cce:	0f 88 8e 00 00 00    	js     80106d62 <allocuvm+0xa2>
  if(newsz < oldsz)
80106cd4:	3b 7d 0c             	cmp    0xc(%ebp),%edi
80106cd7:	0f 82 93 00 00 00    	jb     80106d70 <allocuvm+0xb0>
  a = PGROUNDUP(oldsz);
80106cdd:	8b 45 0c             	mov    0xc(%ebp),%eax
80106ce0:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80106ce6:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; a < newsz; a += PGSIZE){
80106cec:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80106cef:	0f 86 7e 00 00 00    	jbe    80106d73 <allocuvm+0xb3>
80106cf5:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80106cf8:	8b 7d 08             	mov    0x8(%ebp),%edi
80106cfb:	eb 42                	jmp    80106d3f <allocuvm+0x7f>
80106cfd:	8d 76 00             	lea    0x0(%esi),%esi
    memset(mem, 0, PGSIZE);
80106d00:	83 ec 04             	sub    $0x4,%esp
80106d03:	68 00 10 00 00       	push   $0x1000
80106d08:	6a 00                	push   $0x0
80106d0a:	50                   	push   %eax
80106d0b:	e8 d0 d9 ff ff       	call   801046e0 <memset>
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
80106d10:	58                   	pop    %eax
80106d11:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
80106d17:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106d1c:	5a                   	pop    %edx
80106d1d:	6a 06                	push   $0x6
80106d1f:	50                   	push   %eax
80106d20:	89 da                	mov    %ebx,%edx
80106d22:	89 f8                	mov    %edi,%eax
80106d24:	e8 67 fb ff ff       	call   80106890 <mappages>
80106d29:	83 c4 10             	add    $0x10,%esp
80106d2c:	85 c0                	test   %eax,%eax
80106d2e:	78 50                	js     80106d80 <allocuvm+0xc0>
  for(; a < newsz; a += PGSIZE){
80106d30:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106d36:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80106d39:	0f 86 81 00 00 00    	jbe    80106dc0 <allocuvm+0x100>
    mem = kalloc();
80106d3f:	e8 0c ba ff ff       	call   80102750 <kalloc>
    if(mem == 0){
80106d44:	85 c0                	test   %eax,%eax
    mem = kalloc();
80106d46:	89 c6                	mov    %eax,%esi
    if(mem == 0){
80106d48:	75 b6                	jne    80106d00 <allocuvm+0x40>
      cprintf("allocuvm out of memory\n");
80106d4a:	83 ec 0c             	sub    $0xc,%esp
80106d4d:	68 85 7a 10 80       	push   $0x80107a85
80106d52:	e8 19 9a ff ff       	call   80100770 <cprintf>
  if(newsz >= oldsz)
80106d57:	83 c4 10             	add    $0x10,%esp
80106d5a:	8b 45 0c             	mov    0xc(%ebp),%eax
80106d5d:	39 45 10             	cmp    %eax,0x10(%ebp)
80106d60:	77 6e                	ja     80106dd0 <allocuvm+0x110>
}
80106d62:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return 0;
80106d65:	31 ff                	xor    %edi,%edi
}
80106d67:	89 f8                	mov    %edi,%eax
80106d69:	5b                   	pop    %ebx
80106d6a:	5e                   	pop    %esi
80106d6b:	5f                   	pop    %edi
80106d6c:	5d                   	pop    %ebp
80106d6d:	c3                   	ret    
80106d6e:	66 90                	xchg   %ax,%ax
    return oldsz;
80106d70:	8b 7d 0c             	mov    0xc(%ebp),%edi
}
80106d73:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106d76:	89 f8                	mov    %edi,%eax
80106d78:	5b                   	pop    %ebx
80106d79:	5e                   	pop    %esi
80106d7a:	5f                   	pop    %edi
80106d7b:	5d                   	pop    %ebp
80106d7c:	c3                   	ret    
80106d7d:	8d 76 00             	lea    0x0(%esi),%esi
      cprintf("allocuvm out of memory (2)\n");
80106d80:	83 ec 0c             	sub    $0xc,%esp
80106d83:	68 9d 7a 10 80       	push   $0x80107a9d
80106d88:	e8 e3 99 ff ff       	call   80100770 <cprintf>
  if(newsz >= oldsz)
80106d8d:	83 c4 10             	add    $0x10,%esp
80106d90:	8b 45 0c             	mov    0xc(%ebp),%eax
80106d93:	39 45 10             	cmp    %eax,0x10(%ebp)
80106d96:	76 0d                	jbe    80106da5 <allocuvm+0xe5>
80106d98:	89 c1                	mov    %eax,%ecx
80106d9a:	8b 55 10             	mov    0x10(%ebp),%edx
80106d9d:	8b 45 08             	mov    0x8(%ebp),%eax
80106da0:	e8 7b fb ff ff       	call   80106920 <deallocuvm.part.0>
      kfree(mem);
80106da5:	83 ec 0c             	sub    $0xc,%esp
      return 0;
80106da8:	31 ff                	xor    %edi,%edi
      kfree(mem);
80106daa:	56                   	push   %esi
80106dab:	e8 f0 b7 ff ff       	call   801025a0 <kfree>
      return 0;
80106db0:	83 c4 10             	add    $0x10,%esp
}
80106db3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106db6:	89 f8                	mov    %edi,%eax
80106db8:	5b                   	pop    %ebx
80106db9:	5e                   	pop    %esi
80106dba:	5f                   	pop    %edi
80106dbb:	5d                   	pop    %ebp
80106dbc:	c3                   	ret    
80106dbd:	8d 76 00             	lea    0x0(%esi),%esi
80106dc0:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80106dc3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106dc6:	5b                   	pop    %ebx
80106dc7:	89 f8                	mov    %edi,%eax
80106dc9:	5e                   	pop    %esi
80106dca:	5f                   	pop    %edi
80106dcb:	5d                   	pop    %ebp
80106dcc:	c3                   	ret    
80106dcd:	8d 76 00             	lea    0x0(%esi),%esi
80106dd0:	89 c1                	mov    %eax,%ecx
80106dd2:	8b 55 10             	mov    0x10(%ebp),%edx
80106dd5:	8b 45 08             	mov    0x8(%ebp),%eax
      return 0;
80106dd8:	31 ff                	xor    %edi,%edi
80106dda:	e8 41 fb ff ff       	call   80106920 <deallocuvm.part.0>
80106ddf:	eb 92                	jmp    80106d73 <allocuvm+0xb3>
80106de1:	eb 0d                	jmp    80106df0 <deallocuvm>
80106de3:	90                   	nop
80106de4:	90                   	nop
80106de5:	90                   	nop
80106de6:	90                   	nop
80106de7:	90                   	nop
80106de8:	90                   	nop
80106de9:	90                   	nop
80106dea:	90                   	nop
80106deb:	90                   	nop
80106dec:	90                   	nop
80106ded:	90                   	nop
80106dee:	90                   	nop
80106def:	90                   	nop

80106df0 <deallocuvm>:
{
80106df0:	55                   	push   %ebp
80106df1:	89 e5                	mov    %esp,%ebp
80106df3:	8b 55 0c             	mov    0xc(%ebp),%edx
80106df6:	8b 4d 10             	mov    0x10(%ebp),%ecx
80106df9:	8b 45 08             	mov    0x8(%ebp),%eax
  if(newsz >= oldsz)
80106dfc:	39 d1                	cmp    %edx,%ecx
80106dfe:	73 10                	jae    80106e10 <deallocuvm+0x20>
}
80106e00:	5d                   	pop    %ebp
80106e01:	e9 1a fb ff ff       	jmp    80106920 <deallocuvm.part.0>
80106e06:	8d 76 00             	lea    0x0(%esi),%esi
80106e09:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi
80106e10:	89 d0                	mov    %edx,%eax
80106e12:	5d                   	pop    %ebp
80106e13:	c3                   	ret    
80106e14:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106e1a:	8d bf 00 00 00 00    	lea    0x0(%edi),%edi

80106e20 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80106e20:	55                   	push   %ebp
80106e21:	89 e5                	mov    %esp,%ebp
80106e23:	57                   	push   %edi
80106e24:	56                   	push   %esi
80106e25:	53                   	push   %ebx
80106e26:	83 ec 0c             	sub    $0xc,%esp
80106e29:	8b 75 08             	mov    0x8(%ebp),%esi
  uint i;

  if(pgdir == 0)
80106e2c:	85 f6                	test   %esi,%esi
80106e2e:	74 59                	je     80106e89 <freevm+0x69>
80106e30:	31 c9                	xor    %ecx,%ecx
80106e32:	ba 00 00 00 80       	mov    $0x80000000,%edx
80106e37:	89 f0                	mov    %esi,%eax
80106e39:	e8 e2 fa ff ff       	call   80106920 <deallocuvm.part.0>
80106e3e:	89 f3                	mov    %esi,%ebx
80106e40:	8d be 00 10 00 00    	lea    0x1000(%esi),%edi
80106e46:	eb 0f                	jmp    80106e57 <freevm+0x37>
80106e48:	90                   	nop
80106e49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106e50:	83 c3 04             	add    $0x4,%ebx
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80106e53:	39 fb                	cmp    %edi,%ebx
80106e55:	74 23                	je     80106e7a <freevm+0x5a>
    if(pgdir[i] & PTE_P){
80106e57:	8b 03                	mov    (%ebx),%eax
80106e59:	a8 01                	test   $0x1,%al
80106e5b:	74 f3                	je     80106e50 <freevm+0x30>
      char * v = P2V(PTE_ADDR(pgdir[i]));
80106e5d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
      kfree(v);
80106e62:	83 ec 0c             	sub    $0xc,%esp
80106e65:	83 c3 04             	add    $0x4,%ebx
      char * v = P2V(PTE_ADDR(pgdir[i]));
80106e68:	05 00 00 00 80       	add    $0x80000000,%eax
      kfree(v);
80106e6d:	50                   	push   %eax
80106e6e:	e8 2d b7 ff ff       	call   801025a0 <kfree>
80106e73:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80106e76:	39 fb                	cmp    %edi,%ebx
80106e78:	75 dd                	jne    80106e57 <freevm+0x37>
    }
  }
  kfree((char*)pgdir);
80106e7a:	89 75 08             	mov    %esi,0x8(%ebp)
}
80106e7d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106e80:	5b                   	pop    %ebx
80106e81:	5e                   	pop    %esi
80106e82:	5f                   	pop    %edi
80106e83:	5d                   	pop    %ebp
  kfree((char*)pgdir);
80106e84:	e9 17 b7 ff ff       	jmp    801025a0 <kfree>
    panic("freevm: no pgdir");
80106e89:	83 ec 0c             	sub    $0xc,%esp
80106e8c:	68 b9 7a 10 80       	push   $0x80107ab9
80106e91:	e8 fa 94 ff ff       	call   80100390 <panic>
80106e96:	8d 76 00             	lea    0x0(%esi),%esi
80106e99:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106ea0 <setupkvm>:
{
80106ea0:	55                   	push   %ebp
80106ea1:	89 e5                	mov    %esp,%ebp
80106ea3:	56                   	push   %esi
80106ea4:	53                   	push   %ebx
  if((pgdir = (pde_t*)kalloc()) == 0)
80106ea5:	e8 a6 b8 ff ff       	call   80102750 <kalloc>
80106eaa:	85 c0                	test   %eax,%eax
80106eac:	89 c6                	mov    %eax,%esi
80106eae:	74 42                	je     80106ef2 <setupkvm+0x52>
  memset(pgdir, 0, PGSIZE);
80106eb0:	83 ec 04             	sub    $0x4,%esp
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80106eb3:	bb 20 a4 10 80       	mov    $0x8010a420,%ebx
  memset(pgdir, 0, PGSIZE);
80106eb8:	68 00 10 00 00       	push   $0x1000
80106ebd:	6a 00                	push   $0x0
80106ebf:	50                   	push   %eax
80106ec0:	e8 1b d8 ff ff       	call   801046e0 <memset>
80106ec5:	83 c4 10             	add    $0x10,%esp
                (uint)k->phys_start, k->perm) < 0) {
80106ec8:	8b 43 04             	mov    0x4(%ebx),%eax
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
80106ecb:	8b 4b 08             	mov    0x8(%ebx),%ecx
80106ece:	83 ec 08             	sub    $0x8,%esp
80106ed1:	8b 13                	mov    (%ebx),%edx
80106ed3:	ff 73 0c             	pushl  0xc(%ebx)
80106ed6:	50                   	push   %eax
80106ed7:	29 c1                	sub    %eax,%ecx
80106ed9:	89 f0                	mov    %esi,%eax
80106edb:	e8 b0 f9 ff ff       	call   80106890 <mappages>
80106ee0:	83 c4 10             	add    $0x10,%esp
80106ee3:	85 c0                	test   %eax,%eax
80106ee5:	78 19                	js     80106f00 <setupkvm+0x60>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80106ee7:	83 c3 10             	add    $0x10,%ebx
80106eea:	81 fb 60 a4 10 80    	cmp    $0x8010a460,%ebx
80106ef0:	75 d6                	jne    80106ec8 <setupkvm+0x28>
}
80106ef2:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106ef5:	89 f0                	mov    %esi,%eax
80106ef7:	5b                   	pop    %ebx
80106ef8:	5e                   	pop    %esi
80106ef9:	5d                   	pop    %ebp
80106efa:	c3                   	ret    
80106efb:	90                   	nop
80106efc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      freevm(pgdir);
80106f00:	83 ec 0c             	sub    $0xc,%esp
80106f03:	56                   	push   %esi
      return 0;
80106f04:	31 f6                	xor    %esi,%esi
      freevm(pgdir);
80106f06:	e8 15 ff ff ff       	call   80106e20 <freevm>
      return 0;
80106f0b:	83 c4 10             	add    $0x10,%esp
}
80106f0e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106f11:	89 f0                	mov    %esi,%eax
80106f13:	5b                   	pop    %ebx
80106f14:	5e                   	pop    %esi
80106f15:	5d                   	pop    %ebp
80106f16:	c3                   	ret    
80106f17:	89 f6                	mov    %esi,%esi
80106f19:	8d bc 27 00 00 00 00 	lea    0x0(%edi,%eiz,1),%edi

80106f20 <kvmalloc>:
{
80106f20:	55                   	push   %ebp
80106f21:	89 e5                	mov    %esp,%ebp
80106f23:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
80106f26:	e8 75 ff ff ff       	call   80106ea0 <setupkvm>
80106f2b:	a3 a4 54 11 80       	mov    %eax,0x801154a4
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106f30:	05 00 00 00 80       	add    $0x80000000,%eax
80106f35:	0f 22 d8             	mov    %eax,%cr3
}
80106f38:	c9                   	leave  
80106f39:	c3                   	ret    
80106f3a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106f40 <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80106f40:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80106f41:	31 c9                	xor    %ecx,%ecx
{
80106f43:	89 e5                	mov    %esp,%ebp
80106f45:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
80106f48:	8b 55 0c             	mov    0xc(%ebp),%edx
80106f4b:	8b 45 08             	mov    0x8(%ebp),%eax
80106f4e:	e8 bd f8 ff ff       	call   80106810 <walkpgdir>
  if(pte == 0)
80106f53:	85 c0                	test   %eax,%eax
80106f55:	74 05                	je     80106f5c <clearpteu+0x1c>
    panic("clearpteu");
  *pte &= ~PTE_U;
80106f57:	83 20 fb             	andl   $0xfffffffb,(%eax)
}
80106f5a:	c9                   	leave  
80106f5b:	c3                   	ret    
    panic("clearpteu");
80106f5c:	83 ec 0c             	sub    $0xc,%esp
80106f5f:	68 ca 7a 10 80       	push   $0x80107aca
80106f64:	e8 27 94 ff ff       	call   80100390 <panic>
80106f69:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106f70 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80106f70:	55                   	push   %ebp
80106f71:	89 e5                	mov    %esp,%ebp
80106f73:	57                   	push   %edi
80106f74:	56                   	push   %esi
80106f75:	53                   	push   %ebx
80106f76:	83 ec 1c             	sub    $0x1c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80106f79:	e8 22 ff ff ff       	call   80106ea0 <setupkvm>
80106f7e:	85 c0                	test   %eax,%eax
80106f80:	89 45 e0             	mov    %eax,-0x20(%ebp)
80106f83:	0f 84 9f 00 00 00    	je     80107028 <copyuvm+0xb8>
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
80106f89:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80106f8c:	85 c9                	test   %ecx,%ecx
80106f8e:	0f 84 94 00 00 00    	je     80107028 <copyuvm+0xb8>
80106f94:	31 ff                	xor    %edi,%edi
80106f96:	eb 4a                	jmp    80106fe2 <copyuvm+0x72>
80106f98:	90                   	nop
80106f99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
80106fa0:	83 ec 04             	sub    $0x4,%esp
80106fa3:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
80106fa9:	68 00 10 00 00       	push   $0x1000
80106fae:	53                   	push   %ebx
80106faf:	50                   	push   %eax
80106fb0:	e8 db d7 ff ff       	call   80104790 <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0) {
80106fb5:	58                   	pop    %eax
80106fb6:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
80106fbc:	b9 00 10 00 00       	mov    $0x1000,%ecx
80106fc1:	5a                   	pop    %edx
80106fc2:	ff 75 e4             	pushl  -0x1c(%ebp)
80106fc5:	50                   	push   %eax
80106fc6:	89 fa                	mov    %edi,%edx
80106fc8:	8b 45 e0             	mov    -0x20(%ebp),%eax
80106fcb:	e8 c0 f8 ff ff       	call   80106890 <mappages>
80106fd0:	83 c4 10             	add    $0x10,%esp
80106fd3:	85 c0                	test   %eax,%eax
80106fd5:	78 61                	js     80107038 <copyuvm+0xc8>
  for(i = 0; i < sz; i += PGSIZE){
80106fd7:	81 c7 00 10 00 00    	add    $0x1000,%edi
80106fdd:	39 7d 0c             	cmp    %edi,0xc(%ebp)
80106fe0:	76 46                	jbe    80107028 <copyuvm+0xb8>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
80106fe2:	8b 45 08             	mov    0x8(%ebp),%eax
80106fe5:	31 c9                	xor    %ecx,%ecx
80106fe7:	89 fa                	mov    %edi,%edx
80106fe9:	e8 22 f8 ff ff       	call   80106810 <walkpgdir>
80106fee:	85 c0                	test   %eax,%eax
80106ff0:	74 61                	je     80107053 <copyuvm+0xe3>
    if(!(*pte & PTE_P))
80106ff2:	8b 00                	mov    (%eax),%eax
80106ff4:	a8 01                	test   $0x1,%al
80106ff6:	74 4e                	je     80107046 <copyuvm+0xd6>
    pa = PTE_ADDR(*pte);
80106ff8:	89 c3                	mov    %eax,%ebx
    flags = PTE_FLAGS(*pte);
80106ffa:	25 ff 0f 00 00       	and    $0xfff,%eax
    pa = PTE_ADDR(*pte);
80106fff:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    flags = PTE_FLAGS(*pte);
80107005:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if((mem = kalloc()) == 0)
80107008:	e8 43 b7 ff ff       	call   80102750 <kalloc>
8010700d:	85 c0                	test   %eax,%eax
8010700f:	89 c6                	mov    %eax,%esi
80107011:	75 8d                	jne    80106fa0 <copyuvm+0x30>
    }
  }
  return d;

bad:
  freevm(d);
80107013:	83 ec 0c             	sub    $0xc,%esp
80107016:	ff 75 e0             	pushl  -0x20(%ebp)
80107019:	e8 02 fe ff ff       	call   80106e20 <freevm>
  return 0;
8010701e:	83 c4 10             	add    $0x10,%esp
80107021:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
}
80107028:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010702b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010702e:	5b                   	pop    %ebx
8010702f:	5e                   	pop    %esi
80107030:	5f                   	pop    %edi
80107031:	5d                   	pop    %ebp
80107032:	c3                   	ret    
80107033:	90                   	nop
80107034:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      kfree(mem);
80107038:	83 ec 0c             	sub    $0xc,%esp
8010703b:	56                   	push   %esi
8010703c:	e8 5f b5 ff ff       	call   801025a0 <kfree>
      goto bad;
80107041:	83 c4 10             	add    $0x10,%esp
80107044:	eb cd                	jmp    80107013 <copyuvm+0xa3>
      panic("copyuvm: page not present");
80107046:	83 ec 0c             	sub    $0xc,%esp
80107049:	68 ee 7a 10 80       	push   $0x80107aee
8010704e:	e8 3d 93 ff ff       	call   80100390 <panic>
      panic("copyuvm: pte should exist");
80107053:	83 ec 0c             	sub    $0xc,%esp
80107056:	68 d4 7a 10 80       	push   $0x80107ad4
8010705b:	e8 30 93 ff ff       	call   80100390 <panic>

80107060 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80107060:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107061:	31 c9                	xor    %ecx,%ecx
{
80107063:	89 e5                	mov    %esp,%ebp
80107065:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
80107068:	8b 55 0c             	mov    0xc(%ebp),%edx
8010706b:	8b 45 08             	mov    0x8(%ebp),%eax
8010706e:	e8 9d f7 ff ff       	call   80106810 <walkpgdir>
  if((*pte & PTE_P) == 0)
80107073:	8b 00                	mov    (%eax),%eax
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}
80107075:	c9                   	leave  
  if((*pte & PTE_U) == 0)
80107076:	89 c2                	mov    %eax,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107078:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  if((*pte & PTE_U) == 0)
8010707d:	83 e2 05             	and    $0x5,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107080:	05 00 00 00 80       	add    $0x80000000,%eax
80107085:	83 fa 05             	cmp    $0x5,%edx
80107088:	ba 00 00 00 00       	mov    $0x0,%edx
8010708d:	0f 45 c2             	cmovne %edx,%eax
}
80107090:	c3                   	ret    
80107091:	eb 0d                	jmp    801070a0 <copyout>
80107093:	90                   	nop
80107094:	90                   	nop
80107095:	90                   	nop
80107096:	90                   	nop
80107097:	90                   	nop
80107098:	90                   	nop
80107099:	90                   	nop
8010709a:	90                   	nop
8010709b:	90                   	nop
8010709c:	90                   	nop
8010709d:	90                   	nop
8010709e:	90                   	nop
8010709f:	90                   	nop

801070a0 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
801070a0:	55                   	push   %ebp
801070a1:	89 e5                	mov    %esp,%ebp
801070a3:	57                   	push   %edi
801070a4:	56                   	push   %esi
801070a5:	53                   	push   %ebx
801070a6:	83 ec 1c             	sub    $0x1c,%esp
801070a9:	8b 5d 14             	mov    0x14(%ebp),%ebx
801070ac:	8b 55 0c             	mov    0xc(%ebp),%edx
801070af:	8b 7d 10             	mov    0x10(%ebp),%edi
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
801070b2:	85 db                	test   %ebx,%ebx
801070b4:	75 40                	jne    801070f6 <copyout+0x56>
801070b6:	eb 70                	jmp    80107128 <copyout+0x88>
801070b8:	90                   	nop
801070b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
801070c0:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801070c3:	89 f1                	mov    %esi,%ecx
801070c5:	29 d1                	sub    %edx,%ecx
801070c7:	81 c1 00 10 00 00    	add    $0x1000,%ecx
801070cd:	39 d9                	cmp    %ebx,%ecx
801070cf:	0f 47 cb             	cmova  %ebx,%ecx
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
801070d2:	29 f2                	sub    %esi,%edx
801070d4:	83 ec 04             	sub    $0x4,%esp
801070d7:	01 d0                	add    %edx,%eax
801070d9:	51                   	push   %ecx
801070da:	57                   	push   %edi
801070db:	50                   	push   %eax
801070dc:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
801070df:	e8 ac d6 ff ff       	call   80104790 <memmove>
    len -= n;
    buf += n;
801070e4:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
  while(len > 0){
801070e7:	83 c4 10             	add    $0x10,%esp
    va = va0 + PGSIZE;
801070ea:	8d 96 00 10 00 00    	lea    0x1000(%esi),%edx
    buf += n;
801070f0:	01 cf                	add    %ecx,%edi
  while(len > 0){
801070f2:	29 cb                	sub    %ecx,%ebx
801070f4:	74 32                	je     80107128 <copyout+0x88>
    va0 = (uint)PGROUNDDOWN(va);
801070f6:	89 d6                	mov    %edx,%esi
    pa0 = uva2ka(pgdir, (char*)va0);
801070f8:	83 ec 08             	sub    $0x8,%esp
    va0 = (uint)PGROUNDDOWN(va);
801070fb:	89 55 e4             	mov    %edx,-0x1c(%ebp)
801070fe:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
    pa0 = uva2ka(pgdir, (char*)va0);
80107104:	56                   	push   %esi
80107105:	ff 75 08             	pushl  0x8(%ebp)
80107108:	e8 53 ff ff ff       	call   80107060 <uva2ka>
    if(pa0 == 0)
8010710d:	83 c4 10             	add    $0x10,%esp
80107110:	85 c0                	test   %eax,%eax
80107112:	75 ac                	jne    801070c0 <copyout+0x20>
  }
  return 0;
}
80107114:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80107117:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010711c:	5b                   	pop    %ebx
8010711d:	5e                   	pop    %esi
8010711e:	5f                   	pop    %edi
8010711f:	5d                   	pop    %ebp
80107120:	c3                   	ret    
80107121:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107128:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010712b:	31 c0                	xor    %eax,%eax
}
8010712d:	5b                   	pop    %ebx
8010712e:	5e                   	pop    %esi
8010712f:	5f                   	pop    %edi
80107130:	5d                   	pop    %ebp
80107131:	c3                   	ret    
