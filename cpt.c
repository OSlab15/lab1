#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define STANDARD_INPUT 0
#define STANDARD_OUTPUT 1

char buf[512];

int cpt(int fdInput, int fdOutput){
  int n;
  char* out;
  if(fdInput == STANDARD_INPUT){
    out = gets(buf, 512);
    if (write(fdOutput, buf, sizeof(out)) != sizeof(out)) {
        printf(STANDARD_OUTPUT, "cpt: write error1\n");
        exit();
      }
  }
  else{
    while((n = read(fdInput, buf, sizeof(buf))) > 0){
      if (write(fdOutput, buf, n) != n) {
        printf(STANDARD_OUTPUT, "cpt: write error2\n");
        exit();
      }
    }
  }
  return 1;
}

int main(int argc, char *argv[])
{
  int fdInput, fdOutput;

  if(argc == 2){
    fdOutput = open(argv[1], O_CREATE | O_RDWR);
    cpt(STANDARD_INPUT, fdOutput);
    close(fdOutput);
  }
  else if(argc == 3){
    if((fdInput = open(argv[1], 0)) < 0){
      printf(STANDARD_OUTPUT, "File %s does not exist", argv[1]);
      exit();
    }
    fdOutput = open(argv[2], O_CREATE | O_RDWR);
    cpt(fdInput, fdOutput);
    close(fdInput);
    close(fdOutput);
  }
  else{
    printf(STANDARD_OUTPUT, "Invalid arguments");
  }
  exit();
}